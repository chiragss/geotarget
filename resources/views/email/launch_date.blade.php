<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<title>Geotarget | Location intelligence Marketing | Geotarget Advertising</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="x-apple-disable-message-reformatting">
	<meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
	<style>
		html, body {
			margin: 0 !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			font-family: 'Lato', sans-serif;
		}
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}
		div[style*="margin: 16px 0"] {
			margin: 0 !important;
		}
		table, td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
		}
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;
		}
		a {
			text-decoration: none;
		}
		img {
			-ms-interpolation-mode: bicubic;
		}
		a[x-apple-data-detectors],  /* iOS */ .unstyle-auto-detected-links a, .aBn {
			border-bottom: 0 !important;
			cursor: default !important;
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}
		.im {
			color: inherit !important;
		}
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		img.g-img + div {
			display: none !important;
		}
		@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
			u ~ div .email-container {
				min-width: 320px !important;
			}
		}
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
			u ~ div .email-container {
				min-width: 375px !important;
			}
		}
		@media only screen and (min-device-width: 414px) {
			u ~ div .email-container {
				min-width: 414px !important;
			}
		}
	</style>
	<style>
		.button-td,  .button-a {
			transition: all 100ms ease-in;
		}
		.button-td-primary:hover,  .button-a-primary:hover {
			background: #555555 !important;
			border-color: #555555 !important;
		}
		@media screen and (max-width: 600px) {
			.email-container {
				width: 100% !important;
				margin: auto !important;
			}
			.stack-column,  .stack-column-center {
				display: block !important;
				width: 100% !important;
				max-width: 100% !important;
				direction: ltr !important;
			}
			.stack-column-center {
				text-align: center !important;
			}
			.center-on-narrow {
				text-align: center !important;
				display: block !important;
				margin-left: auto !important;
				margin-right: auto !important;
				float: none !important;
			}
			table.center-on-narrow {
				display: inline-block !important;
			}
			.email-container p {
				font-size: 13px !important;
			}

			.email-container td{ font-size:13px !important;}
		}
	</style>
</head>
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;">
	<center style="width: 100%; background-color: #fff;">
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Lato', sans-serif;"> &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp; </div>
		<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" style="margin: auto; 
		-moz-box-shadow: 1px 0px 13px #959393;
		-webkit-box-shadow: 1px 0px 13px #959393;
		box-shadow: 1px 0px 13px #959393;border:1px solid #e5e5e5;" class="email-container">
		<tr>
			<td style="padding: 10px 40px 10px 40px; background:#333333">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td><a href="https://geotargetus.net"><img src="https://admin.geotargetus.net/resources/assets/img/logo22.png" alt=""></a></td>
						<td style="text-align:right"><a href="https://admin.geotargetus.net/userlogin"><img src="https://admin.geotargetus.net/resources/assets/img/white-btn.png" alt="Login"></a></td>
					</tr>
				</table>

			</td>
		</tr>
		<tr>
			<td style="background-color: #ffffff;"><table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td style="padding: 20px 40px; font-family: sans-serif; font-size: 15px; line-height: 22px; color: #2d2d2d;">
						<?php $uname=explode(',',$username);
          // var_dump($uname);
						if(!empty($uname[1])) {
							$name=$uname[1];
						}else{
							$name=$uname[0];
						}
						?>
						<p style="margin: 0 0 10px;">Dear <?php echo ucfirst(strtolower($name));?>,</p>
						<p>Your Geotarget campaign has been launched. Please find the below link to your landing page as well as links to the preview of your ads.</p>
						<p>If you should need any changes or corrections, please contact us within 10 days of this email.</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 40px; background-color: #ffffff; font-family: sans-serif; color:#96588a; font-size:15px; line-height:22px;"><table role="presentation" cellspacing="0" cellpadding="6" border="0" width="100%" style="text-align:center">
			<tr>
				<span><b>Facebook Ad:</b></span>
				<br>
				@if(!empty($fb))
				<?php 
				foreach($fb as $fb_data){
					$time=bin2hex(random_bytes(10));
					echo '<a href="'.$fb_data[0].'">'.'https://fb.me/'.$time.'</a>';
					echo '<br>';
				}
				?>
				@endif
				<br>
				<span><b>Google Ad:</b></span>

				@if(!empty($url))
				<?php 
				$time=bin2hex(random_bytes(5));
				echo '<a href="'.$url.'">'.'https://shorturl.at/'.$time.'</a>';

				?>

				@endif
				<br>
				<span><b>Landing Page:</b></span>
				@if(!empty($landing_page))
				<?php 
				echo '<a href="'.strtolower($landing_page).'">'.strtolower($landing_page).'</a>';
				?>
				@endif
				<br>
				<p>Also, you will be able to monitor your campaign with a dashboard login! The dashboard will provide all campaign specifics including: ✔️ Lifetime numbers ✔️Interaction data ✔️ Placement Reports ✔️ Access to all static ads, video, and landing page.

					The data provided in the dashboard is refreshed every 24 hours so that you may monitor the progress of your campaign.
				</p>
				<p>To login to your dashboard, simply go to this link:  <a href="https://admin.geotargetus.net/userlogin">https://admin.geotargetus.net/userlogin</a>
				</p>
				<h2>Generate Your Password</h2>
				<p>Need to Generate your password?No problem!Just click the button below and you'll be on yur way. if you did not make this request , Please Ignore This mail</p>
				<br>
				<a href="https://beta.geotargetus.net/geotarget/passreset?token={{ $token }}"><img src="https://beta.geotargetus.net/geotarget/resources/assets/img/reset.png" alt="Contact-Us"></a>
			</tr>
		</table></td>
	</tr>


	<tr>
		<td style="padding: 10px 40px; background-color: #ffffff; font-family: sans-serif; color:#555555; font-size:15px; line-height:22px;"><table role="presentation" cellspacing="0" cellpadding="6" border="0" width="100%">
			<tr>
				<td style="font-size:15px; line-height:22px; color: #2d2d2d;">
					<p style="margin-bottom:5px;"><b>Have any questions about the Ad Details?</b></p>
					<p style="margin-top:0;">Our Support team can answer any questions about the renewal
						account. We are on call 24 hours 7 days a week and
						respond to all questions in under an hour. If you need
						assistance, contact us anytime by visiting our Help Center.
					</p>
				</td>
			</tr>
			<tr>
				<td style="padding-top:20px;">
					<a href="https://geotargetus.net/contact-us/"><img src="https://admin.geotargetus.net/resources/assets/img/yellow-btn.png" alt="Contact-Us"></a>
				</td>
			</tr>
			<tr>
				<td style="font-size:15px; line-height:22px; color: #2d2d2d; padding:20px 0;">
					<p>Let us know if there's anything we can do.</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td aria-hidden="true" height="30" style="font-size: 15px; line-height: 20px; background: #2d2c2c; text-align:center"><p style="color:#fff;"><span style="color: #fcd21c;">Geotarget:</span> Featuring you in front of your highest value prospects!</p></td>
</tr>
</table>
</center>
</body>
</html>

