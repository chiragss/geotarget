<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ $titlePage }}</title>
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('resources/assets') }}/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('resources/assets') }}/img/favicon.png">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/solid.css" integrity="sha384-HTDlLIcgXajNzMJv5hiW5s2fwegQng6Hi+fN6t5VAcwO/9qbg2YEANIyKBlqLsiT" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/regular.css" integrity="sha384-R7FIq3bpFaYzR4ogOiz75MKHyuVK0iHja8gmH1DHlZSq4tT/78gKAa7nl4PJD7GP" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/brands.css" integrity="sha384-KtmfosZaF4BaDBojD9RXBSrq5pNEO79xGiggBxf8tsX+w2dBRpVW5o0BPto2Rb2F" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/fontawesome.css" integrity="sha384-8WwquHbb2jqa7gKWSoAwbJBV2Q+/rQRss9UXL5wlvXOZfSodONmVnifo/+5xJIWX" crossorigin="anonymous">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
  <!-- CSS Files -->
  <link href="{{ asset('resources/assets') }}/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link href="{{ asset('resources/assets') }}/demo/demo.css" rel="stylesheet" /> -->
  <!-- <link href="{{ asset('resources/assets') }}/md/mdb.min.css" rel="stylesheet" /> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
</head>
<body class="{{ 'class' }}">
 
  @if(Auth::guard('user')->check() || Auth::guard('web')->check() || Auth::guard('rep_user')->check())
  <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
 </form>
  @include('layouts.page_templates.auth')
 @elseif(Auth::guard('user')->check())
 @include('layouts.page_templates.guest')
 @else
 @include('layouts.page_templates.guest')
 @endif

 <!--   Core JS Files   -->
 <!-- <script src="{{ asset('resources/assets') }}/js/core/jquery.min.js"></script> -->
 <script src="{{ asset('resources/assets') }}/js/core/popper.min.js"></script>
 <script src="{{ asset('resources/assets') }}/js/core/bootstrap-material-design.min.js"></script>
 <script src="{{ asset('resources/assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
 <!-- Plugin for the momentJs  -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/moment.min.js"></script> -->
 <!--  Plugin for Sweet Alert -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/sweetalert2.js"></script> -->
 <!-- Forms Validations Plugin -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/jquery.validate.min.js"></script> -->
 <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/jquery.bootstrap-wizard.js"></script> -->
 <!-- Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/bootstrap-selectpicker.js"></script> -->
 <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/bootstrap-datetimepicker.min.js"></script> -->
 <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/jquery.dataTables.min.js"></script> -->
 <!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/bootstrap-tagsinput.js"></script> -->
 <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/jasny-bootstrap.min.js"></script> -->
 <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/fullcalendar.min.js"></script> -->
 <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/jquery-jvectormap.js"></script> -->
 <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/nouislider.min.js"></script> -->
 <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script> -->
 <!-- Library for adding dinamically elements -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/arrive.min.js"></script> -->
 <!--  Google Maps Plugin    -->
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE'"></script> -->
 <!-- Chartist JS -->
 <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgZ2XTdjwWGgaSt6X32FXZbKellYLt4BI" type="text/javascript"></script> -->
 <!-- <script src="{{ asset('resources/assets') }}/js/plugins/chartist.min.js"></script> -->
 <!--  Notifications Plugin    -->
 <script src="{{ asset('resources/assets') }}/js/plugins/bootstrap-notify.js"></script>
 <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
 <script src="{{ asset('resources/assets') }}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
 <!-- Material Dashboard DEMO methods, don't include it in your project! -->
 <!-- <script src="{{ asset('resources/assets') }}/demo/demo.js"></script> -->
 <!-- <script src="{{ asset('resources/assets') }}/js/settings.js"></script> -->
 <script src="{{ asset('resources/assets') }}/md/js/mdb.js"></script>
 <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->

 @stack('js')
</body>
</html>