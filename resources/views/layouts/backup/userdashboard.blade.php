@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => ('View Campaigns Reports'), 'breadcumbs' => ('Dashboard > View report')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/dataTables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/dataTables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<script src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chartjs-datalabels.js"></script>
<!-- <script src="{{ asset('resources/assets') }}/js/core/popper.min.js"></script> -->
<!-- <script src="{{ asset('resources/assets') }}/js/core/bootstrap-material-design.min.js"></script> -->
<!-- <script src="{{ asset('resources/assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chart.js"></script> -->
<script type="text/javascript" src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chartjs-datalabels.js"></script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header card-header-tabs card-header-primary">
            <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#facebook" data-toggle="tab">
                      <i class="material-icons">laptop</i> Social Media
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#google" data-toggle="tab">
                      <i class="material-icons">code</i> Google/Youtube
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#groundtruth" data-toggle="tab">
                      <i class="material-icons">cloud</i> Games & Applications
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#target_location" data-toggle="tab">
                      <i class="material-icons">map</i>Targeted Location
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                 <!--  <li class="nav-item">
                    <a class="nav-link" href="#view_stats" data-toggle="tab">
                      <i class="material-icons">list</i> View Stats
                      <div class="ripple-container"></div>
                    </a>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
          <?php 
          if($camp_type=='0'){
            $camptype='Monthly';
          }else{
            $camptype='Lifetime';
          }
          ?>
          <div class="card-body">
            <div class="tab-content">
              <div class="tab-pane active" id="facebook">
                <?php 
                $total_spend=0;
                $total_clicks=0;$dec_clicks=0;$jan_clicks=0;$feb_clicks=0;$mar_clicks=0;$apr_clicks=0;$may_clicks=0;$jun_clicks=0;$jul_clicks=0;$aug_clicks=0;$sep_clicks=0;$oct_clicks=0;$nov_clicks=0;
                $total_impression=0;$mon_impression=0;$tue_impression=0;$wed_impression=0;$thr_impression=0;$fri_impression=0;$sat_impression=0;$sun_impression=0;
                $female_age1=0;$female_age2=0;$female_age3=0;$female_age4=0;$female_age5=0;$female_age6=0;$female_age7=0;  $male_age1=0;$male_age2=0;$male_age3=0;$male_age4=0;$male_age5=0;$male_age6=0;$male_age7=0;
                $total_ctr=0;$us_impression=0;$uk_impression=0;
                $fb_impression=0;$insta_impression=0;
                $total_reach=0;$jan_spent=0;$feb_spent=0;$mar_spent=0;$apr_spent=0;$may_spent=0;$jun_spent=0;$jul_spent=0;$aug_spent=0;$sep_spent=0;$oct_spent=0;$nov_spent=0;$dec_spent=0;
                $male_impression=0;$female_impression=0; $fb_feed_impression=0;$fb_instant_article_impression=0;$fb_marketplace_impression=0;$fb_video_feeds_impression=0;$insta_stories_impression=0;$insta_feed_impression=0;$desktop_impression=0;$mobile_app_impression=0;$mobile_web_impression=0;$videoimpression=0;$displayimpression=0;
                ?>
                @if(!empty($fbresult))
                @foreach($fbresult as $campaign)
                <?php 
                if(!empty($campaign->keyword)){
                 $keyword=json_decode($campaign->keyword);
                 $date[]=$campaign->camp_date;
                 $spend=$keyword->spend;
                 $age_gender_camp=json_decode($campaign->age_gender_camp);
                 $platform_impression=json_decode($campaign->pub_platform_campaign);
                 $loc_impresison=json_decode($campaign->loc_impression);
                 $device_campaign=json_decode($campaign->device_campaign);
                 $video_impression=json_decode($campaign->video_campaign);
                 $display_impression=json_decode($campaign->display_campaign);
                 if(!empty($loc_impresison)){
                  foreach($loc_impresison as $data){
                    if($data->country=='US'){
                     $us_impression=$us_impression + $data->impressions;
                   }
                   if($data->country=='UK'){
                     $uk_impression=$uk_impression + $data->impressions;
                   }
                 }
               }
               if(!empty($platform_impression)){
                 foreach ($platform_impression as $data) {
         // echo $data->publisher_platform;
      // var_dump($data);die;
                   if($data->publisher_platform=='facebook'){
                    if($data->platform_position=='feed'){
                      $fb_feed_impression=$fb_feed_impression + $data->impressions;
                    }elseif($data->platform_position=='instant_article'){
                      $fb_instant_article_impression= $fb_instant_article_impression + $data->impressions;
                    }elseif($data->platform_position=='marketplace'){
                      $fb_marketplace_impression=$fb_marketplace_impression + $data->impressions;
                    }elseif($data->platform_position=='video_feeds'){
                      $fb_video_feeds_impression=$fb_video_feeds_impression + $data->impressions;
                    }else{
                    }
                  }
                  if($data->publisher_platform=='instagram'){
                    if($data->platform_position=='instagram_stories'){
                      $insta_stories_impression=$insta_stories_impression + $data->impressions;
                    }elseif($data->platform_position=='feed'){
                     $insta_feed_impression=$insta_feed_impression + $data->impressions;
                   }
                 }
               }
             }
             if(!empty($age_gender_camp)){
              foreach ($age_gender_camp as $age) {
                if($age->gender=='female'){
                  $female_impression=$female_impression + $age->impressions;
                  if($age->age=='13-17'){
                    $female_age1=$female_age1 + $age->impressions;
                  }elseif ($age->age=='18-24') {
                   $female_age2=$female_age2 + $age->impressions;
                 }elseif ($age->age=='25-34') {
                   $female_age3=$female_age3 + $age->impressions;
                 }elseif ($age->age=='35-44') {
                   $female_age4=$female_age4 + $age->impressions;
                 }elseif ($age->age=='45-54') {
                   $female_age5=$female_age5 + $age->impressions;
                 }elseif ($age->age=='55-64') {
                   $female_age6=$female_age6 + $age->impressions;
                 }else{
                   $female_age7=$female_age7 + $age->impressions;
                 }
               }elseif ($age->gender=='male') {
                $male_impression=$male_impression + $age->impressions;
                if($age->age=='13-17'){
                 $male_age1=$male_age1 + $age->impressions;
               }elseif ($age->age=='18-24') {
                 $male_age2=$male_age2 + $age->impressions;
               }elseif ($age->age=='25-34') {
                $male_age3=$male_age3 + $age->impressions;
              }elseif ($age->age=='35-44') {
                $male_age4=$male_age4 + $age->impressions;
              }elseif ($age->age=='45-54') {
                $male_age5=$male_age5 + $age->impressions;
              }elseif ($age->age=='55-64') {
                $male_age6=$male_age6 + $age->impressions;
              }else{
                $male_age7=$male_age7 + $age->impressions;
              }
            }else{
            }
          }
        }
        if(!empty($device_campaign)){
          foreach($device_campaign as $device){
            if($device->device_platform=='desktop'){
              $desktop_impression=$desktop_impression + $device->impressions;
            }else if($device->device_platform=='mobile_app'){
              $mobile_app_impression= $mobile_app_impression + $device->impressions;
            }else{
              $mobile_web_impression= $mobile_web_impression + $device->impressions;
            }
          }
        }
        if(!empty($video_impression)){
          foreach($video_impression as $v_impression){
           $videoimpression=$videoimpression + $v_impression->impresison;
         }
       }
       if(!empty($display_impression)){
        foreach($display_impression as $d_impression){
          $displayimpression=$displayimpression + $d_impression->impresison;
        }
      }
      $clicks=$keyword->clicks;
      $total_clicks=$total_clicks + $clicks;
      $impressions=$keyword->impressions;
      $impr[]=$keyword->impressions;
      $ctr=$keyword->ctr;
      $reach=$keyword->reach;
      $total_spend=$total_spend + $spend;
      $m=date('M',strtotime($campaign->camp_date));
      $d=date('D',strtotime($campaign->camp_date));
      if($d=='Mon'){
       $mon_impression=$mon_impression + $impressions;
     }elseif ($d=='Tue') {
       $tue_impression=$tue_impression + $impressions;
     }elseif ($d=='Wed') {
      $wed_impression=$wed_impression + $impressions;
    }elseif ($d=='Thr') {
      $thr_impression=$tru_impression + $impressions;
    }elseif ($d=='Fri') {
      $fri_impression=$fri_impression + $impressions;
    }elseif ($d=='Sat') {
     $sat_impression=$sat_impression + $impressions;
   }else{
    $sun_impression=$sun_impression + $impressions;
  }
  if($m=='Dec'){
    $dec_clicks=$dec_clicks + $clicks;
    $dec_spent=$dec_spent + $spend;
  }elseif ($m=='Jan') {
    $jan_clicks=$jan_clicks + $clicks;
    $jan_spent=$jan_spent + $spend;
  }elseif ($m=='Feb') {
    $feb_clicks=$feb_clicks + $clicks;
    $feb_spent=$feb_spent + $spend;
  }elseif($m=='Mar'){
   $mar_clicks=$mar_clicks + $clicks;
   $mar_spent=$mar_spent + $spend;
 }elseif ($m=='Apr') {
   $apr_clicks=$apr_clicks + $clicks;
   $apr_spent=$apr_spent + $spend;
 }elseif ($m=='May') {
   $may_clicks=$may_clicks + $clicks;
   $may_spent=$may_spent + $spend;
 }elseif ($m=='Jun') {
  $jun_clicks=$jun_clicks + $clicks;
  $jun_spent=$jun_spent + $spend;
}elseif ($m=='Jul') {
 $jul_clicks=$jul_clicks + $clicks;
 $jul_spent=$jul_spent + $spend;
}elseif ($m=='Aug') {
 $aug_clicks=$aug_clicks + $clicks;
 $aug_spent=$aug_spent + $spend;
}elseif ($m=='Sep') {
 $sep_clicks=$sep_clicks + $clicks;
 $sep_spent=$sep_spent + $spend;
}elseif ($m=='Oct') {
  $oct_clicks=$oct_clicks + $clicks;
  $oct_spent=$oct_spent + $spend;
}else{
 $nov_clicks=$nov_clicks + $clicks;
 $nov_spent=$nov_spent + $spend;
}
$total_impression=$total_impression + $impressions;
$total_ctr=$total_ctr + $ctr;
$total_reach=$total_reach + $reach;
$updated_at=$campaign->updated_at;
}
?>
@endforeach
<?php 
$date2=date('Y-m-d');
$diff = strtotime($date2) - strtotime($updated_at); 
$day_diff1=abs(round($diff / 86400));
if($day_diff1>0){
  $day_diff=$day_diff1.' days';
}else{
  $day_diff='Sometimes';
}
?>
<input type="hidden" id="us_impression" value="{{ $us_impression }}">
<input type="hidden" id="uk_impression" value="{{ $uk_impression }}">
<input type='hidden' id="desktop_impression" value="{{ $desktop_impression }}">
<input type='hidden' id="mobile_app_impression" value="{{ $mobile_app_impression }}">
<input type='hidden' id="mobile_web_impression" value="{{ $mobile_web_impression }}">
<input type="hidden" id="fb_feed_impression" value="{{ $fb_feed_impression }}">
<input type="hidden" id="fb_instant_article_impression" value="{{ $fb_instant_article_impression }}">
<input type="hidden" id="fb_marketplace_impression" value="{{ $fb_marketplace_impression }}">
<input type="hidden" id="fb_video_feeds_impression" value="{{ $fb_video_feeds_impression }}">
<input type="hidden" id="insta_stories_impression" value="{{ $insta_stories_impression }}">
<input type="hidden" id="insta_feed_impression" value="{{ $insta_feed_impression }}">
<input type="hidden" id="male_age1" value="{{ $male_age1 }}">
<input type="hidden" id="male_age2" value="{{ $male_age2 }}">
<input type="hidden" id="male_age3" value="{{ $male_age3 }}">
<input type="hidden" id="male_age4" value="{{ $male_age4 }}">
<input type="hidden" id="male_age5" value="{{ $male_age5 }}">
<input type="hidden" id="male_age6" value="{{ $male_age6 }}">
<input type="hidden" id="male_age7" value="{{ $male_age7 }}">
<input type="hidden" id="female_age1" value="{{ $female_age1 }}">
<input type="hidden" id="female_age2" value="{{ $female_age2 }}">
<input type="hidden" id="female_age3" value="{{ $female_age3 }}">
<input type="hidden" id="female_age4" value="{{ $female_age4 }}">
<input type="hidden" id="female_age5" value="{{ $female_age5 }}">
<input type="hidden" id="female_age6" value="{{ $female_age6 }}">
<input type="hidden" id="female_age7" value="{{ $female_age7 }}">
<input type="hidden" id="male_impression" value="{{ $male_impression }}">
<input type="hidden" id="female_impression" value="{{ $female_impression }}">
<input type="hidden" id="dec_click" value="{{ $dec_clicks }}">
<input type="hidden" id="nov_click" value="{{ $nov_clicks }}">
<input type="hidden" id="oct_click" value="{{ $oct_clicks }}">
<input type="hidden" id="sep_click" value="{{ $sep_clicks }}">
<input type="hidden" id="aug_click" value="{{ $aug_clicks }}">
<input type="hidden" id="jul_click" value="{{ $jul_clicks }}">
<input type="hidden" id="jun_click" value="{{ $jun_clicks }}">
<input type="hidden" id="may_click" value="{{ $may_clicks }}">
<input type="hidden" id="apr_click" value="{{ $apr_clicks }}">
<input type="hidden" id="mar_click" value="{{ $mar_clicks }}">
<input type="hidden" id="feb_click" value="{{ $feb_clicks }}">
<input type="hidden" id="jan_click" value="{{ $jan_clicks }}">
<input type="hidden" id="dec_spent" value="{{ $dec_spent }}">
<input type="hidden" id="nov_spent" value="{{ $nov_spent }}">
<input type="hidden" id="oct_spent" value="{{ $oct_spent }}">
<input type="hidden" id="sep_spent" value="{{ $sep_spent }}">
<input type="hidden" id="aug_spent" value="{{ $aug_spent }}">
<input type="hidden" id="jul_spent" value="{{ $jul_spent }}">
<input type="hidden" id="jun_spent" value="{{ $jun_spent }}">
<input type="hidden" id="may_spent" value="{{ $may_spent }}">
<input type="hidden" id="apr_spent" value="{{ $apr_spent }}">
<input type="hidden" id="mar_spent" value="{{ $mar_spent }}">
<input type="hidden" id="feb_spent" value="{{ $feb_spent }}">
<input type="hidden" id="jan_spent" value="{{ $jan_spent }}">
<input type="hidden" id="m" value="{{ $mon_impression }}">
<input type="hidden" id="t" value="{{ $tue_impression }}">
<input type="hidden" id="w" value="{{ $wed_impression }}">
<input type="hidden" id="th" value="{{ $thr_impression }}">
<input type="hidden" id="f" value="{{ $fri_impression }}">
<input type="hidden" id="sat" value="{{ $sat_impression }}">
<input type="hidden" id="sun" value="{{ $sun_impression }}">
<h4>{{ $camptype }} Social Media Records | Overview</h4>
<div class="row">
  <!-- <div class="col-lg-4 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">content_copy</i>
        </div>
        <p class="card-category">Total Clicks</p>
        <h3 class="card-title">{{ $total_clicks }}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div> -->

 <!--  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="material-icons">content_copy</i>
        </div>
        <p class="card-category">Total Spent</p>
        <h3 class="card-title">{{ $total_spend }}
          <small>$</small>
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div> -->
  <div class="col-lg-4 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="material-icons">store</i>
        </div>
        <p class="card-category">Display Impressions</p>
        <h3 class="card-title">{{ $displayimpression }}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-info card-header-icon">
        <div class="card-icon">
          <i class="material-icons">store</i>
        </div>
        <p class="card-category">Video Impressions</p>
        <h3 class="card-title">{{ $videoimpression }}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">store</i>
        </div>
        <p class="card-category">Total Reach</p>
        <h3 class="card-title">{{ $total_reach }}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
 <!--  <div class="col-md-12">
    <div class="card card-chart">
      <div class="card-header card-header-success">
        <div class="ct-chart" id="dailySalesChart"></div>
      </div>
      <div class="card-body">
        <h4 class="card-title">Month Spent($)</h4>
        <p class="card-category">
          <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
          </div>
        </div>
      </div>
    </div> -->
   <!--  <div class="col-md-6">
      <div class="card card-chart">
        <div class="card-header card-header-warning">
          <div class="ct-chart" id="websiteViewsChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Month Clicks</h4>
          <p class="card-category">Last Campaign Performance</p>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
          </div>
        </div>
      </div>
    </div> -->
  </div>
  <div class="row">
   <div class="col-md-6">
    <div class="card card-chart">
      <div class="card-header card-header-success" style="background: white;">
       <canvas id="pieChart" style="max-width: 500px;"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Gender Impression</h4>
     <!--  <p class="card-category">
      <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
    </div>
    <div class="card-footer">
      <div class="stats">
        <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
      </div>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
      <canvas id="lineChart"></canvas>
    </div>
    <div class="card-body">
      <h4 class="card-title">Age Group Impression</h4>
      <!--   <p class="card-category">
        <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
      <canvas id="pieChart1" style="max-width: 500px;"></canvas>
    </div>
    <div class="card-body">
      <h4 class="card-title">Platform Impressions</h4>
       <!--  <p class="card-category">
        <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">access_time</i> updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
      <canvas id="devicePieChart" style="max-width: 500px;"></canvas>
    </div>
    <div class="card-body">
      <h4 class="card-title">Device Impressions</h4>
       <!--  <p class="card-category">
        <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">access_time</i> updated {{ $day_diff }} ago
        </div>
      </div>
    </div>
  </div>
</div>
@else
<h4>No Campaigns For Facebook</h4>
@endif
</div>
<div class="tab-pane" id="google">
  <?php 
  $clicks=0;$spends=0;$impressions=0;$ctr=0;$budget=0; $invalid_clicks=0;
  $dnspend=0;$dnclicks=0;$dnimpression=0; $dnbudget=0;$dninvalid=0;
  $spspend=0;$spclicks=0;$spimpression=0;$spbudget=0;$spinvalid=0;
  $gsspend=0;$gsclicks=0;$gsimpression=0;$gsbudget=0;$gsinvalid=0;
  $ytvspend=0;$ytvclicks=0;$ytvimpression=0;$ytvbudget=0;$ytvinvalid=0;
  $ytsspend=0;$ytsclicks=0;$ytsimpression=0;$ytsbudget=0;$ytsinvalid=0;
  $cnspend=0;$cnclicks=0;$cnimpression=0;$cnbudget=0;$cninvalid=0;
  $mobcost=0;$mobclick=0;$mobimpression=0;
  $deskcost=0; $deskclick=0;$deskimpression=0;
  $tabcost=0;$tabclick=0;$tabimpression=0;$malecost=0;$maleclick=0;$maleimpression=0;$femalecost=0;$femaleclick=0;$femaleimpression=0;
  $unknowncost=0;$unknownclick=0;$unknownimpression=0; $uscost=0; $usclick=0;$usimpression=0;$ukcost=0;$ukclick=0;$ukimpression=0;
  $russiacost=0; $russiaclick=0;$russiaimpression=0; $chinacost=0;$chinaclick=0;$chinaimpression=0;$uaecost=0;$uaeclick=0;$uaeimpression=0;$othercost=0;$otherclick=0;$otherimpression=0;
  // variable for video campaign
  $video_mobile_views=0;$video_tab_views=0;$video_desktop_views=0;$video_other_views=0;
  $sunday_video_impression=0;$sunday_video_view=0;$monday_video_impression=0;$monday_video_view=0;$tuesday_video_impression=0;$tuesday_video_view=0;$wednesday_video_impression=0;$wednesday_video_view=0;$thrusday_video_impression=0;$thrusday_video_view=0;$friday_video_impression=0;$friday_video_view=0;$saturday_video_impresison=0;$saturday_video_view=0;
  $video_25=0;$video_50=0;$video_75=0;$video_100=0;$video_views=0;
  // varibales for display
  $display_sun_impression=0;$display_mon_impression=0;$display_tue_impression=0;$display_wed_impression=0;$display_thru_impression=0;$display_fri_impression=0;$display_sat_impression=0;
  $total_display_impression=0;$total_video_impression=0;$i=0;

  // variables for time impression
  $video0=0;$video1=0;$video2=0;$video3=0;$video4=0;$video5=0;$video6=0;$video7=0;$video8=0;$video9=0;$video10=0;$video11=0;$video12=0;$video13=0;$video14=0;$video15=0;$video16=0;$video17=0;$video18=0;$video19=0;$video20=0;$video21=0;$video22=0;$video23=0;$display0=0;$display1=0;$display2=0;$display3=0;$display4=0;$display5=0;$display6=0;$display7=0;$display8=0;$display9=0;$display10=0;$display11=0;$display12=0;$display13=0;$display14=0;$display15=0;$display16=0;$display17=0;$display18=0;$display19=0;$display20=0;$display21=0;$display22=0;$display23=0;$video_campaign=0;$display_campaign=0;$total_display_clicks=0;
  ?>
  @if(!empty($googleresult))
  @foreach($googleresult as $campaign)
  <?php
  // echo '<pre>';
  // var_dump($campaign);die;
  if(!empty($campaign->video_campaign)){
     if($campaign->video_campaign!=='null'){//for video cmapaign only
      $video=json_decode($campaign->video_campaign);
      $video_campaign='1';
    // Video Views by device
      if(!empty($video->device)){
        $video_mobile_views=$video_mobile_views + $video->device->mobile_views;
        $video_tab_views=$video_tab_views + $video->device->tab_views;
        $video_desktop_views=$video_desktop_views + $video->device->desk_views;
        $video_other_views=$video_other_views + $video->device->other_views;
      }



    // Video impression and views by week
      if(!empty($video->video->sun)){
       $sunday_video_impression=$sunday_video_impression + $video->video->sun->sun_impression;
       $sunday_video_view=$sunday_video_view + $video->video->sun->sun_views;
     }
     if(!empty($video->video->mon)){
      $monday_video_impression=$monday_video_impression + $video->video->mon->mon_impression;
      $monday_video_view=$monday_video_view + $video->video->mon->mon_views;
    }
    if(!empty($video->video->tue)){
     $tuesday_video_impression=$tuesday_video_impression + $video->video->tue->tue_impression;
     $tuesday_video_view=$tuesday_video_view + $video->video->tue->tue_views;
   }
   if(!empty($video->video->wed)){
    $wednesday_video_impression=$wednesday_video_impression + $video->video->wed->wed_impression;
    $wednesday_video_view=$wednesday_video_view + $video->video->wed->wed_views;
  }
  if(!empty($video->video->thru)){
    $thrusday_video_impression=$thrusday_video_impression + $video->video->thru->thru_impression;
    $thrusday_video_view=$thrusday_video_view + $video->video->thru->thru_views;
  }
  if(!empty($video->video->fri)){
    $friday_video_impression=$friday_video_impression + $video->video->fri->fri_impression;
    $friday_video_view=$friday_video_view + $video->video->fri->fri_views;
  }
  if(!empty($video->video->sat)){
    $saturday_video_impresison=$saturday_video_impresison + $video->video->sat->sat_impression;
    $saturday_video_view=$saturday_video_view + $video->video->sat->sat_views;
  }

  $total_video_impression=$sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
    // video impression,views,view rate
  $video_impression= $sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
  $video_views=$sunday_video_view + $monday_video_view + $tuesday_video_view + $wednesday_video_view + $thrusday_video_view + $friday_video_view + $saturday_video_view;
  if($video_impression>0 && $video_views>0){
    $video_rate=round($video_views/$video_impression *100, 2);
  }else{
   $video_rate=0.00;
 }

    // video campaign Statistics
 if(!empty($video->video_rate)){
  $video_25=$video_25 + $video->video_rate->video_25;
  $video_50=$video_50 + $video->video_rate->video_50;
  $video_75=$video_75 + $video->video_rate->video_75;
  $video_100=$video_100 + $video->video_rate->video_100;
}




}
}
 // for display campaign only

   /* echo '<pre>';
   var_dump($display);*/
   if(!empty($campaign->display_campaign)){
    if($campaign->display_campaign!=='null'){
      $display_campaign='1';
      $display=json_decode($campaign->display_campaign);
      // Display impression by device
      $mobimpression=$mobimpression + $display->display_device_impression->mobile;
      $deskimpression=$deskimpression + $display->display_device_impression->desktop;
      $tabimpression=$tabimpression + $display->display_device_impression->tablet;
      $otherimpression=$otherimpression + $display->display_device_impression->other;
      // Display Impression by week of the day
      $display_sun_impression=$display_sun_impression + $display->week_of_day_impression->sun;
      $display_mon_impression=$display_mon_impression + $display->week_of_day_impression->mon;
      $display_tue_impression=$display_tue_impression + $display->week_of_day_impression->tue;
      $display_wed_impression=$display_wed_impression + $display->week_of_day_impression->wed;
      $display_thru_impression=$display_thru_impression + $display->week_of_day_impression->thru;
      $display_fri_impression=$display_fri_impression + $display->week_of_day_impression->fri;
      $display_sat_impression=$display_sat_impression + $display->week_of_day_impression->sat;
      $total_display_impression= $display_sun_impression + $display_mon_impression + $display_tue_impression + $display_wed_impression + $display_thru_impression + $display_fri_impression + $display_sat_impression;
      // Display Gneder views and impression
      if(!empty($display->gender->male)){
       $maleclick=$maleclick + $display->gender->male->clicks;
       $maleimpression=$maleimpression + $display->gender->male->impressions;
     }

     if(!empty($display->gender->female)){
       $femaleclick=$femaleclick + $display->gender->female->clicks;
       $femaleimpression=$femaleimpression + $display->gender->female->impressions;
     }

     if(!empty($display->gender->unknown)){
       $unknownclick=$unknownclick + $display->gender->unknown->clicks;
       $unknownimpression=$unknownimpression + $display->gender->unknown->impressions;
     }


     $total_display_clicks=$maleclick + $femaleclick + $unknownclick;

   }
 }



 if(!empty($campaign->time_impression)){
  $time_impression=json_decode($campaign->time_impression);
  if(!empty($time_impression->video_impression)){

    $video0=$video0 + $time_impression->video_impression[0];
    $video1=$video1 + $time_impression->video_impression[1];
    $video2=$video2 + $time_impression->video_impression[2];
    $video3=$video3 + $time_impression->video_impression[3];
    $video4=$video4 + $time_impression->video_impression[4];
    $video5=$video5 + $time_impression->video_impression[5];
    $video6=$video6 + $time_impression->video_impression[6];
    $video7=$video7 + $time_impression->video_impression[7];
    $video8=$video8 + $time_impression->video_impression[8];
    $video9=$video9 + $time_impression->video_impression[9];
    $video10=$video10 + $time_impression->video_impression[10];
    $video11=$video11 + $time_impression->video_impression[11];
    $video12=$video12 + $time_impression->video_impression[12];
    $video13=$video13 + $time_impression->video_impression[13];
    $video14=$video14 + $time_impression->video_impression[14];
    $video15=$video15 + $time_impression->video_impression[15];
    $video16=$video16 + $time_impression->video_impression[16];
    $video17=$video17 + $time_impression->video_impression[17];
    $video18=$video18 + $time_impression->video_impression[18];
    $video19=$video19 + $time_impression->video_impression[19];
    $video20=$video20 + $time_impression->video_impression[20];
    $video21=$video21 + $time_impression->video_impression[21];
    $video22=$video22 + $time_impression->video_impression[22];
    $video23=$video23 + $time_impression->video_impression[23];
  }else{
    if(!empty($time_impression->display_impression)){
     $display0=$display0 + $time_impression->display_impression[0];
     $display1=$display1 + $time_impression->display_impression[1];
     $display2=$display2 + $time_impression->display_impression[2];
     $display3=$display3 + $time_impression->display_impression[3];
     $display4=$display4 + $time_impression->display_impression[4];
     $display5=$display5 + $time_impression->display_impression[5];
     $display6=$display6 + $time_impression->display_impression[6];
     $display7=$display7 + $time_impression->display_impression[7];
     $display8=$display8 + $time_impression->display_impression[8];
     $display9=$display9 + $time_impression->display_impression[9];
     $display10=$display10 + $time_impression->display_impression[10];
     $display11=$display11 + $time_impression->display_impression[11];
     $display12=$display12 + $time_impression->display_impression[12];
     $display13=$display13 + $time_impression->display_impression[13];
     $display14=$display14 + $time_impression->display_impression[14];
     $display15=$display15 + $time_impression->display_impression[15];
     $display16=$display16 + $time_impression->display_impression[16];
     $display17=$display17 + $time_impression->display_impression[17];
     $display18=$display18 + $time_impression->display_impression[18];
     $display19=$display19 + $time_impression->display_impression[19];
     $display20=$display20 + $time_impression->display_impression[20];
     $display21=$display21 + $time_impression->display_impression[21];
     $display22=$display22 + $time_impression->display_impression[22];
     $display23=$display23 + $time_impression->display_impression[23];
   }


 }
}


$updated_at=$campaign->updated_at;
$key= json_decode($campaign->keyword);
$spends=$spends + $key->cost;
$clicks= $clicks + $key->clicks;
$impressions= $impressions + $key->impressions;
$ctr=$ctr + $key->ctr;
$budget=$budget + $key->budget;
$invalid_clicks=$invalid_clicks + $key->invalid_clicks;
$platform=json_decode($campaign->pub_platform_campaign);
  // var_dump($platform->Display_Network);
?>
@endforeach
<?php
$date2=date('Y-m-d');
$diff = strtotime($date2) - strtotime($updated_at); 
$day_diff1=abs(round($diff / 86400));
if($day_diff1>0){
  $day_diff=$day_diff1.' days';
}else{
  $day_diff='Sometimes';
}
?>

<!-- video time impression -->
@if($video_campaign=='1')
<input type="hidden" id="video0" value="{{ $video0 }}">
<input type="hidden" id="video1" value="{{ $video1 }}">
<input type="hidden" id="video2" value="{{ $video2 }}">
<input type="hidden" id="video3" value="{{ $video3 }}">
<input type="hidden" id="video4" value="{{ $video4 }}">
<input type="hidden" id="video5" value="{{ $video5 }}">
<input type="hidden" id="video6" value="{{ $video6 }}">
<input type="hidden" id="video7" value="{{ $video7 }}">
<input type="hidden" id="video8" value="{{ $video8 }}">
<input type="hidden" id="video9" value="{{ $video9 }}">
<input type="hidden" id="video10" value="{{ $video10 }}">
<input type="hidden" id="video11" value="{{ $video11 }}">
<input type="hidden" id="video12" value="{{ $video12 }}">
<input type="hidden" id="video13" value="{{ $video13 }}">
<input type="hidden" id="video14" value="{{ $video14 }}">
<input type="hidden" id="video15" value="{{ $video15 }}">
<input type="hidden" id="video16" value="{{ $video16 }}">
<input type="hidden" id="video17" value="{{ $video17 }}">
<input type="hidden" id="video18" value="{{ $video18 }}">
<input type="hidden" id="video19" value="{{ $video19 }}">
<input type="hidden" id="video20" value="{{ $video20 }}">
<input type="hidden" id="video21" value="{{ $video21 }}">
<input type="hidden" id="video22" value="{{ $video22 }}">
<input type="hidden" id="video23" value="{{ $video23 }}">
@endif
<!-- Video time impression -->
<!-- display time impression -->
@if($display_campaign=='1')
<input type="hidden" id="display0" value="{{ $display0 }}">
<input type="hidden" id="display1" value="{{ $display1 }}">
<input type="hidden" id="display2" value="{{ $display2 }}">
<input type="hidden" id="display3" value="{{ $display3 }}">
<input type="hidden" id="display4" value="{{ $display4 }}">
<input type="hidden" id="display5" value="{{ $display5 }}">
<input type="hidden" id="display6" value="{{ $display6 }}">
<input type="hidden" id="display7" value="{{ $display7 }}">
<input type="hidden" id="display8" value="{{ $display8 }}">
<input type="hidden" id="display9" value="{{ $display9 }}">
<input type="hidden" id="display10" value="{{ $display10 }}">
<input type="hidden" id="display11" value="{{ $display11 }}">
<input type="hidden" id="display12" value="{{ $display12 }}">
<input type="hidden" id="display13" value="{{ $display13 }}">
<input type="hidden" id="display14" value="{{ $display14 }}">
<input type="hidden" id="display15" value="{{ $display15 }}">
<input type="hidden" id="display16" value="{{ $display16 }}">
<input type="hidden" id="display17" value="{{ $display17 }}">
<input type="hidden" id="display18" value="{{ $display18 }}">
<input type="hidden" id="display19" value="{{ $display19 }}">
<input type="hidden" id="display20" value="{{ $display20 }}">
<input type="hidden" id="display21" value="{{ $display21 }}">
<input type="hidden" id="display22" value="{{ $display22 }}">
<input type="hidden" id="display23" value="{{ $display23 }}">

@endif
<!-- Close display time impression -->


<!-- video impression by week of the day -->
@if($video_campaign=='1')
<input type="hidden" id="sunday_video_impression" value="{{ $sunday_video_impression }}">
<input type="hidden" id="monday_video_impression" value="{{ $monday_video_impression }}">
<input type="hidden" id="tuesday_video_impression" value="{{ $tuesday_video_impression }}">
<input type="hidden" id="wednesday_video_impression" value="{{ $wednesday_video_impression }}">
<input type="hidden" id="thrusday_video_impression" value="{{ $thrusday_video_impression }}">
<input type="hidden" id="friday_video_impression" value="{{ $friday_video_impression }}">
<input type="hidden" id="saturday_video_impresison" value="{{ $saturday_video_impresison }}">
@endif
<!--Close  video impression by week of the day -->
<!-- Open Video Views by days of the week -->
@if($video_campaign=='1')
<input type="hidden" id="sunday_video_view" value="{{ $sunday_video_view }}">
<input type="hidden" id="monday_video_view" value="{{ $monday_video_view }}">
<input type="hidden" id="tuesday_video_view" value="{{ $tuesday_video_view }}">
<input type="hidden" id="wednesday_video_view" value="{{ $wednesday_video_view }}">
<input type="hidden" id="thrusday_video_view" value="{{ $thrusday_video_view }}">
<input type="hidden" id="friday_video_view" value="{{ $friday_video_view }}">
<input type="hidden" id="saturday_video_view" value="{{ $saturday_video_view }}">
@endif
<!-- Close video views by days of the week -->
<!-- Open google videoes views by device -->
@if($video_campaign=='1')
<input type="hidden" id="video_mobile_views" value="{{ $video_mobile_views }}">
<input type="hidden" id="video_tab_views" value="{{ $video_tab_views }}">
<input type="hidden" id="video_desktop_views" value="{{ $video_desktop_views }}">
<input type="hidden" id="video_other_views" value="{{ $video_other_views }}">
@endif
<!-- Close google videos views by device -->
<!-- Open Google video play statistics -->
@if($video_campaign=='1')
<input type="hidden" id="video_25" value="{{ $video_25 }}">
<input type="hidden" id="video_50" value="{{ $video_50 }}"> 
<input type="hidden" id="video_75" value="{{ $video_75 }}">
<input type="hidden" id="video_100" value="{{ $video_100 }}">
@endif
<!-- Close Google Video View Statistics -->
<!-- for display gooogle campaigns impression by week -->
@if($display_campaign=='1')
<input type="hidden" id="display_sun_impression" value="{{ $display_sun_impression }}">
<input type="hidden" id="display_mon_impression" value="{{ $display_mon_impression }}">
<input type="hidden" id="display_tue_impression" value="{{ $display_tue_impression }}">
<input type="hidden" id="display_wed_impression" value="{{ $display_wed_impression }}">
<input type="hidden" id="display_thru_impression" value="{{ $display_thru_impression }}">
<input type="hidden" id="display_fri_impression" value="{{ $display_fri_impression }}">
<input type="hidden" id="display_sat_impression" value="{{ $display_sat_impression }}"> 
@endif
<!-- For google display campaigns impression by week -->
<!-- For google display impression by device -->
@if($display_campaign=='1')
<input type="hidden" id="mobimpression" value="{{ $mobimpression }}">
<input type="hidden" id="deskimpression" value="{{ $deskimpression }}">
<input type="hidden" id="tabimpression" value="{{ $tabimpression }}">
<input type="hidden" id="otherimpression" value="{{ $otherimpression }}">
@endif
<!-- For google display impression by device -->
<!-- Open for google display impression by gender -->
@if($display_campaign=='1')
<input type="hidden" id="maleimpression" value="{{ $maleimpression }}">
<input type="hidden" id="femaleimpression" value="{{ $femaleimpression }}">
<input type="hidden" id="unknownimpression" value="{{ $unknownimpression }}">
<input type="hidden" id="maleclick" value="{{ $maleclick }}">
<input type="hidden" id="femaleclick" value="{{ $femaleclick }}">
<input type="hidden" id="unknownclick" value="{{ $unknownclick }}">
@endif
<!-- Close for google display impression by gender -->
<h4>{{ $camptype }} Google Video Stats | Overview</h4>
<!-- for google geotarget exposures and target Area -->
<!-- <div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
   <div class="card card-stats">
     <div class="card-header card-header-primary card-header-icon">
       <div class="card-icon">
         <i class="material-icons">content_copy</i>
       </div>
       <p class="card-category">Dis.&Vid. Imp.)</p>
       <h3 class="card-title">{{ $total_display_impression+$total_video_impression }}</h3>
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 
</div> -->
<!-- For Video impression,Views and View Rate -->
<div class="row">
  <div class="col-lg-4 col-md-6 col-sm-6">
   <div class="card card-stats">
     <div class="card-header card-header-primary card-header-icon">
       <div class="card-icon">
         <i class="material-icons">content_copy</i>
       </div>
       <p class="card-category">Total Impression</p>
       <h3 class="card-title">{{ $total_display_impression+$total_video_impression }}</h3>
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 <div class="col-lg-4 col-md-6 col-sm-6">
   <div class="card card-stats">
     <div class="card-header card-header-warning card-header-icon">
       <div class="card-icon">
         <i class="material-icons">content_copy</i>
       </div>
       <p class="card-category">Video Impression</p>
       <h3 class="card-title">{{ $video_impression }}</h3>
       <!-- <h3 class="card-title">{{ $total_display_clicks }}</h3> -->
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 <div class="col-lg-4 col-md-6 col-sm-6">
   <div class="card card-stats">
     <div class="card-header card-header-success card-header-icon">
       <div class="card-icon">
         <i class="material-icons">store</i>
       </div>
       <p class="card-category">Views</p>
       <h3 class="card-title">{{ $video_views }}</h3>
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">date_range</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 
</div>
<!-- Close for Video impression,Views and View Rate-->

<!-- Open Video Graph for impression week of the day -->
<div class="row">
 <div class="col-md-6">
   <div class="card card-chart">
     <div class="card-header card-header-success" style="background: white;">
       <canvas id="googlebardisplay"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Video Impression By Week</h4>
     <!--    <p class="card-category">
       <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 <div class="col-md-6">
   <div class="card card-chart">
     <div class="card-header card-header-success" style="background: white;">
       <canvas id="pieChartVideoViews"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Video Views By Day of Week</h4>
       <!--  <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       <div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- Close video graph for impression week of the day-->
 <!-- Open Google videos views by device -->
 <div class="row">
   <div class="col-md-6">
     <div class="card card-chart">
       <div class="card-header card-header-success" style="background: white;">
         <canvas id="pieChartVideoViewsByDevice"></canvas>
       </div>
       <div class="card-body">
        <h4 class="card-title">Video Views By Device</h4>
       <!--  <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       <div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>
     </div>
   </div>
   <div class="col-md-6">
     <div class="card card-chart">
      <div class="card-header card-header-success" style="background: white;">
       <canvas id="pieChartVideoViewsState"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Video Campaign Stats</h4>
        <!-- <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       <div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>
     </div>
   </div>
 </div>

 <!-- new row for video time impression -->
 <div class="row">
   <div class="col-md-12">
     <div class="card card-chart">
      <div class="card-header card-header-success" style="background: white;">
       <canvas id="linechartvideoimpression"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Video Time Impression</h4>
        <!-- <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       <div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>
     </div>
   </div>
 </div>

 <!--Close row for video time impression -->
 <!-- <h4>Targeted Location</h4>
 <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div id="map_canvas" style="width: 1000px; height: 400px;"></div>
  </div>
</div>
<br> -->
<!-- Close google videos views by device -->
<h4>{{ $camptype }} Google Display Stats| Overview</h4>
<!-- Div for google display views  -->
<!-- <div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
   <div class="card border-primary mb-3">
    <div class="card-header">Geo Target Exposures(Display Impressions)</div>
    <div class="card-body text-primary">
      <h3 class="card-title">{{ $total_display_impression }}</h3>
      <p class="card-text">Some quick example text to build on the panel title.</p>
    </div>
  </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
 <div class="card border-info mb-3">
  <div class="card-header">Clicks</div>
  <div class="card-body text-primary">
    <h3 class="card-title">{{ $total_display_clicks }}</h3>
    <p class="card-text">Some quick example text to build on the panel title.</p>
  </div>
</div>
</div>
</div> -->
<!-- Open div for google -->
<div class="row">
 <div class="col-md-6">
   <div class="card card-chart">
     <div class="card-header card-header-success" style="background: white;">
       <canvas id="googlebardisplayImpression"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Display Impression By Week</h4>
     <!--  <p class="card-category">
       <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 <div class="col-md-6">
   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
     <canvas id="pieChartDisplayViewsbydevice"></canvas>
   </div>
   <div class="card-body">
    <h4 class="card-title">Display Impression by device</h4>
     <!--  <p class="card-category">
       <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
</div>
<div class="row">
 <div class="col-md-6">
   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
     <canvas id="googledisplayImpressionbyGender"></canvas>
   </div>
   <div class="card-body">
    <h4 class="card-title">Display Impression By Gender</h4>
      <!-- <p class="card-category">
       <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div>
 <!-- <div class="col-md-6">
   <div class="card card-chart">
     <div class="card-header card-header-success" style="background: white;">
       <canvas id="pieChartDisplayClickbyGender"></canvas>
     </div>
     <div class="card-body">
      <h4 class="card-title">Display Click by Gender</h4>
      <p class="card-category">
       <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
     </div>
     <div class="card-footer">
       <div class="stats">
         <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
       </div>
     </div>
   </div>
 </div> -->
</div>
<!-- Close div for google -->
<!-- Open for  Google display time impression-->
<div class="row">
 <div class="col-md-12">
   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
     <canvas id="linechartdisplayimpression"></canvas>
   </div>
   <div class="card-body">
    <h4 class="card-title">Display Time Impression</h4>
        <!-- <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       <div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>
     </div>
   </div>
 </div>

 <!-- Close for google display time impression -->
 <!-- Close div for google display views -->
 <div class="row">
  <div class="col-md-6">
   <?php 
   if($camp_type=='0'){ ?>
    <a href="{{ url('monthlydownloadcsv') }}"><button type="button" class="btn btn-primary">Download Monthly Video Placement Report</button></a>
  <?php }else{ ?>
   <a href="{{ url('downloadcsv') }}"><button type="button" class="btn btn-primary">Download Lifetime Video Placement Report</button></a>
 <?php }
 ?>


</div>
<div class="col-md-6">
 <?php 
 if($camp_type=='0'){ ?>
  <a href="{{ url('monthlydownloadcsvdisplay') }}"><button type="button" class="btn btn-success">Download Monthly Display Placement Report</button></a>
<?php }else{ ?>
 <a href="{{ url('downloadcsvdisplay') }}"><button type="button" class="btn btn-success">Download Lifetime Display Placement Report</button></a>
<?php }
?>
</div>
<!-- <a href="{{ url('monthlydownloadcsvdisplay') }}"><button type="button" class="btn btn-success">Download Video Placement Report</button></a> -->
<!-- <div class="col-md-6">

  <a href="{{ url('downloadcsv') }}"><button type="button" class="btn btn-primary">Download Video Placement Report</button></a>
 

 


</div> -->
<!-- <div class="col-md-6">
 <a href="{{ url('downloadcsvdisplay') }}"><button type="button" class="btn btn-success">Download Display Placement Report</button></a>
</div> -->
</div>

<!-- <div class="row">
  <div class="col-md-12">
    <div class="card card">
      <div class="card-header card-header-primary">
        <h4 class="card-title mt-0">Ad Placement List</h4>
        
      </div>
      @foreach($googleresult as $data)
      <?php 
      $ads[]=json_decode($data->ad_placement);
      ?>
      @endforeach
      <div class="card-body">
        <div class="table-responsive">
          <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="th-sm">S.No
                </th>
                <th class="th-sm">Placement Name</th>
                <th class="th-sm">Impression</th>
              </tr>
            </thead>
            <tbody>
             <?php 
             $i=1;
             ?>
             @for($j=0; $j < count($ads); $j++)
             @if(!empty($ads[$j]))
             @foreach($ads[$j] as $placement)
             <tr>
              <td>{{ $i }}</td>
              <td>{{ $placement->ad_name}}</td>
              <td>{{ $placement->ad_impresison }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @endif
            @endfor
          </tbody>
          <tfoot>
            <tr>
              <th>S.No</th>
              <th>Placement Name</th>
              <th>Impression</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
</div> -->
@else
<h4>No Campaigns For google</h4>
@endif
</div>
<div class="tab-pane" id="groundtruth">
 <?php 
 $total_spend=0;
 $total_clicks=0;$dec_clicks=0;$jan_clicks=0;$feb_clicks=0;$mar_clicks=0;$apr_clicks=0;$may_clicks=0;$jun_clicks=0;$jul_clicks=0;$aug_clicks=0;$sep_clicks=0;$oct_clicks=0;$nov_clicks=0;
 $total_impression=0;$mon_impression=0;$tue_impression=0;$wed_impression=0;$thr_impression=0;$fri_impression=0;$sat_impression=0;$sun_impression=0;
 $total_ctr=0;$us_impression=0;$uk_impression=0;$total_cpm=0;$total_video_start=0;
 $total_reach=0;$jan_spent=0;$feb_spent=0;$mar_spent=0;$apr_spent=0;$may_spent=0;$jun_spent=0;$jul_spent=0;$aug_spent=0;$sep_spent=0;$oct_spent=0;$nov_spent=0;$dec_spent=0;
 $coral_imp=0;$davie_imp=0;$key_largo_imp=0;$key_west_imp=0;$delray_imp=0;$reach=0;
 ?>
 @if(!empty($grndadminresult))
 @foreach($grndadminresult as $camp)
 <?php 
 if(!empty($camp->loc_impression)){
   $loc_impresison=json_decode($camp->loc_impression);
   if(!empty($loc_impresison)){
    foreach($loc_impresison as $data){
      if($data->country=='us'){
       $us_impression=$us_impression + $data->impressions;
     }
     if($data->country=='uk'){
      $uk_impression=$uk_impression + $data->impressions;
    }
    if($data->city=='coral springs'){
     $coral_imp=$coral_imp + $data->impressions;
   }
   if($data->city=='davie'){
    $davie_imp=$davie_imp + $data->impressions;
  }
  if($data->city=='key largo'){
    $key_largo_imp=$key_largo_imp + $data->impressions;
  }
  if($data->city=='key west'){
   $key_west_imp=$key_west_imp + $data->impressions;
 }
 if($data->city=='delray beach'){
  $delray_imp=$delray_imp + $data->impressions;
}
}
}
}
?>
@endforeach
@foreach($grndadminresult as $campaign)
<?php 
if(!empty($campaign->keyword)){
 $keyword=json_decode($campaign->keyword);
 
 $spend=$keyword->spend;
 $clicks=$keyword->clicks;
 $impressions=$keyword->impressions;
 $impr[]=$keyword->impressions;
 $video_start=$keyword->video_start;
 $ctr=$keyword->ctr;
 $cpm=$keyword->cpm;
 $reach=$keyword->cumulative_reach;
 $newarray[$campaign->camp_id]=$keyword->cumulative_reach;
 $total_spend=$total_spend + $spend;
 $m=date('M',strtotime($campaign->camp_date));
 $d=date('D',strtotime($campaign->camp_date));
 if($d=='Mon'){
   $mon_impression=$mon_impression + $impressions;
 }elseif ($d=='Tue') {
   $tue_impression=$tue_impression + $impressions;
 }elseif ($d=='Wed') {
  $wed_impression=$wed_impression + $impressions;
}elseif ($d=='Thr') {
  $thr_impression=$tru_impression + $impressions;
}elseif ($d=='Fri') {
  $fri_impression=$fri_impression + $impressions;
}elseif ($d=='Sat') {
 $sat_impression=$sat_impression + $impressions;
}else{
  $sun_impression=$sun_impression + $impressions;
}
if($m=='Dec'){
  $dec_clicks=$dec_clicks + $clicks;
  $dec_spent=$dec_spent + $spend;
}elseif ($m=='Jan') {
  $jan_clicks=$jan_clicks + $clicks;
  $jan_spent=$jan_spent + $spend;
}elseif ($m=='Feb') {
  $feb_clicks=$feb_clicks + $clicks;
  $feb_spent=$feb_spent + $spend;
}elseif($m=='Mar'){
 $mar_clicks=$mar_clicks + $clicks;
 $mar_spent=$mar_spent + $spend;
}elseif ($m=='Apr') {
 $apr_clicks=$apr_clicks + $clicks;
 $apr_spent=$apr_spent + $spend;
}elseif ($m=='May') {
 $may_clicks=$may_clicks + $clicks;
 $may_spent=$may_spent + $spend;
}elseif ($m=='Jun') {
  $jun_clicks=$jun_clicks + $clicks;
  $jun_spent=$jun_spent + $spend;
}elseif ($m=='Jul') {
 $jul_clicks=$jul_clicks + $clicks;
 $jul_spent=$jul_spent + $spend;
}elseif ($m=='Aug') {
 $aug_clicks=$aug_clicks + $clicks;
 $aug_spent=$aug_spent + $spend;
}elseif ($m=='Sep') {
 $sep_clicks=$sep_clicks + $clicks;
 $sep_spent=$sep_spent + $spend;
}elseif ($m=='Oct') {
  $oct_clicks=$oct_clicks + $clicks;
  $oct_spent=$oct_spent + $spend;
}else{
 $nov_clicks=$nov_clicks + $clicks;
 $nov_spent=$nov_spent + $spend;
}
$total_clicks=$total_clicks + $clicks;
$total_impression=$total_impression + $impressions;
$total_ctr=$total_ctr + $ctr;
$total_cpm=$total_cpm + $cpm;
$total_reach=$reach;
$total_video_start=$total_video_start + $video_start;
$date1=$campaign->camp_date;
}
?>
@endforeach
<?php 
$newreach=0;
if(!empty($newarray)){
  foreach($newarray as $reach){
    $newreach=$newreach + $reach;
  }
}

$date2=date('Y-m-d');
$diff = strtotime($date2) - strtotime($date1); 
$day_diff1=abs(round($diff / 86400));
if($day_diff1>0){
 $day_diff=$day_diff1.' days';
}else{
  $day_diff='Sometimes';
}
?>
<input type="hidden" id="coral_imp" value="{{ $coral_imp }}">
<input type="hidden" id="davie_imp" value="{{ $davie_imp }}">
<input type="hidden" id="key_largo_imp" value="{{ $key_largo_imp }}">
<input type="hidden" id="key_west_imp" value="{{ $key_west_imp }}">
<input type="hidden" id="delray_imp" value="{{ $delray_imp }}">
<input type="hidden" id="total_impression1" value="{{ $total_impression }}">
<input type="hidden" id="total_ctr1" value="{{ $total_ctr }}">
<input type="hidden" id="total_clicks1" value="{{ $total_clicks }}">
<input type="hidden" id="total_reach1" value="{{ $total_reach }}">
<input type="hidden" id="total_video_start1" value="{{ $total_video_start }}">
<input type="hidden" id="us_impression1" value="{{ $us_impression }}">
<input type="hidden" id="uk_impression1" value="{{ $uk_impression }}">
<input type="hidden" id="dec_click1" value="{{ $dec_clicks }}">
<input type="hidden" id="nov_click1" value="{{ $nov_clicks }}">
<input type="hidden" id="oct_click1" value="{{ $oct_clicks }}">
<input type="hidden" id="sep_click1" value="{{ $sep_clicks }}">
<input type="hidden" id="aug_click1" value="{{ $aug_clicks }}">
<input type="hidden" id="jul_click1" value="{{ $jul_clicks }}">
<input type="hidden" id="jun_click1" value="{{ $jun_clicks }}">
<input type="hidden" id="may_click1" value="{{ $may_clicks }}">
<input type="hidden" id="apr_click1" value="{{ $apr_clicks }}">
<input type="hidden" id="mar_click1" value="{{ $mar_clicks }}">
<input type="hidden" id="feb_click1" value="{{ $feb_clicks }}">
<input type="hidden" id="jan_click1" value="{{ $jan_clicks }}">
<input type="hidden" id="dec_spent1" value="{{ $dec_spent }}">
<input type="hidden" id="nov_spent1" value="{{ $nov_spent }}">
<input type="hidden" id="oct_spent1" value="{{ $oct_spent }}">
<input type="hidden" id="sep_spent1" value="{{ $sep_spent }}">
<input type="hidden" id="aug_spent1" value="{{ $aug_spent }}">
<input type="hidden" id="jul_spent1" value="{{ $jul_spent }}">
<input type="hidden" id="jun_spent1" value="{{ $jun_spent }}">
<input type="hidden" id="may_spent1" value="{{ $may_spent }}">
<input type="hidden" id="apr_spent1" value="{{ $apr_spent }}">
<input type="hidden" id="mar_spent1" value="{{ $mar_spent }}">
<input type="hidden" id="feb_spent1" value="{{ $feb_spent }}">
<input type="hidden" id="jan_spent1" value="{{ $jan_spent }}">
<input type="hidden" id="m1" value="{{ $mon_impression }}">
<input type="hidden" id="t1" value="{{ $tue_impression }}">
<input type="hidden" id="w1" value="{{ $wed_impression }}">
<input type="hidden" id="th1" value="{{ $thr_impression }}">
<input type="hidden" id="f1" value="{{ $fri_impression }}">
<input type="hidden" id="sat1" value="{{ $sat_impression }}">
<input type="hidden" id="sun1" value="{{ $sun_impression }}">
<h4>{{ $camptype }} Games & Applications Records | Overview</h4>
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-info card-header-icon">
        <div class="card-icon">
          <i class="material-icons">store</i>
        </div>
        <p class="card-category">Impressions</p>
        <h3 class="card-title">{{ $total_impression }}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{$day_diff}} ago
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="col-lg-4 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="material-icons">content_copy</i>
        </div>
        <p class="card-category">Total Clicks</p>
        <h3 class="card-title">{{ $total_clicks }}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{$day_diff}} ago
        </div>
      </div>
    </div>
  </div> -->
  <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="material-icons">store</i>
        </div>
        <p class="card-category">Total Reach</p>
        <h3 class="card-title">{{ $newreach }}</h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{$day_diff}} ago
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="material-icons">content_copy</i>
        </div>
        <p class="card-category">Video Start</p>
        <h3 class="card-title">{{ $total_video_start }}
        </h3>
      </div>
      <div class="card-footer">
        <div class="stats">
          <i class="material-icons">date_range</i> Updated {{$day_diff}} ago
        </div>
      </div>
    </div>
  </div> -->
  
</div>
<div class="row">
 <div class="col-md-6">
  <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
      <canvas id="BarChartForImpression"></canvas>
    </div>
    <div class="card-body">
      <h4 class="card-title">Week Of The Day Impression</h4>
               <!--  <p class="card-category">
                <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i>Updated {{$day_diff}} ago
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-md-6">
            <div class="card card-chart">
              <div class="card-header card-header-success" style="background: white;">
                <canvas id="barChartgrnd"></canvas>
              </div>
              <div class="card-body">
                <h4 class="card-title">Month Clicks</h4>
                <p class="card-category">Last Campaign Performance</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> Updated {{$day_diff}} ago
                </div>
              </div>
            </div>
          </div> -->
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card card">
              <div class="card-header card-header-primary">
                <h4 class="card-title mt-0">Recently Updated Ad Creative Stats</h4>

              </div>
              @foreach($grndadminresult as $data)
              <?php 
              $ads1[]=json_decode($data->ad_creative_image);

              ?>
              @endforeach
              <?php 
              foreach ($ads1 as $ads2) {
               $ads3=count($ads2);
               // echo  $ads2->creative_name;
             }
             
             ?>
             <div class="card-body">
              <div class="table-responsive">
                <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="th-sm">S.No</th>
                      <th>Creative Name</th>
                      <th>Image</th>
                      <th class="th-sm">Recently Impression</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
                   $i=1;
                   ?>
                   @for($j=0; $j < count($ads3); $j++)
                   @if(!empty($ads1[$j]))
                   @foreach($ads1[$j] as $placement)
                   <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $placement->creative_name}}</td>
                    <td><img src="{{  $placement->creative_url }}" style="height: 40%;"></td>
                    <td>{{ $placement->impression }}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach
                  @endif
                  @endfor
                </tbody>
                <tfoot>
                  <tr>
                    <th>S.No</th>
                    <th>Creative Name</th>
                    <th>Image</th>
                    <th>Impression</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    @else
    <h4>No Groundtruth Records</h4>
    @endif
  </div>
  <div class="tab-pane" id="target_location">
    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12">
        <h4>Targeted Location</h4>
        <?php if(!empty($target_location)){
          foreach($target_location as $loc){
          // var_dump($loc->target_location);
            $target=json_decode($loc->target_location);
            foreach($target as $tar){
              $address[]=$tar->location;
            }
          }
          if(!empty($address)){
           $i=1;
           foreach($address as $mainaddress){

            if(strlen($mainaddress)>50){
             // echo $mainaddress;
             // echo '<br>';
              $mainaddress = trim($mainaddress," ");
              $mainaddress = str_replace(" ", "+", $mainaddress);

              $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$mainaddress."&key=AIzaSyDDJMIkgBgtv54G_H-EHM7muG_qW7pdz28"; 

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
              $response = curl_exec($ch);
              curl_close($ch);
              $response_a = json_decode($response);
              $lat = $response_a->results[0]->geometry->location->lat;  
              $lng = $response_a->results[0]->geometry->location->lng;
              echo  '<input type="hidden" id="lat'.$i.'" value="'.$lat.'">';
              echo '<input type="hidden" id="lng'.$i.'" value="'.$lng.'">';

            }else{
              $str=strpos($mainaddress,"around");
              $cal=$str + 6;
              $locc=substr($mainaddress, $cal);
              $find=explode(',', $locc);
              $lat = $find[0];  
              $lng = $find[1];
              echo  '<input type="hidden" id="lat'.$i.'" value="'.$lat.'">';
              echo '<input type="hidden" id="lng'.$i.'" value="'.$lng.'">';
            }

            $i++;

          }
        }



        ?>



        <div id="map_canvas" style="width: 1000px; height: 400px;"></div>
      <?php }else{ ?>
        <h4>No Targeted Location Found</h4>
      <?php  }
      ?>

    </div>
  </div>
</div>

<div class="tab-pane" id="view_stats">
  <div class="row">

    <div class="col-md-12">
     <h4><span id="stattext">Lifetime</span> Stats Records | Overview</h4>
     <!--  <div class="card card"> -->
      <!-- <div class="card-header card-header-primary"> -->
        <div class="circle" align="center" id="circle" style="display:none;">
          <img src="{{ asset('resources/assets') }}/img/ring-loader.gif" style="height: 300px;position: absolute !important;
          top: 10%;
          left: 50%;
          border-radius: 5px;
          z-index: 11;
          margin: 0 auto;
          -webkit-transform: translate(-50%);
          -moz-transform: translate(-50%);
          -ms-transform: translate(-50%);
          -o-transform: translate(-50%);
          transform: translate(-50%);">
        </div>

        <!-- <p class="card-category"> Here is a subtitle for this table</p> -->

        <?php 
        $total_spend=0;
        $total_clicks=0;
        $total_impression=0;
        $total_ctr=0;
        $total_reach=0;
        ?>
        @if(!empty($fbresult))
        @foreach($fbresult as $campaign)
        <?php 
        if(!empty($campaign->keyword)){
         $keyword=json_decode($campaign->keyword);
         $spend=$keyword->spend;
         $clicks=$keyword->clicks;
         $impressions=$keyword->impressions;
         $ctr=$keyword->ctr;
         $reach=$keyword->reach;
         $total_spend=$total_spend + $spend;
         $total_clicks=$total_clicks + $clicks;
         $total_impression=$total_impression + $impressions;
         $total_ctr=$total_ctr + $ctr;
         $total_reach=$total_reach + $reach;
       }
       ?>
       @endforeach
       @endif
       <?php 
       $fb_reach=$total_reach;
       $fb_clicks=$total_clicks;
       $fb_impression=$total_impression;
       $fb_ctr=$total_ctr;
       $fb_spend=$total_spend;
       ?>
       <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Network. Name</th>
              <th>IMPR.</th>
              <th>REACH</th>
              <!-- <th>SPEND</th> -->
              <th>VID.IMP.</th>
              <th>VIDEO VIEWS</th>
              <th>VIDEO RATE</th>
              <th>CTR</th>
              <!-- <th>BUDGET</th> -->
              <th>CLICKS</th>
              <th>ACTIONS</th>
              <th>SOC CLICKS</th>
              <th>FOLLOW</th>
              <th>REPLIES</th>
              <th>RETWE.</th>
              <th>LIKES</th>
              <th>RESULTS</th>
              <th>VISITS</th>
              <th>SAR</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ 'Social Media' }}</td>
                <td><span id="fbimpression">{{ $total_impression }}</span></td>
                <td><span id="fbreach">{{ $total_reach }}</span></td>
                <!-- <td><span id="fbspend">{{ $total_spend }}</span></td> -->
                <td>--</td>
                <td>--</td>
                <td>--</td>
                <td><span id="fbctr">{{ $total_ctr }}</span></td>
                <!-- <td>--</td> -->
                <td><span id="fbclicks">{{ $total_clicks }}</span></td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
              <?php 
              $total_spend=0;
              $total_clicks=0;
              $total_impression=0;
              $total_ctr=0;
              $total_budget=0;$video_mobile_views=0;$video_tab_views=0;$video_desktop_views=0;
              $video_other_views=0;$sunday_video_impression=0;$monday_video_impression=0;$tuesday_video_impression=0;$wednesday_video_impression=0;$thrusday_video_impression=0;$friday_video_impression=0;$saturday_video_impresison=0;$sunday_video_view=0;$monday_video_view=0; $tuesday_video_view=0; $wednesday_video_view=0; $thrusday_video_view=0; $friday_video_view=0; $saturday_video_view=0; $total_video_impression=0;$video_views=0;
              ?>
              @if(!empty($googleresult))
              @foreach($googleresult as $campaign)
              <?php 
              if(!empty($campaign->keyword)){
               $keyword=json_decode($campaign->keyword);
               $spend=$keyword->cost;
               $clicks=$keyword->clicks;
               $impressions=$keyword->impressions;
               $ctr=$keyword->ctr;
               $budget=$keyword->budget;
               $total_clicks= $total_clicks + $clicks;
               $total_impression=$total_impression + $impressions;
               $total_ctr=$total_ctr + $ctr;
               $total_budget=$total_budget + $budget;
               $total_spend=$total_spend + $spend;
             }
             if(!empty($campaign->video_campaign)){
              if($campaign->video_campaign!=='null'){
                $video=json_decode($campaign->video_campaign);
                if(!empty($video->video->sun)){
                 $sunday_video_view=$sunday_video_view + $video->video->sun->sun_views;
                 $monday_video_view=$monday_video_view + $video->video->mon->mon_views;
               }

               $tuesday_video_view=$tuesday_video_view + $video->video->tue->tue_views;
               $wednesday_video_view=$wednesday_video_view + $video->video->wed->wed_views;
               $thrusday_video_view=$thrusday_video_view + $video->video->thru->thru_views;
               $friday_video_view=$friday_video_view + $video->video->fri->fri_views;
               $saturday_video_view=$saturday_video_view + $video->video->sat->sat_views;

               $video_views=$sunday_video_view + $monday_video_view + $tuesday_video_view + $wednesday_video_view + $thrusday_video_view + $friday_video_view + $saturday_video_view;

               $sunday_video_impression=$sunday_video_impression + $video->video->sun->sun_impression;
               $monday_video_impression=$monday_video_impression + $video->video->mon->mon_impression;
               $tuesday_video_impression=$tuesday_video_impression + $video->video->tue->tue_impression;
               $wednesday_video_impression=$wednesday_video_impression + $video->video->wed->wed_impression;
               $thrusday_video_impression=$thrusday_video_impression + $video->video->thru->thru_impression;
               $friday_video_impression=$friday_video_impression + $video->video->fri->fri_impression;
               $saturday_video_impresison=$saturday_video_impresison + $video->video->sat->sat_impression;
               $total_video_impression=$sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
             }
           }

           ?>
           @endforeach
           @endif
           <?php 
           $google_spend=$total_spend;
           $google_clicks=$total_clicks;
           $google_impression=$total_impression;
           $google_ctr=$total_ctr;
           $google_video_imp=$total_video_impression;
           $google_video_view=$video_views;
           if($video_views>0 && $total_video_impression>0){
             $google_video_rate=round($video_views/$total_video_impression * 100,2);
           }else{
            $google_video_rate=0;
          }

          ?>
          <tr>
            <td>{{ 'Google/Youtube' }}</td>
            <td id="googleimpression">{{ $total_impression }}</td>
            <td>--</td>
            <!-- <td>--</td> -->
            <td id="googlevideoimp">{{ $total_video_impression }}</td>
            <td id="googlevideoviews">{{ $video_views }}</td>
            <td id="googlevideorate">{{  $google_video_rate }}{{ '%' }}</td>
            <td id="googlectr">{{ $total_ctr }}</td>
            <!-- <td id="googlebudget">{{ $total_budget }}</td> -->
            <td id="googleclick">{{ $total_clicks }}</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
          </tr>
          <?php 
          $total_spend=0;
          $total_clicks=0;
          $total_impression=0;
          $total_ctr=0;
          $total_reach=0;
          $total_budget=0; 
          $total_visits=0;
          $total_sar=0;
          ?>
          @if(!empty($grndadminresult))
          @foreach($grndadminresult as $campaign1)
          <?php
          // echo '<pre>';
          // var_dump($campaign);die; 
          if(!empty($campaign1->keyword)){
           $keyword=json_decode($campaign1->keyword);
           $spend=$keyword->spend;
           $clicks=$keyword->clicks;
           $impressions=$keyword->impressions;
           $ctr=$keyword->ctr;
           $reach=$keyword->cumulative_reach;
           $total_spend=$total_spend + $spend;
           $total_clicks=$total_clicks + $clicks;
           $total_impression=$total_impression + $impressions;
           $total_ctr=$total_ctr + $ctr;
           $total_reach=$reach;
         }
         ?>
         @endforeach
         @endif
         <?php 
         $grnd_reach=$total_reach;
         $grnd_clicks=$total_clicks;
         $grnd_impression=$total_impression;
         $grnd_ctr=$total_ctr;
         $grnd_spend=$total_spend;
         ?>

         <tr>
          <td>{{ 'Games & Application' }}</td>
          <td><span id="grndimp">{{ $total_impression }}</span></td>
          <td><span id="grndreach">{{ $total_reach }}</span></td>
          <!-- <td><span id="grndspend">{{ $total_spend }}</span></td> -->
          <td><span id="">--</span></td>
          <td>0.00</td>
          <td>0.00%</td>
          <td><span id="grndctr">{{ $total_ctr }}</span></td>
          <!-- <td>-</td>  -->
          <td><span id="grndclick">{{ $total_clicks }}</span></td>

          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>{{ $total_visits }}</td>
          <td>{{ $total_sar }}</td>
        </tr>
        <tr>
          <td><b>{{ 'Total' }}</b></td>
          <td><span id="totalimp">{{ $google_impression + $fb_impression + $grnd_impression }}</span></td>
          <td><span id="totalreach">{{ $fb_reach + $grnd_reach }}</span></td>
          <!-- <td><span id="totalspend">{{ $fb_spend + $grnd_spend }}</span></td> -->
          <td><span id="videoimp">{{ $google_video_imp }}</span></td>
          <td><span id="videoviews">{{ $google_video_view }}</span></td>
          <td><span id="videorate">{{ $google_video_rate }}{{ '%' }}</span></td>
          <td><span id="totalctr">{{ $google_ctr + $fb_ctr + $grnd_ctr }}</span></td>
          <!-- <td><span id="total_budget">--</span></td> -->
          <td><span id="totalclick">{{ $google_clicks + $fb_clicks + $grnd_clicks }}</span></td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>{{ $total_visits }}</td>
          <td>{{ $total_sar }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

</div>
</div>
<div class="row">
 <input type="checkbox" id="checkbox" class="larger" value="{{Auth::guard('user')->user()->customer_id}}" onclick="check_function(this.value)" style="
 margin-right: 5px; margin-top: 2px;
 ">Get Last 30 Days Report
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  var dec_spent=$("#dec_spent").val(); var jan_spent=$("#jan_spent").val(); var feb_spent=$("#feb_spent").val(); var mar_spent=$("#mar_spent").val(); var apr_spent=$("#apr_spent").val(); var may_spent=$("#may_spent").val(); var jun_spent=$("#jun_spent").val(); var jul_spent=$("#jul_spent").val(); var aug_spent=$("#aug_spent").val();
  var sep_spent=$("#sep_spent").val(); var oct_spent=$("#oct_spent").val(); var nov_spent=$("#nov_spent").val();
  dataDailySalesChart = {
    labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J','A','S','O','N','D'],
    series: [
    [jan_spent, feb_spent, mar_spent, apr_spent, may_spent, jun_spent, jul_spent, aug_spent, sep_spent, oct_spent, nov_spent, dec_spent]
    ]
  };
  optionsDailySalesChart = {
    lineSmooth: Chartist.Interpolation.cardinal({
      tension: 0
    }),
    low: 0,
        high: 500, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
      }
      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
      md.startAnimationForLineChart(dailySalesChart);
    </script>
    <!-- for groundtruth -->
    <script type="text/javascript">
      var dec_spent=$("#dec_spent1").val(); var jan_spent=$("#jan_spent1").val(); 
      var feb_spent=$("#feb_spent1").val(); var mar_spent=$("#mar_spent1").val(); 
      var apr_spent=$("#apr_spent1").val(); var may_spent=$("#may_spent1").val(); 
      var jun_spent=$("#jun_spent1").val(); var jul_spent=$("#jul_spent1").val(); 
      var aug_spent=$("#aug_spent1").val();
      var sep_spent=$("#sep_spent1").val(); var oct_spent=$("#oct_spent1").val(); 
      var nov_spent=$("#nov_spent1").val();
      var ctxB = document.getElementById("barChart").getContext('2d');
      var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [{
            label: 'Month Spent',
            data: [jan_spent, feb_spent, mar_spent, apr_spent, may_spent, jun_spent, jul_spent, aug_spent, sep_spent, oct_spent, nov_spent, dec_spent],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    </script>
    <!-- close for groundtruth -->
    @endpush
    @push('js')
    <script type="text/javascript">
      var dec_click=$("#dec_click").val();
      var dataWebsiteViewsChart = {
        labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
        series: [
        [jan_click, feb_click, mar_click, apr_click, may_click, jun_click, jul_click, aug_click, sep_click, oct_click, nov_click, dec_click]
        ]
      };
      var optionsWebsiteViewsChart = {
        axisX: {
          showGrid: false
        },
        low: 0,
        high: 500,
        chartPadding: {
          top: 0,
          right: 45,
          bottom: 0,
          left: 0
        }
      };
      var responsiveOptions = [
      ['screen and (max-width: 1040px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function(value) {
            return value[0];
          }
        }
      }]
      ];
      var websiteViewsChart = Chartist.Bar('#websiteViewsChart', dataWebsiteViewsChart, optionsWebsiteViewsChart, responsiveOptions);
      //start animation for the Emails Subscription Chart
      md.startAnimationForBarChart(websiteViewsChart);
    </script>
    <!-- clicks for groundtruth -->
    <script type="text/javascript">
     //bar
     var jan_click=$("#jan_click1").val();var feb_click=$("#feb_click1").val();
     var mar_click=$("#mar_click1").val();var apr_click=$("#apr_click1").val();
     var jun_click=$("#jun_click1").val();var jul_click=$("#jul_click1").val();
     var aug_click=$("#aug_click1").val();var sep_click=$("#sep_click1").val();
     var oct_click=$("#oct_click1").val();var nov_click=$("#nov_click1").val();
     var dec_click=$("#dec_click1").val();
     var ctxB = document.getElementById("barChartgrnd").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: 'No. Of Clicks',
          data:   [jan_click, feb_click, mar_click, apr_click, may_click, jun_click, jul_click, aug_click, sep_click, oct_click, nov_click, dec_click],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  </script>
  <!-- close for groundtruth -->
  <!-- google Bar chart for Display Network -->
  <script type="text/javascript">
     //bar
     var sunday_video_impression=$("#sunday_video_impression").val();
     var monday_video_impression=$("#monday_video_impression").val();
     var tuesday_video_impression=$("#tuesday_video_impression").val();
     var wednesday_video_impression=$("#wednesday_video_impression").val();
     var thrusday_video_impression=$("#thrusday_video_impression").val();
     var friday_video_impression=$("#friday_video_impression").val();
     var saturday_video_impresison=$("#saturday_video_impresison").val();
     var ctxB = document.getElementById("googlebardisplay").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thru", "Fri", "Sat"],
        datasets: [{
          label: 'Video Impression By Week',
          data:   [sunday_video_impression,monday_video_impression,tuesday_video_impression,wednesday_video_impression,thrusday_video_impression,friday_video_impression,saturday_video_impresison],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  </script>
  <!-- Close Google Bar Chart For Display Network -->
  <!-- Open for google display impression by week -->
  <script type="text/javascript">
     //bar
     var sunday_video_impression=$("#display_sun_impression").val();
     var monday_video_impression=$("#display_mon_impression").val();
     var tuesday_video_impression=$("#display_tue_impression").val();
     var wednesday_video_impression=$("#display_wed_impression").val();
     var thrusday_video_impression=$("#display_thru_impression").val();
     var friday_video_impression=$("#display_fri_impression").val();
     var saturday_video_impresison=$("#display_sat_impression").val();
     var ctxB = document.getElementById("googlebardisplayImpression").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thru", "Fri", "Sat"],
        datasets: [{
          label: 'Display Impression By Week',
          data:   [sunday_video_impression,monday_video_impression,tuesday_video_impression,wednesday_video_impression,thrusday_video_impression,friday_video_impression,saturday_video_impresison],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  </script>
  <!-- Close for google display impresison by week -->
  <!-- Open For google display impression by device -->
  <script type="text/javascript">
      //pie
      var mobimpression=$("#mobimpression").val();
      var deskimpression=$("#deskimpression").val();
      var tabimpression=$("#tabimpression").val();
      var otherimpression=$("#otherimpression").val();
      var ctxP = document.getElementById("pieChartDisplayViewsbydevice").getContext('2d');
      var myPieChart = new Chart(ctxP, {
       plugins: [ChartDataLabels],
       type: 'pie',
       data: {
        labels: ["Mobile", "Desktop", "Tab", "Other"],
        datasets: [{
          data: [parseInt(mobimpression), parseInt(deskimpression), parseInt(tabimpression), parseInt(otherimpression)],
          backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
          hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
        }]
      },
      options: {
        responsive: true,
        legend: {
          position: 'right',
          labels: {
            padding: 20,
            boxWidth: 10
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              let dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });

              let percentage = (value * 100 / sum).toFixed(2) + "%";
              let percentage1=(value * 100 / sum).toFixed(2);
              if(percentage1>15){
               return percentage;
             }else{
               return null;
             }

             
           },
           color: 'white',
           labels: {
            title: {
              font: {
                size: '12'
              }
            }
          }
        }
      }
    }
  });
</script>
<!-- Close For google display impression by device -->
<!-- Open for facebook device impression -->
<script type="text/javascript">
      //pie
      var desktop_impression=$("#desktop_impression").val();
      var mobile_app_impression=$("#mobile_app_impression").val();
      var mobile_web_impression=$("#mobile_web_impression").val();
      var ctxP = document.getElementById("devicePieChart").getContext('2d');
      var myPieChart = new Chart(ctxP, {
        plugins: [ChartDataLabels],
        type: 'pie',
        data: {
          labels: ["Mobile APP", "Desktop", "Mobile Web"],
          datasets: [{
            data: [parseInt(mobile_app_impression), parseInt(desktop_impression), parseInt(mobile_web_impression)],
            backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
            hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
          }]
        },
        options: {
          responsive: true,
          legend: {
            position: 'right',
            labels: {
              padding: 20,
              boxWidth: 10
            }
          },
          plugins: {
            datalabels: {
              formatter: (value, ctx) => {
                let sum = 0;
                let dataArr = ctx.chart.data.datasets[0].data;
                dataArr.map(data => {
                  sum += data;
                });
                let percentage = (value * 100 / sum).toFixed(2) + "%";
                let percentage1 = (value * 100 / sum).toFixed(2);
                if(percentage1>15){
                  return percentage;
                }else{
                  return null;
                }

              },
              color: 'white',
              labels: {
                title: {
                  font: {
                    size: '12'
                  }
                }
              }
            }
          }
        }
      });
    </script>
    <!-- CLose for facebook device impression -->
    @endpush
    @push('js')
    <script type="text/javascript">
      //pie
      var male=$("#male_impression").val();
      var female=$("#female_impression").val();
      var ctxP = document.getElementById("pieChart").getContext('2d');
      var myPieChart = new Chart(ctxP, {
        plugins: [ChartDataLabels],
        type: 'pie',
        data: {
          labels: ["Male", "Female"],
          datasets: [{
           data: [parseInt(male), parseInt(female)],
           backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
           hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
         }]
       },
       options: {
        responsive: true,
        legend: {
          position: 'right',
          labels: {
            padding: 20,
            boxWidth: 10
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              let dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              let percentage = (value * 100 / sum).toFixed(2) + "%";
              let percentage1 = (value * 100 / sum).toFixed(2);
              if(percentage1>15){
                return percentage;
              }else{
                return null;
              }
              
            },
            color: 'white',
            labels: {
              title: {
                font: {
                  size: '12'
                }
              }
            }
          }
        }
      }
    });
  </script>
  <script type="text/javascript">
      //line
      var male_age1=$("#male_age1").val();
      var male_age2=$("#male_age2").val();
      var male_age3=$("#male_age3").val();
      var male_age4=$("#male_age4").val();
      var male_age5=$("#male_age5").val();
      var male_age6=$("#male_age6").val();
      var male_age7=$("#male_age7").val();
      var female_age1=$("#female_age1").val();
      var female_age2=$("#female_age2").val();
      var female_age3=$("#female_age3").val();
      var female_age4=$("#female_age4").val();
      var female_age5=$("#female_age5").val();
      var female_age6=$("#female_age6").val();
      var female_age7=$("#female_age7").val();
      var ctxL = document.getElementById("lineChart").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: ["13-17", "18-24", "25-34", "35-44", "45-54", "55-64", "65+"],
          datasets: [{
            label: "Male",
            data: [male_age1, male_age2, male_age3, male_age4, male_age5, male_age6, male_age7],
            backgroundColor: [
            'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
            'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2
          },
          {
            label: "Female",
            data: [female_age1, female_age2, female_age3, female_age4, female_age5, female_age6, female_age7],
            backgroundColor: [
            'rgba(0, 137, 132, .2)',
            ],
            borderColor: [
            'rgba(0, 10, 130, .7)',
            ],
            borderWidth: 2
          }
          ]
        },
        options: {
          responsive: true
        }
      });
    </script>

    <!-- For google video impression for time -->
    <script type="text/javascript">
      //line
      var video0=$("#video0").val();var video1=$("#video1").val();var video2=$("#video2").val();var video3=$("#video3").val();var video4=$("#video4").val();var video5=$("#video5").val();var video6=$("#video6").val();var video7=$("#video7").val();var video8=$("#video8").val();var video9=$("#video9").val();var video10=$("#video10").val();var video11=$("#video11").val();var video12=$("#video12").val();var video13=$("#video13").val();var video14=$("#video14").val();var video15=$("#video15").val();var video16=$("#video16").val();var video17=$("#video17").val();var video18=$("#video18").val();var video19=$("#video19").val();var video20=$("#video20").val();var video21=$("#video21").val();var video22=$("#video22").val();var video23=$("#video23").val();

      var ctxL = document.getElementById("linechartvideoimpression").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: ["12AM", "1AM", "2AM", "3AM", "4AM", "5AM", "6AM", "7AM", "8AM", "9AM", "10AM", "11AM", "12PM", "1PM", "2PM", "3PM", "4PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM"],
          datasets: [{
            label: "Video Time Impression",
            data:  [video0, video1, video2, video3, video4, video5, video6, video7, video8, video9, video10, video11, video12, video13, video14, video15, video16, video17, video18, video19, video20, video21, video22, video23],
            backgroundColor: [
            'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
            'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2
          },

          ]
        },
        options: {
          responsive: true
        }
      });
    </script>
    <!--close  For google video impresison for time -->
    <!-- Open for google display time impresison -->
    <script type="text/javascript">
      //line
      var display0=$("#display0").val();var display1=$("#display1").val();var display2=$("#display2").val();var display3=$("#display3").val();var display4=$("#display4").val();var display5=$("#display5").val();var display6=$("#display6").val();var display7=$("#display7").val();var display8=$("#display8").val();var display9=$("#display9").val();var display10=$("#display10").val();var display11=$("#display11").val();var display12=$("#display12").val();var display13=$("#display13").val();var display14=$("#display14").val();var display15=$("#display15").val();var display16=$("#display16").val();var display17=$("#display17").val();var display18=$("#display18").val();var display19=$("#display19").val();var display20=$("#display20").val();var display21=$("#display21").val();var display22=$("#display22").val();var display23=$("#display23").val();

      var ctxL = document.getElementById("linechartdisplayimpression").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: ["12AM", "1AM", "2AM", "3AM", "4AM", "5AM", "6AM", "7AM", "8AM", "9AM", "10AM", "11AM", "12PM", "1PM", "2PM", "3PM", "4PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM"],
          datasets: [{
            label: "Display Time Impression",
            data:   [display0, display1, display2, display3, display4, display5, display6, display7, display8, display9, display10, display11, display12, display13, display14, display15, display16, display17, display18, display19, display20, display21, display22, display23],
            backgroundColor: [
            'rgba(0, 137, 132, .2)',
            ],
            borderColor: [
            'rgba(0, 10, 130, .7)',
            ],
            borderWidth: 2
          },

          ]
        },
        options: {
          responsive: true
        }
      });
    </script>

    <!-- Close for google display time impression -->



    <script type="text/javascript">
         //pie
         var fb_feed_impression=$("#fb_feed_impression").val();
         var fb_instant_article_impression=$("#fb_instant_article_impression").val();
         var fb_marketplace_impression=$("#fb_marketplace_impression").val();
         var fb_video_feeds_impression=$("#fb_video_feeds_impression").val();
         var insta_stories_impression=$("#insta_stories_impression").val();
         var insta_feed_impression=$("#insta_feed_impression").val();
         var ctxP = document.getElementById("pieChart1").getContext('2d');
         var myPieChart = new Chart(ctxP, {
           plugins: [ChartDataLabels],
           type: 'pie',
           data: {
            labels: ["Facebook | Feed", "Facebook | Instant_Article","Facebook | Marketplace","Facebook | Video_Feeds","Instagram | Instagram_Stories","Instagram | Feed"],
            datasets: [{
              data: [parseInt(fb_feed_impression), parseInt(fb_instant_article_impression),parseInt(fb_marketplace_impression),parseInt(fb_video_feeds_impression),parseInt(insta_stories_impression),parseInt(insta_feed_impression)],
              backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
              hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
            }]
          },
          options: {
            responsive: true,
            legend: {
              position: 'right',
              labels: {
                padding: 20,
                boxWidth: 10
              }
            },
            plugins: {
              datalabels: {
                formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                    sum += data;
                  });
                  let percentage = (value * 100 / sum).toFixed(2) + "%";
                  let percentage1 = (value * 100 / sum).toFixed(2);
                  if(percentage1>15){
                   return percentage;
                 }else{
                   return null;
                 }
                 
               },
               color: 'white',
               labels: {
                title: {
                  font: {
                    size: '12'
                  }
                }
              }
            }
          }
        }
      });
    </script>
    <!-- impression by device for google -->
    <script type="text/javascript">
         //pie
         var mobcost=$("#mobcost").val();var mobclick=$("#mobclick").val();
         var mobimpression=$("#mobimpression").val();var deskcost=$("#deskcost").val();
         var deskclick=$("#deskclick").val();var deskimpression=$("#deskimpression").val();
         var tabcost=$("#tabcost").val();var tabclick=$("#tabclick").val();
         var tabimpression=$("#tabimpression").val();
         var ctxP = document.getElementById("pieChartGoogleDevice").getContext('2d');
         var myPieChart = new Chart(ctxP, {
          type: 'pie',
          data: {
            labels: ["Mobile", "Desktop","Tablet"],
            datasets: [{
              data: [mobclick, deskclick, tabclick],
              backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
              hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
            }]
          },
          options: {
            responsive: true,
            tooltips:{
            bodyFontSize:12 //default
          }
        }
      });
    </script>
    <!--  close Impression by device for google-->
    <!-- Open google video views by day of week -->
    <script type="text/javascript">
         //pie
         var sunday_video_view=$("#sunday_video_view").val();var monday_video_view=$("#monday_video_view").val();
         var tuesday_video_view=$("#tuesday_video_view").val();var wednesday_video_view=$("#wednesday_video_view").val();
         var thrusday_video_view=$("#thrusday_video_view").val();
         var friday_video_view=$("#friday_video_view").val();var saturday_video_view=$("#saturday_video_view").val();
         var ctxP = document.getElementById("pieChartVideoViews").getContext('2d');
         var myPieChart = new Chart(ctxP, {
          plugins: [ChartDataLabels],
          type: 'pie',
          data: {
            labels: ["Sun", "Mon","Tue", "Wed", "Thru", "Fri", "Sat"],
            datasets: [{
              data: [parseInt(sunday_video_view), parseInt(monday_video_view), parseInt(tuesday_video_view), parseInt(wednesday_video_view), parseInt(thrusday_video_view), parseInt(friday_video_view), parseInt(saturday_video_view)],
              backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
              hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
            }]
          },
          options: {
            responsive: true,
            legend: {
              position: 'right',
              labels: {
                padding: 20,
                boxWidth: 10
              }
            },
            plugins: {
              datalabels: {
                formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                    sum += data;
                  });
                  let percentage = (value * 100 / sum).toFixed(2) + "%";
                  let percentage1 = (value * 100 / sum).toFixed(2);
                  if(percentage1>12){
                    return percentage;
                  }else{
                    return null;
                  }
                  
                },
                color: 'white',
                labels: {
                  title: {
                    font: {
                      size: '12'
                    }
                  }
                }
              }
            }
          }
        });
      </script>
      <!-- Close google video views by day of week -->
      <!-- Open google video views by device -->
      <script type="text/javascript">
         //pie
         var video_mobile_views=$("#video_mobile_views").val();var video_tab_views=$("#video_tab_views").val();
         var video_desktop_views=$("#video_desktop_views").val();var video_other_views=$("#video_other_views").val();
         var ctxP = document.getElementById("pieChartVideoViewsByDevice").getContext('2d');
         var myPieChart = new Chart(ctxP, {
           plugins: [ChartDataLabels],
           type: 'pie',
           data: {
            labels: ["Mobile", "Tab", "Desktop", "Other"],
            datasets: [{
              data: [parseInt(video_mobile_views), parseInt(video_tab_views), parseInt(video_desktop_views), parseInt(video_other_views)],
              backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
              hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
            }]
          },
          options: {
            responsive: true,
            legend: {
              position: 'right',
              labels: {
                padding: 20,
                boxWidth: 10
              }
            },
            plugins: {
              datalabels: {
                formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                    sum += data;
                  });
                  //console.log(value);
                  if(value>0){
                    let percentage = (value * 100 / sum).toFixed(2) + "%";
                    //console.log(percentage);
                    return percentage;
                  }else{
                    return null;
                  }

                },
                color: 'white',
                labels: {
                  title: {
                    font: {
                      size: '12'
                    }
                  }
                }
              }
            }
          }
        });
      </script>
      <!-- Close google video views by device -->
      <!-- Open For google video campaign statistics -->
      <script type="text/javascript">
         //pie
         var video_25=$("#video_25").val();var video_50=$("#video_50").val();
         var video_75=$("#video_75").val();var video_100=$("#video_100").val();
         var ctxP = document.getElementById("pieChartVideoViewsState").getContext('2d');
         var myPieChart = new Chart(ctxP, {
          plugins: [ChartDataLabels],
          type: 'pie',
          data: {
            labels: ["Views Upto 25%", "Views Upto 50%", "Views Upto 75%", "Views Upto 100%"],
            datasets: [{
              data: [parseInt(video_25), parseInt(video_50), parseInt(video_75), parseInt(video_100)],
              backgroundColor: ["#5f6ea2", "#d89bbe","#39a2c5","#6fdeb9","#aab0db","#31bde6"],
              hoverBackgroundColor: ["#4b5781", "#cd7eab","#2e829e","#44d5a4","#949bd1","#19a2cc"]
            }]
          },
          options: {
            responsive: true,
            legend: {
              position: 'right',
              labels: {
                padding: 20,
                boxWidth: 10
              }
            },
            plugins: {
              datalabels: {
                formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                    sum += data;
                  });
                  let percentage = (value * 100 / sum).toFixed(2) + "%";
                  let percentage1 = (value * 100 / sum).toFixed(2);
                  if(percentage1){
                   return percentage;
                 }else{
                  return null;
                }

              },
              color: 'white',
              labels: {
                title: {
                  font: {
                    size: '12'
                  }
                }
              }
            }
          }
        }
      });
    </script>
    <!-- Close for google video campaign statistics -->
    <!-- Open display google impression by gender-->
    <script type="text/javascript">
      //pie
      var maleimpression=$("#maleimpression").val();
      var femaleimpression=$("#femaleimpression").val();
      var unknownimpression=$("#unknownimpression").val();
      var ctxP = document.getElementById("googledisplayImpressionbyGender").getContext('2d');
      var myPieChart = new Chart(ctxP, {
       plugins: [ChartDataLabels],
       type: 'pie',
       data: {
        labels: ["Male", "Female", "Unknown"],
        datasets: [{
          data: [parseInt(maleimpression), parseInt(femaleimpression), parseInt(unknownimpression)],
          backgroundColor: ["#F7464A", "#46BFBD", "#39a2c5"],
          hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#2e829e"]
        }]
      },
      options: {
        responsive: true,
        legend: {
          position: 'right',
          labels: {
            padding: 10,
            boxWidth: 10
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              let dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              let percentage = (value * 100 / sum).toFixed(2) + "%";
              let percentage1 = (value * 100 / sum).toFixed(2);
              if(percentage1>15){
                return percentage;
              }else{
                return null;
              }
              
            },
            color: 'white',
            labels: {
              title: {
                font: {
                  size: '12'
                }
              }
            }
          }
        }
      }
    });
  </script>
  <!-- Close display google impression by gender -->
  <!-- Open for google display clicks by gender -->
  <script type="text/javascript">
      //pie
      var maleclick=$("#maleclick").val();
      var femaleclick=$("#femaleclick").val();
      var unknownclick=$("#unknownclick").val();
      var ctxP = document.getElementById("pieChartDisplayClickbyGender").getContext('2d');
      var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
          labels: ["Male", "Female", "Unknown"],
          datasets: [{
            data: [maleclick, femaleclick, unknownclick],
            backgroundColor: ["#F7464A", "#46BFBD", "#39a2c5"],
            hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#2e829e"]
          }]
        },
        options: {
          responsive: true
        }
      });
    </script>
    <!-- Close for google display clicks by gender -->
    <!-- Open for google country -->
    <script type="text/javascript">
     var usclick=$("#usclick").val();
     var ukclick=$("#ukclick").val();
     var russiaclick=$("#russiaclick").val();
     var otherclick=$("#otherclick").val();
     var uaeclick=$("#uaeclick").val();
     var chinaclick=$("#chinaclick").val();
     new Chart(document.getElementById("googleCountrywise"), {
      "type": "horizontalBar",
      "data": {
        "labels": ["US", "UK", "RUSSIA", "UAE", "CHINA","OTHERS"],
        "datasets": [{
          "label": "Countries Clicks",
          "data": [usclick, ukclick, russiaclick, uaeclick, chinaclick,otherclick],
          "fill": false,
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
          ],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
          ],
          "borderWidth": 1
        }]
      },
      "options": {
        "scales": {
          "xAxes": [{
            "ticks": {
              "beginAtZero": true
            }
          }]
        }
      }
    });
  </script>
  <!-- Close for Google Country -->
  <script type="text/javascript">
    var us=$("#us_impression").val();
    var uk=$("#uk_impression").val();
    new Chart(document.getElementById("horizontalBar"), {
      "type": "horizontalBar",
      "data": {
        "labels": ["US", "UK"],
        "datasets": [{
          "label": "Countries Impression",
          "data": [us, uk],
          "fill": false,
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
          ],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
          ],
          "borderWidth": 1
        }]
      },
      "options": {
        "scales": {
          "xAxes": [{
            "ticks": {
              "beginAtZero": true
            }
          }]
        }
      }
    });
  </script>
  <!-- for groundtruth -->
  <script type="text/javascript">
    var us=$("#us_impression1").val();
    var uk=$("#uk_impression1").val();
    new Chart(document.getElementById("horizontalBar1"), {
      "type": "horizontalBar",
      "data": {
        "labels": ["US", "UK"],
        "datasets": [{
          "label": "Countries Impression",
          "data": [us, uk],
          "fill": false,
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
          ],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
          ],
          "borderWidth": 1
        }]
      },
      "options": {
        "scales": {
          "xAxes": [{
            "ticks": {
              "beginAtZero": true
            }
          }]
        }
      }
    });
  </script>
  <!-- close for groundtruth -->
  <!-- for grndtruth all keyword graph -->
  <script type="text/javascript">
      //polar
      var total_impression=$("#total_impression1").val(); var total_ctr=$("#total_ctr1").val();
      var total_clicks=$("#total_clicks1").val();var total_reach=$("#total_reach1").val();
      var total_video_start=$("#total_video_start1").val();
      var ctxPA = document.getElementById("polarChartgrnd").getContext('2d');
      var myPolarChart = new Chart(ctxPA, {
        type: 'polarArea',
        data: {
          labels: ["Videostart", "Ctr", "Click"],
          datasets: [{
            data: [total_video_start, total_ctr, total_clicks],
            backgroundColor: ["rgba(219, 0, 0, 0.1)", "rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)",
            "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)"
            ],
            hoverBackgroundColor: ["rgba(219, 0, 0, 0.2)", "rgba(0, 165, 2, 0.2)",
            "rgba(255, 195, 15, 0.3)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)"
            ]
          }]
        },
        options: {
          responsive: true
        }
      });
    </script>
    <!-- Close grndtruth keyword graph -->
    <!-- Open for week of the day impression for groundtruth -->
    <script type="text/javascript">
     //bar
     var sunday_impression=$("#sun1").val();
     var monday_impression=$("#m1").val();
     var tuesday_impression=$("#t1").val();
     var wednesday_impression=$("#w1").val();
     var thrusday_impression=$("#th1").val();
     var friday_impression=$("#f1").val();
     var saturday_impresison=$("#sat1").val();
     var ctxB = document.getElementById("BarChartForImpression").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thru", "Fri", "Sat"],
        datasets: [{
          label: 'Impression By Week',
          data:   [sunday_impression,monday_impression,tuesday_impression,wednesday_impression,thrusday_impression,friday_impression,saturday_impresison],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  </script>
  <!-- Close for week of the day impression for groundtruth -->
  <!-- CityWise grndtruth Chart -->
  <!-- CityWise grndtruth Chart -->
  @endpush
  @push('js')
  <script type="text/javascript">
    var coral_imp=$("#coral_imp").val();var davie_imp=$("#davie_imp").val();
    var key_largo_imp=$("#key_largo_imp").val();var key_west_imp=$("#key_west_imp").val();
    var delray_imp=$("#delray_imp").val();
    var ctxP = document.getElementById("pieChartgrnd").getContext('2d');
    var myPieChart = new Chart(ctxP, {
      type: 'pie',
      data: {
        labels: ["Coral Springs", "Delray Beach", "Key Largo", "Key West","Davie"],
        datasets: [{
          data: [coral_imp, delray_imp, key_largo_imp, key_west_imp, davie_imp],
          backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
          hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
        }]
      },
      options: {
        responsive: true
      }
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#dtBasicExample').DataTable();
      buttons: [
      'copy', 'excel', 'pdf'
      ]
      $('.dataTables_length').addClass('bs-select');
    });
  </script>
  <!-- <script type="text/javascript">
    function initMap() {
      var map;
      var latlng = new google.maps.LatLng(26.300531, -80.282524);
      var mapOptions = {
        center: latlng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var el=document.getElementById("map_canvas");
      map = new google.maps.Map(el, mapOptions);
      var marker = new google.maps.Marker({
        map: map,
        position: latlng
      });
      var sunCircle = {
        strokeColor: "#c3fc49",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#c3fc49",
        fillOpacity: 0.35,
        map: map,
        center: latlng,
        radius: 7000 // in meters
      };
      cityCircle = new google.maps.Circle(sunCircle);
      cityCircle.bindTo('center', marker, 'position');
    }
  </script> -->
  <script>
    var map;
    function initMap() {
      var lt1=$("#lat1").val();
      var lg1=$("#lng1").val();
      var lt2=$("#lat2").val();
      var lg2=$("#lng2").val();
      var lt3=$("#lat3").val();
      var lg3=$("#lng3").val();

      var lt4=$("#lat4").val();
      var lg4=$("#lng4").val();
      var lt5=$("#lat5").val();
      var lg5=$("#lng5").val();


      var lat=parseFloat(lt1);
      var lng=parseFloat(lg1);
      var lat1=parseFloat(lt2);
      var lng1=parseFloat(lg2);
      var lat2=parseFloat(lt3);
      var lng2=parseFloat(lg3);

      var lat3=parseFloat(lt4);
      var lng3=parseFloat(lg4);

      var lat4=parseFloat(lt5);
      var lng4=parseFloat(lg5);
     // console.log(lat2);
     // console.log(lng2);
     map = new google.maps.Map(
      document.getElementById('map_canvas'),
      {center: new google.maps.LatLng(lat,lng), zoom: 11});

     var iconBase =
              // 'http://localhost/laraveladmin/resources/assets/img/images.png';
              'https://developers.google.com/maps/documentation/javascript/examples/full/images/';
              var sunCircle = {
                strokeColor: "#c3fc49",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#c3fc49",
                fillOpacity: 0.35,
                  radius: 7000 // in meters
                };

                var icons = {
                  parking: {
                    icon: iconBase 
                  },
                  library: {
                    icon: iconBase
                  },
                  info: {
                    icon: sunCircle
                  }
                };

                var features = [
                {
                  position: new google.maps.LatLng(lat, lng),
                  type: 'info'
                }, {
                  position: new google.maps.LatLng(lat1,lng1),
                  type: 'info'
                }, {
                  position: new google.maps.LatLng(lat2,lng2),
                  type: 'info'
                }, {
                  position: new google.maps.LatLng(lat3,lng3),
                  type: 'info'
                }, {
                  position: new google.maps.LatLng(lat4,lng4),
                  type: 'info'
                }
                ];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
          var marker = new google.maps.Marker({
            position: features[i].position,
            icon: icons[features[i].type].icon,
            map: map
          });
        };
      }
    </script>
    <script type="text/javascript">
      var ctxP = document.getElementById("testing").getContext('2d');
      var myPieChart = new Chart(ctxP, {
        plugins: [ChartDataLabels],
        type: 'pie',
        data: {
          labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
          datasets: [{
            data: [210, 130, 120, 160, 120],
            backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
            hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
          }]
        },
        options: {
          responsive: true,
          legend: {
            position: 'right',
            labels: {
              padding: 20,
              boxWidth: 10
            }
          },
          plugins: {
            datalabels: {
              formatter: (value, ctx) => {
                let sum = 0;
                let dataArr = ctx.chart.data.datasets[0].data;
                dataArr.map(data => {
                  sum += data;
                });
                let percentage = (value * 100 / sum).toFixed(2) + "%";
                console.log(percentage);
                return percentage;
              },
              color: 'white',
              labels: {
                title: {
                  font: {
                    size: '12'
                  }
                }
              }
            }
          }
        }
      });
    </script>
    <script type="text/javascript">
      function check_function(value){
      // console.log(value);
      var checkBox = document.getElementById("checkbox");
        // If the checkbox is checked, display the output text
        if (checkBox.checked == true){
          var camp_id='all';
          var type='2';
          $("#stattext").html('30 Days');
        }else {
         var camp_id='all';
         var type='1';
         $("#stattext").html('Lifetime');
       }
       $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
       $.ajax({
        url:"{{ url('usercampaignsbydate') }}",
        type:'POST',
        data:{'id':value,type:type,camp_id:camp_id},
        beforeSend:function(data){
          $("#circle").show();
        },
        success:function(data){
          $("#circle").hide();
         // console.log(data);
         var impression= JSON.parse(data);
         // data for facebook
         var fb_clicks=impression.facebook.total_clicks;
         var fb_impression=impression.facebook.total_impression;
         var fb_ctr=impression.facebook.total_ctr;
         var fb_reach=impression.facebook.total_reach;
         // data for google
         var google_clicks=impression.google.total_clicks;
         var google_impression=impression.google.total_impression;
         var google_ctr=impression.google.total_ctr;
         var google_video_impression=impression.google.video_impression;
         var google_video_views=impression.google.video_views;
         var google_video_rate=impression.google.video_rate;
         // data for groundtruth
         var grnd_clicks=impression.groundtruth.total_clicks;
         var grnd_impression=impression.groundtruth.total_impression;
         var grnd_ctr=impression.groundtruth.total_ctr;
         var grnd_reach=impression.groundtruth.total_reach;
         // for total data
         var total_ctr=fb_ctr + google_ctr + grnd_ctr;
         var total_impression=fb_impression + google_impression + grnd_impression;
         var total_clicks=fb_clicks + google_clicks + grnd_clicks;
         var total_reach=fb_reach + grnd_reach;
         var total_video_impression=google_video_impression;
         var total_video_views=google_video_views;
         var total_video_rate=google_video_rate;

         $("#fbimpression").html(fb_impression);$("#fbreach").html(fb_reach);$("#fbctr").html(fb_ctr);$("#fbclicks").html(fb_clicks);
         $("#googleimpression").html(google_impression);$("#googlevideoimp").html(google_video_impression);$("#googlevideoviews").html(google_video_views);$("#googlevideorate").html(google_video_rate);$("#googlectr").html(google_ctr);$("#googleclick").html(google_clicks);$("#grndimp").html(grnd_impression);$("#grndreach").html(grnd_reach);$("#grndctr").html(grnd_ctr);$("#grndclick").html(grnd_clicks);
         $("#totalimp").html(total_impression);$("#totalreach").html(total_reach);$("#videoimp").html(total_video_impression);$("#videoviews").html(total_video_views);$("#videorate").html(total_video_rate);$("#totalctr").html(total_ctr);$("#totalclick").html(total_clicks);

       }
     });
     }
   </script>
   <script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDJMIkgBgtv54G_H-EHM7muG_qW7pdz28&callback=initMap">
 </script>
 <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
 </script>
 @endpush