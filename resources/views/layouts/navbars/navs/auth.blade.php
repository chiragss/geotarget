<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
  <div class="container-fluid">
    <div class="navbar-wrapper">

      <div class="navbar-brand" style="font-size: 1.4rem; line-height:40px;">{{ $titlePage }}&nbsp;&nbsp;&nbsp;&nbsp; 
        <span style="color: #8e24aa;"><?php if(!empty($breadcumbs)){echo $breadcumbs;}else{}?></span>
        
       <div> @if(Auth::guard('web')->check() && Auth::guard('user')->user())
      <p style="font-size:14px; line-height:20px; margin:0; padding:0">{{ Auth::guard('user')->user()->business_name }}&nbsp;-&nbsp;
        <?php $uname=explode(',', Auth::guard('user')->user()->username);
        if(!empty($uname[0]) && !empty($uname[1])) {
         $name=$uname[1].' '.$uname[0];
       }else{
        $name=$uname[0];
        
      }
      ?>
      {{ $name }}&nbsp;-&nbsp;{{ Auth::guard('user')->user()->customer_id }}
      
    </p>
     @endif
    <p style="font-size:14px; line-height:20px; margin:0; padding:0; color:#942bad ">
          @if(!empty($google_start_date[0]))
          {{ 'Start Date:-' }} {{ $google_start_date[0]->camp_start_date }}
          @endif
          @if(!empty($google_end_date[0]))
          {{ 'End Date:-' }} {{ $google_end_date[0]->camp_end_date }}
          @endif

        </p>
   </div> 
        
      </div>



     

  </div>

  <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
    <span class="sr-only">Toggle navigation</span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-end">
    <form class="navbar-form">
      <div class="input-group no-border">
        <input type="text" value="" class="form-control" placeholder="Search...">
         <!--  <button type="submit" class="btn btn-white btn-round btn-just-icon">
            <i class="material-icons">search</i>
            <div class="ripple-container"></div>
          </button> -->
        </div>
      </form>
      <ul class="navbar-nav">
       @if(Auth::guard('user')->check())
       <li class="nav-item">
        <a class="nav-link" href="{{ url('userhomedashboard').'/'.Auth::guard('user')->user()->customer_id }}">
          <i class="material-icons">dashboard</i>
          <p class="d-lg-none d-md-block">
            {{ ('Home') }}
          </p>
        </a>
      </li>
      @else
      <li class="nav-item">
        <a class="nav-link" href="{{ url('admindashboard') }}">
          <i class="material-icons">dashboard</i>
          <p class="d-lg-none d-md-block">
            {{ ('Home') }}
          </p>
        </a>
      </li>
      @endif
      @if(Auth::guard('user')->check())
      {{-- <li class="nav-item dropdown">
        <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">notifications</i>
          <span class="notification">5</span>
          <p class="d-lg-none d-md-block">
            {{ ('Some Actions') }}
          </p>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">{{ ('Mike John responded to your email') }}</a>
          <a class="dropdown-item" href="#">{{ ('You have 5 new tasks') }}</a>
          <a class="dropdown-item" href="#">{{ ('You\'re now friend with Andrew') }}</a>
          <a class="dropdown-item" href="#">{{ ('Another Notification') }}</a>
          <a class="dropdown-item" href="#">{{ ('Another One') }}</a>
        </div>
      </li>--}}
      @endif
      <li class="nav-item dropdown">
        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">person</i>
          <p class="d-lg-none d-md-block">
            {{ ('Account') }}
          </p>
        </a>
        @if(Auth::guard('web')->check() && Auth::guard('user')->check() || Auth::guard('web')->check())
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
          @if(Auth::guard('web')->check() && Auth::guard('user')->check())
          <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ ('Profile') }}</a>
          @endif
          {{-- <a class="dropdown-item" href="#">{{ ('Settings') }}</a>--}}
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ url('adminlogout') }}">{{ ('Log out') }}</a>
        </div>
        @endif
        @if(Auth::guard('user')->check() && empty(Auth::guard('web')->check() || Auth::guard('rep_user')->check()))
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
          <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ ('Profile') }}</a>
          {{-- <a class="dropdown-item" href="#">{{ ('Settings') }}</a>--}}
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ url('userlogout') }}">{{ ('Log out') }}</a>
        </div>
        @endif
         @if(Auth::guard('user')->check() && Auth::guard('rep_user')->check())
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
          {{--<a class="dropdown-item" href="{{ route('profile.edit') }}">{{ ('Profile') }}</a>--}}
          {{-- <a class="dropdown-item" href="#">{{ ('Settings') }}</a>--}}
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ url('repuserlogout') }}">{{ ('Log out') }}</a>
        </div>
        @endif
      </li>
    </ul>
  </div>
</div>
</nav>
<br>