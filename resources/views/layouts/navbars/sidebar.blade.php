<div class="sidebar" data-color="orange" data-background-color="black" data-image="{{ asset('resources/assets') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
      Tip 2: you can also add an image using data-image tag
    -->
    <style type="text/css">
      a.disabled {
        pointer-events: none;
        cursor: default;
      }
    </style>
    <div class="logo">
      <a href="" class="simple-text logo-normal">
        <img src="{{ asset('resources/assets') }}/img/logo.png">
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
         
        @if(Auth::guard('web')->check() && empty(Auth::guard('user')->check()))
        <li class="nav-item{{ $activePage == 'admindashboard' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('admindashboard') }}">
            <i class="material-icons">dashboard</i>
            <p>{{ ('Admin Dashboard') }}</p>
          </a>
        </li>
        @endif
        @if(Auth::guard('web')->check() && Auth::guard('user')->check())
        <li class="nav-item{{ $activePage == 'admindashboard' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('admindashboard') }}">
            <i class="material-icons">dashboard</i>
            <p>{{ ('Admin Dashboard') }}</p>
          </a>
        </li>
        @endif
         @if(Auth::guard('rep_user')->check())
        <li class="nav-item{{ $activePage == 'admindashboard' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('repadmindashboard') }}">
            <i class="material-icons">dashboard</i>
            <p>{{ ('Admin Dashboard') }}</p>
          </a>
        </li>
        @endif
        @if(Auth::guard('web')->check() && empty(Auth::guard('user')->check()))
        <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('users').'/'.'1' }}">
            <i class="material-icons">perm_identity</i>
            <p>{{ ('Customers') }}</p>
          </a>
        </li>
        @endif
          @if(Auth::guard('rep_user')->check())
        <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('repusers').'/'.'1' }}">
            <i class="material-icons">perm_identity</i>
            <p>{{ ('Customers') }}</p>
          </a>
        </li>
        @endif
        @if(Auth::guard('web')->check() && Auth::guard('user')->check())
        <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('users').'/'.'1' }}">
            <i class="material-icons">perm_identity</i>
            <p>{{ ('Customers') }}</p>
          </a>
        </li>
        @endif
        @if(Auth::guard('user')->check())
        <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
          <a class="nav-link" href="{{ url('userhomedashboard').'/'.Auth::guard('user')->user()->customer_id }}">
            <i class="material-icons">dashboard</i>
            <p>{{ ('User Dashboard') }}</p>
          </a>
        </li>
        @endif

    <!--   @if(!Auth::guard('user')->check())
      <li class="nav-item{{ $activePage == 'campaign' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('allcampaigns') }}">
          <i class="material-icons">perm_identity</i>
          <p>{{ ('Campaigns') }}</p>
        </a>
      </li>
      @endif -->
      @if(Auth::guard('user')->check())
      @if(Auth::guard('user')->check() && Auth::guard('web')->check() || Auth::guard('rep_user')->check())
      <li class="nav-item{{ $activePage == 'campaign' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('usercampaigns').'/'.Auth::guard('user')->user()->customer_id }}">
          <i class="material-icons">view_list</i>
          <p>{{ ('Campaigns') }}</p>
        </a>
      </li>
      @endif
      <li class="nav-item {{ ($activePage == 'creative' || $activePage == 'video') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i class="material-icons"><i class="fas fa-images"></i></i>
          <p>{{ ('Creatives') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse hide" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'creative' ? ' active' : '' }}">
              <a class="nav-link" href="{{ url('ad_creatives') }}">
                <span class="sidebar-mini">CR </span>
                <span class="sidebar-normal">{{ ('Creatives') }} </span>
              </a>
            </li>
            
            <li class="nav-item{{ $activePage == '' ? ' active' : '' }}">
             @if(!empty(Auth::guard('user')->user()->landing_page))
             <a class="nav-link" href="{{strtolower(Auth::guard('user')->user()->landing_page)}}" target="_blank">
              @else
              <a class="nav-link" href="#">
                @endif
                <span class="sidebar-mini">LP </span>
                <span class="sidebar-normal"> {{ ('Landing Page') }} </span>
              </a>
            </li>
            


            {{--$file = Auth::guard('user')->user()->landing_page."/video/vid.mp4";
            $f= file_exists($file);--}}
            
            <li class="nav-item{{ $activePage == 'video' ? ' active' : '' }}">
              <a class="nav-link" href="{{ url('video') }}">
                <span class="sidebar-mini">VA </span>
                <span class="sidebar-normal"> {{ ('Video Ads') }} </span>
              </a>
            </li>

            
            {{--$url='https://geotargetus.net/videos/'.Auth::guard('user')->user()->customer_id.'/vid.mp4';--}}


            

          </ul>
        </div>
      </li>
    
    

      @if(Auth::guard('user')->check() && Auth::guard('web')->check() || Auth::guard('rep_user')->check())
      <li class="nav-item{{ $activePage == 'note' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('notes') }}">
          <i class="material-icons"><i class="far fa-sticky-note"></i></i>
          <p>{{ ('Notes') }}</p>
        </a>
      </li>
      @endif


    @if(Auth::guard('user')->check() && !Auth::guard('web')->check() || !Auth::guard('rep_user')->check())
    <li class="nav-item{{ $activePage == 'interaction' ? ' active' : '' }}">
      <a class="nav-link" href="{{ url('interaction') }}">
        <i class="material-icons"><i class="fas fa-ellipsis-v"></i></i>
        <p>{{ ('Interaction') }}</p>
      </a>
    </li>
    <li class="nav-item{{ $activePage == 'reports' ? ' active' : '' }}">
      <a class="nav-link" href="{{ url('reports') }}">
        <i class="material-icons"><i class="fas fa-list"></i></i>
        <p>{{ ('Report') }}</p>
      </a>
    </li>
    @endif

    <li class="nav-item{{ $activePage == 'contact' ? ' active' : '' }}">
      <a class="nav-link" href="{{ url('contactus') }}">
        <i class="material-icons">contact_mail</i>
        <p>{{ ('Contact us') }}</p>
      </a>
    </li>

    @endif

    <!-- sidebar for admin  -->


    <!-- close for admin sidebar -->

  </ul>
</div>
</div>