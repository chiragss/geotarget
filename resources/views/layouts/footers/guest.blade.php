<footer class="footer">
    <div class="container">
        <nav class="float-left">
            <ul>

                <li>
                  <a href="https://geotargetus.net/about-us/" target="_blank">
                      {{ ('About Us') }}
                  </a>
              </li>
              <li>
                  <a href="https://geotargetus.net/privacy-policy/" target="_blank">
                      {{ ('Privacy Policy') }}
                  </a>
              </li>
              <li>
                  <a href="https://geotargetus.net/services/" target="_blank">
                      {{ ('Services') }}
                  </a>
              </li>
          </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script> <a href="https://geotargetus.net/" target="_blank" style="font-weight:600;color:#ff9800; font-size: 19px;">Geotarget</a> <!-- by
        <a href="https://shopperlocal.com/" target="_blank" style="font-weight:500;color:#fcd21d;">Shopperlocal</a> -->
    </div>
</div>
</footer>