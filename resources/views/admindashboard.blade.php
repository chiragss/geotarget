@extends('layouts.app', ['activePage' => 'admindashboard', 'titlePage' => ('Admin Dashboard')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<!-- MDBootstrap Datatables  -->
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css" rel="stylesheet">
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css" rel="stylesheet">
<!-- DataTables Select JS -->
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<div class="content">
  <div class="container-fluid">
    <div class="row">
      @if(Auth::guard('web')->check())
      <div class="col-md-4">   

        <a href="{{ url('users').'/'.'1' }}"><div class="card text-white bg-primary mb-3">
          <div class="card-header">{{'Total Customers'}}&nbspActive&nbsp{{ $count_active_rep }}&nbspKilled&nbsp{{ $total_customer-$count_active_rep }}</div>
          <div class="card-body">
           <!-- <h2 class="card-title">25000</h2> -->
           <h2 class="card-title">{{ $total_customer }}</h2>
           {{--<p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $total_customer-$count_active_rep }}</p>--}}
         </div>
       </div></a>
       <a href="{{ url('users').'/'.'2' }}"><div class="card text-white bg-info mb-3">
        <div class="card-header">{{'Total Customers Launched'}}</div>
        <div class="card-body">
         <!-- <h2 class="card-title">25000</h2> -->
         <h2 class="card-title">{{ count($cust_launch) }}</h2>
         {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
       </div>
     </div></a>
     <a href="{{ url('users').'/'.'3' }}"><div class="card text-white bg-warning mb-3">
      <div class="card-header">{{'Customers Launched in Last 7 Days'}}</div>
      <div class="card-body">
       <!-- <h2 class="card-title">25000</h2> -->
       <h2 class="card-title">{{ count($last_seven_day_launch) }}</h2>
       {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
     </div>
   </div></a>

   <a href="{{ url('users').'/'.'4' }}"><div class="card text-white bg-success mb-3">
    <div class="card-header">{{'Report Sent in Last 30 Days'}}</div>
    <div class="card-body">
     <!-- <h2 class="card-title">25000</h2> -->
     <h2 class="card-title">{{ count($last_thirty_day_report_sent) }}</h2>
     {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
   </div>
 </div></a>

</div>
@endif
 @if(Auth::guard('rep_user')->check())
      <div class="col-md-4">   

        <a href="{{ url('repusers').'/'.'1' }}"><div class="card text-white bg-primary mb-3">
          <div class="card-header">{{'Total Customers'}}&nbspActive&nbsp{{ $count_active_rep }}&nbspKilled&nbsp{{ $total_customer-$count_active_rep }}</div>
          <div class="card-body">
           <!-- <h2 class="card-title">25000</h2> -->
           <h2 class="card-title">{{ $total_customer }}</h2>
           {{--<p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $total_customer-$count_active_rep }}</p>--}}
         </div>
       </div></a>
       <a href="{{ url('repusers').'/'.'2' }}"><div class="card text-white bg-info mb-3">
        <div class="card-header">{{'Total Customers Launched'}}</div>
        <div class="card-body">
         <!-- <h2 class="card-title">25000</h2> -->
         <h2 class="card-title">{{ count($cust_launch) }}</h2>
         {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
       </div>
     </div></a>
     <a href="{{ url('repusers').'/'.'3' }}"><div class="card text-white bg-warning mb-3">
      <div class="card-header">{{'Customers Launched in Last 7 Days'}}</div>
      <div class="card-body">
       <!-- <h2 class="card-title">25000</h2> -->
       <h2 class="card-title">{{ count($last_seven_day_launch) }}</h2>
       {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
     </div>
   </div></a>

   <a href="{{ url('repusers').'/'.'4' }}"><div class="card text-white bg-success mb-3">
    <div class="card-header">{{'Report Sent in Last 30 Days'}}</div>
    <div class="card-body">
     <!-- <h2 class="card-title">25000</h2> -->
     <h2 class="card-title">{{ count($last_thirty_day_report_sent) }}</h2>
     {{-- <p class="card-text text-white">Active Customers:-{{ $count_active_rep }}&nbspKilled Customers:-{{ $count-$count_active_rep }}</p>--}}
   </div>
 </div></a>

</div>
@endif

<div class="col-md-8">

  <div class="card">
    <div class="card-header card-header-primary card-header-icon">
      <div class="card-icon">
        <i class="material-icons">perm_identity</i>
      </div>
      <h4 class="card-title">Total Customers by Rep</h4>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table">
          <thead class=" text-primary">
            <th>Rep Name</th>
            <th>
              Rep Id
            </th>
            <th>
              Total Customers
            </th>
            <th>Active Customers</th>
            <th>Killed Customers</th>
            <th>Camp Launched</th>
          </thead>
          <tbody>

            @if(!empty($final))
            @foreach($final as $data)
            @php
            $repp=0;
            @endphp
            <tr>
              <td>{{ $data['rep_name'] }}</td>
              <td>
               {{ $data['rep_id'] }}
             </td>
             <td>
              {{ $data['count'] }}
            </td>
            <td>
              {{$data['active']}}
            </td>
            <td>{{ $data['killed'] }}</td>
            @if(!empty($launch_reps))
            @foreach($launch_reps as $rep)
            @if($data['rep_id']==$rep->rep_id)
            @php
            $repp=$rep->rep_count;
            @endphp 
            @endif
            @endforeach
            @endif
            <td>{{ $repp }}</td>
          </tr>

          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>

{{--<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="material-icons">perm_identity</i>
        </div>
        <h4 class="card-title">Total Customers by Rep</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Rep Name</th>
              <th>
                Rep Id
              </th>
              <th>
                Total Customers
              </th>
              <th>Active Customers</th>
              <th>Killed Customers</th>
            </thead>
            <tbody>

              @if(!empty($final))
              @foreach($final as $data)
              <tr>
                <td>{{ $data['rep_name'] }}</td>
                <td>
                 {{ $data['rep_id'] }}
               </td>
               <td>
                {{ $data['count'] }}
              </td>
              <td class="text-primary">
                {{$data['active']}}
              </td>
              <td>{{ $data['killed'] }}</td>
            </tr>

            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>--}}



{{--<div class="row">
  <div class="col-md-6">

    <div class="card card-chart">
      <div class="card-header card-header-success" data-header-animation="true">
        <div class="ct-chart" id="dailySalesChart"></div>
      </div>
      <div class="card-body">
        <div class="card-actions">
          <button type="button" class="btn btn-danger btn-link fix-broken-card">
            <i class="material-icons">build</i> Fix Header!
          </button>
          <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
            <i class="material-icons">refresh</i>
          </button>
          <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
            <i class="material-icons">edit</i>
          </button>
        </div>
        <h4 class="card-title">Daily Sales</h4>
        <p class="card-category">
          <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">access_time</i> updated 4 minutes ago
          </div>
        </div>
      </div>
      
    </div>
  </div>--}}
  <div class="row">


  </div>
  @if(!empty($user_impression))
  <div class="row">
   <div class="col-md-12">

    <div class="card card-chart">
      <div class="card-header card-header-success" style="background: white;">
       <canvas id="lowestimpression"></canvas>
     </div>
     <div class="card-body">
       <h4 class="text-danger">Last 30 Days Record-Not Acheived Impression</h4>
        <!-- <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       {{--<div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>--}}
     </div>
   </div>
 </div>
 @endif
 <div class="row">
   <div class="col-md-12">
    <div class="card card">
     <div class="card-header card-header-primary card-header-icon">
      <div class="card-icon">
        <i class="material-icons">perm_identity</i>
      </div>
      <h4 class="card-title">Last 30 Days Record-Not Acheived Impression Customers List</h4>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <th>Customer ID</th>
            <th>Bussiness Name</th>
            <th>Targeted Impression</th>
            <th>Achieved Impression</th>
            <th>Entry Date</th>
            {{--<th>Report Sent Date</th>--}}
            <th>Action</th>
          </thead>
          <tbody>
            @if(!empty($user_impression))
            @foreach($user_impression as $data)
            <tr>
              <td>{{ $data['customer_id'] }}</td>
              <td>{{ $data['bussiness_name'] }}</td>
              <td>{{ $data['target_impression'] }}</td>
              <td>{{ $data['achieved_impression'] }}</td>
              <td>{{ $data['entry_date'] }}</td>
              {{--<td>{{ $data['report_sent_date'] }}</td>--}}
              <td><a href="{{ url('usersession').'/'.$data['customer_id'] }}"><button class="btn btn-purple btn-round btn-fab"><i class="material-icons">dashboard</i></button></a></td>
            </tr>
            @endforeach
            @endif
          </tbody> 
        </table>
      </div>
    </div>
  </div>
</div>
</div>



@if(!empty($user_impression))
<?php $i=1; ?>
@foreach($user_impression as $impression)
<input type="hidden" name="lower[]" value="{{ $impression['achieved_impression'] }}">
<input type="hidden" name="cust_id[]" value="{{ $impression['customer_id'] }}">
<input type="hidden" name="target[]" value="{{ $impression['target_impression'] }}">
<?php $i++; ?>
@endforeach
@endif
@if(!empty($user_high_impression))
<div class="row">
 <div class="col-md-12">

   <div class="card card-chart">
    <div class="card-header card-header-success" style="background: white;">
     <canvas id="highestimpression"></canvas>
   </div>
   <div class="card-body">
     <h4 class="text-primary">Last 30 Days Record-Achieved Impressions</h4>
        <!-- <p class="card-category">
         <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
       </div>
       {{--<div class="card-footer">
         <div class="stats">
           <i class="material-icons">access_time</i> Updated {{ $day_diff }} ago
         </div>
       </div>--}}
     </div>
   </div>
 </div>
 @endif
 <div class="row">
   <div class="col-md-4">
     <a href="{{ url('camplaunchdownload') }}"><div class="card text-white bg-primary mb-3">
      <div class="card-header" style="font-size: initial;">{{ "Download Campaign Launched Report" }}</div>
      <div class="card-body">
       <!-- <h2 class="card-title">25000</h2> -->
       <h2 class="card-title"></h2>
     </div>
   </div></a>
 </div>

 <div class="col-md-4">
   <a href="{{ url('reminderreportdownload') }}"><div class="card text-white bg-info mb-3">
    <div class="card-header" style="font-size: initial;">{{ "Download Reminder Email Report" }}</div>
    <div class="card-body">
     <!-- <h2 class="card-title">25000</h2> -->
     <h2 class="card-title"></h2>
   </div>
 </div>
</a>
</div>

</div>
@if(!empty($user_high_impression))
<?php $i=1; ?>
@foreach($user_high_impression as $impression)
<input type="hidden" name="higher[]" value="{{ $impression['achieved_impression'] }}">
<input type="hidden" name="higher_cust_id[]" value="{{ $impression['customer_id'] }}">
<input type="hidden" name="higher_target[]" value="{{ $impression['target_impression'] }}">
<?php $i++; ?>
@endforeach
@endif


</div>
</div>
@endsection

@push('js')

<script type="text/javascript">
      //line
      
      var impressions = $("input[name='lower[]']")
      .map(function(){return $(this).val();}).get();
      
      var cust_id=  $("input[name='cust_id[]']")
      .map(function(){return $(this).val();}).get();

      var target=$("input[name='target[]']")
      .map(function(){return $(this).val();}).get();
              // console.log(impressions);
              // console.log(cust_id);
              var ctxL = document.getElementById("lowestimpression").getContext('2d');
              var myLineChart = new Chart(ctxL, {
                type: 'line',
                data: {
                  labels: cust_id,
                  datasets: [{
                    label: "Lowest Achieved Impression",
                    data:  impressions,
                    fill: true,
                    backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                    ],
                    borderColor: [
                    'rgba(200, 99, 132, .7)',
                    ],
                    borderWidth: 2
                  },
                  {
                    label: "Target Impression",
                    data: target,
                    backgroundColor: [
                    'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                    'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                  }
                  ]
                },
                options: {
                  responsive: true
                }
              });
            </script>

            <!-- for higher impressions -->
            <script type="text/javascript">
      //line
      
      var impressions = $("input[name='higher[]']")
      .map(function(){return $(this).val();}).get();
      
      var cust_id=  $("input[name='higher_cust_id[]']")
      .map(function(){return $(this).val();}).get();

      var target=$("input[name='higher_target[]']")
      .map(function(){return $(this).val();}).get();

      var ctxL = document.getElementById("highestimpression").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: cust_id,
          datasets: [
          {
            label: "Target Impression",
            data: target,
            fill: true,
            backgroundColor: [
            'rgba(0, 137, 132, .2)',
            ],
            borderColor: [
            'rgba(0, 10, 130, .7)',
            ],
            borderWidth: 2
          },
          {
            label: "Highest Achieved Impression",
            data:  impressions,
            fill: true,
            backgroundColor: [
            'rgba(12, 245, 0, .2)',
            ],
            borderColor: [
            'rgba(9, 173, 0, .7)',
            ],
            borderWidth: 2
          }

          ]
        },
        options: {
          responsive: true
        }
      });
    </script>
    <script type="text/javascript">

      dataDailySalesChart = {
        labels: ['J', 'F', 'M'],
        series: [
        [10, 34, 17, 5]
        ]
      };
      optionsDailySalesChart = {
        lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
        }),
        low: 0,
        high: 500, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
      }
      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
      md.startAnimationForLineChart(dailySalesChart);
    </script>
    <script>
      $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#dtBasicExample').DataTable({
        aoColumnDefs: [
        {
         bSortable: false,
         aTargets: [ -1 ]
       }
       ]
     });

      $('.dataTables_length').addClass('bs-select');
    });
  </script>
  <style type="text/css">
   a.dropdown-item.waves-effect.waves-light:hover {
    color: #fff;

  }
  .navbar .dropdown-menu a:not(.active) {

    color: #333;
    font-weight: normal;
    text-decoration: none;
    font-size: .8125rem;
  }
</style>
<!-- Close for higher impressions -->
@endpush