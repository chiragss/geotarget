@extends('layouts.app', ['activePage' => 'contact', 'titlePage' => ('Contact Us')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <form method="post" action="{{ url('contactus') }}" autocomplete="off" class="form-horizontal">
          {{ csrf_field() }}
          <div class="card card">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="material-icons">contact_mail</i>
              </div>
              <h4 class="card-title">Nothing would make us happier than helping your business increase exposure in your geographical area. </h4>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-md-12">
                 <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                  </button>
                  <span>{{ session('status') }}</span>
                </div>
              </div>


            </div>
            @endif
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Subject') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="subject" id="input-email" type="text" placeholder="{{ ('Enter Your Subject') }}" value="" required />
                  @if ($errors->has('email'))
                  <span id="email-error" class="error text-danger" for="input-email"></span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Message') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                  <textarea  name="msg" id="input-message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" required=""></textarea>
                </div>
              </div>
            </div>
          </div>
          <!--   <div class="card-footer ml-auto mr-auto"> -->
            <div class="card-footer mr-auto">
              <button type="submit" class="btn btn-purple">{{ ('Submit') }}</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-4">
        <div class="card card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">contact_mail</i>
            </div>
            <h4 class="card-title">Contact Details</h4>
          </div>
          <div class="card-body ">

            <p><b>ADDRESS</b><br>
              Geotarget LLC<br>
              2220 Sedwick Road Durham NC 27713<br>
              Phone: <a href="tel:877-596-6232">877-596-6232</a><br>
              Email: <a href="mailto:Info@geotargetus.com">Info@geotargetus.com</a> <br><br>

              <b>CORPORATE OFFICE</b><br>
            2222 Sedwick Road Durham, NC 27713</p><br>

            <b>MARKETING CONSULTANT</b><br>
            Rep ID:- {{ Auth::guard('user')->user()->rep_id }}<br>
            Rep Name:- {{ Auth::guard('user')->user()->rep_name }}<br>
            Rep Email:- <a href="mailto:{{Auth::guard('user')->user()->salesrep_mail}}">{{ Auth::guard('user')->user()->salesrep_mail }}</a>


          </div>
          <!--   <div class="card-footer ml-auto mr-auto"> -->

          </div>


        </div>
      </div>

    </div>
  </div>
  <style type="text/css">
   a.dropdown-item.waves-effect.waves-light:hover {
    color: #fff;

  }
  .navbar .dropdown-menu a:not(.active) {

    color: #333;
    font-weight: normal;
    text-decoration: none;
    font-size: .8125rem;
  }
</style>
@endsection