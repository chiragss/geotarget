@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'titlePage' => ('Page Expire')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <h3></h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8 ml-auto mr-auto">
      <div style="text-align: center;"><img src="https://geotargetus.net/wp-content/uploads/2019/06/Logo-with-shopperlocal-1.png" alt="Geotarget Logo"/></div>
      <form class="form" method="POST" action="{{ url('userlogin') }}">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       
        </form>
        <div class="row">
          <h3>Your Reset Token has been Expired..Please Contact to site administrator..</h3><a href="https://geotargetus.net/contact-us/"><img src="https://admin.geotargetus.net/resources/assets/img/yellow-btn.png" alt="Contact-Us"></a>
        </div>
        {{--<div class="row">
                          <div class="clearfix"></div>
                          <div class="col-6">
                            <a href="https://geotargetus.net/contact-us/" target="_blank" class="text-light">
                              <small>{{ ('Need Help?') }}</small>
                            </a>
                          </div>
                          <div class="col-6 text-right">
                            <a href="#" class="text-light">
                              <small>{{ ('Forgot Your Password') }}</small>
                            </a>
                          </div>
                        </div>--}}
      </div>
    </div>
  </div>
  @endsection
