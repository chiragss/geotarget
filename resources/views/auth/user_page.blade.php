@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'titlePage' => ('APP Dashboard')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <h3></h3>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <div style="text-align: center;"><img src="https://geotargetus.net/wp-content/uploads/2019/06/Logo-with-shopperlocal-1.png" alt="Geotarget Logo"/></div>
      <form class="form" method="POST" action="{{ url('userloginhome') }}">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <div class="card card-login card-hidden mb-3">
        <div class="card-header card-header-primary text-center">
          <h4 class="card-title"><strong>{{ ('User Login Panel') }}</strong><br/>{{('Geotargetus')}}</h4>
            <!-- <div class="social-line">
              <a href="#pablo" class="btn btn-just-icon btn-link btn-white">
                <i class="fa fa-facebook-square"></i>
              </a>
              <a href="#pablo" class="btn btn-just-icon btn-link btn-white">
                <i class="fa fa-twitter"></i>
              </a>
              <a href="#pablo" class="btn btn-just-icon btn-link btn-white">
                <i class="fa fa-google-plus"></i>
              </a>
            </div> -->
          </div>
          <div class="card-body">
            <p class="card-description"></p>
            @if (session('status'))
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                  </button>
                  <span>{{ session('status') }}</span>
                </div>
              </div>
            </div>
            @endif
            <!-- <p class="card-description text-center">{{ ('Or Sign in with ') }} <strong></strong> {{ (' and the password ') }}<strong>secret</strong> </p> -->
            <div class="bmd-form-group{{ $errors->has('customer_id') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">perm_identity</i>
                  </span>
                </div>
                <input type="text" name="customer_id" class="form-control" placeholder="{{ ('Enter Customer ID..') }}"  required>
              </div>
             <!--  @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
                @endif -->
              </div>
             {{-- <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
                                          <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="material-icons">lock_outline</i>
                                              </span>
                                            </div>
                                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ ('Password...') }}" required>
                                          </div>
                                          @if ($errors->has('password'))
                                          <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                                            <strong>{{ $errors->first('password') }}</strong>
                                          </div>
                                          @endif
                                        </div>--}}
             {{-- <div class="form-check mr-auto ml-3 mt-3">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ ('Remember me') }}
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>--}}
            </div>
            <div class="card-footer justify-content-center">
              <!-- <button type="submit" class="btn btn-primary btn-link btn-lg">{{ ('Lets Go') }}</button> -->
              <button class="btn btn-primary">{{ ('Lets Go') }}<div class="ripple-container"></div></button>
            </div>
          </div>
        </form>
        {{--<div class="row">
                          <div class="clearfix"></div>
                          <div class="col-6">
                            <a href="https://geotargetus.net/contact-us/" target="_blank" class="text-light">
                              <small>{{ ('Need Help?') }}</small>
                            </a>
                          </div>
                          <div class="col-6 text-right">
                            <a href="#" class="text-light">
                              <small>{{ ('Forgot Your Password') }}</small>
                            </a>
                          </div>
                        </div>--}}
      </div>
    </div>
  </div>
  @endsection
