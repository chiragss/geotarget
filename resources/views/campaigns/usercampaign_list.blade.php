@extends('layouts.app', ['activePage' => 'campaign', 'titlePage' => ('Campaigns')])
@section('content')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<div class="content">
  <div class="container">
<!--     <div class="row">
     <div class="col-lg-4 col-md-6 col-sm-6">
       <form class="form" method="POST" action="{{ url('campaign_bussiness_report'.'/'.$id) }}">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <select class="browser-default custom-select" name="bussiness_name" onchange="change(this.value)">
          <option value="{{ 'all' }},{{ $id }}">ALL VERSION CAMPAIGNS</option>
          @if(!empty($fb_camp))
          @foreach($fb_camp as $buss_name)
          <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'facebook'}}">{{'Fb-'}} {{ $buss_name->camp_name }}</option>
          @endforeach
          @endif
          @if(!empty($google_camp))
          @foreach($google_camp as $buss_name)
          <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'google'}}">{{'Google-'}} {{ $buss_name->camp_name }}</option>
          @endforeach
          @endif
          @if(!empty($groundtruth_camp))
          @foreach($groundtruth_camp as $buss_name)
          <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'ground'}}">{{'Groundtruth-'}} {{ $buss_name->camp_name }}</option>
          @endforeach
          @endif
        </select>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <select class="browser-default custom-select" name="dataType" onchange="change_date(this.value)">
          <option value="{{Auth::guard('user')->user()->customer_id }},{{'1'}}">LIFETIME DATA</option>
          <option value="{{Auth::guard('user')->user()->customer_id }},{{'2'}}">LAST 30 DAYS DATA</option>
        </select>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <button type="submit" class="btn btn-primary report">View Demographics</button>
      </div>
    </form>
    <input type="hidden" id="campaign_type" value="{{ 'all' }},{{ $id }}">
    <input type="hidden" id="data_type" value="{{Auth::guard('user')->user()->customer_id }},{{'1'}}">
  </div> -->
  <div class="row">
    <div class="col-md-8">
      <div class="card card">
       <div class="card-header card-header-primary card-header-icon">
         <div class="card-icon">
          <i class="material-icons">view_list</i>
        </div>
        <div class="circle" align="center" id="circle" style="display: none;">
          <img src="{{ asset('resources/assets') }}/img/ring-loader.gif" style="height: 300px;position: absolute !important;
          top: 50%;
          left: 50%;
          border-radius: 5px;
          z-index: 11;
          margin: 0 auto;
          -webkit-transform: translate(-50%);
          -moz-transform: translate(-50%);
          -ms-transform: translate(-50%);
          -o-transform: translate(-50%);
          transform: translate(-50%);">
        </div>
        <h4 class="card-title">Campaigns List</h4>
        <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
      </div>
      <?php 
      $total_spend=0;
      $total_clicks=0;
      $total_impression=0;
      $total_ctr=0;
      $total_reach=0;$fb_video_impression=0;$fb_display_impression=0;
      ?>
      @if(!empty($fbresult))
      @foreach($fbresult as $campaign)
      <?php 
      if(!empty($campaign->keyword)){
       $keyword=json_decode($campaign->keyword);
       $spend=$keyword->spend;
       $clicks=$keyword->clicks;
       $impressions=$keyword->impressions;
       $ctr=$keyword->ctr;
       $reach=$keyword->reach;
       $total_spend=$total_spend + $spend;
       $total_clicks=$total_clicks + $clicks;
       //$total_impression=$total_impression + $impressions;
       $total_ctr=$total_ctr + $ctr;
       $total_reach=$total_reach + $reach;
     }
     if(!empty($campaign->video_campaign)){
      if($campaign->video_campaign!=='null'){
       $fbimpression=json_decode($campaign->video_campaign);
       if(!empty($fbvideoimpression)){
        foreach($fbvideoimpression as $fbvideo){
          $fb_video_impression=$fb_video_impression + $fbvideo->impresison;
        }
      }
    }
  }
  if(!empty($campaign->display_campaign)){
    if($campaign->display_campaign!=='null'){
      $fbdisplayimpression=json_decode($campaign->display_campaign);
      if(!empty($fbdisplayimpression)){
        foreach($fbdisplayimpression as $fbdisplay){
          $fb_display_impression=$fb_display_impression + $fbdisplay->impresison;
        }
      }
    }
  }
  ?>
  @endforeach
  <?php $total_impression= $total_impression +  $fb_video_impression +  $fb_display_impression; ?>
  @endif
  <?php 
  $fb_reach=$total_reach;
  $fb_clicks=$total_clicks;
  $fb_impression=$total_impression;
  $fb_ctr=$total_ctr;
  $fb_spend=$total_spend;
  ?>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table">
        <thead class=" text-primary">
          <th>Network. Name</th>
          <th>IMPR.</th>
          <th>REACH</th>
          <th>SPEND</th>
          <th>VID.IMP.</th>
          <th>VIDEO VIEWS</th>
          <th>VIDEO RATE</th>
          <th>CTR</th>
          <th>BUDGET</th>
          <th>CLICKS</th>
          <th>START DATE</th>
          <th>END DATE</th>

        </thead>
        <tbody>
          <tr>
            <td>{{ 'Social Media' }}</td>
            <td><span id="fbimpression">{{ $fb_impression }}</span></td>
            <td><span id="fbreach">{{ $total_reach }}</span></td>
            <td><span id="fbspend">{{ round($total_spend, 2) }}</span></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td><span id="fbctr">{{ round($total_ctr, 2) }}</span><span>{{ '%' }}</span></td>
            <td>--</td>
            <td><span id="fbclicks">{{ $total_clicks }}</span></td>
            <td>-</td>
            <td>-</td>

          </tr>
          <?php 
          $total_spend=0;
          $total_clicks=0;
          $total_impression=0;
          $total_ctr=0;
          $total_budget=0;$video_mobile_views=0;$video_tab_views=0;$video_desktop_views=0;
          $video_other_views=0;$sunday_video_impression=0;$monday_video_impression=0;$tuesday_video_impression=0;$wednesday_video_impression=0;$thrusday_video_impression=0;$friday_video_impression=0;$saturday_video_impresison=0;$sunday_video_view=0;$monday_video_view=0; $tuesday_video_view=0; $wednesday_video_view=0; $thrusday_video_view=0; $friday_video_view=0; $saturday_video_view=0;$total_video_impression=0;$video_views=0;
          ?>
          @if(!empty($googleresult))
          @foreach($googleresult as $campaign)
          <?php 
          if(!empty($campaign->keyword)){
           $keyword=json_decode($campaign->keyword);
           $spend=$keyword->cost;
           $clicks=$keyword->clicks;
           $impressions=$keyword->impressions;
           $ctr=$keyword->ctr;
           $budget=$keyword->budget;
           $total_clicks= $total_clicks + $clicks;
           $total_impression=$total_impression + $impressions;
           $total_ctr=$total_ctr + $ctr;
           $total_budget=$total_budget + $budget;
           $total_spend=$total_spend + $spend;
         }
         if(!empty($campaign->video_campaign)){
           if($campaign->video_campaign!=='null'){
            $video=json_decode($campaign->video_campaign);
            if(!empty($video->video->sun)){
             $sunday_video_impression=$sunday_video_impression + $video->video->sun->sun_impression;
             $sunday_video_view=$sunday_video_view + $video->video->sun->sun_views;
           }
           if(!empty($video->video->mon)){
            $monday_video_impression=$monday_video_impression + $video->video->mon->mon_impression;
            $monday_video_view=$monday_video_view + $video->video->mon->mon_views;
          }
          if(!empty($video->video->tue)){
           $tuesday_video_impression=$tuesday_video_impression + $video->video->tue->tue_impression;
           $tuesday_video_view=$tuesday_video_view + $video->video->tue->tue_views;
         }
         if(!empty($video->video->wed)){
          $wednesday_video_impression=$wednesday_video_impression + $video->video->wed->wed_impression;
          $wednesday_video_view=$wednesday_video_view + $video->video->wed->wed_views;
        }
        if(!empty($video->video->thru)){
          $thrusday_video_impression=$thrusday_video_impression + $video->video->thru->thru_impression;
          $thrusday_video_view=$thrusday_video_view + $video->video->thru->thru_views;
        }
        if(!empty($video->video->fri)){
          $friday_video_impression=$friday_video_impression + $video->video->fri->fri_impression;
          $friday_video_view=$friday_video_view + $video->video->fri->fri_views;
        }
        if(!empty($video->video->sat)){
          $saturday_video_impresison=$saturday_video_impresison + $video->video->sat->sat_impression;
          $saturday_video_view=$saturday_video_view + $video->video->sat->sat_views;
        }

       
        $video_views=$sunday_video_view + $monday_video_view + $tuesday_video_view + $wednesday_video_view + $thrusday_video_view + $friday_video_view + $saturday_video_view;


        

        
        $total_video_impression=$sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
      }
    }

    ?>
    @endforeach
    @endif
    <?php 
    $google_spend=$total_spend;
    $google_clicks=$total_clicks;
    $google_impression=$total_impression;
    $google_ctr=$total_ctr;
    $google_video_imp=$total_video_impression;
    $google_video_view=$video_views;
    if($video_views>0 && $total_video_impression>0){
     $google_video_rate=round($video_views/$total_video_impression * 100,2);
   }else{
     $google_video_rate=0;
   }

   ?>
   <tr>
    <td>{{ 'Google/Youtube' }}</td>
    <td id="googleimpression">{{ $total_impression }}</td>
    <td>--</td>
    <td id="googlespend">{{ $google_spend }}</td>
    <td id="googlevideoimp">{{ $total_video_impression }}</td>
    <td id="googlevideoviews">{{ $video_views }}</td>
    <td id="googlevideorate">{{ $google_video_rate }}{{ '%' }}</td>
    <td id="googlectr">{{ $total_ctr }}{{ '%' }}</td>
    <td id="googlebudget">{{ $total_budget }}</td>
    <td id="googleclick">{{ $total_clicks }}</td>
    <td>-</td>
    <td>-</td>

  </tr>
  <?php 
  $total_spend=0;
  $total_clicks=0;
  $total_impression=0;
  $total_ctr=0;
  $total_reach=0;
  $total_budget=0; 
  $total_visits=0;
  $total_sar=0;
  ?>
  @if(!empty($grndresult))
  @foreach($grndresult as $campaign1)
  <?php
          // echo '<pre>';
          // var_dump($campaign);die; 
  if(!empty($campaign1->keyword)){
   $keyword=json_decode($campaign1->keyword);
   $spend=$keyword->spend;
   $clicks=$keyword->clicks;
   $impressions=$keyword->impressions;
   $ctr=$keyword->ctr;
   $reach=$keyword->cumulative_reach;
   $total_spend=$total_spend + $spend;
   $total_clicks=$total_clicks + $clicks;
   $total_impression=$total_impression + $impressions;
   $total_ctr=$total_ctr + $ctr;
   $total_reach=$reach;
 }
 ?>
 @endforeach
 @endif
 <?php 
 $grnd_reach=$total_reach;
 $grnd_clicks=$total_clicks;
 $grnd_impression=$total_impression;
 $grnd_ctr=$total_ctr;
 $grnd_spend=$total_spend;
 ?>

 <tr>
  <td>{{ 'Games & Application' }}</td>
  <td><span id="grndimp">{{ $total_impression }}</span></td>
  <td><span id="grndreach">{{ $total_reach }}</span></td>
  <td><span id="grndspend">{{ round($total_spend, 2) }}</span></td>
  <td><span id="">--</span></td>
  <td>0.00</td>
  <td>0.00%</td>
  <td><span id="grndctr">{{ round($total_ctr, 2) }}</span><span>%</span></td>
  <td>-</td> 
  <td><span id="grndclick">{{ $total_clicks }}</span></td>


  <td>-</td>
  <td>-</td>


</tr>
<tr>
  <td><b>{{ 'Total' }}</b></td>
  <td><span id="totalimp">{{ $google_impression + $fb_impression + $grnd_impression }}</span></td>
  <td><span id="totalreach">{{ $fb_reach + $grnd_reach }}</span></td>
  <td><span id="totalspend">{{ round($fb_spend + $grnd_spend + $google_spend, 2) }}</span></td>
  <td><span id="videoimp">{{ $google_video_imp }}</span></td>
  <td><span id="videoviews">{{ $google_video_view }}</span></td>
  <td><span id="videorate">{{ $google_video_rate }}{{ '%' }}</span></td>
  <td><span id="totalctr"><!-- {{ round($google_ctr + $fb_ctr + $grnd_ctr, 2) }} -->--</span></td>
  <td><span id="total_budget">--</span></td>
  <td><span id="totalclick">{{ $google_clicks + $fb_clicks + $grnd_clicks }}</span></td>
  <td>-</td>
  <td>-</td>

</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="col-md-4">
  <form class="form" method="POST" action="{{ url('campaign_bussiness_report'.'/'.$id) }}">
   <div style="margin-top: 30px;">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <select class="browser-default custom-select" name="bussiness_name" onchange="change(this.value)">
      <option value="{{ 'all' }},{{ $id }}">ALL VERSION CAMPAIGNS</option>
      @if(!empty($fb_camp))
      @foreach($fb_camp as $buss_name)
      <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'facebook'}}">{{'Fb-'}} {{ $buss_name->camp_name }}</option>
      @endforeach
      @endif
      @if(!empty($google_camp))
      @foreach($google_camp as $buss_name)
      <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'google'}}">{{'Google-'}} {{ $buss_name->camp_name }}</option>
      @endforeach
      @endif
      @if(!empty($groundtruth_camp))
      @foreach($groundtruth_camp as $buss_name)
      <option value="{{ $buss_name->camp_id }},{{ $buss_name->camp_name }},{{'ground'}}">{{'Groundtruth-'}} {{ $buss_name->camp_name }}</option>
      @endforeach
      @endif
    </select>
  </div>

  <div style="margin-top: 30px;">
    <select class="browser-default custom-select" name="dataType" onchange="change_date(this.value)">
      <option value="{{Auth::guard('user')->user()->customer_id }},{{'1'}}">LIFETIME DATA</option>
      <option value="{{Auth::guard('user')->user()->customer_id }},{{'2'}}">LAST 30 DAYS DATA</option>
    </select>
  </div>

  <div style="margin-top: 30px;">
    <button class="btn btn-primary report" style="width: 100%;" >
      <i class="fa fa-list"></i>&nbsp;&nbsp;&nbsp;View Demographics
      <div class="ripple-container"></div></button> 
    </div>
    
    <input type="hidden" id="campaign_type" value="{{ 'all' }},{{ $id }}">
    <input type="hidden" id="data_type" value="{{Auth::guard('user')->user()->customer_id }},{{'1'}}">
  </form>
</div>

</div>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  function change(value){
    var campaign_type=$("#campaign_type").val(value);
    var data_type=$("#data_type").val();
     // console.log(data_type);
     var dataa=data_type.split(',');
     var data_type=dataa[1];
     var values = value.split(',');
     var value=values[0];
     var name=values[1];
     var account_type=values[2];
     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
     $.ajax({
      url:"{{ url('usercampaignsbyid') }}",
      type:'POST',
      data:{'id':value,name:name,account_type:account_type,data_type:data_type},
      beforeSend:function(data){
        $("#circle").css('display','block');
        $(".card-body").css('opacity','0.4');
      },
      success:function(data){
      // console.log(data);
      $("#circle").css('display','none');
      $(".card-body").css('opacity','1');
      var fb= JSON.parse(data);
         // console.log(fb);
         $("#fbctr").html(fb.facebook.total_ctr);
         $("#fbimpression").html(fb.facebook.total_impression);
         $("#fbclicks").html(fb.facebook.total_clicks);
         $("#fbspend").html(fb.facebook.total_spend);
         $("#fbreach").html(fb.facebook.total_reach);
         $("#grndreach").html(fb.groundtruth.total_reach);
         $("#grndspend").html(fb.groundtruth.total_spend);
         $("#grndclick").html(fb.groundtruth.total_clicks);
         $("#grndimp").html(fb.groundtruth.total_impression);
         $("#grndctr").html(fb.groundtruth.total_ctr);
         $("#googlespend").html(fb.google.total_cost);
         $("#googleclick").html(fb.google.total_clicks);
         $("#googleimpression").html(fb.google.total_impression);
         $("#googlectr").html(fb.google.total_ctr);
         $("#googlebudget").html(fb.google.total_budget);
         $("#googlevideoimp").html(fb.google.video_impression);
         $("#googlevideoviews").html(fb.google.video_views);
         $("#googlevideorate").html(fb.google.video_rate);
         var totalreach = fb.facebook.total_reach + fb.groundtruth.total_reach;
         var totalspend= fb.facebook.total_spend + fb.groundtruth.total_spend;
         var total_budget= fb.google.total_budget;
         var totalclick=fb.facebook.total_clicks + fb.groundtruth.total_clicks + fb.google.total_clicks;
         var totalimp=fb.facebook.total_impression + fb.groundtruth.total_impression +fb.google.total_impression;
         var totalctr=fb.facebook.total_ctr + fb.groundtruth.total_ctr + fb.google.total_ctr;
         var videoimp=fb.google.video_impression;
         var videoviews=fb.google.video_views;
         var videorate=fb.google.video_rate;
         $("#totalreach").html(totalreach);
         $("#totalspend").html(totalspend);
         $("#total_budget").html(total_budget);
         $("#totalclick").html(totalclick);
         $("#totalimp").html(totalimp);
         $("#totalctr").html(totalctr.toFixed(2));
         $("#videoimp").html(videoimp);
         $("#videoviews").html(videoviews);
         $("#videorate").html(videorate);
       }
     });
   }
 </script>
 <!-- data by time -->
 <script type="text/javascript">
  function change_date(value){
    var data_type=$("#data_type").val(value);
    var campaign_type=$("#campaign_type").val();
    var camp=campaign_type.split(',');
    var camp_id=camp[0];
    var account_type=camp[2];
    var values = value.split(',');
    var value=values[0];
    var type=values[1];
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url:"{{ url('usercampaignsbydate') }}",
      type:'POST',
      data:{'id':value,type:type,camp_id:camp_id,account_type:account_type},
      beforeSend:function(data){
        $("#circle").css('display','block');
        $(".card-body").css('opacity','0.4');
      },
      success:function(data){
         // console.log(data);
         $("#circle").css('display','none');
         $(".card-body").css('opacity','1');
         var fb= JSON.parse(data);
          // console.log(fb);
        /*  console.log(fb.google.video_impression);
          console.log(fb.google.video_views);
          console.log(fb.google.video_rate);*/
          $("#fbctr").html(fb.facebook.total_ctr);
          $("#fbimpression").html(fb.facebook.total_impression);
          $("#fbclicks").html(fb.facebook.total_clicks);
          $("#fbspend").html(fb.facebook.total_spend);
          $("#fbreach").html(fb.facebook.total_reach);
          $("#grndreach").html(fb.groundtruth.total_reach);
          $("#grndspend").html(fb.groundtruth.total_spend);
          $("#grndclick").html(fb.groundtruth.total_clicks);
          $("#grndimp").html(fb.groundtruth.total_impression);
          $("#grndctr").html(fb.groundtruth.total_ctr);
          $("#googlespend").html(fb.google.total_cost);
          $("#googleclick").html(fb.google.total_clicks);
          $("#googleimpression").html(fb.google.total_impression);
          $("#googlectr").html(fb.google.total_ctr);
          $("#googlebudget").html(fb.google.total_budget);
          $("#googlevideoimp").html(fb.google.video_impression);
          $("#googlevideoviews").html(fb.google.video_views);
          $("#googlevideorate").html(fb.google.video_rate);
          var totalreach = fb.facebook.total_reach + fb.groundtruth.total_reach;
          var totalspend= fb.facebook.total_spend + fb.groundtruth.total_spend;
          var total_budget= fb.google.total_budget;
          var totalclick=fb.facebook.total_clicks + fb.groundtruth.total_clicks + fb.google.total_clicks;
          var totalimp=fb.facebook.total_impression + fb.groundtruth.total_impression +fb.google.total_impression;
          var totalctr=fb.facebook.total_ctr + fb.groundtruth.total_ctr + fb.google.total_ctr;
          var videoimp=fb.google.video_impression;
          var videoviews=fb.google.video_views;
          var videorate=fb.google.video_rate;
          $("#totalreach").html(totalreach);
          $("#totalspend").html(totalspend);
          $("#total_budget").html(total_budget);
          $("#totalclick").html(totalclick);
          $("#totalimp").html(totalimp);
          $("#totalctr").html(totalctr.toFixed(2));
          $("#videoimp").html(videoimp);
          $("#videoviews").html(videoviews);
          $("#videorate").html(videorate);
        }
      });
  }
</script>
<!-- Close data by time -->
@endpush
<style>
  button.btn.btn-primary.report {
    margin-top: 0px;
  }
</style>