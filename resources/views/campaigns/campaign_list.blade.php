@extends('layouts.app', ['activePage' => 'campaign', 'titlePage' => ('User List')])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> Users List</h4>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
            
          </div>
      
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>Bussiness Name</th>
                  <th>Camp Status</th>
                 
                </thead>
                <tbody>
                 @foreach($campaigns as $data)
              
            
                  <tr>
                    <td>{{ $data->camp_name }}</td>
                    @if($data->camp_status=='ACTIVE')
                    <td>{{ 'ACTIVE' }}</td>
                    @elseif($data->camp_status=='COMPLETED')
                    <td>{{ 'COMPLETED' }}</td>
                    @else
                     <td>{{ 'PAUSED' }}</td>
                    @endif
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection