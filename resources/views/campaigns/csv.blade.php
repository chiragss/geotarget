@extends('layouts.app', ['activePage' => 'user', 'titlePage' => ('User Management')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('csv.upload') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
         {{ csrf_field() }}


         <div class="card ">
          <div class="card-header card-header-primary">
            <h4 class="card-title">{{ ('Upload CSV') }}</h4>
            <p class="card-category"></p>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 text-right">
             
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Upload CSV') }}</label>
              <div class="col-sm-7">
               
                  <input class="form-control" name="file" id="file" type="file" required="true" aria-required="true"/>
                
              </div>
            </div>
            
          <div class="card-footer ml-auto mr-auto">
            <button type="submit" class="btn btn-primary">{{ ('Upload') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection