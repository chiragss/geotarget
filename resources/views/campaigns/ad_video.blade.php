@extends('layouts.app', ['activePage' => 'video', 'titlePage' => ('Video Ads')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<script src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chartjs-datalabels.js"></script>
<!-- <script src="{{ asset('resources/assets') }}/js/core/popper.min.js"></script> -->
<!-- <script src="{{ asset('resources/assets') }}/js/core/bootstrap-material-design.min.js"></script> -->
<!-- <script src="{{ asset('resources/assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chart.js"></script> -->
<script type="text/javascript" src="{{ asset('resources/assets') }}/md/src/js/vendor/free/chartjs-datalabels.js"></script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card">
          <div class="card-header card-header-primary card-header-icon">
           <div class="card-icon">
            <i class="material-icons">video_label</i>
          </div>
          <h4 class="card-title"></h4>
        </div>
        <div class="card-body">
         <div class="col-md-12" style="text-align: center;">
          <video width="100%" controls>
            <?php
            $url='../videos/'.Auth::guard('user')->user()->customer_id.'/vid.mp4';
            $f= file_exists($url);
            $url1='https://geotargetus.net/videos/'.Auth::guard('user')->user()->customer_id.'/vid.mp4';
            ?>
            @if($f==true)
            <source src="{{ $url1 }}" type="video/mp4">
              @else
              <source src="{{ Auth::guard('user')->user()->landing_page }}/video/vid.mp4" type="video/mp4">
                @endif
              </video>

            </div>
           <!--  <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin-top: 10px;" data-a2a-url="https://{{ Auth::guard('user')->user()->landing_page }}/video/vid.mp4" data-a2a-title="">
              <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
              <a class="a2a_button_facebook"></a>
              <a class="a2a_button_twitter"></a>
              <a class="a2a_button_email"></a>
              <a class="a2a_button_linkedin"></a>
              <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
   a.dropdown-item.waves-effect.waves-light:hover {
    color: #fff;

  }
  .navbar .dropdown-menu a:not(.active) {

    color: #333;
    font-weight: normal;
    text-decoration: none;
    font-size: .8125rem;
  }
</style>
@endsection