@extends('layouts.app', ['activePage' => 'reports', 'titlePage' => ('Reports')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<!-- MDBootstrap Datatables  -->
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css" rel="stylesheet">
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css" rel="stylesheet">
<!-- DataTables Select JS -->
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<div class="content">
  <div class="container-fluid">



   <div class="row">
     <div class="col-md-6">
      <div class="card card">
       <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="material-icons">perm_identity</i>
        </div>
        <h4 class="card-title">Campaign Launched List</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>Customer ID</th>
              <th>Bussiness Name</th>
              <th>Launch Date</th>
            </thead>
            <tbody>
              @if(!empty($camp_launch))
              @foreach($camp_launch as $data)
              <tr>
                <td>{{ $data->cust_id }}</td>
                <td>{{ $data->business_name }}</td>
                <td>{{ $data->launch_date }}</td>
              </tr>
              @endforeach
              @endif
            </tbody> 
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card card">
     <div class="card-header card-header-primary card-header-icon">
      <div class="card-icon">
        <i class="material-icons">perm_identity</i>
      </div>
      <h4 class="card-title">Reminder E-mail List</h4>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table id="dtBasicExample1" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <th>Customer ID</th>
            <th>Bussiness Name</th>
            <th>Reminder Date</th>
          </thead>
          <tbody>
            @if(!empty($reminder_mail))
            @foreach($reminder_mail as $data)
            <tr>
              <td>{{ $data->cust_id }}</td>
              <td>{{ $data->business_name }}</td>
              <td>{{ $data->created_at }}</td>
            </tr>
            @endforeach
            @endif
          </tbody> 
        </table>
      </div>
    </div>
  </div>
</div>
</div>






</div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  $(document).ready(function () {
    $('#dtBasicExample').DataTable({
      aoColumnDefs: [
      {
       bSortable: false,
       aTargets: [ -1 ]
     }
     ]
   });

    $('.dataTables_length').addClass('bs-select');
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#dtBasicExample1').DataTable({
      aoColumnDefs: [
      {
       bSortable: false,
       aTargets: [ -1 ]
     }
     ]
   });

    $('.dataTables_length').addClass('bs-select');
  });
</script>



<style type="text/css">
 a.dropdown-item.waves-effect.waves-light:hover {
  color: #fff;

}
.navbar .dropdown-menu a:not(.active) {

  color: #333;
  font-weight: normal;
  text-decoration: none;
  font-size: .8125rem;
}
</style>
<!-- Close for higher impressions -->
@endpush