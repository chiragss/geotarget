@extends('layouts.app', ['activePage' => 'creative', 'titlePage' => ('Creatives')])
@section('content')
<script src="{{ asset('resources/assets') }}/js/core/jquery.min.js"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
  <!------ Include the above in your HEAD tag ---------->
  <div class="content">
    <div class="container-fluid">
     <div class="row">
       <div class="col-lg-12 col-md-12">
         <div class="card">
          <div class="card-header card-header-tabs card-header-primary">
            <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                
                  <li class="nav-item">
                    <a class="nav-link active" href="#facebook" data-toggle="tab">
                      <i class="material-icons">laptop</i> Social Media
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                
                  
                  <li class="nav-item">
                    <a class="nav-link" href="#google" data-toggle="tab">
                      <i class="material-icons">code</i> Google/Youtube
                      <div class="ripple-container"></div>
                    </a>
                  </li>
               
                   @if(!empty($grnd))
                  <li class="nav-item">
                    <a class="nav-link" href="#groundtruth" data-toggle="tab">
                      <i class="material-icons">cloud</i> Games & Application
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  @endif
               
                </ul>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="tab-content">
              <div class="tab-pane active" id="facebook">
               <div class="col-lg-12 col-md-12">
               
                @if(!empty($data))
                
                @foreach($data as $image)
                <?php 
                $a=explode(',', $image->ad_creative_image);
                ?>
                <h4><b>View {{ $image->camp_name }} Creatives</b></h4><br>
                <div class="row" style="margin-top: 10px;">
                  <div class="col-sm-12 col-md-6">
                    @if(!empty($a[0]))
                    <?php echo $a[0];?>
                    @endif
                  </div>
                  <div class="col-sm-12 col-md-6">
                    @if(!empty($a[1]))
                    <?php echo $a[1];?>
                    @endif
                  </div> 
                </div>
                @endforeach
                @else
                <h4>No Creatives for Social Media</h4>

                @endif
              </div>
            </div>
            <div class="tab-pane" id="groundtruth">
              @if(!empty($grnd))
              <h4><b>Games & Application Ad Creative</b></h4>
              @foreach($grnd as $image)
              @if(!empty($image->ad_creative_image))
              <?php 
              $ads=json_decode($image->ad_creative_image);
              echo '<div class="gallery" id="gallery">';

              foreach($ads as $url){ 
                $i=0;
                $b=$url->creative_url;?>
                <div class="mb-3 pics animation all">
                  <img class="img-fluid" src="{{ $b }}" alt="Card image cap">
                </div>
                <?php  $i++; }?>
                
              </div>

              @else

              <h4>No Creatives for Games & Application</h4>

              @endif

              @endforeach
              @else
              <h4>No Creatives Found For Games & Application</h4>
              @endif
            </div>

            <div class="tab-pane" id="google">
              @if(!empty($google))
              
              <h4><b>Google/Youtube Ad Creative</b></h4>
              @foreach($google as $video)

              <div class="row">
                @if(!empty($video->youtube_link))
                <?php 
                $link= json_decode($video->youtube_link);
                ?>
                <div class="col-md-6" style="float: left;display: inline-block; margin-top: 30px;">
                  <h4><b>{{ $link[0]->ad_name }}</b></h4>
                  <a href="{{ $link[0]->video_url }}" target="_blank"><button type="button" class="btn btn-outline-primary waves-effect">Watch On YouTube</button></a>
                  <?php 
                  $makelink=explode('=',$link[0]->video_url);
                  ?>
                  <iframe width="420" height="345" src="https://www.youtube.com/embed/{{ $makelink[1] }}"></iframe>

                  <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin-top: 10px;" data-a2a-url="https://www.youtube.com/embed/{{ $makelink[1] }}" data-a2a-title="">
                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_email"></a>
                    <a class="a2a_button_linkedin"></a>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                  </div>
                </div>
                @endif

              </div>

              @endforeach
              
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
<style type="text/css">
  iframe {
    width: 100%;
  }
  .gallery {
    -webkit-column-count: 3;
    -moz-column-count: 3;
    column-count: 3;
    -webkit-column-width: 33%;
    -moz-column-width: 33%;
    column-width: 33%; }
    .gallery .pics {
      -webkit-transition: all 350ms ease;
      transition: all 350ms ease; }
      .gallery .animation {
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1); }
        @media (max-width: 450px) {
          .gallery {
            -webkit-column-count: 1;
            -moz-column-count: 1;
            column-count: 1;
            -webkit-column-width: 100%;
            -moz-column-width: 100%;
            column-width: 100%;
          }
        }
        @media (max-width: 400px) {
          .btn.filter {
            padding-left: 1.1rem;
            padding-right: 1.1rem;
          }
        }
/*        .thumbnail {margin-bottom:6px;}

.carousel-control.left,.carousel-control.right{
  background-image:none;
  margin-top:10%;
  width:5%;
  }*/
</style>
@push('js')

     <!--  <script type="text/javascript">
        $(function() {
          var selectedClass = "";
          $(".filter").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#gallery").fadeTo(100, 0.1);
            $("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
            setTimeout(function() {
              $("."+selectedClass).fadeIn().addClass('animation');
              $("#gallery").fadeTo(300, 1);
            }, 300);
          });
        });
      </script> -->

      @endpush