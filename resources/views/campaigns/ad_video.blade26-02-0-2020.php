@extends('layouts.app', ['activePage' => 'video', 'titlePage' => ('Ad Video'), 'breadcumbs' => ('Dashboard')])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Video</h4>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
          </div>
          <div class="card-body">
           <div class="col-md-12" style="text-align: center;">
            <video width="100%" controls>
              <source src="https://{{ Auth::guard('user')->user()->landing_page }}/video/vid.mp4" type="video/mp4">

              </video>

            </div>
           <!--  <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin-top: 10px;" data-a2a-url="https://{{ Auth::guard('user')->user()->landing_page }}/video/vid.mp4" data-a2a-title="">
              <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
              <a class="a2a_button_facebook"></a>
              <a class="a2a_button_twitter"></a>
              <a class="a2a_button_email"></a>
              <a class="a2a_button_linkedin"></a>
              <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection