@extends('layouts.app', ['activePage' => 'user', 'titlePage' => ('User Management')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ route('user.store') }}" autocomplete="off" class="form-horizontal">
         {{ csrf_field() }}


         <div class="card ">
          <div class="card-header card-header-primary">
            <h4 class="card-title">{{ ('Add User') }}</h4>
            <p class="card-category"></p>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 text-right">
                <a href="{{ route('user.list') }}" class="btn btn-sm btn-primary">{{ ('Back to list') }}</a>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Customer Id') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('customer_id') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('customer_id') ? ' is-invalid' : '' }}" name="customer_id" id="input-customer_id" type="text" placeholder="{{ ('Customer Id') }}" value="{{ old('customer_id') }}" required="true" aria-required="true"/>
                  @if ($errors->has('customer_id'))
                  <span id="customer_id-error" class="error text-danger" for="input-customer_id">{{ $errors->first('customer_id') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('UserName') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="input-username" type="text" placeholder="{{ ('Username') }}" value="{{ old('Username') }}" required="true" aria-required="true"/>
                  @if ($errors->has('Username'))
                  <span id="Username-error" class="error text-danger" for="input-Username">{{ $errors->first('Username') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Email') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ ('Email') }}" value="{{ old('email') }}" required />
                  @if ($errors->has('email'))
                  <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Phone.') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="input-phone" type="text" placeholder="{{ ('Phone') }}" value="{{ old('phone') }}" required />
                  @if ($errors->has('mob'))
                  <span id="mob-error" class="error text-danger" for="input-mob">{{ $errors->first('mob') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label" for="input-password">{{ (' Password') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" input type="password" name="password" id="input-password" placeholder="{{ ('Password') }}" value="" required />
                  @if ($errors->has('password'))
                  <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('password') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ ('Confirm Password') }}</label>
              <div class="col-sm-7">
                <div class="form-group">
                  <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ ('Confirm Password') }}" value="" required />
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Business Name') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('business_name') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('business_name') ? ' is-invalid' : '' }}" name="business_name" id="input-business_name" type="text" placeholder="{{ ('Business Name') }}" value="{{ old('business_name') }}" required />
                  @if ($errors->has('business_name'))
                  <span id="business_name-error" class="error text-danger" for="input-business_name">{{ $errors->first('business_name') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Start Date') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('start_date') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}" name="start_date" id="input-start_date" type="date" placeholder="{{ ('Start Date') }}" value="{{ old('start_date') }}" required />
                  @if ($errors->has('start_date'))
                  <span id="start_date-error" class="error text-danger" for="input-start_date">{{ $errors->first('start_date') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Groundtruth Ads') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('groundtruth_ads') ? ' has-danger' : '' }}">
                  <select class="form-control" name="groundtruth_ads">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                  
                  
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Google Ads') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('google_ads') ? ' has-danger' : '' }}">
                  <select class="form-control" name="google_ads">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                  
                  
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Facebook Ads') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('facebook_ads') ? ' has-danger' : '' }}">
                  <select class="form-control" name="facebook_ads">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                  
                  
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Website') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('website') ? ' has-danger' : '' }}">
                  <select class="form-control" name="website">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                  
                  
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Landing Page') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('landing_page') ? ' has-danger' : '' }}">
                  <select class="form-control" name="landing_page">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                  
                  
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Keywords') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('keywords') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('keywords') ? ' is-invalid' : '' }}" name="keywords" id="input-keywords" type="text" placeholder="{{ ('keywords') }}" value="{{ old('keywords') }}" required />
                  @if ($errors->has('keywords'))
                  <span id="keywords-error" class="error text-danger" for="input-keywords">{{ $errors->first('keywords') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('RepId') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('rep_id') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('rep_id') ? ' is-invalid' : '' }}" name="rep_id" id="input-rep_id" type="text" placeholder="{{ ('Rep Id') }}" value="{{ old('rep_id') }}" required />
                  @if ($errors->has('rep_id'))
                  <span id="rep_id-error" class="error text-danger" for="input-rep_id">{{ $errors->first('rep_id') }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ml-auto mr-auto">
            <button type="submit" class="btn btn-primary">{{ ('Add User') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection