@extends('layouts.app', ['activePage' => 'interaction', 'titlePage' => ('interaction')])
@section('content')
<script src="{{ asset('resources/assets') }}/js/core/jquery.min.js"></script>
<script src="{{ asset('resources/assets') }}/js/plugins/jquery.dataTables.min.js"></script>
<link href="{{ asset('resources/assets') }}/md/css/mdb.min.css" rel="stylesheet" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
           <div class="card-icon">
            <i class="material-icons">dashboard</i>
          </div>
          <h4 class="card-title">{{ Auth::guard('user')->user()->business_name }} <b>Interactions</b></h4>
        </div>
        <div class="card-body">
          <div class="circle" align="center" id="circle" style="display:none;">
            <img src="{{ asset('resources/assets') }}/img/ring-loader.gif" style="height: 300px;position: absolute !important;
            top: 10%;
            left: 50%;
            border-radius: 5px;
            z-index: 11;
            margin: 0 auto;
            -webkit-transform: translate(-50%);
            -moz-transform: translate(-50%);
            -ms-transform: translate(-50%);
            -o-transform: translate(-50%);
            transform: translate(-50%);">
          </div>

          <?php $fb_clicks=0;$fb_views=0;
          $google_clicks=0;$grnd_impression=0;$fb_video_views=0;$fb_display_views=0;$fb_clicks=0; $mob_views=0;$tab_views=0;$desk_views=0;$other_views=0;$grnd_clicks=0;$grnd_view=0;$google_views=0;

          ?>
          <!-- facebook total impression -->
          @if(!empty($fbresult))
          @foreach($fbresult as $campaign)
          @if(!empty($campaign->video_campaign))
          @if($campaign->video_campaign!=='null')
          <?php
          $fbvideoviews=json_decode($campaign->video_campaign);
          ?>
          @if(!empty($fbvideoviews))
          @foreach($fbvideoviews as $fbvideo)
          <?php
          $fb_video_views=$fb_video_views + $fbvideo->video_played;
          ?>
          @endforeach
          @endif
          @endif
          @endif
          @if(!empty($campaign->display_campaign))
          @if($campaign->display_campaign!=='null')
          <?php
          $fbdisplayviews=json_decode($campaign->display_campaign);
          ?>
          @if(!empty($fbdisplayviews))
          @foreach($fbdisplayviews as $fbdisplay)
          <?php
          $fb_display_views=$fb_display_views + $fbdisplay->video_played;
          ?>
          @endforeach
          @endif
          @endif
          @endif

          @if(!empty($campaign->keyword))
          <?php 
          $fb_click=json_decode($campaign->keyword);
          ?>
          @if(!empty($fb_click))
          <?php 
          $fb_clicks = $fb_clicks + $fb_click->clicks;
          ?>
          @endif
          @endif
          @endforeach
          <?php 
          $fb_views= $fb_video_views +  $fb_display_views;
          ?>
          @endif
          <!-- for google impression -->
          @if(!empty($googleresult))
          @foreach($googleresult as $campaign)
          @if(!empty($campaign->keyword))
          <?php
          $googleimpression=json_decode($campaign->keyword);
          $google_clicks=$google_clicks + $googleimpression->clicks;
          ?>
          @endif
          @if(!empty($campaign->video_campaign))
          @if($campaign->video_campaign!=='null')
          <?php 
          $views=json_decode($campaign->video_campaign);
          ?>
          @if(!empty($views->device))
          <?php 
          $mob_views=$mob_views + $views->device->mobile_views;
          $tab_views=$tab_views + $views->device->tab_views;
          $desk_views=$desk_views + $views->device->desk_views;
          $other_views=$other_views + $views->device->other_views;
          ?>
          @endif
          @endif
          @endif

          @endforeach
          <?php
          $google_views=$mob_views + $tab_views + $desk_views + $other_views;
          ?>
          @endif
          <!-- for groundtruth impression -->
          @if(!empty($grndadminresult))
          @foreach($grndadminresult as $campaign)
          @if(!empty($campaign->keyword))
          <?php

          $grndimpression=json_decode($campaign->keyword);
          $grnd_clicks=$grnd_clicks + $grndimpression->clicks;
          $grnd_view=$grnd_view + $grndimpression->video_start;
          ?>
          @endif
          @endforeach
          @endif
          <?php 
          $total_views=$fb_views + $google_views + $grnd_view;
          $total_clicks=$fb_clicks + $google_clicks + $grnd_clicks;
          ?>
          <input type="hidden" id="fb_views" value="{{ $fb_views }}">
          <input type="hidden" id="google_views" value="{{ $google_views }}">
          <input type="hidden" id="grnd_views" value="{{ $grnd_view }}">

          <input type="hidden" id="fb_clicks" value="{{ $fb_clicks }}">
          <input type="hidden" id="google_clicks" value="{{ $google_clicks }}">
          <input type="hidden" id="grnd_clicks" value="{{ $grnd_clicks }}">

          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card text-white bg-success mb-3">
                <div class="card-header">Video Campaigns</div>
                <div class="card-body">
                  <h2 class="card-title"><span id="views">{{ $total_views }}</span></h2>
                  <p class="card-text text-white"><span id="views_text">Total Views in last 30 days.</span></p>
                </div>
              </div>
              <br><br><br>                 
              <div class="card text-white bg-info mb-3">
                <div class="card-header">Display Campaigns</div>
                <div class="card-body">
                 <!-- <h2 class="card-title">25000</h2> -->
                 <h2 class="card-title"><span id="clicks">{{ $total_clicks }}</span></h2>
                 <p class="card-text text-white"><span id="clicks_text">Total Clicks in last 30 days.</span></p>
               </div>
             </div>
             
           </div>
           <div class="col-lg-6 col-md-6 col-sm-6">
             <div class="col-md-12">

              <h4 style="text-align: center;"><b>{{'Video Campaign Graph'}}</b></h4>
              

               <canvas id="googlebardisplay"></canvas>
               
               <h4 style="text-align: center;"><b>{{'Display Campaign Graph'}}</b></h4>
               <canvas id="monthlyclicks"></canvas>
             

           </div>
         </div>
       </div>

       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="row">
           <input type="checkbox" id="checkbox" class="larger" value="{{Auth::guard('user')->user()->customer_id}}" onclick="check_function(this.value)" style="margin-right: 5px; margin-top: 2px;">For Lifetime Impressions
         </div>

       </div>
     </div>
   </div>
 </div>
</div>
</div>
@endsection
<!-- for groundtruth -->
@push('js')
<script type="text/javascript">
    //bar
    function chart(total_impression){
      var promise_impression=$("#promise_impression").val();
      var ctxB = document.getElementById("barChart").getContext('2d');
      var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
          labels: ["Targeted Imp", "Achieved Imp"],
          datasets: [{
            label: 'Impressions Data',
            data: [promise_impression, total_impression],
            backgroundColor: [
            'rgba(54, 162, 235, 0.2)',
            'rgba(0,255,0,0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(54, 162, 235, 1)',
            'rgba(0,255,0,1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      

    }
    var total_impression=$("#total_impression").val();
    chart(total_impression);
  </script>
  <script type="text/javascript">
    function check_function(value){
      // console.log(value);
      var checkBox = document.getElementById("checkbox");
      var promise_impression=$("#promise_impression").val();
        // If the checkbox is checked, display the output text
        if (checkBox.checked == true){
          var type='1';
          $("#clicks_text").html('Total Clicks in lifetime.');
          $("#views_text").html('Total Views in lifetime.');
        }else {
          var type='2';
          $("#clicks_text").html('Total Clicks in last 30 days.');
          $("#views_text").html('Total Views in last 30 days.');
        }
        // console.log(type);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url:"{{ url('interactionajax') }}",
          type:'POST',
          data:{'type':type},
          beforeSend:function(data){
            $("#circle").show();
          },
          success:function(data){

           $("#circle").hide();
           var info= JSON.parse(data);
           $("#views").html(info.views);
           $("#clicks").html(info.clicks);
           console.log(info.view.fb_views);
           $("#fb_views").val(info.view.fb_views);
           $("#google_views").val(info.view.google_views);
           $("#grnd_views").val(info.view.grnd_views);
           var fb_views=$("#fb_views").val();
           var google_views=$("#google_views").val();
           var grnd_views=$("#grnd_views").val();
           $("#fb_clicks").val(info.click.fb_clicks);
           $("#google_clicks").val(info.click.google_clicks);
           $("#grnd_clicks").val(info.click.grnd_clicks);

           var fb_clicks= $("#fb_clicks").val();
           var google_clicks= $("#google_clicks").val();
           var grnd_clicks=$("#grnd_clicks").val();
          //console.log(fb_views);
          update_chart(fb_views,google_views,grnd_views);

          update_chart1(fb_clicks,google_clicks,grnd_clicks);

        }
      });
      }
    </script>
    <script type="text/javascript">
     //bar
     var fb_views=$("#fb_views").val();
     var google_views=$("#google_views").val();
     var grnd_views=$("#grnd_views").val();

     var ctxB = document.getElementById("googlebardisplay").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Social Media", "Google/Youtube", "Games&App"],
        datasets: [{
          label: 'Video Views',
          data:   [fb_views,google_views,grnd_views],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem) {
              console.log(tooltipItem)
              return tooltipItem.yLabel;
            }
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });


     function update_chart(fb_views,google_views,grnd_views){
      console.log(fb_views);
      var ctxB = document.getElementById("googlebardisplay").getContext('2d');
      var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
          labels: ["Social Media", "Google/Youtube", "Games&App"],
          datasets: [{
            label: 'Video Views',
            data:   [fb_views,google_views,grnd_views],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          legend: {
            display: false
          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem) {
                console.log(tooltipItem)
                return tooltipItem.yLabel;
              }
            }
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      })
    }
  </script>
  <script type="text/javascript">
     //bar
     var fb_clicks=$("#fb_clicks").val();
     var google_clicks=$("#google_clicks").val();
     var grnd_clicks=$("#grnd_clicks").val();

     var ctxB = document.getElementById("monthlyclicks").getContext('2d');
     var myBarChart = new Chart(ctxB, {
      type: 'bar',
      data: {
        labels: ["Social Media", "Google/Youtube", "Games&App"],
        datasets: [{
          label: 'Clicks',
          data:   [fb_clicks,google_clicks,grnd_clicks],
          backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem) {
              console.log(tooltipItem)
              return tooltipItem.yLabel;
            }
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

     function update_chart1(fb_clicks,google_clicks,grnd_clicks){
      var ctxB = document.getElementById("monthlyclicks").getContext('2d');
      var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
          labels: ["Social Media", "Google/Youtube", "Games&App"],
          datasets: [{
            label: 'Clicks',
            data:   [fb_clicks,google_clicks,grnd_clicks],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          legend: {
            display: false
          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem) {
                console.log(tooltipItem)
                return tooltipItem.yLabel;
              }
            }
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    }
  </script>
  <style>
   input.larger {
    width: 16px;
    height: 16px;
  }
  a.dropdown-item.waves-effect.waves-light:hover {
    color: #fff;

  }
  .navbar .dropdown-menu a:not(.active) {

    color: #333;
    font-weight: normal;
    text-decoration: none;
    font-size: .8125rem;
  }
</style>

@endpush