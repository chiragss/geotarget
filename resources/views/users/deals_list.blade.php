@extends('layouts.app', ['activePage' => 'user', 'titlePage' => ('Deals List')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/dataTables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/dataTables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">

<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <div class="card card">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> Deals List</h4>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
          </div>
         <!--  <div class="col-md-12 text-right">
            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ ('Add User') }}</a>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <th>Deal No</th>
                  <th>No Of Impression</th>
                  <th>Entry Date</th>
                  <th>Kill Status</th>
                </thead>
                <tbody>
                  @foreach($data as $user)
                  <?php 
                    // var_dump($user);
                  ?>
                  <tr>
                    <td>{{ $user->deal_no }}</td>
                    <td>{{ $user->no_of_impression }}</td>
                    <td>{{ $user->entry_date }}</td>
                    <td>@if($user->kill_status=='0')
                    {{ 'Active' }}
                    @else
                    {{ 'Kill' }}
                    @endif
                    </td>
                  <!--   <td><a href="http://{{ $user->landing_page }}" target="_blank">{{ $user->landing_page }}</a></td>
                    <td>@if($user->kill_status=='0')
                    {{ 'Active' }}
                    @else
                    {{ 'Kill' }}
                    @endif -->
                  <!-- </td> -->
                  <!-- <td>VIEW</td> -->
                    <!-- <td><a href="{{ url('deal_list').'/'.$user->customer_id }}">VIEW</a></td> -->
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
  });
</script>
@endpush