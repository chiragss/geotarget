@extends('layouts.app', ['activePage' => 'note', 'titlePage' => ('Add Notes')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{ url('addnote') }}" autocomplete="off" class="form-horizontal">
         {{ csrf_field() }}


         <div class="card ">
          <div class="card-header card-header-primary">
            <h4 class="card-title">{{ ('Add Note') }}</h4>
            <p class="card-category"></p>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 text-right">
                <a href="{{ 'notes' }}" class="btn btn-sm btn-primary">{{ ('Back to list') }}</a>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Username') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="input-username" type="text" placeholder="{{ ('Enter username') }}" value="{{ old('username') }}" required="true" aria-required="true"/>
                  @if ($errors->has('username'))
                  <span id="username-error" class="error text-danger" for="input-username">{{ $errors->first('username') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Note') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('note') ? ' has-danger' : '' }}">
                  <input class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" id="input-note" type="text" placeholder="{{ ('Write a Note') }}" value="{{ old('note') }}" required="true" aria-required="true"/>
                  @if ($errors->has('note'))
                  <span id="note-error" class="error text-danger" for="input-note">{{ $errors->first('note') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-2 col-form-label">{{ ('Select Rank') }}</label>
              <div class="col-sm-7">
                <div class="form-group{{ $errors->has('rank') ? ' has-danger' : '' }}">
                  <select class="form-control{{ $errors->has('rank') ? ' is-invalid' : '' }}" name="rank" id="input-rank">
                    <option value="">--Select Rank---</option>
                    <option value="A">Rank A</option>
                    <option value="B">Rank B</option>
                    <option value="C">Rank C</option>
                  </select>
                  @if ($errors->has('rank'))
                  <span id="rank-error" class="error text-danger" for="input-rank">{{ $errors->first('rank') }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ml-auto mr-auto">
            <button type="submit" class="btn btn-primary">{{ ('Add Note') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection