@extends('layouts.app', ['activePage' => 'note', 'titlePage' => ('Notes List')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<!-- MDBootstrap Datatables  -->
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css" rel="stylesheet">
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<link href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css" rel="stylesheet">
<!-- DataTables Select JS -->
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<style type="text/css">
  .btn.btn-primary {
    color: #fff;
    background-color: #9c27b0 !important;
    border-color: #9c27b0 !important;
    box-shadow: 0 2px 2px 0 rgba(156, 39, 176, 0.14), 0 3px 1px -2px rgba(156, 39, 176, 0.2), 0 1px 5px 0 rgba(156, 39, 176, 0.12);
} 
 
.pagination .page-item.active .page-link {
    background-color: #9c27b0 !important;
} 
 

</style>


<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <div class="card card">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Notes List</h4>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
          </div>
          <div class="col-md-12 text-right">
            <a href="{{ url('writenote') }}" class="btn btn-sm btn-primary">{{ ('Add Note') }}</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <th>Note id</th>
                  <th>username</th>
                  <th>Note</th>
                  <th>Created At</th>
                </thead>
                <tbody>
                  @if(!empty($result))
                  @php $i=1; @endphp
                  @foreach($result as $data)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $data->username }}</td>
                    <td>{{ $data->message }}</td>
                    <td>{{ $data->created_at }}</td>
                    @php $i++; @endphp
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
  });
</script>
@endpush