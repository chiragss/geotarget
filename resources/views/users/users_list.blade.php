@extends('layouts.app', ['activePage' => 'user', 'titlePage' => 'Customers List'])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card">
         <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">perm_identity</i>
          </div>
          <h4 class="card-title">Customers List</h4>
        </div>
         <!--  <div class="col-md-12 text-right">
            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ ('Add User') }}</a>
          </div> -->
          <div class="card-body">
            <!-- <div class="col-md-3 pull-right">
             <select class="browser-default custom-select">
              <option selected>Impression Order</option>
              <option value="1">Low to High Imp.</option>
              <option value="2">High to Low Imp.</option>
            </select>
          </div> -->
          <input type="hidden" id="type" value="{{ $id }}">
          <div class="table-responsive">
            <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <th>Id</th>
                <th>Customer Name</th>
                <th>Business Name</th>
                <th>Entry Date</th>
                <th>Bussiness Type</th>
                <th>Customer Address</th>
                <th>Zip Code</th>
                <th>Rep Name</th>
                <!-- <th>Monthly Impression</th> -->
                <th>Action</th>
              </thead>
              <tbody>
              </tbody> 
            </table>
          </div>

          @foreach($data1 as $user)
          <div id="myModal<?php echo $user->customer_id;?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                 <h4 class="modal-title">{{ $user->username }}</h4>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 
               </div>
               <div class="modal-body">

                <h5><b>{{ 'Address' }}</b>&nbsp; {{ $user->customer_address }},{{ $user->zip_code }}</h5>
                <p><b>{{ 'Business Type' }}</b>&nbsp; {{ $user->bussiness_type }}</p>
                <div class="table-responsive">
                  <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                     <th>CustomerId</th>
                     <th>DealNo</th>
                     <th>NoOfImpression</th>
                     <th>EntryDate</th>
                     <th>CampLaunchedDate</th>
                     <th>ReportSentDate</th>
                     <th>StartDate</th>
                     <th>StopDate</th>
                     <th>Status</th>
                   </thead>
                   <tbody>
                    <?php $data=App\Http\Controllers\HomeController::dealList($user->customer_id); ?>
                    @foreach($data as $user)
                    <tr>
                     <td>{{ $user->customer_id }}</td>
                     <td>{{ $user->deal_no }}</td>
                     <td>{{ $user->no_of_impression }}</td>
                     <td>{{ $user->entry_date }}</td>
                     <td>{{ $user->artwork_lauch_date }}</td>
                     <td>{{ $user->report_sent_date }}</td>
                     <td>{{ $user->start_date }}</td>
                     <td>{{ $user->stop_date }}</td>
                     <td>@if($user->kill_status=='0')
                      <p class="text-danger">Killed</p>
                      @else
                      <p class="text-success">Active</p>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-sm" data-dismiss="modal" style="background: #8e24aa;"><span style="color:aliceblue;">Close</span></button>
          </div>
        </div>
      </div>
    </div>

    @endforeach
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  $(document).ready(function () {
    // $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
  });
</script>
<script>
  $(document).ready(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var id=$("#type").val();
    
    $('#dtBasicExample').DataTable({
     processing: true,
     serverSide: true,
     "pageLength": 10,
     ajax: '{{ url('userslist').'/' }}' + id,

     "aoColumnDefs": [
     {"bSearchable": true, "bVisible": true,"orderable": true, "aTargets": [0]},
     {"bSearchable": true, "bVisible": true,"orderable": true, "aTargets": [1]},
     {"bSearchable": true, "bVisible": true,"orderable": true,"aTargets": [2]},
     {"bSearchable": true, "bVisible": true,"orderable": true, "aTargets": [3]},
     {"bSearchable": true, "bVisible": false,"orderable": true, "aTargets": [4]},
     {"bSearchable": true, "bVisible": false,"orderable": true, "aTargets": [5]},
     {"bSearchable": true, "bVisible": false,"orderable": true, "aTargets": [6]},
     {"bSearchable": true, "bVisible": true,"orderable": true, "aTargets": [7]},
     {"bSearchable": false, "bVisible": true,"orderable": false, "aTargets": [8]},
     ],
      // order: [ [4, 'desc'] ],

      columns: [
      { data: 'customer_id',name: 'customers_list.customer_id' },
      { data: 'username',name: 'customers_list.username' },
      { data: 'business_name',name: 'customers_list.business_name' },
      { data: 'entry_date',name: 'customers_list.entry_date' },
      {data: 'bussiness_type',name:'customers_list.bussiness_type'},
      {data: 'customer_address',name:'customers_list.customer_address'},
      {data: 'zip_code',name:'customers_list.zip_code'},
      {data: 'rep_name',name:'customers_list.rep_name'},
     // { data: 'impression',name: 'impression' },
     {data: 'action',name: 'action'},
     ]

   });
  });
  function confirm_click()
  {
    var x = confirm("Are you sure?");
    if (x)
      return true;
    else
      return false;
  }
</script>
<style>
  button.btn.btn-warning.btn-sm {
    background: #ff9800 !important;
  }
/*.pagination .page-item.active .page-link {
    background-color: #a53fb8 !important;
    }*/
    a.dropdown-item.waves-effect.waves-light:hover {
      color: #fff;

    }
    .navbar .dropdown-menu a:not(.active) {

      color: #333;
      font-weight: normal;
      text-decoration: none;
      font-size: .8125rem;
    }
  </style>
  @endpush