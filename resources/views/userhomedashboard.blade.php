@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => ('User Dashboard')])
@section('content')
<script src="{{ asset('resources/assets') }}/js/core/jquery.min.js"></script>
<script src="{{ asset('resources/assets') }}/js/plugins/jquery.dataTables.min.js"></script>
<link href="{{ asset('resources/assets') }}/md/css/mdb.min.css" rel="stylesheet" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
           <div class="card-icon">
            <i class="material-icons">dashboard</i>
          </div>
          <h4 class="card-title">{{ Auth::guard('user')->user()->business_name }} Impressions</h4>
        </div>
        <div class="card-body">
          <div class="circle" align="center" id="circle" style="display:none;">
            <img src="{{ asset('resources/assets') }}/img/ring-loader.gif" style="height: 300px;position: absolute !important;
            top: 10%;
            left: 50%;
            border-radius: 5px;
            z-index: 11;
            margin: 0 auto;
            -webkit-transform: translate(-50%);
            -moz-transform: translate(-50%);
            -ms-transform: translate(-50%);
            -o-transform: translate(-50%);
            transform: translate(-50%);">
          </div>

          <?php $fb_impression=0;$google_impression=0;$grnd_impression=0;$fb_video_impression=0;$fb_display_impression=0;$basis_impression=0;
          $promise_impression=$pro_impression[0]->promise_impression;
          if(empty($promise_impression)){
            $promise_impression=0;
          }
          ?>
          <!-- facebook total impression -->
          @if(!empty($fbresult))
          @foreach($fbresult as $campaign)

          @if(!empty($campaign->video_campaign))
          @if($campaign->video_campaign!=='null')
          <?php
          $fbvideoimpression=json_decode($campaign->video_campaign);
          ?>
          @if(!empty($fbvideoimpression))
          @foreach($fbvideoimpression as $fbvideo)
          <?php
          $fb_video_impression=$fb_video_impression + $fbvideo->impresison;
          ?>
          @endforeach
          @endif
          @endif
          @endif
          @if(!empty($campaign->display_campaign))
          @if($campaign->display_campaign!=='null')
          <?php
          $fbdisplayimpression=json_decode($campaign->display_campaign);
          ?>
          @if(!empty($fbdisplayimpression))
          @foreach($fbdisplayimpression as $fbdisplay)
          <?php
          $fb_display_impression=$fb_display_impression + $fbdisplay->impresison;
          ?>
          @endforeach
          @endif
          @endif
          @endif
          @endforeach
          <?php
          
          $fb_impression= $fb_impression +  $fb_video_impression +  $fb_display_impression;
          ?>
          @endif
          <!-- for google impression -->
          @if(!empty($googleresult))
          @foreach($googleresult as $campaign)
          @if(!empty($campaign->keyword))
          <?php
          $googleimpression=json_decode($campaign->keyword);
          $google_impression=$google_impression + $googleimpression->impressions;
          ?>
          @endif
          @endforeach
          @endif
          <!-- for groundtruth impression -->
          @if(!empty($grndadminresult))
          @foreach($grndadminresult as $campaign)
          @if(!empty($campaign->keyword))
          <?php
          $grndimpression=json_decode($campaign->keyword);
          $grnd_impression=$grnd_impression + $grndimpression->impressions;
          ?>
          @endif
          @endforeach
          @endif
          <!-- for basis campaign -->
          @if(!empty($basisresult))
          @foreach($basisresult as $campaign)
          @if(!empty($campaign->keyword))
          <?php
          $basisimpression=json_decode($campaign->keyword);
          $basis_impression=$basis_impression + $basisimpression->delivered_impressions;
          ?>
          @endif
          @endforeach
          @endif

          <?php 
          $total_impression=$fb_impression + $google_impression + $grnd_impression + $basis_impression;
          if(empty($promise_impression)){
            $compareper=0.00;
          }else{
           $compareper=$total_impression/$promise_impression *100;
         }

         ?>
         <input type="hidden" id="promise_impression" value="{{ $promise_impression }}">
         <input type="hidden" id="total_impression" value="{{ $total_impression }}">
         <input type="hidden" id="compareper" value="{{ $compareper }}">
         <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">                 
            <div class="card text-white bg-info mb-3">
              <div class="card-header">Target Impressions/Month</div>
              <div class="card-body">
               <!-- <h2 class="card-title">25000</h2> -->
               <h2 class="card-title">{{ $promise_impression }}</h2>
               <p class="card-text text-white">Promised Impressions Goals in a Month.</p>
             </div>
           </div>
           <div class="card text-white bg-success mb-3">
            <div class="card-header">Achieved Impressions</div>
            <div class="card-body">
              <h2 class="card-title"><span id="achieved_impression">{{ $total_impression }}</span></h2>
              <p class="card-text text-white">Achieved impressions in last 30 days ads, click on below checkbox for lifetime report.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="card card-chart">
            <div class="card-header card-header-success" id="imp_chart" style="background: white;">
              <canvas id="barChart"></canvas>
            </div>
            <div class="card-body">
              <h4 class="card-title">Impressions</h4>
              <p class="card-category">
                <span class="text-success"><i class="arrow_right_alt"></i> <span id="view_rate">{{round($compareper,2)}}</span>{{'%'}} </span> Achieved in Impressions till now.</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <!-- <i class="material-icons">access_time</i> Updated ago -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          @if(!empty($date[0]->launch_date))
          <?php
          $date2=$date[0]->launch_date;
          $date=date('Y-m-d');
          $new_date=date('Y-m-d', strtotime($date2. ' +28 days'));
          ?>
          @if($new_date<=$date)
          <div class="row">
           <!-- Default unchecked -->
           <!--   <input type="checkbox" id="checkbox" value="{{Auth::guard('user')->user()->customer_id}}" onclick="check_function(this.value)"> -->
           <input type="checkbox" id="checkbox" class="larger" value="{{Auth::guard('user')->user()->customer_id}}" onclick="check_function(this.value)" style="
           margin-right: 5px; margin-top: 2px;
           ">For Lifetime Impressions
         </div>
         @endif
         @endif
         <div class="row">
          <form method="POST" action="{{  url('userhome'.'/'.$id) }}">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type="hidden" name="camp_type" value="0" id="camp_type">
           @if(!Auth::guard('web')->check() && Auth::guard('user')->user())
           @if(!empty($report_show))
           
           <input type="submit" name="submit" class="btn btn-purple" style="color:white; margin:0px; margin-top:5px;" value="View Report">
           
           
           @else
           <p style="font-size: 22px;">Reports Will be Generated After 30 Days of Launched Email</p>
           @endif
           @endif
           @if(Auth::guard('web')->check())
           {{-- <a href="{{ url('userhome'.'/'.$id) }}"><button type="button" class="btn btn-secondary" style="color:white; margin:0px; margin-top:5px;">View Report</button></a>--}}
           <input type="submit" name="submit" class="btn btn-purple" style="color:white; margin:0px; margin-top:5px;" value="View Report">
           @endif
         </form>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
@endsection
<!-- for groundtruth -->
@push('js')
<script type="text/javascript">
    //bar
    function chart(total_impression){
      var promise_impression=$("#promise_impression").val();
      var ctxB = document.getElementById("barChart").getContext('2d');
      var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
          labels: ["Targeted Imp", "Achieved Imp"],
          datasets: [{
            label: 'Impressions Data',
            data: [promise_impression, total_impression],
            backgroundColor: [
            'rgba(54, 162, 235, 0.2)',
            'rgba(0,255,0,0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(54, 162, 235, 1)',
            'rgba(0,255,0,1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      

    }
    var total_impression=$("#total_impression").val();
    chart(total_impression);
  </script>
  <script type="text/javascript">
    function check_function(value){
      // console.log(value);
      var checkBox = document.getElementById("checkbox");
      var promise_impression=$("#promise_impression").val();
        // If the checkbox is checked, display the output text
        if (checkBox.checked == true){
          $("#camp_type").val(1);
          var camp_id='all';
          var type='1';
        }else {
          $("#camp_type").val(0);
          var camp_id='all';
          var type='2';
        }
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url:"{{ url('usercampaignsbydate') }}",
          type:'POST',
          data:{'id':value,type:type,camp_id:camp_id},
          beforeSend:function(data){
            $("#circle").show();
          },
          success:function(data){
            $("#circle").hide();
            var impression= JSON.parse(data);
            var grndturthimp=impression.groundtruth.total_impression;
            var googleimp=impression.google.total_impression;
            var fbimp=impression.facebook.total_impression;
            var basis=impression.basis.impression;
            var total=grndturthimp + googleimp + fbimp + basis;

            $("#total_impression").val(total);
            $("#achieved_impression").html(total);
            var total_view_rate=total/promise_impression *100;
            // $("#view_rate").html(Math.round(total_view_rate));
         // $("#imp_chart").load(location.href + "# imp_chart");
         var total_impression=$("#total_impression").val();
         // chart(total_impression);
       }
     });
      }
    </script>
    <style>
     input.larger {
      width: 16px;
      height: 16px;
    }
    a.dropdown-item.waves-effect.waves-light:hover {
      color: #fff;

    }
    .navbar .dropdown-menu a:not(.active) {

      color: #333;
      font-weight: normal;
      text-decoration: none;
      font-size: .8125rem;
    }
  </style>

  @endpush