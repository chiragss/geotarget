@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => ('User Profile')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <form method="post" action="{{ route('profile.update') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
          

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ ('Edit Profile') }}</h4>
                <p class="card-category">{{ ('User information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                 
                  <label class="col-sm-2 col-form-label">{{ ('Name') }}</label><b>{{ Auth::guard('user')->user()->username }}</b>
                  {{--<div class="col-sm-7">
                                                       <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ ('Name') }}" value="{{ old('name',Auth::guard('user')->user()->username) }}" required="true" aria-required="true"/>
                                                        @if ($errors->has('name'))
                                                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                                        @endif
                                                      </div>
                                                      
                                                    </div>--}}
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ ('Email') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ ('Email') }}" value="{{ old('email',Auth::guard('user')->user()->email) }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ ('Business Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ ('Email') }}" value="{{ old('email',Auth::guard('user')->user()->business_name) }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ ('Business Type') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="business_type" id="input-email" type="email" placeholder="{{ ('Business Type') }}" value="{{ old('email',Auth::guard('user')->user()->bussiness_type) }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      {{--<div class="row">
                    <div class="col-md-12">
                      <form method="post" action="{{ route('profile.password') }}" class="form-horizontal">
                       {{ csrf_field() }}
                      
            
                        <div class="card ">
                          <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ ('Change password') }}</h4>
                            <p class="card-category">{{ ('Password') }}</p>
                          </div>
                          <div class="card-body ">
                            @if (session('status_password'))
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ session('status_password') }}</span>
                                  </div>
                                </div>
                              </div>
                            @endif
                            <div class="row">
                              <label class="col-sm-2 col-form-label" for="input-current-password">{{ ('Current Password') }}</label>
                              <div class="col-sm-7">
                                <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                  <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" input type="password" name="old_password" id="input-current-password" placeholder="{{ ('Current Password') }}" value="" required />
                                  @if ($errors->has('old_password'))
                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('old_password') }}</span>
                                  @endif
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label" for="input-password">{{ ('New Password') }}</label>
                              <div class="col-sm-7">
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                  <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ ('New Password') }}" value="" required />
                                  @if ($errors->has('password'))
                                    <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                  @endif
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ ('Confirm New Password') }}</label>
                              <div class="col-sm-7">
                                <div class="form-group">
                                  <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ ('Confirm New Password') }}" value="" required />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ ('Change password') }}</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>--}}
    </div>
  </div>
@endsection