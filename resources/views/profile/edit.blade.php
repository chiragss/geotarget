@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => ('Customer Profile')])
@section('content')
<script src="{{ asset('resources/assets') }}/md/js/jquery.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables.min.js"></script>
<script src="{{ asset('resources/assets') }}/md/js/addons/datatables-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/addons/datatables-select.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('resources/assets') }}/md/css/mdb.css">
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">


        <div class="card ">
          <div class="card-header card-header-primary">
            <h4 class="card-title">{{ ('Customer Profile') }}</h4>
            <p class="card-category">{{ Auth::guard('user')->user()->business_name }}</p>
          </div>
          <div class="card-body ">
            <div class="table-responsive">
              <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                </thead>
                <tbody>
                  <tr>
                    <th>Name</th>
                    <td>{{ Auth::guard('user')->user()->username }}</td>
                  </tr>
                  <tr>
                    <th>Business Name</th>
                    <td>{{ Auth::guard('user')->user()->business_name }}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>{{ Auth::guard('user')->user()->email }}</td>
                  </tr>
                  <tr>
                    <th>Bussiness Type</th>
                    <td>{{ Auth::guard('user')->user()->bussiness_type }}</td>
                  </tr>
                  <tr>
                    <th>Customer Address</th>
                    <td>{{ Auth::guard('user')->user()->customer_address }}</td>
                  </tr>

                </tbody> 
              </table>
            </div>

          </div>

        </div>

      </div>

      <div class="col-md-4"></div>


  </div>
  <div class="row">
        <div class="col-sm-12">
        <div class="card card">
         <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">perm_identity</i>
          </div>
          <h4 class="card-title">Customer's Deals</h4>
        </div>
        <div class="card-body">

          <div class="table-responsive">
            <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <th>DealNo</th>
                <th>CustomerName</th>
                <th>BusinessName</th>
                <th>EntryDate</th>
                <th>BussinessType</th>
                <th>CustomerAddress</th>
                <th>ZipCode</th>
                <th>Status</th>
              </thead>
              <tbody>

                @if(!empty($profile))
                @foreach($profile as $data)
                <tr>
                  <td>{{ $data->deal_no }}</td>
                  <td>{{ $data->username }}</td>
                  <td>{{ $data->business_name }}</td>
                  <td>{{ $data->entry_date }}</td>
                  <td>{{ $data->bussiness_type }}</td>
                  <td>{{ $data->customer_address }}</td>
                  <td>{{ $data->zip_code }}</td>
                  @if($data->kill_status=='1')
                  <td class="text-success">{{ 'Active'}}</td>
                  @else
                  <td class="text-danger">{{ 'Kill' }}</td>
                  @endif
                </tr>

                @endforeach
                @endif

              </tbody> 
            </table>
          </div>


        </div>
      </div>
    </div>  
  </div>


  {{--<div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ route('profile.password') }}" class="form-horizontal">
       {{ csrf_field() }}


       <div class="card ">
        <div class="card-header card-header-primary">
          <h4 class="card-title">{{ ('Change password') }}</h4>
          <p class="card-category">{{ ('Password') }}</p>
        </div>
        <div class="card-body ">
          @if (session('status_password'))
          <div class="row">
            <div class="col-sm-12">
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <i class="material-icons">close</i>
                </button>
                <span>{{ session('status_password') }}</span>
              </div>
            </div>
          </div>
          @endif
          <div class="row">
            <label class="col-sm-2 col-form-label" for="input-current-password">{{ ('Current Password') }}</label>
            <div class="col-sm-7">
              <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" input type="password" name="old_password" id="input-current-password" placeholder="{{ ('Current Password') }}" value="" required />
                @if ($errors->has('old_password'))
                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('old_password') }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <label class="col-sm-2 col-form-label" for="input-password">{{ ('New Password') }}</label>
            <div class="col-sm-7">
              <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ ('New Password') }}" value="" required />
                @if ($errors->has('password'))
                <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ ('Confirm New Password') }}</label>
            <div class="col-sm-7">
              <div class="form-group">
                <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ ('Confirm New Password') }}" value="" required />
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ml-auto mr-auto">
          <button type="submit" class="btn btn-primary">{{ ('Change password') }}</button>
        </div>
      </div>
    </form>
  </div>
</div>--}}
</div>
</div>
@endsection