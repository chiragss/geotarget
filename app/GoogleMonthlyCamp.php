<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleMonthlyCamp extends Model
{
   protected $table='google_monthly_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
