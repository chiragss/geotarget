<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerListNew extends Model
{
    protected $table='customer_deals';

	protected $fillable = [
		'customer_id','deal_no','no_of_impression','contract_term','entry_date','google_ads','groundtruth_ads','facebook_ads','campaign_launch_date','reporting_launch_date','artwork_recevied_date','artwork_deadline_date','geotarget_camp_date','start_date','stop_date','kill_status'
	];
}
