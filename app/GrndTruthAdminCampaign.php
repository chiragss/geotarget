<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrndTruthAdminCampaign extends Model
{
     protected $table='groundtruth_admin_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
