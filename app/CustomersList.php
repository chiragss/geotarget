<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomersList extends Authenticatable
{
    //

	//protected $guard = 'user';
	protected $table='customers_list';

	protected $fillable = [
		'customer_id', 'username', 'password','no_of_impression','reset_token','token_pass','business_name','email','phone','contract_term','entry_date','start_date','stop_date','posted_by','bussiness_type','customer','customer_name','bussiness_phone','additional_phone','cell_phone','home_phone','customer_address','customer_city','customer_state','zip_code','groundtruth_ads','google_ads','facebook_ads','website','landing_page','created_page','keywords','rep_name','salesrep_mail','rep_id','kill_status'
	];

	protected $hidden = [
		'password','remember_token',
	];
	
}
