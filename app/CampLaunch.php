<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampLaunch extends Model
{
    protected $table='camp_launch';

	protected $fillable = [
		'cust_id','launch_date','flag','next_launch_date'
	];
}
