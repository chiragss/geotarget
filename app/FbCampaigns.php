<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FbCampaigns extends Model
{
	protected $table='fb_admin_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
