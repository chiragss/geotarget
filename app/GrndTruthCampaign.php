<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrndTruthCampaign extends Model
{
     protected $table='groundtruth_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
