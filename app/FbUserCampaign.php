<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FbUserCampaign extends Model
{
   protected $table='fb_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
