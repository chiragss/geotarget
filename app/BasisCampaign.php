<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasisCampaign extends Model
{
    protected $table='basis_campaigns';

	protected $fillable = [
		'cust_id','camp_id','camp_name','client_id','brand_id','keyword','creatives','team_users','camp_start_date','camp_end_date','camp_date','status'
	];
	public $timestamps = true;
}
