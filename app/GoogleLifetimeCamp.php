<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleLifetimeCamp extends Model
{
	protected $table='google_campaigns';

	protected $fillable = [
		'cust_id', 'camp_id','camp_name','keyword'
	];

	
	public $timestamps = true;
}
