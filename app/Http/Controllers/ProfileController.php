<?php

namespace App\Http\Controllers;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use App\userList;
use Session;
use Datatables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Redirect;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
     $this->middleware('auth');
 }


 public function edit()
 {
   if(Auth::guard('user')->check()){
    $customer_id=Auth::guard('user')->user()->customer_id;
    $profile=DB::Select("SELECT customers_list.*,customer_deals.* FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customers_list.customer_id='".$customer_id."'");
    return view('profile.edit',compact('profile'));
}else{
    return Redirect::to('userlogin'); 
}

}

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        auth()->user()->update($request->all());


        return back()->withStatus(('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withStatusPassword(('Password successfully updated.'));
    }
}
