<?php
namespace App\Http\Controllers;
use Auth;
use App\User;
use App\GoogleLifetimeCamp;
use App\GoogleMonthlyCamp;
use App\FbCampaigns;
use Illuminate\Http\Request;
use App\CustomersList;
use App\CustomerListNew;
use Session;
use Datatables;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Mail;
class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
    	$this->middleware('auth');
    } 
    public function index(User $model)
    {
    	return view('users.index', ['users' => $model->paginate(15)]);
    }
    public function csv()
    {
    	return view('campaigns.csv');
    }

    public function contact(){
    	if(Auth::guard('user')->check()){
    		return view('pages/contact');
    	}else{
    		return Redirect::to('userlogin');
    	}

    }


    public function addcontact(Request $request){
    	$subject=$request->subject;
    	$msg=$request->msg;
    	$customer_id=Auth::guard('user')->user()->customer_id;
    	$bussiness_name=Auth::guard('user')->user()->business_name;
    	$created_at=date('Y-m-d H:i:s');
    	$data=array('user_id'=>$customer_id,'subject'=>$subject,'msg'=>ltrim($msg),'created_at'=>$created_at,'bussiness_name'=>$bussiness_name,'username'=>Auth::guard('user')->user()->username,'bussiness_phone'=>Auth::guard('user')->user()->bussiness_phone);
    	Mail::send('email.email',$data , function($message) use($data)
    	{
  // $message->to($data1)->subject('This is test e-mail');
    		$message->from('info@geotargetus.net', 'Geotargetus');
    //$message->to('chirags@shopperlocal.com');
    		$message->to('info@geotargetus.net');
    		$message->Bcc('chirags@shopperlocal.com');
    		$message->Bcc('faisalk@shopperlocal.com');
    // $message->replyTo('customerservice@localtext.net', 'LocalText Customer Service');
    		$message->subject($data['subject']);

    	}); 
      // Mail::send();
  // DB::table('contact_us')->insert($data);
    	return back()->withStatus(('Your Message Has been successfully Sent to Admin.'));
    }

    public function userHomeDashboard($id){
    	if(Auth::guard('user')->check()){
    		$fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
    		$googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");
    		$grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
    		$pro_impression=DB::Select("SELECT customers_list.customer_id,customer_deals.no_of_impression as promise_impression,customer_deals.id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customers_list.customer_id='".$id."' and customer_deals.geotarget_camp_date=(SELECT MAX(geotarget_camp_date) FROM customer_deals WHERE customer_id='".$id."')");

        $date=DB::Select("SELECT camp_start_date FROM google_campaigns WHERE cust_id='".$id."' and camp_start_date=(SELECT MIN(camp_start_date) FROM google_campaigns WHERE cust_id='".$id."')");
        $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
        set_time_limit(0);
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-campaign-launched-interval.php?cust_id='.$id);
        curl_setopt($curl_handle, CURLOPT_HEADER, 0);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        $results = curl_exec($curl_handle);
        $results = json_decode($results);
        // var_dump($results);
        if(!empty($results)){
          foreach($results as $data){
            $a='CSCST'."#";
            $cust_id=$data->$a;
            $launchdate=$data->WDCDAT;
            $year=substr($launchdate,-2);
            $day=substr($launchdate,-4,2);
            $month=substr($launchdate,0, -4);
            $start_date=$year.'-'.$month.'-'.$day;
            
            $date_create=date_create($start_date);
            $date1=date_format($date_create,"Y-m-d");

            $date2= date('Y-m-d');
            $start_date = strtotime($date1); 
            $end_date = strtotime($date2); 
          // Get the difference and divide into  
          // total no. seconds 60/60/24 to get  
          // number of days 
            $total_days=($end_date - $start_date)/60/60/24;
            

            if($total_days>29){
              $report_show=2;
            }


          }
        }


        //$check_result=DB::Select("SELECT * FROM camp_launch where cust_id='".$id."'  and launch_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
        //echo "SELECT * FROM camp_launch where cust_id='".$id."'  and launch_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()";

        return view('userhomedashboard',compact('fbresult','googleresult','grndadminresult','id','pro_impression','date','basisresult','report_show'));
      }else{
        return Redirect::to('userlogin');
      }

    }

    public function downloadCsv(){
     $id=Auth::guard('user')->user()->customer_id;
     $googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."'");
     foreach($googleresult as $data){
      $ads[]=json_decode($data->ad_placement);
    }

    for($j=0; $j < count($ads); $j++){
      if(!empty($ads[$j])){
       foreach($ads[$j] as $placement){
        $report[]=array('ad_name'=>$placement->ad_name,'ad_impression'=>$placement->ad_impresison);
      }
    }
  }

     // file name for download
  $fileName = "report" . date('Ymd') . ".xls";

    // headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
            // display column names as first row
     echo implode("\t", array_keys($row)) . "\n";
     $flag = true;
   }
        // filter data
      // array_walk($row, 'filterData');
   echo implode("\t", array_values($row)) . "\n";

 }

 exit;
    // echo '<pre>';
    // var_dump($ads);
    // echo 'chirag';die;
}

// for camp launch report
public function campLaunchReportDownload(){
  //$id=Auth::guard('user')->user()->customer_id;
// echo "SELECT * FROM google_campaigns where cust_id='".$id."' and camp_type='Video' and camp_date=(SELECT max(camp_date) FROM google_campaigns)";die;
  $camp_launch=DB::Select("SELECT * FROM camp_launch ORDER BY launch_date ASC");
  foreach($camp_launch as $data){
    $report[]=array('cust_id'=>$data->cust_id,'launch_date'=>$data->launch_date);
  }



// file name for download
  $fileName = "campLaunchreport" . date('Ymd') . ".xls";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}

  // Add NEw Campaign to google 
public function csvUpload0(Request $request){
 if($request->file('file')){
  $file = $request->file('file');
   // File Details 
  $filename = $file->getClientOriginalName();
   /*$extension = $file->getClientOriginalExtension();
   $tempPath = $file->getRealPath();
   $fileSize = $file->getSize();
   $mimeType = $file->getMimeType();*/
   // File upload location
   $location = 'public';
   // Upload file
   $file->move($location,$filename);
   // Import CSV to Database
   $filepath = public_path($filename);
   // Reading file
   $file = fopen($filepath,"r");
   $importData_arr = array();
   $i = 0;
   while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
   	$num = count($filedata );
   //Skip first row (Remove below comment if you want to skip the first row)
   	if($i == 0){
   		$i++;
   		continue; 
   	}
   	for ($c=0; $c < $num; $c++) {
   		$importData_arr[$i][] = $filedata [$c];
   	}
   	$i++;
   }
/* echo '<pre>';
  var_dump($importData_arr);
  die;*/
  foreach($importData_arr as $data){
  	$account_name[]=array('account'=>$data[0],'camp_id'=>$data[2]);
  } 
  $input = array_map("unserialize", array_unique(array_map("serialize", $account_name)));
// var_dump($input);
  foreach($input as $acc){
  	$account[]=$acc;
  }
  fclose($file);
  foreach($account as $name){
  	$clicks=0;$impressions=0;$ctr=0;$cost=0;$budget=0;$invalid_clicks=0;$dsclicks=0;$dsctr=0;$dsimpressions=0;$dscost=0;$dsbudget=0;$dsinvalid_clicks=0;$spclicks=0;$spctr=0;$spimpressions=0;$spcost=0;$spbudget=0;$spinvalid_clicks=0;$cnclicks=0;$cnctr=0;$cnimpressions=0;$cncost=0;$cnbudget=0;$cninvalid_clicks=0;$gsclicks=0;$gsctr=0;$gsimpressions=0;$gscost=0;$gsbudget=0;$gsinvalid_clicks=0;$ytclicks=0;$ytctr=0;$ytimpressions=0;$ytcost=0;$ytbudget=0;$ytinvalid_clicks=0;$ysclicks=0;$ysctr=0;$ysimpressions=0;$yscost=0;$ysbudget=0;$ysinvalid_clicks=0; $display_network=NULL;$youtube_video=NULL;$youtube_search=null;$search_partner=null;$google_search=null;$cross_network=null;
/*   echo '<pre>';
  var_dump($importData_arr);
  die;*/
  foreach($importData_arr as $data1){
  // OPen if loop
  	if($name['account']==$data1[0]){
  // echo $name['account'];
  // echo $data1[0];
  		if($data1[6]=='Display Network'){
  			$dsclicks=$dsclicks + $data1[7];
  			$dsctr=$dsctr + $data1[9];
  			$dsimpressions=$dsimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$dscost=$dscost + $data1[8];
  			$dsbudget=$dsbudget + $data1[3];
  			$dsinvalid_clicks=$dsinvalid_clicks + $data1[12];
  			$display_network=array('cost'=>$dscost,'clicks'=>$dsclicks,'impressions'=>$dsimpressions,'ctr'=>$dsctr,'budget'=>$dsbudget,'invalid_clicks'=>$dsinvalid_clicks);
  		}elseif($data1[6]=='Search partners'){
  			$spclicks=$spclicks + $data1[7];
  			$spctr=$spctr + $data1[9];
  			$spimpressions=$spimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$spcost=$spcost + $data1[8];
  			$spbudget=$spbudget + $data1[3];
  			$spinvalid_clicks=$spinvalid_clicks + $data1[12];
  			$search_partner=array('cost'=>$spcost,'clicks'=>$spclicks,'impressions'=>$spimpressions,'ctr'=>$spctr,'budget'=>$spbudget,'invalid_clicks'=>$spinvalid_clicks);
  		}elseif($data1[6]=='Google search'){
  			$gsclicks=$gsclicks + $data1[7];
  			$gsctr=$gsctr + $data1[9];
  			$gsimpressions=$gsimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$gscost=$gscost + $data1[8];
  			$gsbudget=$gsbudget + $data1[3];
  			$gsinvalid_clicks=$gsinvalid_clicks + $data1[12];
  			$google_search=array('cost'=>$gscost,'clicks'=>$gsclicks,'impressions'=>$gsimpressions,'ctr'=>$gsctr,'budget'=>$gsbudget,'invalid_clicks'=>$gsinvalid_clicks);
  		}elseif($data1[6]=='YouTube Videos'){
  			$ytclicks=$ytclicks + $data1[7];
  			$ytctr=$ytctr + $data1[9];
  			$ytimpressions=$ytimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$ytcost=$ytcost + $data1[8];
  			$ytbudget=$ytbudget + $data1[3];
  			$ytinvalid_clicks=$ytinvalid_clicks + $data1[12];
  			$youtube_video=array('cost'=>$ytcost,'clicks'=>$ytclicks,'impressions'=>$ytimpressions,'ctr'=>$ytctr,'budget'=>$ytbudget,'invalid_clicks'=>$ytinvalid_clicks);
  		}elseif($data1[6]=='YouTube Search'){
  			$ysclicks=$ysclicks + $data1[7];
  			$ysctr=$ysctr + $data1[9];
  			$ysimpressions=$ysimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$yscost=$yscost + $data1[8];
  			$ysbudget=$ysbudget + $data1[3];
  			$ysinvalid_clicks=$ysinvalid_clicks + $data1[12];
  			$youtube_search=array('cost'=>$yscost,'clicks'=>$ysclicks,'impressions'=>$ysimpressions,'ctr'=>$ysctr,'budget'=>$ysbudget,'invalid_clicks'=>$ysinvalid_clicks);
  		}elseif($data1[6]=='Cross-network'){
  			$cnclicks=$cnclicks + $data1[7];
  			$cnctr=$cnctr + $data1[9];
  			$cnimpressions=$cnimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  			$cncost=$cncost + $data1[8];
  			$cnbudget=$cnbudget + $data1[3];
  			$cninvalid_clicks=$cninvalid_clicks + $data1[12];
  			$cross_network=array('cost'=>$cncost,'clicks'=>$cnclicks,'impressions'=>$cnimpressions,'ctr'=>$cnctr,'budget'=>$cnbudget,'invalid_clicks'=>$cninvalid_clicks);
  		}else{
  		}
  		$clicks=$clicks + $data1[7];
  		$ctr=$ctr + $data1[9];
  		$impressions=$impressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
  		$cost=$cost + $data1[8];
  		$budget=$budget + $data1[3];
  		$invalid_clicks=$invalid_clicks + $data1[12];
  	}
  	/*Close if loop*/
  }
  /*close for loop*/
  $keyword=array('cost'=>$cost,'clicks'=>$clicks,'impressions'=>$impressions,'ctr'=>$ctr,'budget'=>$budget,'invalid_clicks'=>$invalid_clicks);
  $loc_imp=array('Display_Network'=>$display_network,'Search_Partners'=>$search_partner,'Google_Search'=>$google_search,'YouTube_Videos'=>$youtube_video,'YouTube_Search'=>$youtube_search,'Cross_Network'=>$cross_network);
  $dataa[]=array('camp_id'=>$name['camp_id'],'camp_name'=>$name['account'],'keyword'=>$keyword,'loc_impression'=>$loc_imp);
} 
// echo '<pre>';
  // var_dump($dataa);
  // die;
$date=date('Y-m-d');
foreach($dataa as $result){

	$campaigns=DB::Select("SELECT camp_id FROM google_monthly_campaigns WHERE camp_id='".$result['camp_id']."'");
	if(empty($campaigns)){
		$google = new GoogleMonthlyCamp;
		$jsonkeyword=json_encode($result['keyword']);
		$pub_platform_campaign=json_encode($result['loc_impression']);
		$google->cust_id='667677540320487';
		$google->camp_id=$result['camp_id'];
		$google->camp_name=$result['camp_name'];
		$google->keyword=$jsonkeyword;
		$google->pub_platform_campaign=$pub_platform_campaign;
		$google->camp_date=$date; 
		$google->save();
	}


}
}
}




  // to upload google network geotarget google sheet
public function csvUpload(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
   // File Details 
		$filename = $file->getClientOriginalName();
   /*$extension = $file->getClientOriginalExtension();
   $tempPath = $file->getRealPath();
   $fileSize = $file->getSize();
   $mimeType = $file->getMimeType();*/
   // File upload location
   $location = 'public';
   // Upload file
   $file->move($location,$filename);
   // Import CSV to Database
   $filepath = public_path($filename);
   // Reading file
   $file = fopen($filepath,"r");
   $importData_arr = array();
   $i = 0;
   while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
   	$num = count($filedata );
   //Skip first row (Remove below comment if you want to skip the first row)
   	if($i == 0){
   		$i++;
   		continue; 
   	}
   	for ($c=0; $c < $num; $c++) {
   		$importData_arr[$i][] = $filedata [$c];
   	}
   	$i++;
   }
/* echo '<pre>';
  var_dump($importData_arr);
  die;*/
  foreach($importData_arr as $data){
  	$account_name[]=array('account'=>$data[0],'camp_id'=>$data[2]);
  } 
  $input = array_map("unserialize", array_unique(array_map("serialize", $account_name)));
// var_dump($input);
  foreach($input as $acc){
  	$account[]=$acc;
  }
  fclose($file);
  foreach($account as $name){
  	$clicks=0;$impressions=0;$ctr=0;$cost=0;$budget=0;$invalid_clicks=0;$dsclicks=0;$dsctr=0;$dsimpressions=0;$dscost=0;$dsbudget=0;$dsinvalid_clicks=0;$spclicks=0;$spctr=0;$spimpressions=0;$spcost=0;$spbudget=0;$spinvalid_clicks=0;$cnclicks=0;$cnctr=0;$cnimpressions=0;$cncost=0;$cnbudget=0;$cninvalid_clicks=0;$gsclicks=0;$gsctr=0;$gsimpressions=0;$gscost=0;$gsbudget=0;$gsinvalid_clicks=0;$ytclicks=0;$ytctr=0;$ytimpressions=0;$ytcost=0;$ytbudget=0;$ytinvalid_clicks=0;$ysclicks=0;$ysctr=0;$ysimpressions=0;$yscost=0;$ysbudget=0;$ysinvalid_clicks=0; $display_network=NULL;$youtube_video=NULL;$youtube_search=null;$search_partner=null;$google_search=null;$cross_network=null;
  //  echo '<pre>';
  // var_dump($importData_arr);
  // die;
    foreach($importData_arr as $data1){
  // OPen if loop
     if($name['account']==$data1[0]){
   // echo $name['account'];
   // echo $data1[0];
   // die;
      if($data1[6]=='Display Network'){
       $dsclicks=$dsclicks + $data1[7];
       $dsctr=$dsctr + $data1[9];
       $dsimpressions=$dsimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $dscost=$dscost + $data1[8];
       $dsbudget=$dsbudget + $data1[3];
       $dsinvalid_clicks=$dsinvalid_clicks + $data1[12];
       $display_network=array('cost'=>$dscost,'clicks'=>$dsclicks,'impressions'=>$dsimpressions,'ctr'=>$dsctr,'budget'=>$dsbudget,'invalid_clicks'=>$dsinvalid_clicks);
     }elseif($data1[6]=='Search partners'){
       $spclicks=$spclicks + $data1[7];
       $spctr=$spctr + $data1[9];
       $spimpressions=$spimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $spcost=$spcost + $data1[8];
       $spbudget=$spbudget + $data1[3];
       $spinvalid_clicks=$spinvalid_clicks + $data1[12];
       $search_partner=array('cost'=>$spcost,'clicks'=>$spclicks,'impressions'=>$spimpressions,'ctr'=>$spctr,'budget'=>$spbudget,'invalid_clicks'=>$spinvalid_clicks);
     }elseif($data1[6]=='Google search'){
       $gsclicks=$gsclicks + $data1[7];
       $gsctr=$gsctr + $data1[9];
       $gsimpressions=$gsimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $gscost=$gscost + $data1[8];
       $gsbudget=$gsbudget + $data1[3];
       $gsinvalid_clicks=$gsinvalid_clicks + $data1[12];
       $google_search=array('cost'=>$gscost,'clicks'=>$gsclicks,'impressions'=>$gsimpressions,'ctr'=>$gsctr,'budget'=>$gsbudget,'invalid_clicks'=>$gsinvalid_clicks);
     }elseif($data1[6]=='YouTube Videos'){
       $ytclicks=$ytclicks + $data1[7];
       $ytctr=$ytctr + $data1[9];
       $ytimpressions=$ytimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $ytcost=$ytcost + $data1[8];
       $ytbudget=$ytbudget + $data1[3];
       $ytinvalid_clicks=$ytinvalid_clicks + $data1[12];
       $youtube_video=array('cost'=>$ytcost,'clicks'=>$ytclicks,'impressions'=>$ytimpressions,'ctr'=>$ytctr,'budget'=>$ytbudget,'invalid_clicks'=>$ytinvalid_clicks);
     }elseif($data1[6]=='YouTube Search'){
       $ysclicks=$ysclicks + $data1[7];
       $ysctr=$ysctr + $data1[9];
       $ysimpressions=$ysimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $yscost=$yscost + $data1[8];
       $ysbudget=$ysbudget + $data1[3];
       $ysinvalid_clicks=$ysinvalid_clicks + $data1[12];
       $youtube_search=array('cost'=>$yscost,'clicks'=>$ysclicks,'impressions'=>$ysimpressions,'ctr'=>$ysctr,'budget'=>$ysbudget,'invalid_clicks'=>$ysinvalid_clicks);
     }elseif($data1[6]=='Cross-network'){
       $cnclicks=$cnclicks + $data1[7];
       $cnctr=$cnctr + $data1[9];
       $cnimpressions=$cnimpressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
       $cncost=$cncost + $data1[8];
       $cnbudget=$cnbudget + $data1[3];
       $cninvalid_clicks=$cninvalid_clicks + $data1[12];
       $cross_network=array('cost'=>$cncost,'clicks'=>$cnclicks,'impressions'=>$cnimpressions,'ctr'=>$cnctr,'budget'=>$cnbudget,'invalid_clicks'=>$cninvalid_clicks);
     }else{
     }
     $clicks=$clicks + $data1[7];
     $ctr=$ctr + $data1[9];
     $impressions=$impressions + filter_var($data1[10], FILTER_SANITIZE_NUMBER_INT);
     $cost=$cost + $data1[8];
     $budget=$budget + $data1[3];
     $invalid_clicks=$invalid_clicks + $data1[12];
     $camp_status=$data1[5];
   }
   /*Close if loop*/
 }
 /*close for loop*/
 $keyword=array('cost'=>$cost,'clicks'=>$clicks,'impressions'=>$impressions,'ctr'=>$ctr,'budget'=>$budget,'invalid_clicks'=>$invalid_clicks);
 $loc_imp=array('Display_Network'=>$display_network,'Search_Partners'=>$search_partner,'Google_Search'=>$google_search,'YouTube_Videos'=>$youtube_video,'YouTube_Search'=>$youtube_search,'Cross_Network'=>$cross_network);
 $dataa[]=array('camp_id'=>$name['camp_id'],'camp_name'=>$name['account'],'keyword'=>$keyword,'loc_impression'=>$loc_imp,'camp_status'=>$camp_status);
} 
// echo '<pre>';
//   var_dump($dataa);
//   die;
$date=date('Y-m-d');
foreach($dataa as $result){
  // $google = new GoogleMonthlyCamp;
	$jsonkeyword=json_encode($result['keyword']);
  // $pub_platform_campaign=json_encode($result['loc_impression']);
  // $google->cust_id='667677540320487';
  // $google->camp_id=$result['camp_id'];
  $camp_name=str_replace("'","\'",$result['camp_name']);

  if($result['keyword']['impressions']==0){
    DB::Select("UPDATE google_monthly_campaigns SET camp_name='".$camp_name."',keyword='".$jsonkeyword."',display_campaign='null',video_campaign='null'  WHERE camp_id='".$result['camp_id']."'");
  }else{
    DB::Select("UPDATE google_monthly_campaigns SET camp_name='".$camp_name."',keyword='".$jsonkeyword."',camp_status='".$result['camp_status']."'  WHERE camp_id='".$result['camp_id']."'");
  }
  

}
}
}

/*Close function to upload google sheet*/

/*Function to add google ad placement*/
/*public function csvUpload4(Request $request){
  if($request->file('file')){
    $file = $request->file('file');
    $filename = $file->getClientOriginalName();
    $location = 'public';
    $file->move($location,$filename);
    $filepath = public_path($filename);
    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );

      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }
 
    foreach($importData_arr as $data){
      $campaign_name[]=array('campaign_name'=>$data[1],'camp_id'=>$data[0]);
    } 
    $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

    foreach($input as $acc){
      $campaign[]=$acc;
    }
    fclose($file);

  foreach($campaign as $name){
    $ads=null;
    foreach($importData_arr as $data1){
 
      if($name['campaign_name']==$data1[1]){

  $ad_name=str_replace("'", '', $data1[2]);
  $placement_type=$data1[3];
  $ad_impresison=$data1[5];
  $ads[]=array('ad_name'=>$ad_name,'ad_impresison'=>$ad_impresison,'placement_type'=>$placement_type);
}

}

$dataa[]=array('camp_id'=>$name['camp_id'],'camp_name'=>$name['campaign_name'],'ads'=>$ads);
} 
  $date=date('Y-m-d');
  foreach($dataa as $result){
 
    $ads=json_encode($result['ads']);
    DB::Select("UPDATE google_campaigns SET ad_placement='".$ads."' WHERE camp_id='".$result['camp_id']."' and camp_date='".$date."'");
  }
}
}*/
/*Close to function google ad placement*/
/*Open for google data for display and video*/
public function csvUpload5(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
/*  echo '<pre>';
  var_dump($importData_arr);
  die;*/
  foreach($importData_arr as $data){
  	$campaign_name[]=array('campaign_name'=>$data[0],'camp_id'=>$data[1],'camp_type'=>$data[2],'start_date'=>$data[7],'end_date'=>$data[8]);
  } 
  $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));
//  echo '<pre>';
 // var_dump($input);die;
  foreach($input as $acc){
  	$campaign[]=$acc;
  }

  foreach($campaign as $name){

  	$i=0; $video_rate=0;$video_25=0;$video_50=0;$video_75=0;$video_100=0; $mobile_views=0;
  	$tab_views=0; $desk_views=0;$other_views=0; $sun_view=0;$sun_impression=0;
  	$mon_view=0;$mon_impression=0;$tue_view=0;$tue_impression=0;$wed_view=0; 
  	$wed_impression=0;$thru_view=0;$thru_impression=0;$fri_view=0; $fri_impression=0;
  	$sat_view=0;$video_device_views=null;$videoo=null;$video_view_rate=null;
  	$sat_impression=0; $dis_sun_impression=0;$dis_mon_impression=0;$dis_tue_impression=0;$dis_wed_impression=0;
  	$dis_thru_impression=0;$dis_fri_impression=0;$dis_sat_impression=0; $display_mobile_impression=0;$display_tablet_impression=0;$display_desk_impression=0;
  	$display_other_impression=0;$video_sun=0;$video_mon=0;$video_tue=0;$video_wed=0;$video_thru=0;$video_fri=0;$video_sat=0;$video25=0;$video50=0;$video75=0;$video100=0;
  	$dis_day_impression=null;$display_device_impression=null;  $femclicks=0;$femimpressions=0;$maleclicks=0;$maleimpressions=0;$unknownclicks=0;
  	$unknownimpressions=0;$female=null;$male=null;$unknown=null; $_18_24clicks=0;$_18_24impressions=0;$_25_34clicks=0;$_25_34impressions=0; $_35_44clicks=0;$_35_44impressions=0; $_45_54clicks=0;$_45_54impressions=0;$undeterminedclicks=0;$undeterminedimpressions=0; $_65_aboveclicks=0;$_65_aboveimpressions=0;$_18_24=null;$_25_34=null;$_35_44=null;$_45_54=null;$undetermined=null; $_65_above=null;$total_impp=0;

  	foreach($importData_arr as $data1){
// OPen if loop
  		if($name['campaign_name']==$data1[0]){
  			if($data1[2]=='Video'){
// for video views by device
  				$video_25=$video_25 + $data1[12];
  				$video_50=$video_50 + $data1[13];
  				$video_75=$video_75 + $data1[14];
  				$video_100=$video_100 + $data1[15];
  				if($data1[4]=='Mobile'){
  					$mobile_views=$mobile_views + $data1[11];
  				}elseif($data1[4]=='Tablet'){
  					$tab_views=$tab_views + $data1[11];
  				}elseif($data1[4]=='Desktop'){
  					$desk_views=$desk_views + $data1[11];
  				}else{
  					$other_views=$other_views + $data1[11];
  				}

  				$video_device_views=array('mobile_views'=>$mobile_views,'tab_views'=>$tab_views,'desk_views'=>$desk_views,'other_views'=>$other_views);
// week of the day impressions

  				if($data1[3]=='Sunday'){
  					$sun_view=$sun_view + $data1[10];
  					$sun_impression=   $sun_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_sun=array('sun_views'=>$sun_view,'sun_impression'=>$sun_impression);
  				}elseif($data1[3]=='Monday'){
  					$mon_view=$mon_view + $data1[10];
  					$mon_impression= $mon_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_mon=array('mon_views'=>$mon_view,'mon_impression'=>$mon_impression);
  				}elseif($data1[3]=='Tuesday'){
  					$tue_view=$tue_view + $data1[10];
  					$tue_impression=  $tue_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_tue=array('tue_views'=>$tue_view,'tue_impression'=>$tue_impression);
  				}elseif($data1[3]=='Wednesday'){
  					$wed_view=$wed_view + $data1[10];
  					$wed_impression= $wed_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_wed=array('wed_views'=>$wed_view,'wed_impression'=>$wed_impression);
  				}elseif($data1[3]=='Thursday'){
  					$thru_view=$thru_view + $data1[10];
  					$thru_impression=$thru_impression + filter_var($data1[11],FILTER_SANITIZE_NUMBER_INT);
  					$video_thru=array('thru_views'=>$thru_view,'thru_impression'=>$thru_impression);
  				}elseif($data1[3]=='Friday'){
  					$fri_view=$fri_view + $data1[10];
  					$fri_impression=$fri_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_fri=array('fri_views'=>$fri_view,'fri_impression'=>$fri_impression);
  				}else{
  					$sat_view=$sat_view + $data1[10];
  					$sat_impression=$sat_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$video_sat=array('sat_views'=>$sat_view,'sat_impression'=>$sat_impression);
  				}
  				$videoo=array('sun'=>$video_sun,'mon'=>$video_mon,'tue'=>$video_tue,'wed'=>$video_wed,'thru'=>$video_thru,'fri'=>$video_fri,'sat'=>$video_sat);
  				$total_video_impression=$sun_impression + $mon_impression + $tue_impression + $wed_impression + $thru_impression +$fri_impression +$sat_impression;
  				$total_video_views=$sun_view + $mon_view + $tue_view + $wed_view + $thru_view + $fri_view + $sat_view;
  			}else{
// for display impression  by device
  				$total_impp= $total_impp + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				if($data1[4]=='Mobile'){
  					$display_mobile_impression= $display_mobile_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[4]=='Tablet'){
  					$display_tablet_impression=$display_tablet_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[4]=='Desktop'){
  					$display_desk_impression=$display_desk_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}else{
  					$display_other_impression= $display_other_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}

  				$display_device_impression=array('mobile'=>$display_mobile_impression,'tablet'=>$display_tablet_impression,'desktop'=>$display_desk_impression,'other'=>$display_other_impression);
// Display for week of the day impression
  				if($data1[3]=='Sunday'){

  					$dis_sun_impression=$dis_sun_impression  + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[3]=='Monday'){

  					$dis_mon_impression=$dis_mon_impression  + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[3]=='Tuesday'){

  					$dis_tue_impression=$dis_tue_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[3]=='Wednesday'){

  					$dis_wed_impression=$dis_wed_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[3]=='Thursday'){

  					$dis_thru_impression=$dis_thru_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}elseif($data1[3]=='Friday'){

  					$dis_fri_impression=$dis_fri_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}else{

  					$dis_sat_impression=$dis_sat_impression + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  				}

  				$dis_day_impression=array('sun'=>$dis_sun_impression,'mon'=>$dis_mon_impression,'tue'=>$dis_tue_impression,'wed'=>$dis_wed_impression,'thru'=>$dis_thru_impression,'fri'=>$dis_fri_impression,'sat'=>$dis_sat_impression);
// for gende wise impression for display

  				if($data1[6]=='Female'){
  					$femclicks=$femclicks + $data1[16];
  					$femimpressions=$femimpressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$female=array('clicks'=>$femclicks,'impressions'=>$femimpressions);
  				}elseif($data1[6]=='Male'){
  					$maleclicks=$maleclicks + $data1[16];
  					$maleimpressions=$maleimpressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$male=array('clicks'=>$maleclicks,'impressions'=>$maleimpressions);
  				}else{
  					$unknownclicks=$unknownclicks + $data1[16];
  					$unknownimpressions=$unknownimpressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$unknown=array('clicks'=>$unknownclicks,'impressions'=>$unknownimpressions);
  				}
  				$display_gender=array('male'=>$male,'female'=>$female,'unknown'=>$unknown);
// Close for gender wise impression for display

//open for age wise clicks and impression for display

  				if($data1[5]=='18-24'){
  					$_18_24clicks=$_18_24clicks + $data1[16];
  					$_18_24impressions=$_18_24impressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$_18_24=array('clicks'=>$_18_24clicks,'impressions'=>$_18_24impressions);
  				}elseif($data1[5]=='25-34'){
  					$_25_34clicks=$_25_34clicks + $data1[16];
  					$_25_34impressions=$_25_34impressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$_25_34=array('clicks'=>$_25_34clicks,'impressions'=>$_25_34impressions);
  				}elseif($data1[5]=='35-44'){
  					$_35_44clicks=$_35_44clicks + $data1[16];
  					$_35_44impressions=$_35_44impressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$_35_44=array('clicks'=>$_35_44clicks,'impressions'=>$_35_44impressions);
  				}elseif($data1[5]=='45-54'){
  					$_45_54clicks=$_45_54clicks + $data1[16];
  					$_45_54impressions=$_45_54impressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$_45_54=array('clicks'=>$_45_54clicks,'impressions'=>$_45_54impressions);
  				}elseif($data1[5]=='Undetermined'){
  					$undeterminedclicks=$undeterminedclicks + $data1[16];
  					$undeterminedimpressions=$undeterminedimpressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$undetermined=array('clicks'=>$undeterminedclicks,'impressions'=>$undeterminedimpressions);
  				}else{
  					$_65_aboveclicks=$_65_aboveclicks + $data1[16];
  					$_65_aboveimpressions=$_65_aboveimpressions + filter_var($data1[11], FILTER_SANITIZE_NUMBER_INT);
  					$_65_above=array('clicks'=>$_65_aboveclicks,'impressions'=>$_65_aboveimpressions);
  				}

  				$dispaly_age_wise=array('18_24'=>$_18_24,'25_34'=>$_25_34,'35_44'=>$_35_44,'45_54'=> $_45_54,'65_above'=>$_65_above,'undetermined'=>$undetermined);
// close for age wise clicks and impression for display 
  			}


  		}
  		if($i!==0){
  			$video25=$video_25/$i*100;
  			$video50=$video_50/$i*100;
  			$video75=$video_75/$i*100;
  			$video100=$video_100/$i*100;
  		}

  		$video_view_rate=array('video_25'=>$video25,'video_50'=>$video50,'video_75'=>$video75,'video_100'=>$video100);
  		/*Close if loop*/
  		$i++;
  	}
  	/*close for loop*/
// $keyword=array('cost'=>$cost,'clicks'=>$clicks,'impressions'=>$impressions);
/*echo '<pre>';
var_dump($ads);die;*/
if($name['camp_type']=='Video'){
	if($total_video_impression>0){
		$view_rate=$total_video_views/$total_video_impression * 100;
	}else{
		$view_rate=0;
	}

	$video=array('device'=> $video_device_views,'video'=>$videoo,'video_rate'=>$video_view_rate,'view_rate'=>$view_rate);
	$display=null;
}else{
	$display=array('display_device_impression'=>$display_device_impression,'week_of_day_impression'=>$dis_day_impression,'gender'=>$display_gender,'age_wise'=>$dispaly_age_wise);
	$video=null;
}


$dataa[]=array('camp_id'=>$name['camp_id'],'camp_name'=>$name['campaign_name'],'video'=>$video,'display'=>$display,'start_date'=>$name['start_date'],'camp_type'=>$name['camp_type'],'end_date'=>$name['end_date'],'impp'=>$total_impp);
} 


$date=date('Y-m-d');
foreach($dataa as $result){
	$video=json_encode($result['video']);
	$display=json_encode($result['display']);
	$camp_type=$result['camp_type'];
	$start_date=date('Y-m-d',strtotime($result['start_date']));
	$end_date=date('Y-m-d',strtotime($result['end_date']));
// echo "UPDATE google_campaigns SET display_campaign='".$display."',video_campaign='".$video."',camp_type='".$camp_type."',camp_start_date='".$start_date."',camp_end_date='".$end_date."'  WHERE camp_id='".$result['camp_id']."'";die;
	DB::Select("UPDATE google_monthly_campaigns SET display_campaign='".$display."',video_campaign='".$video."',camp_type='".$result['camp_type']."',camp_start_date='".$start_date."',camp_end_date='".$end_date."'  WHERE camp_id='".$result['camp_id']."'");
}
}
}
/*Close for google data for display and video*/

/*Open for csv upload for facebook video and display impression*/
public function csvUpload6(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		} 
// echo '<pre>';
  // var_dump($importData_arr);
  // die;
		foreach($importData_arr as $data){
			$campaign_name[]=array('campaign_name'=>$data[0]);
		} 
		$input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));
/*echo '<pre>';
var_dump($input);die;*/
foreach($input as $acc){
	$campaign[]=$acc;
}
fclose($file);
// echo '<pre>';
  // var_dump($campaign);
  // die;
foreach($campaign as $name){
	$impression=0;$video_ads=null;$display_ads=null;
	foreach($importData_arr as $data1){
  // OPen if loop
		if($name['campaign_name']==$data1[0]){
			$ad_name=$data1[2];
			$impresison=$impression + $data1[4];
			if(strpos($ad_name, 'Video') !== false || strpos($ad_name, 'VIDEO') !== false || strpos($ad_name, 'video') !== false){
				$video_played=$data1[10];
				$video_ads[]=array('ad_name'=>str_replace("'", '',$ad_name),'impresison'=>$impresison,'video_played'=>$video_played);
			} else{
				$display_ads[]=array('ad_name'=>str_replace("'", '',$ad_name),'impresison'=>$impresison,'video_played'=>NULL);
			}

		}
		/*Close if loop*/
	}
	/*close for loop*/
// $keyword=array('cost'=>$cost,'clicks'=>$clicks,'impressions'=>$impressions);
// echo '<pre>';
  // var_dump($ads);die;
	$dataa[]=array('camp_name'=>$name['campaign_name'],'video_ads'=>$video_ads,'display_ads'=>$display_ads);
} 
// echo '<pre>';
 // var_dump($dataa);
 // die;
$date=date('Y-m-d h:i:s');
 // $date='2020-01-24';
foreach($dataa as $result){
 // $google = new GoogleLifetimeCamp;
 // $jsonkeyword=json_encode($result['keyword']);
	$video=json_encode($result['video_ads']);
	$display=json_encode($result['display_ads']);
	$camp_name=str_replace("'", "\'", $result['camp_name']);

 // echo "UPDATE fb_campaigns SET video_campaign='".$video."',display_campaign='".$display."' WHERE camp_name='".$camp_name."' and camp_date='".$date."'";
// echo '</br>';
	DB::Select("UPDATE fb_lifetime_campaigns SET video_campaign='".$video."',display_campaign='".$display."' WHERE camp_name='".$camp_name."'");
}
}
}
/*Close for csv upload for facebook video and display impressions*/

// FUNCTION TO UPDATE LANDING PAGE FOR ALL USERS
/*public function csvUpload7(Request $request){
  if($request->file('file')){
    $file = $request->file('file');

    $filename = $file->getClientOriginalName();

    $location = 'public';

    $file->move($location,$filename);

    $filepath = public_path($filename);

    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );

      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }

    foreach($importData_arr as $data){
      $campaign_name[]=array('campaign_name'=>$data[0],'landing_page'=>str_replace("/","",$data[1]));
    } 
    $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

    foreach($input as $acc){
      $campaign[]=$acc;
    }
    fclose($file);


    foreach($campaign as $data){
      $id=substr($data['campaign_name'],0,6);

      DB::Select("UPDATE customers_list SET landing_page='".$data['landing_page']."' WHERE customer_id LIKE '".$id."%'");
    }


  }
}*/
/*CLOSE FUNCTION TO UPDATE LANDING PAGE FOR ALL USERS*/


// function to add google time impression
public function csvUpload8(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
    // echo '<pre>';
    // var_dump($importData_arr);
    // die;
		foreach($importData_arr as $data){
			$campaign_name[]=array('campaign_name'=>$data[1],'camp_id'=>$data[0]);
		} 
		$input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));
//  echo '<pre>';
// var_dump($input);die;
		foreach($input as $acc){
			$campaign[]=$acc;
		}
		fclose($file);

		foreach($campaign as $name){
			$video0=0;$video1=0;$video2=0;$video3=0;$video4=0;$video5=0;$video6=0;$video7=0;$video8=0;$video9=0;$video10=0;$video11=0;$video12=0;$video13=0;$video14=0;$video15=0;$video16=0;$video17=0;$video18=0;$video19=0;$video20=0;$video21=0;$video22=0;$video23=0;$display0=0;$display1=0;$display2=0;$display3=0;$display4=0;$display5=0;$display6=0;$display7=0;$display8=0;$display9=0;$display10=0;$display11=0;$display12=0;$display13=0;$display14=0;$display15=0;$display16=0;$display17=0;$display18=0;$display19=0;$display20=0;$display21=0;$display22=0;$display23=0;$display_time_impression=NULL;$video_time_impression=NULL;
			foreach($importData_arr as $data1){
  // OPen if loop
				if($name['campaign_name']==$data1[1]){
  /*echo $name['campaign_name'];
  echo $data1[1];*/
  if(strrpos($name['campaign_name'], 'Video')!==false || strrpos($name['campaign_name'], 'VIDEO')!==false){
  	if($data1[2]=='0'){
  		$video0=$video0 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='1'){
  		$video1=$video1 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='2'){
  		$video2=$video2 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='3'){
  		$video3=$video3 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='4'){
  		$video4=$video4 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='5'){
  		$video5=$video5 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='6'){
  		$video6=$video6 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='7'){
  		$video7=$video7 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='8'){
  		$video8=$video8 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='9'){
  		$video9=$video9 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='10'){
  		$video10=$video10 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='11'){
  		$video11=$video11 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='12'){
  		$video12=$video12 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='13'){
  		$video13=$video13 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='14'){
  		$video14=$video14 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='15'){
  		$video15=$video15 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='16'){
  		$video16=$video16 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='17'){
  		$video17=$video17 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='18'){
  		$video18=$video18 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='19'){
  		$video19=$video19 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='20'){
  		$video20=$video20 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='21'){
  		$video21=$video21 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='22'){
  		$video22=$video22 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='23'){
  		$video23=$video23 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}
  	$video_time_impression=array('0'=>$video0,'1'=>$video1,'2'=>$video2,'3'=>$video3,'4'=>$video4,'5'=>$video5,'6'=>$video6,'7'=>$video7,'8'=>$video8,'9'=>$video9,'10'=>$video10,'11'=>$video11,'12'=>$video12,'13'=>$video13,'14'=>$video14,'15'=>$video15,'16'=>$video16,'17'=>$video17,'18'=>$video18,'19'=>$video19,'20'=>$video20,'21'=>$video21,'22'=>$video22,'23'=>$video23);
  }else{
  	if($data1[2]=='0'){
  		$display0=$display0 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='1'){
  		$display1=$display1 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='2'){
  		$display2=$display2 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='3'){
  		$display3=$display3 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='4'){
  		$display4=$display4 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='5'){
  		$display5=$display5 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='6'){
  		$display6=$display6 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='7'){
  		$display7=$display7 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='8'){
  		$display8=$display8 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='9'){
  		$display9=$display9 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='10'){
  		$display10=$display10 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='11'){
  		$display11=$display11 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='12'){
  		$display12=$display12 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='13'){
  		$display13=$display13 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='14'){
  		$display14=$display14 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='15'){
  		$display15=$display15 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='16'){
  		$display16=$display16 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='17'){
  		$display17=$display17 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='18'){
  		$display18=$display18 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='19'){
  		$display19=$display19 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='20'){
  		$display20=$display20 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='21'){
  		$display21=$display21 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='22'){
  		$display22=$display22 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}elseif($data1[2]=='23'){
  		$display23=$display23 + filter_var($data1[3], FILTER_SANITIZE_NUMBER_INT);
  	}
  	$display_time_impression=array('0'=>$display0,'1'=>$display1,'2'=>$display2,'3'=>$display3,'4'=>$display4,'5'=>$display5,'6'=>$display6,'7'=>$display7,'8'=>$display8,'9'=>$display9,'10'=>$display10,'11'=>$display11,'12'=>$display12,'13'=>$display13,'14'=>$display14,'15'=>$display15,'16'=>$display16,'17'=>$display17,'18'=>$display18,'19'=>$display19,'20'=>$display20,'21'=>$display21,'22'=>$display22,'23'=>$display23);
  }

  $imprssions=array('video_impression'=>$video_time_impression,'display_impression'=>$display_time_impression);
}
/*Close if loop*/
}
/*close for loop*/
// $keyword=array('cost'=>$cost,'clicks'=>$clicks,'impressions'=>$impressions);
/*echo '<pre>';
var_dump($ads);die;*/
$dataa[]=array('camp_id'=>$name['camp_id'],'camp_name'=>$name['campaign_name'],'impressions'=>$imprssions);
} 
// echo '<pre>';
//   var_dump($dataa);
//   die;
$date=date('Y-m-d');
// $date='2020-01-30';
foreach($dataa as $result){

	//$data= DB::Select("SELECT time_impression FROM google_campaigns WHERE camp_id='".$result['camp_id']."'");
  // echo '<pre>';
  // echo "SELECT time_impression FROM google_campaigns WHERE camp_id='".$result['camp_id']."'";

	//if(empty($data[0]->time_impression)){
    // echo '<pre>';
  $time_impressions=json_encode($result['impressions']);
  // echo "UPDATE google_campaigns SET time_impression='".$time_impressions."' WHERE camp_id='".$result['camp_id']."'";
  DB::Select("UPDATE google_monthly_campaigns SET time_impression='".$time_impressions."' WHERE camp_id='".$result['camp_id']."'");
//	}

}
}
}
/*Close function to add google time impressi*/

// function to use groundtruth creatives
public function csvUpload9(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
		$filename = $file->getClientOriginalName();
		$location = 'public';
		$file->move($location,$filename);
		$filepath = public_path($filename);
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
		foreach($importData_arr as $data){
			$campaign_name[]=array('campaign_id'=>$data[0]);
		} 
		$input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

		foreach($input as $acc){
			$campaign[]=$acc;
		}
		fclose($file);
		foreach($campaign as $data){
			$result=NULL;
			foreach($importData_arr as $camp){
				if($data['campaign_id']==$camp[0]){
					$result[]=array('adgroup_name'=>str_replace("'", '', $camp[2]),'creative_url'=>$camp[9],'creative_name'=>str_replace("'", '', $camp[4]),'impression'=>$camp[5],'daily_reach'=>$camp[10]);
				}
			}

			$save[]=array('camp_id'=>$data['campaign_id'],'creative'=>$result);


		}
		foreach($save as $creativess){
			$camp_id=$creativess['camp_id'];
			$creative=json_encode($creativess['creative']);

			DB::Select("UPDATE groundtruth_campaigns SET ad_creative_image='".$creative."' WHERE camp_id='".$camp_id."'");
		}
	}
}
// Close function to use groundtruth creatives


// function to add youtube url for google
public function csvUpload10(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
// echo '<pre>';
//   var_dump($importData_arr);
//   die;
		foreach($importData_arr as $data){
			$campaign_name[]=array('campaign_id'=>$data[5]);
		} 
		$input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

		foreach($input as $acc){
			$campaign[]=$acc;
		}
		fclose($file);
// echo '<pre>';
//   var_dump($campaign);
//    die;

		foreach($campaign as $data){
			$result=NULL;
			foreach($importData_arr as $camp){
				if($data['campaign_id']==$camp[5]){
					$result[]=array('ad_name'=>$camp[1],'video_url'=>$camp[0]);
				}
			}

			$save[]=array('camp_id'=>$data['campaign_id'],'url'=>$result);


		}
    // echo '<pre>';
    // var_dump($save);
    // die;
		foreach($save as $creativess){
      //var_dump($creativess);
			$camp_id=$creativess['camp_id'];
			//$data= DB::Select("SELECT youtube_link FROM google_campaigns WHERE camp_id='".$camp_id."'");

			//if(empty($data[0]->youtube_link)){

      $video=json_encode($creativess['url']);
      DB::Select("UPDATE google_campaigns SET youtube_link='".$video."' WHERE camp_id='".$camp_id."'");
		//	}

    }
  }
}
/*Close to function to add youtube url for google*/
// function to use add google targeted location
public function csvUpload11(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
// echo '<pre>';
//   var_dump($importData_arr);
//   die;
		foreach($importData_arr as $data){
			$campaign_name[]=array('campaign_name'=>substr($data[1],0,6));

		} 
		$input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

		foreach($input as $acc){
			$campaign[]=$acc;
		}
		fclose($file);
// echo '<pre>';
//   var_dump($campaign);
//    die;

		foreach($campaign as $data){
			$result=NULL;
			foreach($importData_arr as $camp){
				if($data['campaign_name']==substr($camp[1],0,6)){
					$result[]=array('location'=>str_replace("'", "", $camp[0]));
				}
			}

			$save[]=array('camp_name'=>str_replace("'s", "\'s", $data['campaign_name']),'location'=>$result);


		}
    // echo '<pre>';
    // var_dump($save);
    // die;
		foreach($save as $creativess){
			$camp_name=$creativess['camp_name'];
		//	$data= DB::Select("SELECT target_location FROM google_campaigns WHERE cust_id='".$camp_name."'");

			//if(empty($data[0]->target_location)){


      $location=json_encode($creativess['location']);
      // echo '<pre>';
      // echo "UPDATE google_campaigns SET target_location='".$location."' WHERE cust_id='".$camp_name."'";
      DB::Select("UPDATE google_campaigns SET target_location='".$location."' WHERE cust_id='".$camp_name."'");
		//	}



    }
  }
}

//Close  function to use add google targeted location

// add all placement google placement report
public function csvUpload12(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}
    // echo '<pre>';
    // var_dump($importData_arr);
    // die;


		fclose($file);
/* echo '<pre>';
  var_dump($campaign);
  die;*/
  
// echo '<pre>';
//   var_dump($dataa);
//   die;
  $date=date('Y-m-d');
  foreach($importData_arr as $data){
    $data=array('camp_id'=>$data[0],"camp_name"=>$data[1],"placement_group"=>$data[2],"placement_type"=>$data[3],'impression'=>$data[5]);
    DB::table('google_monthly_placement_rep')->insert($data);


  }
}
}
 //Close all placement google placement report 

// function to upload GT creatives
public function csvUpload13(Request $request){
	if($request->file('file')){
		$file = $request->file('file');
// File Details 
		$filename = $file->getClientOriginalName();
// File upload location
		$location = 'public';
// Upload file
		$file->move($location,$filename);
// Import CSV to Database
		$filepath = public_path($filename);
// Reading file
		$file = fopen($filepath,"r");
		$importData_arr = array();
		$i = 0;
		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
			if($i == 0){
				$i++;
				continue; 
			}
			for ($c=0; $c < $num; $c++) {
				$importData_arr[$i][] = $filedata [$c];
			}
			$i++;
		}



		fclose($file);
/* echo '<pre>';
  var_dump($campaign);
  die;*/
  
/*echo '<pre>';
  var_dump($dataa);
  die;*/

  foreach($importData_arr as $data){
  	$convertdate=strtotime($data[0]);
  	$date=date('Y-m-d',$convertdate);
  	$data=array('camp_id'=>$data[3],"creative_id"=>$data[7],"creative_url"=>$data[27],"creative_name"=>$data[8],'impression'=>$data[13],'daily_reach'=>$data[28],'creative_date'=>$date);
  	DB::table('gt_creatives')->insert($data);
  }
}
}


// Close to function for GT creatives


// open for add basis device report
public function csvUpload20(Request $request){
  if($request->file('file')){
    $file = $request->file('file');
// File Details 
    $filename = $file->getClientOriginalName();
// File upload location
    $location = 'public';
// Upload file
    $file->move($location,$filename);
// Import CSV to Database
    $filepath = public_path($filename);
// Reading file
    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }
// echo '<pre>';
//   var_dump($importData_arr);
//   die;
    foreach($importData_arr as $data){
      $campaign_name[]=array('campaign_date'=>$data[0],'camp_name'=>$data[1]);

    } 
    $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

    foreach($input as $acc){
      $campaign[]=$acc;
    }
    fclose($file);
// echo '<pre>';
//   var_dump($campaign);
//    die;

    foreach($campaign as $data){
      $result=NULL;
      foreach($importData_arr as $camp){
        if($data['campaign_date']==$camp[0] && $data['camp_name']==$camp[1]){
          $result[]=array('device_type'=>$camp[8],'imp'=>filter_var($camp[9], FILTER_SANITIZE_NUMBER_INT),'video_start'=>filter_var($camp[24], FILTER_SANITIZE_NUMBER_INT));
        }
      }

      $save[]=array('camp_date'=>$data['campaign_date'],'camp_name'=>$data['camp_name'],'result'=>$result);


    }
    // echo '<pre>';
    // var_dump($save);
    // die;
    foreach($save as $creativess){
      $camp_date=$creativess['camp_date'];
      $camp_name=str_replace("'", "\'", $creativess['camp_name']);
      //if(empty($data[0]->target_location)){
      $results=NULL;
      foreach($creativess['result'] as $result1){
       $results[]=array('device_type'=>$result1['device_type'],'imp'=>$result1['imp'],'video_start'=>$result1['video_start']);
       $location=json_encode($results);
     }
     
     

     //echo "UPDATE basis_campaigns SET device_type='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'";
     $update=DB::Select("UPDATE basis_campaigns SET device_type='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'");

//echo '<br>';

     //break;
   }
 }
}

// close for add basis device report
// open for video views of basis
public function csvUpload21(Request $request){
  if($request->file('file')){
    $file = $request->file('file');
// File Details 
    $filename = $file->getClientOriginalName();
// File upload location
    $location = 'public';
// Upload file
    $file->move($location,$filename);
// Import CSV to Database
    $filepath = public_path($filename);
// Reading file
    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }
// echo '<pre>';
//   var_dump($importData_arr);
//   die;
    foreach($importData_arr as $data){
      $campaign_name[]=array('campaign_date'=>$data[0],'camp_name'=>$data[1]);

    } 
    $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

    foreach($input as $acc){
      $campaign[]=$acc;
    }
    fclose($file);
// echo '<pre>';
//   var_dump($campaign);
//    die;

    foreach($campaign as $data){
      $result=NULL;
      foreach($importData_arr as $camp){
        if($data['campaign_date']==$camp[0] && $data['camp_name']==$camp[1]){
          $result[]=array('video_25'=>filter_var($camp[25], FILTER_SANITIZE_NUMBER_INT),'video_50'=>filter_var($camp[26], FILTER_SANITIZE_NUMBER_INT),'video_75'=>filter_var($camp[27], FILTER_SANITIZE_NUMBER_INT),'video_100'=>filter_var($camp[28], FILTER_SANITIZE_NUMBER_INT));
        }
      }

      $save[]=array('camp_date'=>$data['campaign_date'],'camp_name'=>$data['camp_name'],'result'=>$result);


    }
    // echo '<pre>';
    // var_dump($save);
    // die;
    foreach($save as $creativess){
      $camp_date=$creativess['camp_date'];
      $camp_name=str_replace("'", "\'", $creativess['camp_name']);
      //if(empty($data[0]->target_location)){
      $results=NULL;
      foreach($creativess['result'] as $result1){
       $results[]=array('video_25'=>$result1['video_25'],'video_50'=>$result1['video_50'],'video_75'=>$result1['video_75'],'video_100'=>$result1['video_100']);
       $location=json_encode($results);
     }
     // echo '<pre>';

     // echo $location;
     

    // echo "UPDATE basis_campaigns SET video_views='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'";
     $update=DB::Select("UPDATE basis_campaigns SET video_views='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'");



     //break;
   }
 }
}
// Close for video views of basis
// OPen for basis location zipcode
public function csvUpload22(Request $request){
  if($request->file('file')){
    $file = $request->file('file');
// File Details 
    $filename = $file->getClientOriginalName();
// File upload location
    $location = 'public';
// Upload file
    $file->move($location,$filename);
// Import CSV to Database
    $filepath = public_path($filename);
// Reading file
    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }
// echo '<pre>';
//   var_dump($importData_arr);
//   die;
    foreach($importData_arr as $data){
      $campaign_name[]=array('campaign_date'=>$data[0],'camp_name'=>$data[1]);

    } 
    $input = array_map("unserialize", array_unique(array_map("serialize", $campaign_name)));

    foreach($input as $acc){
      $campaign[]=$acc;
    }
    fclose($file);
// echo '<pre>';
//   var_dump($campaign);
//    die;

    foreach($campaign as $data){
      $result=NULL;
      foreach($importData_arr as $camp){
        if($data['campaign_date']==$camp[0] && $data['camp_name']==$camp[1]){
          $result[]=array('city'=>str_replace("'", "\'", $camp[10]),'zip_code'=>$camp[11],'imp'=>filter_var($camp[12], FILTER_SANITIZE_NUMBER_INT),'video_views'=>filter_var($camp[30], FILTER_SANITIZE_NUMBER_INT));
        }
      }

      $save[]=array('camp_date'=>$data['campaign_date'],'camp_name'=>$data['camp_name'],'result'=>$result);


    }
    // echo '<pre>';
    // var_dump($save);
    // die;
    foreach($save as $creativess){
      $camp_date=$creativess['camp_date'];
      $camp_name=str_replace("'", "\'", $creativess['camp_name']);
      //if(empty($data[0]->target_location)){
      $results=NULL;
      foreach($creativess['result'] as $result1){
       $results[]=array('city'=>$result1['city'],'zip_code'=>$result1['zip_code'],'imp'=>$result1['imp'],'video_views'=>$result1['video_views']);
       $location=json_encode($results);
     }
     // echo '<pre>';

     // echo $location;
     

    // echo "UPDATE basis_campaigns SET video_views='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'";
     $update=DB::Select("UPDATE basis_campaigns SET location_zipcode='".$location."' WHERE camp_name='".$camp_name."' and camp_date='".$camp_date."'");

     if(!empty($update)){
      continue;
    }

     //break;
  }
}
}
// close for basis location zipcode


// add basis placement report
public function csvUpload15(Request $request){
  if($request->file('file')){
    $file = $request->file('file');
// File Details 
    $filename = $file->getClientOriginalName();
// File upload location
    $location = 'public';
// Upload file
    $file->move($location,$filename);
// Import CSV to Database
    $filepath = public_path($filename);
// Reading file
    $file = fopen($filepath,"r");
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
      $num = count($filedata );
//Skip first row (Remove below comment if you want to skip the first row)
      if($i == 0){
        $i++;
        continue; 
      }
      for ($c=0; $c < $num; $c++) {
        $importData_arr[$i][] = $filedata [$c];
      }
      $i++;
    }
    


    fclose($file);

    $date=date('Y-m-d');
    foreach($importData_arr as $data){
      $data=array('date'=>$data[0],"camp_name"=>$data[4],"domain"=>$data[9],'impression'=>$data[15]);
      DB::table('basis_placement')->insert($data);


    }
  }
}

// Close add basis placement report


// open for amount spent for platform

public function csvUpload25(Request $request){
  $date1='2020-08-01';
  $date2='2020-08-31';
  $select=DB::Select("SELECT * FROM basis_campaigns WHERE camp_date BETWEEN '".$date1."' and '".$date2."'");
  foreach($select as $data){
    $camp_name=$data->camp_name;
    $keyword=json_decode($data->keyword);
    $spend=$keyword->delivered_data_spend;
    $month='08';
    $year='20';
    $data=array('date'=>$month.'-'.$year,"camp_name"=>$camp_name,'spent'=>$spend,'platform'=>'basis');
    DB::table('platform_spent')->insert($data);

  }
  

}
// close for amount spent platform

public function home(Request $request,$id)
{

	if(Auth::guard('user')->check()){
		// $exists = Storage::disk('s3')->exists('https://s3.us-west-2.amazonaws.com/basis-cloud-ingestion/AKIAV33XP2DQJW6X6JXC');
		// Storage::disk('s3')->files('https://s3.us-west-2.amazonaws.com/basis-cloud-ingestion/AKIAV33XP2DQJW6X6JXC');
		// die;
		$camp_type=$request->camp_type;
		if($request->camp_type=='0'){
    // echo "SELECT * FROM fb_campaigns where cust_id='".$id."' and camp_date=(SELECT max(camp_date) FROM fb_campaigns)";die;
			$fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
			$googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");
			$grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
      $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
      $target_location=DB::Select("SELECT target_location FROM google_campaigns WHERE cust_id='".$id."' and target_location is not NULL GROUP BY cust_id");
      $gt_creative=DB::Select("SELECT gt_creatives.* FROM groundtruth_campaigns INNER JOIN gt_creatives ON groundtruth_campaigns.camp_id=gt_creatives.camp_id WHERE groundtruth_campaigns.cust_id='".$id."' and gt_creatives.creative_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() GROUP BY gt_creatives.id");
      $google_start_date=DB::Select("SELECT camp_start_date FROM google_monthly_campaigns WHERE cust_id='".$id."' and camp_start_date=(SELECT MIN(camp_start_date) FROM google_monthly_campaigns WHERE cust_id='".$id."')");
      $google_end_date=DB::Select("SELECT camp_end_date FROM google_monthly_campaigns WHERE cust_id='".$id."' and camp_end_date=(SELECT MAX(camp_end_date) FROM google_monthly_campaigns WHERE cust_id='".$id."')");
    }else{
     $fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$id."'");

     $googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."'");

     $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");
     $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");
     $b_name=DB::Select("SELECT DISTINCT camp_name,camp_id FROM fb_campaigns where cust_id='".$id."'");
     $target_location=DB::Select("SELECT target_location FROM google_campaigns WHERE cust_id='".$id."' and target_location is not NULL GROUP BY cust_id");
     $gt_creative=DB::Select("SELECT gt_creatives.* FROM groundtruth_campaigns INNER JOIN gt_creatives ON groundtruth_campaigns.camp_id=gt_creatives.camp_id WHERE groundtruth_campaigns.cust_id='".$id."' GROUP BY gt_creatives.id");
     $google_start_date=DB::Select("SELECT camp_start_date FROM google_campaigns WHERE cust_id='".$id."' and camp_start_date=(SELECT MIN(camp_start_date) FROM google_campaigns WHERE cust_id='".$id."')");
     $google_end_date=DB::Select("SELECT camp_end_date FROM google_campaigns WHERE cust_id='".$id."' and camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".$id."')");
   }


   return view('userdashboard',compact('fbresult','googleresult','grndresult','b_name','grndadminresult','target_location','camp_type','gt_creative','basisresult','google_start_date','google_end_date'));
 }else{
  return Redirect::to('userlogin');
}

}
/**
* Show the form for creating a new user
*
* @return \Illuminate\View\View
*/
/*function to use show ad creatives*/
public function adCreatives(){
	if(Auth::guard('user')->check()){
		$cust_id=Auth::guard('user')->user()->customer_id;

		$data=DB::Select("SELECT DISTINCT(camp_name),ad_creative_image FROM fb_admin_campaigns WHERE cust_id='".trim($cust_id)."'");

   // echo "SELECT camp_name,ad_creative_image FROM groundtruth_campaigns WHERE cust_id='".trim($cust_id)."' and camp_date=(SELECT MAX(camp_date) FROM groundtruth_campaigns)";die;
		$grnd=DB::Select("SELECT camp_name,ad_creative_image FROM groundtruth_campaigns WHERE cust_id='".trim($cust_id)."' and camp_date=(SELECT MAX(camp_date) FROM groundtruth_campaigns WHERE cust_id='".trim($cust_id)."')");
		$google=DB::Select("SELECT youtube_link FROM google_campaigns WHERE cust_id='".trim($cust_id)."'");
		return view('campaigns.ad_creative',compact('data','grnd','google'));
	}else{
		return Redirect::to('userlogin');
	}

}
public function create()
{
	return view('users.create');
}
public function adsVideo(){
	if(Auth::guard('user')->check()){
		return view('campaigns.ad_video');
	}else{
		return Redirect::to('userlogin');
	}

}
public function userCampaignList($id){
	if(Auth::guard('user')->check()){
		$today=date('Y-m-d');
		$fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$id."'");
		$googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."'");
		$grndresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");
		$fb_camp=DB::Select("SELECT DISTINCT camp_name,camp_id FROM fb_campaigns where cust_id='".$id."'");
		$google_camp=DB::Select("SELECT DISTINCT camp_name,camp_id FROM google_campaigns where cust_id='".$id."'");
		$groundtruth_camp=DB::Select("SELECT DISTINCT camp_name,camp_id FROM groundtruth_campaigns where cust_id='".$id."'");
		return view('campaigns.usercampaign_list',compact('fbresult','googleresult','grndresult','bussiness_name','fb_camp','google_camp','groundtruth_camp','id'));
	}else{
		return Redirect::to('userlogin');
	}

}
public function userCampaignListbyid(Request $request){
	if($request->ajax()){
if($request->data_type=='1'){//1 for lifetime
	if($request->id=='all'){
		$fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$request->name."'");
		$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->name."' ORDER BY camp_date ASC");
		$google=DB::Select("SELECT * FROM google_campaigns where cust_id='".$request->name."'");
	}else{
		if($request->account_type=='facebook'){
			$fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where camp_id='".$request->id."'");
		}
		elseif($request->account_type=='google'){
			$google=DB::Select("SELECT * FROM google_campaigns where camp_id='".$request->id."'");
		}else{
			$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where camp_id='".$request->id."' ORDER BY camp_date ASC");
		}
	}
}else{//2 for monthly
	if($request->id=='all'){
		$fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$request->name."'");
		$google=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$request->name."'");
		$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->name."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
	}else{
		if($request->account_type=='facebook'){
			$fbresult=DB::Select("SELECT * FROM fb_campaigns where camp_id='".$request->id."'");
		}
		elseif($request->account_type=='google'){
			$google=DB::Select("SELECT * FROM google_monthly_campaigns where camp_id='".$request->id."'");
		}else{
			$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where camp_id='".$request->id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
		}
	}
}
$total_spend=0;
$total_clicks=0;
$total_impression=0;
$total_ctr=0;
$total_reach=0;$fb_video_impression=0;$fb_display_impression=0;
if(!empty($fbresult)){
	foreach($fbresult as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->spend;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$reach=$keyword->reach;
			$total_spend=$total_spend + $spend;
			$total_clicks=$total_clicks + $clicks;
      // $total_impression=$total_impression + $impressions;
			$total_ctr=$total_ctr + $ctr;
			$total_reach=$total_reach + $reach;
		}
		if(!empty($campaign->video_campaign)){
			if($campaign->video_campaign!=='null'){
				$fbimpression=json_decode($campaign->video_campaign);
				if(!empty($fbvideoimpression)){
					foreach($fbvideoimpression as $fbvideo){
						$fb_video_impression=$fb_video_impression + $fbvideo->impresison;
					}
				}
			}
		}
		if(!empty($campaign->display_campaign)){
			if($campaign->display_campaign!=='null'){
				$fbdisplayimpression=json_decode($campaign->display_campaign);
				if(!empty($fbdisplayimpression)){
					foreach($fbdisplayimpression as $fbdisplay){
						$fb_display_impression=$fb_display_impression + $fbdisplay->impresison;
					}
				}
			}
		}
	}
	$total_impression= $total_impression +  $fb_video_impression +  $fb_display_impression;
}
$total_spend1=0;
$total_clicks1=0;
$total_impression1=0;
$total_ctr1=0;
$total_reach1=0;
if(!empty($grndtruth)){
	foreach($grndtruth as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->spend;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$reach=$keyword->cumulative_reach;
			$total_spend1=$total_spend1 + $spend;
			$total_clicks1=$total_clicks1 + $clicks;
			$total_impression1=$total_impression1 + $impressions;
			$total_ctr1=$total_ctr1 + $ctr;
			$total_reach1=$total_reach1 + $reach;
		}
	}
}
$total_spend2=0;
$total_clicks2=0;
$total_impression2=0;
$total_ctr2=0;
$total_budget2=0;$sunday_video_impression=0;$sunday_video_view=0;$monday_video_impression=0;$monday_video_view=0;$tuesday_video_impression=0;$tuesday_video_view=0;$wednesday_video_impression=0;$wednesday_video_view=0;$thrusday_video_impression=0;$thrusday_video_view=0;$friday_video_impression=0;$friday_video_view=0;$saturday_video_impresison=0;$saturday_video_view=0;  $video_impression=0;$video_views=0;$video_rate=0;
if(!empty($google)){
	foreach($google as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->cost;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$budget=$keyword->budget;
			$total_spend2=$total_spend2 + $spend;
			$total_clicks2=$total_clicks2 + $clicks;
			$total_impression2=$total_impression2 + $impressions;
			$total_ctr2=$total_ctr2 + $ctr;
			$total_budget2=$total_budget2 + $budget;
			if(!empty($campaign->video_campaign)){
				if($campaign->video_campaign!=='null'){
					$video=json_decode($campaign->video_campaign);
					$sunday_video_impression=$sunday_video_impression + $video->video->sun->sun_impression;
					$sunday_video_view=$sunday_video_view + $video->video->sun->sun_views;
					$monday_video_impression=$monday_video_impression + $video->video->mon->mon_impression;
					$monday_video_view=$monday_video_view + $video->video->mon->mon_views;
					$tuesday_video_impression=$tuesday_video_impression + $video->video->tue->tue_impression;
					$tuesday_video_view=$tuesday_video_view + $video->video->tue->tue_views;
					$wednesday_video_impression=$wednesday_video_impression + $video->video->wed->wed_impression;
					$wednesday_video_view=$wednesday_video_view + $video->video->wed->wed_views;
					$thrusday_video_impression=$thrusday_video_impression + $video->video->thru->thru_impression;
					$thrusday_video_view=$thrusday_video_view + $video->video->thru->thru_views;
					$friday_video_impression=$friday_video_impression + $video->video->fri->fri_impression;
					$friday_video_view=$friday_video_view + $video->video->fri->fri_views;
					$saturday_video_impresison=$saturday_video_impresison + $video->video->sat->sat_impression;
					$saturday_video_view=$saturday_video_view + $video->video->sat->sat_views;
					$total_video_impression=$sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;

					$video_impression= $sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
					$video_views=$sunday_video_view + $monday_video_view + $tuesday_video_view + $wednesday_video_view + $thrusday_video_view + $friday_video_view + $saturday_video_view;
					$video_rate=round($video_views/$video_impression *100, 2);

				}
			}


		}
	}
}
$grndtrth=array('total_spend'=>round($total_spend1, 2),'total_clicks'=>$total_clicks1,'total_impression'=>$total_impression1,'total_ctr'=>round($total_ctr1, 2),'total_reach'=>$total_reach1);
$fbdata=array('total_spend'=>round($total_spend, 2),'total_clicks'=>$total_clicks,'total_impression'=>$total_impression,'total_ctr'=>round($total_ctr, 2),'total_reach'=>$total_reach);
$google=array('total_cost'=>$total_spend2,'total_clicks'=>$total_clicks2,'total_impression'=>$total_impression2,'total_ctr'=>round($total_ctr2, 2),'total_budget'=>$total_budget2,'video_impression'=>$video_impression,'video_views'=>$video_views,'video_rate'=>$video_rate);
$total_data=array('groundtruth'=>$grndtrth,'facebook'=>$fbdata,'google'=>$google);
echo json_encode($total_data);
}
}
// function to use date wise data
public function userCampaignListbyDate(Request $request){
	if($request->ajax()){
		if($request->camp_id=='all'){
if($request->type=='1'){  // 1 for all time data
// echo "SELECT * FROM fb_lifetime_campaigns where cust_id='".$request->id."' and camp_date=(SELECT max(camp_date) FROM fb_lifetime_campaigns)";die;
	$fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$request->id."'");
	$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->id."' ORDER BY camp_date ASC");
	$google=DB::Select("SELECT * FROM google_campaigns where cust_id='".$request->id."'");
  $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$request->id."' ORDER BY camp_date ASC");
}else{ // 2 for last 30 days data
// echo "SELECT * FROM groundtruth_admin_campaigns where cust_id='".$request->id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()";die;
	$fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$request->id."'");
	$google=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$request->id."'");
	$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
  $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$request->id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
}
}else{

	if($request->account_type=='facebook'){
		if($request->type=='1'){
			$fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$request->id."' and camp_id='".$request->camp_id."'");
		}else{
			$fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$request->id."' and camp_id='".$request->camp_id."'");
		}
	}elseif($request->account_type=='google'){
		if($request->type=='1'){
			$google=DB::Select("SELECT * FROM google_campaigns where cust_id='".$request->id."' and camp_id='".$request->camp_id."'");
		}else{
			$google=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$request->id."' and camp_id='".$request->camp_id."'");
		}
	}else{
		if($request->type=='1'){
			$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->id."' and camp_id='".$request->cmap_id."' ORDER BY camp_date ASC");
		}else{
			$grndtruth=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->id."' and camp_id='".$request->camp_id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
		}
	}
}
}
$total_spend=0;
$total_clicks=0;
$total_impression=0;
$total_ctr=0;
$total_reach=0;$fb_video_impression=0;$fb_display_impression=0;$basis_impression=0;

if(!empty($fbresult)){
	foreach($fbresult as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->spend;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$reach=$keyword->reach;
			$total_spend=$total_spend + $spend;
			$total_clicks=$total_clicks + $clicks;
      // $total_impression=$total_impression + $impressions;
			$total_ctr=$total_ctr + $ctr;
			$total_reach=$total_reach + $reach;
		}
		if(!empty($campaign->video_campaign)){
			if($campaign->video_campaign!=='null'){
				$fbvideoimpression=json_decode($campaign->video_campaign);
				if(!empty($fbvideoimpression)){
					foreach($fbvideoimpression as $fbvideo){
						$fb_video_impression=$fb_video_impression + $fbvideo->impresison;
					}
				}
			}
		}
		if(!empty($campaign->display_campaign)){
			if($campaign->display_campaign!=='null'){
				$fbdisplayimpression=json_decode($campaign->display_campaign);
				if(!empty($fbdisplayimpression)){
					foreach($fbdisplayimpression as $fbdisplay){
						$fb_display_impression=$fb_display_impression + $fbdisplay->impresison;
					}
				}
			}
		}
	}
	$total_impression= $total_impression +  $fb_video_impression +  $fb_display_impression;
}

$total_spend1=0;
$total_clicks1=0;
$total_impression1=0;
$total_ctr1=0;
$total_reach1=0;
if(!empty($grndtruth)){
	foreach($grndtruth as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->spend;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$reach=$keyword->cumulative_reach;
			$total_spend1=$total_spend1 + $spend;
			$total_clicks1=$total_clicks1 + $clicks;
			$total_impression1=$total_impression1 + $impressions;
			$total_ctr1=$total_ctr1 + $ctr;
			$total_reach1=$total_reach1 + $reach;
		}
	}
}

$total_spend2=0;
$total_clicks2=0;
$total_impression2=0;
$total_ctr2=0;
$total_budget2=0;$sunday_video_impression=0;$sunday_video_view=0;$monday_video_impression=0;$monday_video_view=0;$tuesday_video_impression=0;$tuesday_video_view=0;$wednesday_video_impression=0;$wednesday_video_view=0;$thrusday_video_impression=0;$thrusday_video_view=0;$friday_video_impression=0;$friday_video_view=0;$saturday_video_impresison=0;$saturday_video_view=0;  $video_impression=0;$video_views=0;$video_rate=0;

if(!empty($google)){
	foreach($google as $campaign){
		if(!empty($campaign->keyword)){
			$keyword=json_decode($campaign->keyword);
			$spend=$keyword->cost;
			$clicks=$keyword->clicks;
			$impressions=$keyword->impressions;
			$ctr=$keyword->ctr;
			$budget=$keyword->budget;
			$total_spend2=$total_spend2 + $spend;
			$total_clicks2=$total_clicks2 + $clicks;
			$total_impression2=$total_impression2 + $impressions;
			$total_ctr2=$total_ctr2 + $ctr;
			$total_budget2=$total_budget2 + $budget;
// var_dump($campaign->video_campaign);
			if($campaign->camp_type=='Video'){

				if(!empty($video)){
         // var_dump($video);
					if($campaign->video_campaign!=='null'){
						$video=json_decode($campaign->video_campaign);
						$sunday_video_impression=$sunday_video_impression + $video->video->sun->sun_impression;
						$sunday_video_view=$sunday_video_view + $video->video->sun->sun_views;
						$monday_video_impression=$monday_video_impression + $video->video->mon->mon_impression;
						$monday_video_view=$monday_video_view + $video->video->mon->mon_views;
						$tuesday_video_impression=$tuesday_video_impression + $video->video->tue->tue_impression;
						$tuesday_video_view=$tuesday_video_view + $video->video->tue->tue_views;
						$wednesday_video_impression=$wednesday_video_impression + $video->video->wed->wed_impression;
						$wednesday_video_view=$wednesday_video_view + $video->video->wed->wed_views;
						$thrusday_video_impression=$thrusday_video_impression + $video->video->thru->thru_impression;
						$thrusday_video_view=$thrusday_video_view + $video->video->thru->thru_views;
						$friday_video_impression=$friday_video_impression + $video->video->fri->fri_impression;
						$friday_video_view=$friday_video_view + $video->video->fri->fri_views;
						$saturday_video_impresison=$saturday_video_impresison + $video->video->sat->sat_impression;
						$saturday_video_view=$saturday_video_view + $video->video->sat->sat_views;
						$total_video_impression=$sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
						$video_impression= $sunday_video_impression + $monday_video_impression + $tuesday_video_impression + $wednesday_video_impression + $thrusday_video_impression + $friday_video_impression + $saturday_video_impresison;
						$video_views=$sunday_video_view + $monday_video_view + $tuesday_video_view + $wednesday_video_view + $thrusday_video_view + $friday_video_view + $saturday_video_view;
						$video_rate=round($video_views/$video_impression *100, 2);
					}

				}


			}

		}
	}
}

if(!empty($basisresult)){
  foreach($basisresult as $campaign){
    if(!empty($campaign->keyword)){
      $basisimpression=json_decode($campaign->keyword);
      $basis_impression=$basis_impression + $basisimpression->delivered_impressions;
    }
  }
}


$grndtrth=array('total_spend'=>round($total_spend1, 2),'total_clicks'=>$total_clicks1,'total_impression'=>$total_impression1,'total_ctr'=>round($total_ctr1, 2),'total_reach'=>$total_reach1);
$fbdata=array('total_spend'=>round($total_spend, 2),'total_clicks'=>$total_clicks,'total_impression'=>$total_impression,'total_ctr'=>round($total_ctr, 2),'total_reach'=>$total_reach);
$google=array('total_cost'=>$total_spend2,'total_clicks'=>$total_clicks2,'total_impression'=>$total_impression2,'total_ctr'=>round($total_ctr2, 2),'total_budget'=>$total_budget2,'video_impression'=>$video_impression,'video_views'=>$video_views,'video_rate'=>$video_rate);
$basis=array('impression'=>$basis_impression);
$total_data=array('groundtruth'=>$grndtrth,'facebook'=>$fbdata,'google'=>$google,'basis'=>$basis);
echo json_encode($total_data);
}
public function campaignReport($id){
// echo $id;die;
  if(Auth::guard('user')->check()){
    $fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where  cust_id='".$id."'");
    $all='ALL VERSIONS';
    $googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."'");
    $grndresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'");
    $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");
// echo "SELECT * FROM groundtruth_admin_campaigns where cust_id='".$id."'";die;
    $b_name=DB::Select("SELECT DISTINCT camp_name,camp_id FROM fb_campaigns where cust_id='".$id."'");
    return view('campaigns.camp_report',compact('fbresult','googleresult','grndresult','bussiness_name','b_name','id','all','grndadminresult'));
  }else{
    return Redirect::to('userlogin');
  }


}

// for google video ad placement for lifetime
public function downloadCsvVideo(){
  $id=Auth::guard('user')->user()->customer_id;
// echo "SELECT * FROM google_campaigns where cust_id='".$id."' and camp_type='Video' and camp_date=(SELECT max(camp_date) FROM google_campaigns)";die;
  $googleresult=DB::Select("SELECT google_life_placement_rep.*,google_campaigns.camp_id FROM google_campaigns INNER JOIN google_life_placement_rep ON google_campaigns.camp_id=google_life_placement_rep.camp_id where google_campaigns.cust_id='".$id."' and google_campaigns.camp_type='Video'");
  foreach($googleresult as $data){
    $report[]=array('placement_group'=>$data->placement_group,'ad_impression'=>$data->impression,'placement_type'=>$data->placement_type);
  }



// file name for download
  $fileName = "videoreport" . date('Ymd') . ".xls";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}
// for monthly report download
public function monthlydownloadCsvVideo(){
  $id=Auth::guard('user')->user()->customer_id;

// echo "SELECT * FROM google_monthly_campaigns where cust_id='".$id."' and camp_type='Video' and camp_date=(SELECT max(camp_date) FROM google_monthly_campaigns)";die;
  $googleresult=DB::Select("SELECT google_monthly_placement_rep.*,google_monthly_campaigns.camp_id FROM google_monthly_campaigns INNER JOIN google_monthly_placement_rep ON google_monthly_campaigns.camp_id=google_monthly_placement_rep.camp_id where google_monthly_campaigns.cust_id='".$id."' and google_monthly_campaigns.camp_type='Video'");
// echo '<pre>';
  // var_dump($googleresult);

  foreach($googleresult as $data){
    $report[]=array('placement_group'=>$data->placement_group,'ad_impression'=>$data->impression,'placement_type'=>$data->placement_type);
  }
// var_dump($ads[0]);
// die;


// file name for download
  $fileName = "Videoreport" . date('Ymd') . ".csv";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");
  
  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}

// for monthly basis download report
public function monthlydownloadCsvbasis(){
  $id=Auth::guard('user')->user()->customer_id;

// echo "SELECT * FROM google_monthly_campaigns where cust_id='".$id."' and camp_type='Video' and camp_date=(SELECT max(camp_date) FROM google_monthly_campaigns)";die;
  $googleresult=DB::Select("SELECT domain,impression FROM basis_placement WHERE camp_name LIKE '%".$id."%' and DATE(date) > (NOW() - INTERVAL 30 DAY)");
// echo '<pre>';
  // var_dump($googleresult);

  foreach($googleresult as $data){
    $report[]=array('domain'=>$data->domain,'ad_impression'=>$data->impression);
  }
// var_dump($ads[0]);
// die;


// file name for download
  $fileName = "BasisPlacementReport" . date('Ymd') . ".csv";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");
  
  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}
// for Lifetime basis download report
public function lifetimedownloadCsvbasis(){
  $id=Auth::guard('user')->user()->customer_id;

// echo "SELECT * FROM google_monthly_campaigns where cust_id='".$id."' and camp_type='Video' and camp_date=(SELECT max(camp_date) FROM google_monthly_campaigns)";die;
  $googleresult=DB::Select("SELECT domain,impression FROM basis_placement WHERE camp_name LIKE '%".$id."%'");
// echo '<pre>';
  // var_dump($googleresult);

  foreach($googleresult as $data){
    $report[]=array('domain'=>$data->domain,'ad_impression'=>$data->impression);
  }
// var_dump($ads[0]);
// die;


// file name for download
  $fileName = "BasisPlacementReport" . date('Ymd') . ".csv";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");
  
  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}

// for google display ad placement for lifetime

public function downloadCsvDisplay(){
  $id=Auth::guard('user')->user()->customer_id;
  $googleresult=DB::Select("SELECT google_life_placement_rep.*,google_campaigns.camp_id FROM google_campaigns INNER JOIN google_life_placement_rep ON google_campaigns.camp_id=google_life_placement_rep.camp_id where google_campaigns.cust_id='".$id."' and google_campaigns.camp_type='Display Only'");
  foreach($googleresult as $data){
    $report[]=array('placement_group'=>$data->placement_group,'ad_impression'=>$data->impression,'placement_type'=>$data->placement_type);
  }



// file name for download
  $fileName = "displayreport" . date('Ymd') . ".csv";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
  foreach($report as $row) {
    if(!$flag) {
// display column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
// filter data
// array_walk($row, 'filterData');
    echo implode("\t", array_values($row)) . "\n";

  }

  exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}

// for monthly display download
public function monthlydownloadCsvDisplay(){
  $id=Auth::guard('user')->user()->customer_id;
  $googleresult=DB::Select("SELECT google_monthly_placement_rep.*,google_monthly_campaigns.camp_id FROM google_monthly_campaigns INNER JOIN google_monthly_placement_rep ON google_monthly_campaigns.camp_id=google_monthly_placement_rep.camp_id where google_monthly_campaigns.cust_id='".$id."' and google_monthly_campaigns.camp_type='Display Only'");
  foreach($googleresult as $data){
    $report[]=array('placement_group'=>$data->placement_group,'ad_impression'=>$data->impression,'placement_type'=>$data->placement_type);
  }



// file name for download
  $fileName = "displayreport" . date('Ymd') . ".csv";

// headers for download
  header("Content-Disposition: attachment; filename=\"$fileName\"");
  header("Content-Type: text/csv");

  $output = fopen("php://output", "w");
  $headerDisplayed1 = false;
  foreach ($report as $row) {
   if ( !$headerDisplayed1 ) {
                // Use the keys from $data as the titles
    fputcsv($output, array_keys($row));
    $headerDisplayed1 = true;
  }

  fputcsv($output, $row);
}
fclose($output);

//   $flag = false;
//   foreach($report as $row) {
//     if(!$flag) {
// // display column names as first row
//       echo implode("\t", array_keys($row)) . "\n";
//       $flag = true;
//     }
// // filter data
// // array_walk($row, 'filterData');
//     echo implode("\t", array_values($row)) . "\n";

//   }

exit;
// echo '<pre>';
  // var_dump($ads);
  // echo 'chirag';die;
}




// for add customer api
public function addUser(){
  set_time_limit(0);
  $curl_handle=curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-customers.php');
  curl_setopt($curl_handle, CURLOPT_HEADER, 0);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  $results = curl_exec($curl_handle);
  $results = json_decode($results);
// echo '<pre>';
  // var_dump($results);
  // die;

  foreach($results as $data){
  // for deal no
    $CSOFF= $data->CSOFF;
    $CSMKT=$data->CSMKT;
    $CSSUFX=$data->CSSUFX;
  // for BUSSINESS name
    $CSBNAM=trim($data->CSBNAM);
  // for posted by
    $CIENBY=trim($data->CIENBY);
  // for customer id
    $CINUM=trim($data->CINUM);
  // for customer
    $CICNAM=trim($data->CICNAM);
  // email address
    $CIEMAL=trim($data->CIEMAL);
  // for bussiness type
    $CDDESC=trim($data->CDDESC);
  // for cell phone number
    $CSTELE=$data->CSTELE;
  // for bussines phone number
    $CICPHN=$data->CICPHN;
  // for rep id
    $CSREP1=$data->CSREP1;
  // for customer address
    $CIADDR=trim($data->CIADDR);
  // for city
    $CICITY=$data->CICITY;
  // for customer state
    $CIST=$data->CIST;
  // for customer zip code
    $CIZIP=$data->CIZIP;
  // for customer webaddress
    $CIWEB=trim($data->CIWEB);
  // for customer kill status
    $CSKILL=$data->CSKILL;
  // for customer impression for deal
    $GEOIMPRE=$data->GEOIMPRE;
  // for entry date

    $entry_date=$data->CIENDT;
  // For Start Date
    $start_day=$data->CSSTDD;
    $start_month=$data->CSSTMM;
    $start_year=$data->CSSTYY;
    $start_date1=$start_year.'-'.$start_month.'-'.$start_day;
    $date1=date_create($start_date1);
    $start_date=date_format($date1,"Y-m-d");
  // For Stop Date
    $stop_day=$data->CSCMPD;
    $stop_month=$data->CSCMPM;
    $stop_year=$data->CSCMPY;
    $stop_date1=$stop_year.'-'.$stop_month.'-'.$stop_day;
    $date2=date_create($stop_date1);
    $stop_date=date_format($date2,"Y-m-d");
  // For Rep Name
    $rep_name=trim($data->SMNAME);

  // For sale rep email
    $sale_rep_email=trim($data->SMEMAL);



    if($CSKILL=='Y'){
      $CSKILL=0;
    }else{
      $CSKILL=1;
    }
// for groundtruth status
    $GEOGRDTR=$data->GEOGRDTR;
    if($GEOGRDTR=='Y'){
      $GEOGRDTR=1;
    }else{
      $GEOGRDTR=0;
    }

// for facebook status
    $GEOFBADS=$data->GEOFBADS;
    if($GEOFBADS=='Y'){
      $GEOFBADS=1;
    }else{
      $GEOFBADS=0;
    }

// for google status
    $GEOGOGLE=$data->GEOGOGLE;
    if($GEOGOGLE=='Y'){
      $GEOGOGLE=1;
    }else{
      $GEOGOGLE=0;
    }

    $find=DB::Select("SELECT customer_id FROM customers_list WHERE customer_id='".$CINUM."'");
// var_dump($find);
// die;
    if(!empty($find)){
      $customerlist = new CustomerListNew;
      $customerlist->customer_id=$CINUM;
      $customerlist->deal_no=$CSOFF.$CSMKT.$CSSUFX;
      $customerlist->kill_status=$CSKILL;
      $customerlist->no_of_impression=$GEOIMPRE;
      $customerlist->entry_date=$entry_date;
      $customerlist->google_ads=$GEOGOGLE;
      $customerlist->groundtruth_ads=$GEOGRDTR;
      $customerlist->facebook_ads=$GEOFBADS;
      $customerlist->start_date=$start_date;
      $customerlist->stop_date=$stop_date;
// $customerlist->no_of_impression=;
// $customerlist->contract_term=;
// $customerlist->entry_date=;
// $customerlist->artwork_deadline_date=; 
// $customerlist->start_date=;
// $customerlist->end_date=;
// $customerlist->kill_status=;
      $customerlist->save();


    }else{
// 'customer_id', 'username', 'password','no_of_impression','business_name','email','phone','contract_term','entry_date','start_date','stop_date','posted_by','bussiness_type','customer','customer_name','bussiness_phone','additional_phone','cell_phone','home_phone','customer_address','customer_city','customer_state','zip_code','groundtruth_ads','google_ads','facebook_ads','website','landing_page','created_page','keywords','rep_id','kill_status'
      $customerlist = new CustomerListNew;
      $customerlist->customer_id=$CINUM;
      $customerlist->deal_no=$CSOFF.$CSMKT.$CSSUFX;
      $customerlist->kill_status=$CSKILL;
      $customerlist->no_of_impression=$GEOIMPRE;
      $customerlist->entry_date=$entry_date;
      $customerlist->google_ads=$GEOGOGLE;
      $customerlist->groundtruth_ads=$GEOGRDTR;
      $customerlist->facebook_ads=$GEOFBADS;
      $customerlist->start_date=$start_date;
      $customerlist->stop_date=$stop_date;
// $customerlist->no_of_impression=;
// $customerlist->contract_term=;
// $customerlist->entry_date=;
// $customerlist->artwork_deadline_date=; 
// $customerlist->start_date=;
// $customerlist->end_date=;
// $customerlist->kill_status=;
      $customerlist->save();



      $customer = new CustomersList;
      $customer->customer_id=$CINUM;
      $customer->username=$CICNAM;
      $customer->password=Hash::make($CINUM);
      $customer->no_of_impression=$GEOIMPRE;
      $customer->business_name=$CSBNAM;
      $customer->email=$CIEMAL;
      $customer->posted_by=$CIENBY;
      $customer->bussiness_type=$CDDESC;
      $customer->customer=$CICNAM;
      $customer->bussiness_phone=$CICPHN;
      $customer->entry_date=$entry_date;
// $customer->additional_phone=;
      $customer->cell_phone=$CSTELE;
      $customer->customer_address=$CIADDR;
      $customer->customer_city=$CICITY;
      $customer->customer_state=$CIST;
      $customer->zip_code=$CIZIP;
      $customer->rep_name=$rep_name;
      $customer->salesrep_mail=$sale_rep_email;
      $customer->rep_id=$CSREP1;

      $customer->website=$CIWEB;
      $customer->kill_status=$CSKILL;
      $customer->google_ads=$GEOGOGLE;
      $customer->groundtruth_ads=$GEOGRDTR;
      $customer->facebook_ads=$GEOFBADS;
      $customer->save();
    }
  }

}


// for art received customers
public function addUserArtRecv(){
  set_time_limit(0);
  $curl_handle=curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-artReceived.php');
  curl_setopt($curl_handle, CURLOPT_HEADER, 0);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  $results = curl_exec($curl_handle);
  $results = json_decode($results);
// echo '<pre>';
  // var_dump($results);
  // die;

  foreach($results as $data){
  // for deal no
    $CSOFF= $data->WDCOFF;
    $CSMKT=$data->WDCMKT;
    $CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
    $CINUM=$CSOFF.$CSMKT.$CSSUFX;
    $WDCDAT=$data->WDCDAT;

    $year=substr($WDCDAT, -2);
    $date=substr($WDCDAT, -4,-2);
    $month=substr($WDCDAT, -6,-4);
    $date1=date_create($year.'-'.$month.'-'.$date);
    $date=date_format($date1,'Y-m-d');

    $find=DB::Select("UPDATE customer_deals SET artwork_recevied_date='".$date."' WHERE deal_no='".$CINUM."'");
  // var_dump($find);
  // die;
  }

}
/*Close for art received customers*/

/*For add user update for art launched*/
public function addUserArtLaunch(){
  set_time_limit(0);
  $curl_handle=curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-artLaunched.php');
  curl_setopt($curl_handle, CURLOPT_HEADER, 0);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  $results = curl_exec($curl_handle);
  $results = json_decode($results);
// echo '<pre>';
  // var_dump($results);
  // die;

  foreach($results as $data){
  // for deal no
    $CSOFF= $data->WDCOFF;
    $CSMKT=$data->WDCMKT;
    $CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
    $CINUM=$CSOFF.$CSMKT.$CSSUFX;
    $WDCDAT=$data->WDCDAT;
    $year=substr($WDCDAT, -2);
    $date=substr($WDCDAT, -4,-2);
    $month=substr($WDCDAT, -6,-4);
    $date1=date_create($year.'-'.$month.'-'.$date);
    $date=date_format($date1,'Y-m-d');

    $find=DB::Select("UPDATE customer_deals SET artwork_lauch_date='".$date."' WHERE deal_no='".$CINUM."'");
  // var_dump($find);
  // die;
  }

}
/*Close For add user update for art launched*/
// for add user update for user campaign report sent
public function addUserCampRepSent(){
  set_time_limit(0);
  $curl_handle=curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-campReportSent.php');
  curl_setopt($curl_handle, CURLOPT_HEADER, 0);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  $results = curl_exec($curl_handle);
  $results = json_decode($results);
// echo '<pre>';
  // var_dump($results);
  // die;

  foreach($results as $data){
  // for deal no
    $CSOFF= $data->WDCOFF;
    $CSMKT=$data->WDCMKT;
    $CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
    $CINUM=$CSOFF.$CSMKT.$CSSUFX;
    $WDCDAT=$data->WDCDAT;
    $year=substr($WDCDAT, -2);
    $date=substr($WDCDAT, -4,-2);
    $month=substr($WDCDAT, -6,-4);
    $date1=date_create($year.'-'.$month.'-'.$date);
    $date=date_format($date1,'Y-m-d');

    $find=DB::Select("UPDATE customer_deals SET report_sent_date='".$date."' WHERE deal_no='".$CINUM."'");
  // var_dump($find);
  // die;
  }

}
/*Close for add user update for user campaign report sent*/
// function to update user landing page url
public function addUserLandingPage(){
  set_time_limit(0);
  $curl_handle=curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-landingpageURL.php');
  curl_setopt($curl_handle, CURLOPT_HEADER, 0);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  $results = curl_exec($curl_handle);
  $results = json_decode($results);
// echo '<pre>';
  // var_dump($results);
  // die;

  foreach($results as $data){

  // for BUSSINESS name
    $CINUM=$data->GTDEAL;
    $url1=trim($data->GTHYPLNK);
    $url=substr_replace($url1,"", -1);
    if(!empty($url)){
  // echo "UPDATE customers_list SET landing_page='".$url."' WHERE deal_no='".$CINUM."'";
      $find=DB::Select("UPDATE customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id SET customers_list.landing_page='".$url."' WHERE customer_deals.deal_no='".$CINUM."'");
    }


// var_dump($find);
// die;
  }

}


/*Close function to update landing page url*/





// for monthly report
public function campaignReportMonthly($id){
// echo $id;die;
  $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$request->name."'");
  $all='ALL VERSIONS';
  $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$request->name."'");
  $grndresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$request->name."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
  $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'");
  $b_name=DB::Select("SELECT DISTINCT camp_name,camp_id FROM fb_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");

  return view('campaigns.camp_report',compact('fbresult','googleresult','grndresult','bussiness_name','b_name','id','all','grndadminresult'));
}
public function campaignReportByBussiness(Request $request,$id){
  $name=explode(',', $request->bussiness_name);
  $type=explode(',',$request->dataType);

$datewise=$type[1];//1 for all data 2 for monthly
// echo $name[0];die;
if($name[0]=='all'){
  if($datewise=='1'){
    return Redirect::to('campaign_report'.'/'.$id);
  }else{
// echo "SELECT * FROM fb_campaigns where cust_id='".$id."' and camp_date=(SELECT max(camp_date) FROM fb_campaigns)";die;
    $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
    $all='ALL VERSIONS';
    $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");

    $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' ORDER BY camp_date ASC");
    $b_name=DB::Select("SELECT DISTINCT camp_name,camp_id FROM fb_campaigns where cust_id='".$id."'");

    return view('campaigns.camp_report',compact('fbresult','googleresult','grndresult','bussiness_name','b_name','id','all','grndadminresult'));
  }

}else{
  $camp_id=$name[0];

  $bussiness_name=$name[1];

  $account_type=$name[2];
if($datewise=='1'){//for lifetime
  if($account_type=='facebook'){
    $fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$id."' and camp_id='".$camp_id."'");
  }elseif($account_type=='google'){
// echo "SELECT * FROM google_campaigns where cust_id='".$id."' and camp_id='".$camp_id."' and camp_date=(SELECT max(camp_date) FROM google_campaigns)";die;
    $googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."' and camp_id='".$camp_id."'");
  }else{

    $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' and camp_id='".$camp_id."' ORDER BY camp_date ASC");
  }
// var_dump($googleresult);die;
  return view('campaigns.camp_report',compact('fbresult','grndadminresult','googleresult','bussiness_name','b_name','id','account_type'));
}else{// for last 30 days
  if($account_type=='facebook'){
    $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."' and camp_id='".$camp_id."'");
  }elseif($account_type=='google'){
    $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."' and camp_id='".$camp_id."'");
  }else{
    $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."' and camp_id='".$camp_id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
  }
  return view('campaigns.camp_report',compact('fbresult','grndadminresult','googleresult','bussiness_name','b_name','id','account_type'));
}
}
}
/**
* Store a newly created user in storage
*
* @param  \App\Http\Requests\UserRequest  $request
* @param  \App\User  $model
* @return \Illuminate\Http\RedirectResponse
*/
public function store(Request $request)
{
	$input = $request->all();
	$input['password']=Hash::make($request->get('password'));
	CustomersList::create($input);
	return redirect()->route('user.list')->withStatus(('User successfully created.'));
}
/**
* Show the form for editing the specified user
*
* @param  \App\User  $user
* @return \Illuminate\View\View
*/
public function edit(User $user)
{
	return view('users.edit', compact('user'));
}
/**
* Update the specified user in storage
*
* @param  \App\Http\Requests\UserRequest  $request
* @param  \App\User  $user
* @return \Illuminate\Http\RedirectResponse
*/
public function update(UserRequest $request, User  $user)
{
	$hasPassword = $request->get('password');
	$user->update(
    $request->merge(['password' => Hash::make($request->get('password'))])
    ->except([$hasPassword ? '' : 'password']
  ));
  return redirect()->route('user.index')->withStatus(('User successfully updated.'));
}
/**
* Remove the specified user from storage
*
* @param  \App\User  $user
* @return \Illuminate\Http\RedirectResponse
*/
public function destroy(User  $user)
{
	$user->delete();
	return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
}
}
