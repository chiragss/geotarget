<?php
namespace App\Http\Controllers;
use App\CustomersList;
use App\CustomerListNew;
use App\FbCampaigns;
use App\GrndTruthCampaign;
use App\GrndTruthAdminCampaign;
use App\FbUserCampaign;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Redirect;
use Session;
use Datatables;
use Carbon\Carbon;
use Mail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     $this->middleware('auth');
   }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
     Auth::guard('user')->logout();
     Auth::guard('web')->logout();
     return Redirect::to('userlogin');
   }


   public function writeNotes()
   {
    if(Auth::guard('web')->check()){
      return view('users/addnote');
    }
  }


  public function addNotes(Request $request)
  {
    if(Auth::guard('web')->check()){
     $client_id=Auth::guard('user')->user()->customer_id;
     $bussiness_name=Auth::guard('user')->user()->business_name;

      //$client_id=2;
     $username= $request->username;
     $note=$request->note;
     $rank=$request->rank;
     $created_at=date('Y-m-d H:i:s');
     $select=DB::Select("SELECT rank FROM customers_list WHERE customer_id='".$client_id."'");

     if(!empty($select[0]->rank)){
      if(!empty($rank)){
        //echo "UPDATE customers_list SET rank='".'Rank '.$rank."' WHERE customer_id='".$client_id."'";
       $update=DB::Select("UPDATE customers_list SET rank='".'Rank '.$rank."' WHERE customer_id='".$client_id."'");
     }
   }else{
    if(!empty($rank)){
     // echo "UPDATE customers_list SET rank='".'Rank '.$rank."' WHERE customer_id='".$client_id."'";
      $update=DB::Select("UPDATE customers_list SET rank='".'Rank '.$rank."' WHERE customer_id='".$client_id."'");
    }
    
  }

 // $update=DB::Select("UPDATE customers_list SET rank='".$rank."' WHERE customer_id='".$client_id."'");
  $data1=array('client_id'=>$client_id,'username'=>$username,'message'=>$note,'created_at'=>$created_at);
  DB::table('notes')->insert($data1);
  $data=array('msg'=>'New Notes Notification','client_id'=>$client_id,'username'=>$username,'note'=>$note,'bussiness_name'=>$bussiness_name,'rank'=>$rank);


  Mail::send('email.notes',$data , function($message) use($data)
  {
       // $message->to($data1)->subject('This is test e-mail');
   $message->from('info@geotargetus.net', 'Geotargetus');
   $message->to('faisalk@shopperlocal.com');
   $message->Bcc('chirags@shopperlocal.com');
   $message->subject($data['msg']);

 });
  return Redirect::to('notes');
}
}

public function noteList()
{
  if(Auth::guard('web')->check()){
   $client_id=Auth::guard('user')->user()->customer_id;
     // $client_id=2;
   $created_at=date('Y-m-d H:i:s');
   $result=DB::Select("SELECT * FROM notes WHERE client_id='".$client_id."'");
   return view('users/notes_list',compact('result'));
 }
}


public function adminindex(){
 Auth::guard('user')->logout();
 if(Auth::guard('web')->check()){
       //$users=DB::Select("SELECT customer_deals.*,customers_list.business_name FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id   WHERE customer_deals.id IN (SELECT MAX(id) FROM customer_deals GROUP BY customer_id) and customer_deals.report_sent_date is not null and DATE(customer_deals.report_sent_date) > (NOW() - INTERVAL 30 DAY) ORDER BY customer_deals.customer_id ASC");
  $users=DB::Select("SELECT customer_deals.*,customers_list.business_name FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id INNER JOIN camp_launch ON customer_deals.customer_id=camp_launch.cust_id WHERE customer_deals.id IN (SELECT MAX(id) FROM customer_deals GROUP BY customer_id) and camp_launch.launch_date is not null and DATE(camp_launch.launch_date) > (NOW() - INTERVAL 30 DAY) ORDER BY customer_deals.customer_id ASC");
  $rep=DB::Select("SELECT rep_id,count(rep_id) AS count,rep_name FROM customers_list GROUP BY rep_id HAVING count(rep_id)>0");
  $active_rep=DB::Select("SELECT rep_id,count(rep_id) AS count,rep_name FROM customers_list WHERE kill_status='1' GROUP BY rep_id HAVING count(rep_id)>0");
  $launch_reps=DB::Select("SELECT rep_id,rep_name,count(customers_list.rep_id) as rep_count FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customer_deals.artwork_lauch_date Is NOT null  GROUP BY customers_list.rep_id HAVING count(customers_list.rep_id)");
      //$cust_launch=DB::Select("SELECT customers_list.customer_id,customer_deals.id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customer_deals.artwork_lauch_date IS not NULL");
  $cust_launch=DB::Select("SELECT customers_list.customer_id FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE camp_launch.launch_date IS not NULL");
      //$last_seven_day_launch=DB::Select("SELECT customers_list.customer_id,customer_deals.id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.artwork_lauch_date) > (NOW() - INTERVAL 7 DAY) and  customer_deals.artwork_lauch_date IS not NULL");
  $last_seven_day_launch=DB::Select("SELECT customers_list.customer_id FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE DATE(camp_launch.launch_date) > (NOW() - INTERVAL 7 DAY) and  camp_launch.launch_date IS not NULL");
      // $last_thirty_day_report_sent=DB::Select("SELECT customers_list.customer_id,customer_deals.report_sent_date FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.report_sent_date) > (NOW() - INTERVAL 30 DAY) and  customer_deals.report_sent_date IS not NULL");
  $last_thirty_day_report_sent=DB::Select("SELECT customers_list.customer_id,camp_launch.launch_date FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE DATE(camp_launch.launch_date) > (NOW() - INTERVAL 30 DAY) and  camp_launch.launch_date IS not NULL");
  $new_rep=array_merge($rep,$active_rep);
  foreach ($new_rep as $data) {
   $result[]=array('rep_id'=>$data->rep_id,'count'=>$data->count,'rep_name'=>$data->rep_name);
 }

 $count=count($result);

 for ($i=0; $i <$count/2 ; $i++) { 
  $final[]=array('rep_name'=>$result[$i]['rep_name'],'rep_id'=>$result[$i]['rep_id'],'count'=>$result[$i]['count'],'active'=>$result[$count/2 + $i]['count'],'killed'=>$result[$i]['count'] - $result[$count/2 + $i]['count']);
}

$count_active_rep=DB::table('customers_list')->where('kill_status','1')->count();
$total_customer=DB::table('customers_list')->count();
$count=count($users);


foreach($users as $userdata){
  $achieve_imp=HomeController::countImpression($userdata->customer_id);
  if($achieve_imp!==0 && $userdata->no_of_impression>0){
    $per=$achieve_imp/$userdata->no_of_impression*100;
    if($per<100.00){
      $user_impression[]=array('customer_id'=>$userdata->customer_id,'target_impression'=>$userdata->no_of_impression,'achieved_impression'=>$achieve_imp,'lower_perchantage'=>$per,'deal_no'=>$userdata->deal_no,'bussiness_name'=>$userdata->business_name,'report_sent_date'=>$userdata->report_sent_date,'entry_date'=>$userdata->entry_date);
    }
    if($per>100.00){
     $user_high_impression[]=array('customer_id'=>$userdata->customer_id,'target_impression'=>$userdata->no_of_impression,'achieved_impression'=>$achieve_imp,'higer_perchantage'=>$per);
   }

 }


}


return view('admindashboard',compact('user_impression','user_high_impression','count','rep','active_rep','count_active_rep','final','cust_launch','last_seven_day_launch','last_thirty_day_report_sent','total_customer','launch_reps'));
}else{
 return Redirect::to('login'); 
}

}
    // public function campaignList()
    // {
    //      $data = DB::select("SELECT * from user_lists");
    //     return view('users/users_list',compact('data'));
    // }


// for rep admin home page
public function repadminindex(){
 Auth::guard('user')->logout();
 if(Auth::guard('rep_user')->check()){
  $email=Auth::guard('rep_user')->user()->email;
       //$users=DB::Select("SELECT customer_deals.*,customers_list.business_name FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id   WHERE customer_deals.id IN (SELECT MAX(id) FROM customer_deals GROUP BY customer_id) and customer_deals.report_sent_date is not null and DATE(customer_deals.report_sent_date) > (NOW() - INTERVAL 30 DAY) ORDER BY customer_deals.customer_id ASC");
  $users=DB::Select("SELECT customer_deals.*,customers_list.business_name FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id INNER JOIN camp_launch ON customer_deals.customer_id=camp_launch.cust_id WHERE customers_list.salesrep_mail='".$email."' and customer_deals.id IN (SELECT MAX(id) FROM customer_deals GROUP BY customer_id) and camp_launch.launch_date is not null and DATE(camp_launch.launch_date) > (NOW() - INTERVAL 30 DAY) ORDER BY customer_deals.customer_id ASC");
  $rep=DB::Select("SELECT rep_id,count(rep_id) AS count,rep_name FROM customers_list GROUP BY rep_id HAVING count(rep_id)>0");
  $active_rep=DB::Select("SELECT rep_id,count(rep_id) AS count,rep_name FROM customers_list WHERE kill_status='1' GROUP BY rep_id HAVING count(rep_id)>0");
  $launch_reps=DB::Select("SELECT rep_id,rep_name,count(customers_list.rep_id) as rep_count FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customer_deals.artwork_lauch_date Is NOT null  GROUP BY customers_list.rep_id HAVING count(customers_list.rep_id)");
      //$cust_launch=DB::Select("SELECT customers_list.customer_id,customer_deals.id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE customer_deals.artwork_lauch_date IS not NULL");
  $cust_launch=DB::Select("SELECT customers_list.customer_id FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE customers_list.salesrep_mail='".$email."' and camp_launch.launch_date IS not NULL");
      //$last_seven_day_launch=DB::Select("SELECT customers_list.customer_id,customer_deals.id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.artwork_lauch_date) > (NOW() - INTERVAL 7 DAY) and  customer_deals.artwork_lauch_date IS not NULL");
  $last_seven_day_launch=DB::Select("SELECT customers_list.customer_id FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE customers_list.salesrep_mail='".$email."' and DATE(camp_launch.launch_date) > (NOW() - INTERVAL 7 DAY) and  camp_launch.launch_date IS not NULL");
      // $last_thirty_day_report_sent=DB::Select("SELECT customers_list.customer_id,customer_deals.report_sent_date FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.report_sent_date) > (NOW() - INTERVAL 30 DAY) and  customer_deals.report_sent_date IS not NULL");
  $last_thirty_day_report_sent=DB::Select("SELECT customers_list.customer_id,camp_launch.launch_date FROM customers_list INNER JOIN camp_launch ON customers_list.customer_id=camp_launch.cust_id WHERE customers_list.salesrep_mail='".$email."' and DATE(camp_launch.launch_date) > (NOW() - INTERVAL 30 DAY) and  camp_launch.launch_date IS not NULL");
  $new_rep=array_merge($rep,$active_rep);
  foreach ($new_rep as $data) {
   $result[]=array('rep_id'=>$data->rep_id,'count'=>$data->count,'rep_name'=>$data->rep_name);
 }

 $count=count($result);

 for ($i=0; $i <$count/2 ; $i++) { 
  $final[]=array('rep_name'=>$result[$i]['rep_name'],'rep_id'=>$result[$i]['rep_id'],'count'=>$result[$i]['count'],'active'=>$result[$count/2 + $i]['count'],'killed'=>$result[$i]['count'] - $result[$count/2 + $i]['count']);
}

$count_active_rep=DB::table('customers_list')->where('kill_status','1')->where('salesrep_mail',$email)->count();
$total_customer=DB::table('customers_list')->where('salesrep_mail',$email)->count();
$count=count($users);


foreach($users as $userdata){
  $achieve_imp=HomeController::countImpression($userdata->customer_id);
  if($achieve_imp!==0 && $userdata->no_of_impression>0){
    $per=$achieve_imp/$userdata->no_of_impression*100;
    if($per<100.00){
      $user_impression[]=array('customer_id'=>$userdata->customer_id,'target_impression'=>$userdata->no_of_impression,'achieved_impression'=>$achieve_imp,'lower_perchantage'=>$per,'deal_no'=>$userdata->deal_no,'bussiness_name'=>$userdata->business_name,'report_sent_date'=>$userdata->report_sent_date,'entry_date'=>$userdata->entry_date);
    }
    if($per>100.00){
     $user_high_impression[]=array('customer_id'=>$userdata->customer_id,'target_impression'=>$userdata->no_of_impression,'achieved_impression'=>$achieve_imp,'higer_perchantage'=>$per);
   }

 }


}


return view('admindashboard',compact('user_impression','user_high_impression','count','rep','active_rep','count_active_rep','final','cust_launch','last_seven_day_launch','last_thirty_day_report_sent','total_customer','launch_reps'));
}else{
 return Redirect::to('replogin'); 
}

}

// close for rep admin home page
public function campaignList($id){
  $date=date('Y-m-d');
      // $date='2020-01-09';
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/"."act_".$id."/"."campaigns?pretty=0&limit=300&fields=name,status"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
        // echo $result[0]->name;
  $a=json_decode($result1);
  $data=$a->data;
    //   echo '<pre>';
    // var_dump($data);die;
  foreach($data as $result){
   $find=DB::Select("SELECT camp_id FROM fb_admin_campaigns WHERE camp_id='".$result->id."'");
   if(empty($find)){
    $fb = new FbCampaigns;
    $jsonkeyword=json_encode($data);
    $fb->cust_id=$id;
    $fb->camp_id=$result->id;
    $fb->camp_name = $result->name;
    $fb->camp_date = $date; 
    $fb->camp_status=$result->status;
    $fb->save();
  }
}
}
    // function to update user id in campaign list  for facebook
public function userUpdateId(){
  $data=DB::Select('SELECT * FROM customers_list');
      // echo '<pre>';
      // var_dump($data);
  foreach ($data as $user) {
    $update2=DB::Select("UPDATE fb_admin_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
    $update2=DB::Select("UPDATE fb_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
     //$update2=DB::Select("UPDATE fb_lifetime_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
  }
}

// function to use user interaction
public function interaction(){
 if(Auth::guard('user')->check()){
  $id=Auth::guard('user')->user()->customer_id;
  $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
  $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");
  // echo "SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()";
  $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
  $google_start_date=DB::Select("SELECT camp_start_date FROM google_campaigns WHERE cust_id='".$id."' and camp_start_date=(SELECT MIN(camp_start_date) FROM google_campaigns WHERE cust_id='".$id."')");
  $google_end_date=DB::Select("SELECT camp_end_date FROM google_campaigns WHERE cust_id='".$id."' and camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".$id."')");
  return view('users/interaction',compact('fbresult','googleresult','grndadminresult','google_start_date','google_end_date'));
}else{
 return Redirect::to('userlogin');
}
}

// function to list of camp launch date and reminder mail
public function reports(){
  if(Auth::guard('user')->check()){
    $id=Auth::guard('user')->user()->customer_id;
    $camp_launch=DB::Select("SELECT customers_list.business_name,camp_launch.* FROM camp_launch INNER JOIN customers_list ON camp_launch.cust_id=customers_list.customer_id WHERE camp_launch.cust_id='".$id."'");
    $reminder_mail=DB::Select("SELECT customers_list.business_name,reminder_mail.* FROM reminder_mail INNER JOIN customers_list ON reminder_mail.cust_id=customers_list.customer_id WHERE reminder_mail.cust_id='".$id."'");
    $google_start_date=DB::Select("SELECT camp_start_date FROM google_campaigns WHERE cust_id='".$id."' and camp_start_date=(SELECT MIN(camp_start_date) FROM google_campaigns WHERE cust_id='".$id."')");
    $google_end_date=DB::Select("SELECT camp_end_date FROM google_campaigns WHERE cust_id='".$id."' and camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".$id."')");
    return view('campaigns/reports',compact('camp_launch','reminder_mail','google_start_date','google_end_date'));

  }
}

// function to use user interaction using ajax
public function interactionajax(Request $request){
  if($request->ajax()){
   $id=Auth::guard('user')->user()->customer_id;
   if($request->type=='1'){
     $fbresult=DB::Select("SELECT * FROM fb_lifetime_campaigns where cust_id='".$id."'");
     $googleresult=DB::Select("SELECT * FROM google_campaigns where cust_id='".$id."'");
     $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'");
   }else{
     $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
     $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");
  // echo "SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()";
     $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
   }
   $fb_clicks=0;$fb_views=0;$google_views=0;
   $google_clicks=0;$grnd_impression=0;$fb_video_views=0;$fb_display_views=0;$fb_clicks=0; $mob_views=0;$tab_views=0;$desk_views=0;$other_views=0;$grnd_clicks=0;$grnd_view=0;
// FOR FB
   if(!empty($fbresult)){
     foreach($fbresult as $campaign){
       if(!empty($campaign->video_campaign)){
        if($campaign->video_campaign!=='null'){
         $fbvideoviews=json_decode($campaign->video_campaign);
         if(!empty($fbvideoviews)){
           foreach($fbvideoviews as $fbvideo){
             $fb_video_views=$fb_video_views + $fbvideo->video_played;
           }
         }
       }
     }
     if(!empty($campaign->display_campaign)){
       if($campaign->display_campaign!=='null'){
         $fbdisplayviews=json_decode($campaign->display_campaign);
         if(!empty($fbdisplayviews)){
           foreach($fbdisplayviews as $fbdisplay){
             $fb_display_views=$fb_display_views + $fbdisplay->video_played;
           }
         }
       }
     }
     if(!empty($campaign->keyword)){
       $fb_click=json_decode($campaign->keyword);
       if(!empty($fb_click)){
         $fb_clicks = $fb_clicks + $fb_click->clicks;
       }
     }
   }
   $fb_views= $fb_video_views +  $fb_display_views;
 }
 // CLOSE FOR FB

 // FOR GOOGLE
 if(!empty($googleresult)){
   foreach($googleresult as $campaign){
     if(!empty($campaign->keyword)){

       $googleimpression=json_decode($campaign->keyword);
       $google_clicks=$google_clicks + $googleimpression->clicks;

     }
     if(!empty($campaign->video_campaign)){
       if($campaign->video_campaign!=='null'){

         $views=json_decode($campaign->video_campaign);

         if(!empty($views->device)){

           $mob_views=$mob_views + $views->device->mobile_views;
           $tab_views=$tab_views + $views->device->tab_views;
           $desk_views=$desk_views + $views->device->desk_views;
           $other_views=$other_views + $views->device->other_views;

         }
       }
     }
   }

   $google_views=$mob_views + $tab_views + $desk_views + $other_views;

 }

 // CLOSE FOR GOOGLE
 // FOR GT
 if(!empty($grndadminresult)){
  foreach($grndadminresult as $campaign){
    if(!empty($campaign->keyword)){
      $grndimpression=json_decode($campaign->keyword);
      $grnd_clicks=$grnd_clicks + $grndimpression->clicks;
      $grnd_view=$grnd_view + $grndimpression->video_start;

    }
  }
}
 // CLOSE FOR GT
$total_views=$fb_views + $google_views + $grnd_view;
$total_clicks=$fb_clicks + $google_clicks + $grnd_clicks;
$data=array('views'=>$total_views,'clicks'=>$total_clicks,'view'=>array('fb_views'=>$fb_views,'google_views'=>$google_views,'grnd_views'=>$grnd_view),'click'=>array('fb_clicks'=>$fb_clicks,'google_clicks'=>$google_clicks,'grnd_clicks'=>$grnd_clicks));
echo json_encode($data);
}
}


// function to update user id in ground camp list for groundtruth
public function userGrndUpdateId(){
  $data=DB::Select('SELECT * FROM customers_list');
// echo '<pre>';
  // var_dump($data);
  foreach ($data as $user) {
  //$update=DB::Select("UPDATE groundtruth_admin_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
    $update=DB::Select("UPDATE groundtruth_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
  }
}


/*Function to use update user id for google*/
// function to update user id in ground camp list for groundtruth
public function userGoogleUpdateId(){
  $data=DB::Select('SELECT * FROM customers_list');
  foreach ($data as $user) {
    $update2=DB::Select("UPDATE google_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
    $update1=DB::Select("UPDATE google_monthly_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
  }
}
// function to get all campaigns for users
public  function userCampaignList($id){
  $date=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/"."act_".$id."/"."campaigns?pretty=0&limit=300&fields=name,status"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
// echo $result[0]->name;
  $a=json_decode($result1);
  $data=$a->data;
// var_dump($data);die;
  foreach($data as $result){
    $find=DB::Select("SELECT camp_id FROM fb_campaigns WHERE camp_id='".$result->id."'");
    if(empty($find)){
      $fb = new FbUserCampaign;
      $fb->cust_id=$id;
      $fb->camp_id=$result->id;
      $fb->camp_name = $result->name;
      $fb->camp_date = $date; 
      $fb->camp_status=$result->status;
      $fb->save();
    }
  }
}
// function to use add all campaigns of groundtruth
// function to get all campaigns for users
public  function grndUserCampaigns(){
// $date=date('Y-m-d');
// $seven_day_before=date('Y-m-d',strtotime("-7 days"));
  $date='2019-11-21';
  $seven_day_before='2019-11-15';
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://reporting.groundtruth.com/demand/v1/org/1008848/totals?start_date=".$seven_day_before."&end_date=".$date."&all_campaigns=1",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "X-GT-USER-ID: 49177",
      "X-GT-API-KEY: =ab6hp-0'xVL<f]b"
    ),
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  $data=json_decode($response);
  foreach($data as $result){
    $json=array("cumulative_reach"=>$result->cumulative_reach,"video_start"=>$result->video_start,"click_to_call"=>$result->click_to_call,"video"=>$result->video,"visits"=>$result->visits,"spend"=>$result->spend,"ctr"=>$result->ctr,"cpm"=>$result->cpm,"impressions"=>$result->impressions,"clicks"=>$result->clicks);
    $grnd = new GrndTruthCampaign;
    $jsonkeyword=json_encode($json);
    $grnd->cust_id = '1008848';
    $grnd->camp_id = $result->campaign_id;
    $grnd->camp_name = $result->campaign_name;
    $grnd->keyword = $jsonkeyword;
    $grnd->camp_start_date = $seven_day_before;
    $grnd->camp_end_date = $date;
    $grnd->camp_date = $date; 
    $grnd->save();
// $this->findGrndCampDetails($result->campaign_id,$date,$seven_day_before);
  }

}
// cron job for grndtruth all campaigns for admin
public function getAllGrndCamp(){
  $grndcamp= DB::Select('SELECT camp_id FROM groundtruth_campaigns GROUP BY camp_id');
// echo '<pre>';
 // var_dump($grndcamp);
  foreach($grndcamp as $camp){
   $this->grndAdminCampaigns($camp->camp_id);
 }
}
// cron job for grndtruth daily basis
public  function grndAdminCampaigns($camp_id){
// echo '<br>';
// echo $camp_id;
// $date=date('Y-m-d',strtotime("-1 days"));
// $seven_day_before=date('Y-m-d',strtotime("-2 days"));
// echo $camp_id;die;
  $date='2020-01-07';
  $seven_day_before='2020-01-01';
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://reporting.groundtruth.com/demand/v1/campaign/".trim($camp_id)."/daily?start_date=".$seven_day_before."&end_date=".$date."&all_campaigns=1",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "X-GT-USER-ID: 49177",
      "X-GT-API-KEY: =ab6hp-0'xVL<f]b"
    ),
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  $data=json_decode($response);
/*echo '<pre>';
var_dump($data);*/
if(!empty($data)){
  foreach($data as $result){
  // echo '<pre>';
   // var_dump($result);
   /* if(!empty($result->campaign_name)){
   echo $result->campaign_name;
 }*/
 if(!empty($result->campaign_id)){
   $json=array("cumulative_reach"=>$result->cumulative_reach,"video_start"=>$result->video_start,"click_to_call"=>$result->click_to_call,"video"=>$result->video,"visits"=>$result->visits,"spend"=>$result->spend,"ctr"=>$result->ctr,"cpm"=>$result->cpm,"impressions"=>$result->impressions,"clicks"=>$result->clicks);
   $grnd = new GrndTruthAdminCampaign;
   $jsonkeyword=json_encode($json);
   $grnd->cust_id = '1008848';
   $grnd->camp_id = $result->campaign_id;
   $grnd->camp_name = $result->campaign_name;
   $grnd->keyword = $jsonkeyword;
   $grnd->camp_start_date = $seven_day_before;
   $grnd->camp_end_date = $date;
   $grnd->camp_date = $result->date; 
   $grnd->save();
 }

}
}
}
public function getCampaign(){
  $campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
  foreach($campaigns as $camp){
// $this->findCampDetails($camp->camp_id);
// $this->ageGenderCampaign($camp->camp_id);
// $this->locImpressionCampaign($camp->camp_id);
//$this->pubPlatformCampaign($camp->camp_id);
    $this->adCreativeImage($camp->camp_id);
  }
}
// Cron job function to use groundtruth data
public function getGrndCampaign(){
/*$date=date('Y-m-d',strtotime("-1 days"));
$seven_day_before=date('Y-m-d',strtotime("-2 days"));*/
$date='2020-02-05';
$seven_day_before='2020-01-29';
$campaigns=DB::Select('SELECT camp_id,camp_date FROM groundtruth_admin_campaigns GROUP BY camp_id');
// echo '<pre>';
//  var_dump($campaigns);die;
foreach($campaigns as $camp){
 // $this->findGrndCampDetails($camp->camp_id,$date,$seven_day_before);
 // $this->ageGenderCampaign($camp->camp_id);
 // $this->locImpressionCampaign($camp->camp_id);
 $this->adCreativeImageGrnd($camp->camp_id,$date,$seven_day_before);
}
}
// function to use cron job for user only
public function userGetCampaign(){
  $campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
  foreach($campaigns as $camp){
  // $this->userfindCampDetails($camp->camp_id);
   //$this->userageGenderCampaign($camp->camp_id);
   /* $this->userlocImpressionCampaign($camp->camp_id);*/
  // $this->userpubPlatformCampaign($camp->camp_id);
   $this->userdeviceCampaign($camp->camp_id);
   //$this->adCreativeImage($camp->camp_id);
 }
}
public function allcampaigns(){
  $today=date('Y-m-d');
  $campaigns=DB::Select("SELECT * FROM fb_admin_campaigns WHERE camp_date='".$today."'");
  return view('campaigns.campaign_list',compact('campaigns'));
}
// function to use admin fb campaigns only
public function findCampDetails($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?fields=spend,clicks,impressions,ctr,cpc,conversions,reach,cost_per_conversion,website_ctr&date_preset=today"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    $data=$a->data[0];
    $data1=array("spend"=>$data->spend,"clicks"=>$data->clicks,"impressions"=>$data->impressions,"ctr"=>$data->ctr,"reach"=>$data->reach);
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_admin_campaigns SET keyword='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$data->date_start."'");
  }else{
    DB::Select("UPDATE fb_admin_campaigns SET camp_status='COMPLETED' WHERE camp_id='".$camp_id."' and camp_status='ACTIVE' and camp_date='".$today."'");
  }
}
// function to use user groundtruth campaign update loc_impression
public function findGrndCampDetails($camp_id,$date,$seven_day_before){
// echo "https://reporting.groundtruth.com/demand/v1/campaign/product/".trim($camp_id)."?start_date=".trim($seven_day_before)."&end_date=".trim($date);

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL =>  "https://reporting.groundtruth.com/demand/v1/campaign/product/".trim($camp_id)."?start_date=".trim($seven_day_before)."&end_date=".trim($date),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "X-GT-USER-ID: 49177",
      "X-GT-API-KEY: =ab6hp-0'xVL<f]b"
    ),
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  $data1=json_decode($response);
/*echo "<pre>";
var_dump($data1);*/
if(!empty($data1)){
  foreach($data1 as $data){
    if(!empty($data->country)){
      $data2[]=array("country"=>$data->country,"state"=>$data->state,"city"=>$data->city,"impressions"=>$data->imp,"zip"=>$data->zip,"spent"=>$data->spt,"clicks"=>$data->clks,"ctr"=>$data->ctr);
    }else{
      $data3[]=array("value"=>NULL);
    }
  }
  if(!empty($data2)){
    $jsonkeyword=json_encode($data2);
    DB::Select("UPDATE groundtruth_campaigns SET loc_impression='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$date."'");
  }
}
}
// function to use add adcreative image for groundtruth
public function adCreativeImageGrnd($camp_id,$date,$seven_day_before){
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL =>  "https://reporting.groundtruth.com/demand/v1/creatives/product/".trim($camp_id)."?start_date=".$seven_day_before."&end_date=".$date."&all_campaigns=1",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "X-GT-USER-ID: 49177",
      "X-GT-API-KEY: =ab6hp-0'xVL<f]b"
    ),
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  $data1=json_decode($response);
  echo '<pre>';
  var_dump($data1);
}
// function to use fb campaigns for user only
public function userfindCampDetails($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?fields=spend,clicks,impressions,ctr,cpc,conversions,reach,cost_per_conversion,website_ctr"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;

  if(!empty($data)){
    $data=$a->data[0];

    $data1=array("spend"=>$data->spend,"clicks"=>$data->clicks,"impressions"=>$data->impressions,"ctr"=>$data->ctr,"reach"=>$data->reach);
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_campaigns SET keyword='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
  }else{
    DB::Select("UPDATE fb_campaigns SET camp_status='COMPLETED' WHERE camp_id='".$camp_id."' and camp_status='ACTIVE'");
  }
}
// function to use admin fb campaigns only based on age gender
public function ageGenderCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=age,gender&date_preset=today"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'age'=>$result->age,'gender'=>$result->gender);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'age'=>$result->age,'gender'=>$result->gender);
      }

    }

    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_admin_campaigns SET age_gender_camp='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}
// function to use fb campaigns for user based on age gender
public function userageGenderCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=age,gender"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'age'=>$result->age,'gender'=>$result->gender);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'age'=>$result->age,'gender'=>$result->gender);
      }

    }

    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_campaigns SET age_gender_camp='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}

// function to use device based impresison for facebook last 30 days
public function userdeviceCampaign($camp_id){
  $today=date('Y-m-d');
// $today='2020-01-24';
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=device_platform"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>$result->spend);
      }else{
        $data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>0);
      }

    }

    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_campaigns SET device_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
  }
}
// function to use admin fb campaigns only based on location impression
public function locImpressionCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=country,region&date_preset=today"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'country'=>$result->country,'region'=>$result->region);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'country'=>$result->country,'region'=>$result->region);
      }
    }
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_admin_campaigns SET loc_impression='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}
// function to use fb campaigns for user only based on location impressions
public function userlocImpressionCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=country,region"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'country'=>$result->country,'region'=>$result->region);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'country'=>$result->country,'region'=>$result->region);
      }
    }
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_campaigns SET loc_impression='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}
// function to use admin fb campaigns only for platforn based 
public function pubPlatformCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=publisher_platform,impression_device,platform_position&date_preset=today"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
      }

    }
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_admin_campaigns SET pub_platform_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}
// function to use fb campaigns for user only based on platform
public function userpubPlatformCampaign($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=publisher_platform,impression_device,platform_position"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->spend)){
        $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
      }else{
        $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
      }

    }
    $jsonkeyword=json_encode($data1);
    DB::Select("UPDATE fb_campaigns SET pub_platform_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }
}
// function to use update ad creative images
public function adCreativeImage($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v7.0/".$camp_id."/"."ads"."?access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      $url1="https://graph.facebook.com/v7.0/".$result->id."/"."previews?ad_format=DESKTOP_FEED_STANDARD"."&access_token=".$access_token;
      $result1 = file_get_contents($url1);
      $a=json_decode($result1);
      $data1=$a->data;
// echo '<pre>';
 // var_dump($data1);die;
      if(!empty($data1)){
       foreach($data1 as $result1){
         if(!empty($result1->body)){
           $data3[]=$result1->body;
         }else{
          $data3[]=NULL;
        }
      }
// var_dump($data3);die;
      $jsonkeyword=implode(',', $data3);
// echo "UPDATE fb_admin_campaigns SET ad_creative_image='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'";die;
      DB::Select("UPDATE fb_admin_campaigns SET ad_creative_image='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
    }
  }
}
}
public function userList($id){

  if(!empty(Auth::guard('user')->logout())){
    Auth::guard('user')->logout();
  }
  if(Auth::guard('web')->check()){
    $data=CustomersList::all()->take(10);
    $data1=CustomersList::all()->take(10);

    return view('users/users_list',compact('data','data1','id'));
  }else{
    return Redirect::to('login'); 
  }


}
public function repuserList($id){

  if(!empty(Auth::guard('user')->logout())){
    Auth::guard('user')->logout();
  }
  if(Auth::guard('rep_user')->check()){
    $email=Auth::guard('rep_user')->user()->email;
    $data=CustomersList::where('salesrep_mail', '=', $email)->get();
    $data1=CustomersList::where('salesrep_mail', '=', $email)->get();
   
    return view('users/rep_users_list',compact('data','data1','id'));
  }else{
    return Redirect::to('replogin'); 
  }


}

public function userListajax(Request $request,$id){
  if($id=='1'){
    if($request->ajax()){
      $customer=CustomersList::query();
      return Datatables::of($customer)
// ->addColumn('impression', function($row) {
//   return $p=HomeController::countImpression($row->customer_id);
// })
      ->addColumn('action', function($row) {
        $data=$this->dealList($row->customer_id);
        $d="";
        foreach($data as $user){
          $d  .='<tr>';
          $d .='<td>'.$user->customer_id.'</td>';
          $d .='<td>'.$user->deal_no.'</td>';
          $d .='<td>'.$user->no_of_impression.'</td>';
          $d .='<td>'.$user->entry_date.'</td>';
          $d .='<td>'.$user->artwork_lauch_date.'</td>';
          $d .='<td>'.$user->report_sent_date.'</td>';
          $d .='<td>'.$user->start_date.'</td>';
          $d .='<td>'.$user->stop_date.'</td>';
          if($user->kill_status=='0'){
            $d .='<td><p class="text-danger">Killed</p></td>';
          }else{
            $d .='<td><p class="text-success">Active</p></td>';
          }
          $d .='</tr>';
        }

        
        
        $a='<a href="'.url('usersession/'.$row->customer_id).'"><button class="btn btn-purple btn-round btn-fab"><i class="material-icons">dashboard</i></button></a>';
        $b='<a href="'.url('deals/'.$row->customer_id).'" data-toggle="modal" data-target="#myModal'.$row->customer_id.'"><button class="btn btn-warning btn-round btn-fab"><i class="material-icons">list_alt</i></button></a>';
        $c=
        '<div id="myModal'.$row->customer_id.'" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">'.$row->username.'</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body">
        <h5><b>Address</b>&nbsp; '.$row->customer_address.'</h5>
        <p><b>Business Type</b>&nbsp; '.$row->bussiness_type.'</p>
        <div class="table-responsive">
        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr><th>CustomerId</th>
        <th>DealNo</th>
        <th>NoOfImpression</th>
        <th>EntryDate</th>
        <th>CampLaunchedDate</th>
        <th>ReportSentDate</th>
        <th>StartDate</th>
        <th>StopDate</th>
        <th>Status</th>
        </tr></thead>
        <tbody>'.
        $d.

        '</tbody>

        </table>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-sm waves-effect waves-light" data-dismiss="modal" style="background: #8e24aa;"><span style="color:aliceblue;">Close</span></button>
        </div>
        </div>
        </div>
        </div>';
        return $a.' '.$b.' '.$c;
      })


      ->make(true);

    }
  }else if($id=='2'){
    if($request->ajax()){
      $query = CustomersList::query()
      ->select(array('customers_list.customer_id','customers_list.username','customers_list.business_name','customers_list.entry_date','customers_list.bussiness_type','customers_list.customer_address','customers_list.zip_code','customers_list.rep_name'))
      ->join('customer_deals','customers_list.customer_id','=','customer_deals.customer_id')
      ->join('camp_launch','customer_deals.customer_id','=','camp_launch.cust_id')
      ->whereNotNull('camp_launch.cust_id')
      ->get();
      return Datatables::of($query)

      ->addColumn('action', function($row) {
        $a='<a href="'.url('usersession/'.$row->customer_id).'"><button class="btn btn-purple btn-round btn-fab"><i class="material-icons">dashboard</i></button></a>';
        $b='<a href="'.url('deals/'.$row->customer_id).'" data-toggle="modal" data-target="#myModal'.$row->customer_id.'"><button class="btn btn-warning btn-round btn-fab"><i class="material-icons">list_alt</i></button></a>';
        return $a.' '.$b;
      })


      ->make(true);

    }
  }else if($id=='3'){
// $last_seven_day_launch=DB::Select("SELECT customers_list.customer_id FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.artwork_lauch_date) > (NOW() - INTERVAL 7 DAY) and  customer_deals.artwork_lauch_date IS not NULL");
    if($request->ajax()){
      $date = \Carbon\Carbon::today()->subDays(7);
      $query = CustomersList::query()
      ->select(array('customers_list.customer_id','customers_list.username','customers_list.business_name','customers_list.entry_date','customers_list.bussiness_type','customers_list.customer_address','customers_list.zip_code','customers_list.rep_name'))
      ->join('customer_deals','customers_list.customer_id','=','customer_deals.customer_id')
      ->where('customer_deals.artwork_lauch_date','>',$date)
      ->whereNotNull('customer_deals.artwork_lauch_date')
      ->get();
      return Datatables::of($query)

      ->addColumn('action', function($row) {
        $a='<a href="'.url('usersession/'.$row->customer_id).'"><button class="btn btn-purple btn-round btn-fab"><i class="material-icons">dashboard</i></button></a>';
        $b='<a href="'.url('deals/'.$row->customer_id).'" data-toggle="modal" data-target="#myModal'.$row->customer_id.'"><button class="btn btn-warning btn-round btn-fab"><i class="material-icons">list_alt</i></button></a>';
        return $a.' '.$b;
      })


      ->make(true);

    }
  }else{
// $last_thirty_day_report_sent=DB::Select("SELECT customers_list.customer_id,customer_deals.report_sent_date FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id WHERE DATE(customer_deals.report_sent_date) > (NOW() - INTERVAL 30 DAY) and  customer_deals.report_sent_date IS not NULL");
    if($request->ajax()){
      $date = \Carbon\Carbon::today()->subDays(30);
      $query = CustomersList::query()
      ->select(array('customers_list.customer_id','customers_list.username','customers_list.business_name','customers_list.entry_date','customers_list.bussiness_type','customers_list.customer_address','customers_list.zip_code','customers_list.rep_name'))
      ->join('customer_deals','customers_list.customer_id','=','customer_deals.customer_id')
      ->where('customer_deals.report_sent_date','>',$date)
      ->whereNotNull('customer_deals.report_sent_date')
      ->get();
      return Datatables::of($query)

      ->addColumn('action', function($row) {
        $a='<a href="'.url('usersession/'.$row->customer_id).'"><button class="btn btn-purple btn-round btn-fab"><i class="material-icons">dashboard</i></button></a>';
        $b='<a href="'.url('deals/'.$row->customer_id).'" data-toggle="modal" data-target="#myModal'.$row->customer_id.'"><button class="btn btn-warning btn-round btn-fab"><i class="material-icons">list_alt</i></button></a>';
        return $a.' '.$b;
      })


      ->make(true);

    }
  }


}


public static function countImpression($id){

  $fbresult=DB::Select("SELECT * FROM fb_campaigns where cust_id='".$id."'");
  $googleresult=DB::Select("SELECT * FROM google_monthly_campaigns where cust_id='".$id."'");
  $grndadminresult=DB::Select("SELECT * FROM groundtruth_campaigns where cust_id='".$id."'  and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
  $basisresult=DB::Select("SELECT * FROM basis_campaigns where cust_id='".$id."' and camp_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ORDER BY camp_date ASC");
  $fb_impression=0;$google_impression=0;$grnd_impression=0;$fb_video_impression=0;$fb_display_impression=0;$basis_impression=0;
// facebook total impression
  if(!empty($fbresult)){
    foreach($fbresult as $campaign){
      if(!empty($campaign->video_campaign)){
       if($campaign->video_campaign!=='null'){
         $fbvideoimpression=json_decode($campaign->video_campaign);
         if(!empty($fbvideoimpression)){
           foreach($fbvideoimpression as $fbvideo){
            $fb_video_impression=$fb_video_impression + $fbvideo->impresison;
          }
        }
      }
    }
    if(!empty($campaign->display_campaign)){
      if($campaign->display_campaign!=='null'){
        $fbdisplayimpression=json_decode($campaign->display_campaign);
        if(!empty($fbdisplayimpression)){
          foreach($fbdisplayimpression as $fbdisplay){
            $fb_display_impression=$fb_display_impression + $fbdisplay->impresison;
          }
        }
      }
    }
  }
  $fb_impression= $fb_impression +  $fb_video_impression +  $fb_display_impression;
}
// FOR GOOGLE 
if(!empty($googleresult)){
  foreach($googleresult as $campaign){
    if(!empty($campaign->keyword)){
      $googleimpression=json_decode($campaign->keyword);
      $google_impression=$google_impression + $googleimpression->impressions;
    }
  }
}

// FOR GROUNDTRUTH
if(!empty($grndadminresult)){
  foreach($grndadminresult as $campaign){
    if(!empty($campaign->keyword)){
      $grndimpression=json_decode($campaign->keyword);
      $grnd_impression=$grnd_impression + $grndimpression->impressions;
    }
  }
}

   // for basis campaign
if(!empty($basisresult)){
  foreach($basisresult as $campaign){
    if(!empty($campaign->keyword)){

      $basisimpression=json_decode($campaign->keyword);
      $basis_impression=$basis_impression + $basisimpression->delivered_impressions;

    }
  }
}

return $total_impression=$fb_impression + $google_impression + $grnd_impression + $basis_impression;




}
public static function dealList($id){
  return $data=CustomerListNew::all()->where('customer_id',$id);

// return view('users/deals_list',compact('data'));
}
public function campaignsView($id){
  $data=FbCampaigns::all();
  return view('campaigns/campaign_list',compact('data'));
}
}
