<?php
namespace App\Http\Controllers\Auth;
use Redirect;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use App\CustomersList;
use Session;
use Illuminate\Contracts\Encryption\Encrypter;
use Mail;

class LoginController extends Controller
{

	
	public function showLoginForm()
	{
    	Auth::logout(); // logging out user
    	Auth::guard('user')->logout();
    	Auth::guard('web')->logout();
    	if (!Auth::guard('user')){
    		return Redirect::to('userhomedashboard'.'/'.Auth::guard('user')->user()->customer_id);
    	}else{
    		return view('auth/login');
    	}

    }

    public function showRepLoginForm()
	{
    	Auth::logout(); // logging out user
    	Auth::guard('user')->logout();
    	Auth::guard('web')->logout();
    	Auth::guard('rep_user')->logout();
    	if (!Auth::guard('user')){
    		return Redirect::to('userhomedashboard'.'/'.Auth::guard('user')->user()->customer_id);
    	}else{
    		return view('auth/rep_login');
    	}

    }

    public function showuserLoginForm()
    {
// echo 'chirag';
    	Auth::logout(); // logging out user
    	Auth::guard('user')->logout();
    	Auth::guard('web')->logout();
    	if (!empty(Auth::guard('user')->user())){
			//var_dump(Auth::guard('user'));die;
    		return Redirect::to('userhomedashboard'.'/'.Auth::guard('user')->user()->customer_id);

    	}else{
			//var_dump(Auth::guard('user')->user());
			//echo 'chirag1';
    		return view('auth/userlogin');
    	}


    }

    public function showUserLogin()
    {
 // echo 'chirag';
    	Auth::logout(); // logging out user
    	Auth::guard('user')->logout();
    	Auth::guard('web')->logout();
			//var_dump(Auth::guard('user')->user());
			//echo 'chirag1';
    	return view('auth/user_page');


    }

    public function checkuser(Request $request)
    {

    	$cust_id=$request->customer_id;
    	Auth::logout(); // logging out user
    	Auth::guard('user')->logout();
    	Auth::guard('web')->logout();

    	$data=DB::Select("SELECT reset_token,email FROM customers_list WHERE customer_id='".$cust_id."' and pass_status='1'");

    	if(!empty($data[0]->reset_token)){
    		return Redirect::to('userloginhome');
    	}else{	
    		$token=str_random(100);
    		DB::Select("UPDATE customers_list SET reset_token='".$token."' WHERE customer_id='".$cust_id."'");
    		$data=array('msg'=>'Reset Your Password','token'=>$token);
    		Mail::send('email.reset',$data , function($message) use($data)
    		{
  			// $message->to($data1)->subject('This is test e-mail');
    			$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Cc('mharoon@geotargetus.com');
    			$message->Bcc('chirags@shopperlocal.com');
    			$message->subject($data['msg']);

    		});

    		echo 'Password sent to your email';
    	}
 // echo 'chirag';
    	
			//var_dump(Auth::guard('user')->user());
			//echo 'chirag1';



    }



    public function login(Request $request){
		Auth::logout(); // logging out user
		Auth::guard('user')->logout();
		Auth::guard('web')->logout();
		return view('auth/login');
	}



	function doLogout()
	{
	    Auth::logout(); // logging out user
	    Auth::guard('user')->logout();

	    
	    return Redirect::to('login'); // redirection to login screen
	}

	function doadminLogout()
	{
	     Auth::logout(); // logging out user
	     Auth::guard('user')->logout();
	     Auth::guard('web')->logout();

	    // echo 'chirag';die;
	     return Redirect::to('login');
	 }

	 function douserLogout()
	 {
	     Auth::logout(); // logging out user
	     Auth::guard('user')->logout();
	    return Redirect::to('userlogin'); // redirection to login screen
	}

	 function dorepuserLogout()
	 {
	     Auth::logout(); // logging out user
	     Auth::guard('user')->logout();
	     Auth::guard('rep_user')->logout();
	    return Redirect::to('replogin'); // redirection to login screen
	}

	function addSessionUser($id){
		$find=DB::Select("SELECT username,token_pass FROM customers_list WHERE customer_id='".$id."'");
		$explode=explode(',', $find[0]->username);
		$new_pass=decrypt($find[0]->token_pass);
		$admindata=array('customer_id'=>$id,'password'=>$new_pass);
		Auth::guard('user')->attempt($admindata);
		if(Auth::guard('user')->user()){
			return Redirect::to('userhomedashboard'.'/'.Auth::guard('user')->user()->customer_id);
		}
	}

	


	function doLogin(Request $request)
	{
		$this->validate($request,[
			'email'=>'required',
			'password'=>'required',
			
		]);

		$userdata = array(
			'email' => Input::get('email'),
			'password' =>Input::get('password')
		);
		   // echo Hash::make(Input::get('password'));
		   // die;


		if (Auth::guard('web')->attempt($userdata))
		{
			return Redirect::to('admindashboard'); 	
		}
		else
		{
			return back()->withStatus(('Email or Password is Incorrect'));
		}

	}

	function doRepLogin(Request $request)
	{
		$this->validate($request,[
			'email'=>'required',
			'password'=>'required',
			
		]);

		$userdata = array(
			'email' => Input::get('email'),
			'password' =>Input::get('password')
		);
		   // echo Hash::make(Input::get('password'));
		   // die;


		if (Auth::guard('rep_user')->attempt($userdata))
		{
			return Redirect::to('repadmindashboard'); 	
		}
		else
		{
			return back()->withStatus(('Email or Password is Incorrect'));
		}

	}

	function douserLogin(Request $request)
	{
		$this->validate($request,[
			'customer_id'=>'required',
			'password'=>'required',
			
		]);

		$userdata = array(
			'customer_id' => Input::get('customer_id'),
			'password' =>Input::get('password'),
			'kill_status'=> 1

		);


		$cust_id=$userdata['customer_id'];
		$data=DB::Select("SELECT reset_token,email FROM customers_list WHERE customer_id='".$cust_id."' and pass_status='1'");

		// var_dump($data);
		// die;

		if(!empty($data[0]->reset_token)){
			if (Auth::guard('user')->attempt($userdata))
			{
				return Redirect::to('userhomedashboard'.'/'.Auth::guard('user')->user()->customer_id); 
			}
			else
			{
          // validation not successful, send back to form
				return back()->with('status', 'Email or Password is Incorrect!');
			}
		}else{

			$token=str_random(100);
			$update = DB::table('customers_list')
              ->where('customer_id', $cust_id)
              ->update(['reset_token' => $token]);
		
			
			if($update>0){


				$data=array('msg'=>'Reset Your Password','token'=>$token);
				Mail::send('email.reset',$data , function($message) use($data)
				{
  			// $message->to($data1)->subject('This is test e-mail');
					$message->from('info@geotargetus.net', 'Geotargetus');
					$message->to('faisalk@shopperlocal.com');
					$message->Cc('mharoon@geotargetus.com');
					$message->Bcc('chirags@shopperlocal.com');
					$message->subject($data['msg']);

				});
				return back()->with('status1', 'Check your email to reset your password..your old password has been expired..!');
			}else{
				return back()->with('status', 'Email or Password is Incorrect!');
			}
			
		}



	}

}
