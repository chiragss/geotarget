<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
	public function showRegistrationForm(){
		return view('auth/register');
	}

	public function register(Request $request){
		$this->validate($request,[
         'name'=>'required|max:8',
         'email'=>'required|unique:users,email',
         'password'=>'required|required_with:password_confirmation|same:password_confirmation',
         'password_confirmation'=>'required'
      ]);
		User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' =>bcrypt($request->password),
		]);
		return redirect('/login')->with('Success');
	}
}
