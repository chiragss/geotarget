<?php
namespace App\Http\Controllers;
use App\CustomersList;
use App\FbLifelineCampaign;
use App\FbUserCampaign;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Datatables;
class LifetimeController extends Controller
{
     // function to get all campaigns for users
  public  function userCampaignList($id){
   $date=date('Y-m-d');
   $access_token=$this->access_token();
   $url="https://graph.facebook.com/v5.0/"."act_".$id."/"."campaigns?pretty=0&limit=200&fields=name,status"."&access_token=".$access_token;
   $result1 = file_get_contents($url);
        // echo $result[0]->name;
   $a=json_decode($result1);
   $data=$a->data;
   // var_dump($data);die;
   foreach($data as $result){
    $fb = new FbLifelineCampaign;
    $jsonkeyword=json_encode($data);
    $fb->cust_id=$id;
    $fb->camp_id=$result->id;
    $fb->camp_name = $result->name;
    $fb->camp_date = $date; 
    $fb->camp_status=$result->status;
    $fb->save();
  }
}

  // function to use cron job for user only
public function getCampaign(){
  $campaigns=DB::Select('SELECT camp_id FROM fb_lifetime_campaigns GROUP BY camp_id');
  foreach($campaigns as $camp){
   // $this->findCampDetails($camp->camp_id);
   // $this->ageGenderCampaign($camp->camp_id);
   // $this->locImpressionCampaign($camp->camp_id);
   // $this->pubPlatformCampaign($camp->camp_id);
     // $this->deviceCampaign($camp->camp_id);
 }
}
 // function to use admin fb campaigns only
public function findCampDetails($camp_id){
  $today=date('Y-m-d');
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v5.0/".$camp_id."/"."insights?fields=spend,clicks,impressions,ctr,cpc,conversions,reach,cost_per_conversion,website_ctr&date_preset=lifetime"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    $data=$a->data[0];
    $data1=array("spend"=>$data->spend,"clicks"=>$data->clicks,"impressions"=>$data->impressions,"ctr"=>$data->ctr,"reach"=>$data->reach);
    $jsonkeyword=json_encode($data1);
    // var_dump($jsonkeyword);die;
    DB::Select("UPDATE fb_lifetime_campaigns SET keyword='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
  }else{
   DB::Select("UPDATE fb_lifetime_campaigns SET camp_status='COMPLETED' WHERE camp_id='".$camp_id."' and camp_status='ACTIVE' and camp_date='".$today."'");
 }
}
 // function to use admin fb campaigns only based on age gender
public function ageGenderCampaign($camp_id){
 $today=date('Y-m-d');
 $access_token=$this->access_token();
 $url="https://graph.facebook.com/v5.0/".$camp_id."/"."insights?breakdowns=age,gender&date_preset=lifetime"."&access_token=".$access_token;
 $result1 = file_get_contents($url);
 $a=json_decode($result1);
 $data=$a->data;
 if(!empty($data)){
  foreach($data as $result){
    if(!empty($result->spend)){
      $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'age'=>$result->age,'gender'=>$result->gender);
    }else{
     $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'age'=>$result->age,'gender'=>$result->gender);
   }
   
 }
 
 $jsonkeyword=json_encode($data1);
 DB::Select("UPDATE fb_lifetime_campaigns SET age_gender_camp='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
}
}

// function to use impression based of device for facebook lifetime
public function deviceCampaign($camp_id){
  $today=date('Y-m-d');
   // $today='2020-01-24';
  $access_token=$this->access_token();
  $url="https://graph.facebook.com/v5.0/".$camp_id."/"."insights?breakdowns=device_platform&date_preset=lifetime"."&access_token=".$access_token;
  $result1 = file_get_contents($url);
  $a=json_decode($result1);
  $data=$a->data;
  if(!empty($data)){
    foreach($data as $result){
      if(!empty($result->impressions)){
        $data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>$result->spend);
      }else{
       $data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>0);
     }

   }

   $jsonkeyword=json_encode($data1);
   DB::Select("UPDATE fb_lifetime_campaigns SET device_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
 }
}
// function to use admin fb campaigns only based on location impression
public function locImpressionCampaign($camp_id){
 $today=date('Y-m-d');
 $access_token=$this->access_token();
 $url="https://graph.facebook.com/v5.0/".$camp_id."/"."insights?breakdowns=country,region&date_preset=lifetime"."&access_token=".$access_token;
 $result1 = file_get_contents($url);
 $a=json_decode($result1);
 $data=$a->data;
 if(!empty($data)){
  foreach($data as $result){
    if(!empty($result->spend)){
     $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'country'=>$result->country,'region'=>$result->region);
   }else{
     $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'country'=>$result->country,'region'=>$result->region);
   }
   
 }
 $jsonkeyword=json_encode($data1);
 DB::Select("UPDATE fb_lifetime_campaigns SET loc_impression='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
}
}
// function to use admin fb campaigns only for platforn based 
public function pubPlatformCampaign($camp_id){
 $today=date('Y-m-d');
 $access_token=$this->access_token();
 $url="https://graph.facebook.com/v5.0/".$camp_id."/"."insights?breakdowns=publisher_platform,impression_device,platform_position&date_preset=lifetime"."&access_token=".$access_token;
 $result1 = file_get_contents($url);
 $a=json_decode($result1);
 $data=$a->data;
 if(!empty($data)){
  foreach($data as $result){
    if(!empty($result->spend)){
      $data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
    }else{
      $data1[]=array('impressions'=>$result->impressions,'spend'=>0,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
    }
    
  }
  $jsonkeyword=json_encode($data1);
  DB::Select("UPDATE fb_lifetime_campaigns SET pub_platform_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."' and camp_date='".$today."'");
}
}
// function to update user id in campaign list 
public function userUpdateId(){
  $data=DB::Select('SELECT * FROM customers_list');
      // echo '<pre>';
      // var_dump($data);
  foreach ($data as $user) {
        // echo "UPDATE fb_campaigns SET cust_id='".$user->customer_id."' WHERE camp_name LIKE '".$user->keywords."%'";die;
    $update=DB::Select("UPDATE fb_lifetime_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->keywords."%'");
  }
}
}
