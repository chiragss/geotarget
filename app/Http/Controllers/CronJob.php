<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\CustomersList;
use App\CustomerListNew;
use App\GrndTruthCampaign;
use App\GrndTruthAdminCampaign;
use App\BasisCampaign;
use App\CampLaunch;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Support\Facades\Input;
use Redirect;
use Auth;

class CronJob extends Controller
{

	// Truncate user table
	public function truncateUser(){
		DB::Select("TRUNCATE TABLE customer_deals");
		//DB::Select("TRUNCATE TABLE customers_list");
	}

	// function to remove extra rep id's

	public function removeRep(){
		DB::Select("DELETE FROM customers_list WHERE rep_id='1'");
	}


	// AS400 add user cron job
	public function addUser(){

		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-customers.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
		
		if(!empty($results)){
			$this->truncateUser();
			foreach($results as $data)
			{
  // for deal no
				$CSOFF= $data->CSOFF;
				$CSMKT=$data->CSMKT;
				$CSSUFX=$data->CSSUFX;
  // for BUSSINESS name
				$CSBNAM=trim($data->CSBNAM);
  // for posted by
				$CIENBY=trim($data->CIENBY);
  // for customer id
				$CINUM=trim($data->CINUM);
  // for customer
				$CICNAM=trim($data->CICNAM);

				$explode=explode(',', $CICNAM);
				//var_dump($explode);
				if(!empty($explode[0])){
					$fname=strtolower($explode[0]);
					$user_id=substr($CINUM, -4) ;
					$new_pass=$fname.$user_id;
				}

  // email address
				$CIEMAL=trim($data->CIEMAL);
  // for bussiness type
				$CDDESC=trim($data->CDDESC);
  // for cell phone number
				$CSTELE=$data->CSTELE;
  // for bussines phone number
				$CICPHN=$data->CICPHN;
  // for rep id
				$CSREP1=$data->CSREP1;
  // for customer address
				$CIADDR=trim($data->CIADDR);
  // for city
				$CICITY=$data->CICITY;
  // for customer state
				$CIST=$data->CIST;
  // for customer zip code
				$CIZIP=$data->CIZIP;
  // for customer webaddress
				$CIWEB=trim($data->CIWEB);
  // for customer kill status
				$CSKILL=$data->CSKILL;
  // for customer impression for deal
				$GEOIMPRE=$data->GEOIMPRE;
    // for entry date

				$entry_date=$data->CIENDT;
    // For Start Date
				$start_day=$data->CSSTDD;
				$start_month=$data->CSSTMM;
				$start_year=$data->CSSTYY;
				$start_date1=$start_year.'-'.$start_month.'-'.$start_day;
				$date1=date_create($start_date1);
				$start_date=date_format($date1,"Y-m-d");
    // For Stop Date
				$stop_day=$data->CSCMPD;
				$stop_month=$data->CSCMPM;
				$stop_year=$data->CSCMPY;
				$stop_date1=$stop_year.'-'.$stop_month.'-'.$stop_day;
				$date2=date_create($stop_date1);
				$stop_date=date_format($date2,"Y-m-d");
    // For Rep Name
				$rep_name=trim($data->SMNAME);

    // For sale rep email
				$sale_rep_email=trim($data->SMEMAL);
// geotarget camp date
				$geodate=$data->GEOTGEDT;


				if($CSKILL=='Y'){
					$CSKILL=0;
				}else{
					$CSKILL=1;
				}
    // for groundtruth status
				$GEOGRDTR=$data->GEOGRDTR;
				if($GEOGRDTR=='Y'){
					$GEOGRDTR=1;
				}else{
					$GEOGRDTR=0;
				}

    // for facebook status
				$GEOFBADS=$data->GEOFBADS;
				if($GEOFBADS=='Y'){
					$GEOFBADS=1;
				}else{
					$GEOFBADS=0;
				}

    // for google status
				$GEOGOGLE=$data->GEOGOGLE;
				if($GEOGOGLE=='Y'){
					$GEOGOGLE=1;
				}else{
					$GEOGOGLE=0;
				}

				$find=DB::Select("SELECT customer_id FROM customers_list WHERE customer_id='".$CINUM."'");

				// var_dump($find);
				// die;

				if(!empty($find)){
					$customerlist = new CustomerListNew;
					$customerlist->customer_id=$CINUM;
					$customerlist->deal_no=$CSOFF.$CSMKT.$CSSUFX;
					$customerlist->kill_status=$CSKILL;
					$customerlist->no_of_impression=$GEOIMPRE;
					$customerlist->entry_date=$entry_date;
					$customerlist->google_ads=$GEOGOGLE;
					$customerlist->groundtruth_ads=$GEOGRDTR;
					$customerlist->facebook_ads=$GEOFBADS;
					$customerlist->start_date=$start_date;
					$customerlist->stop_date=$stop_date;
					$customerlist->geotarget_camp_date=$geodate;
					$customerlist->save();

					DB::table('customers_list')
					->where('customer_id', $CINUM)
					->update(array('no_of_impression'=>$GEOIMPRE,'token_pass'=>encrypt($new_pass),'email'=>$CIEMAL,'kill_status'=>$CSKILL));

					


				}else{

					$customer = new CustomersList;
					$customer->customer_id=$CINUM;
					$customer->username=$CICNAM;
					$customer->password=Hash::make($new_pass);
					$customer->no_of_impression=$GEOIMPRE;
					$customer->token_pass=encrypt($new_pass);
					$customer->business_name=$CSBNAM;
					$customer->email=$CIEMAL;
					$customer->posted_by=$CIENBY;
					$customer->bussiness_type=$CDDESC;
					$customer->customer=$CICNAM;
					$customer->bussiness_phone=$CICPHN;
					$customer->entry_date=$entry_date;
					$customer->cell_phone=$CSTELE;
					$customer->customer_address=$CIADDR;
					$customer->customer_city=$CICITY;
					$customer->customer_state=$CIST;
					$customer->zip_code=$CIZIP;
					$customer->rep_name=$rep_name;
					$customer->salesrep_mail=$sale_rep_email;
					$customer->rep_id=$CSREP1;
					$customer->website=$CIWEB;
					$customer->kill_status=$CSKILL;
					$customer->google_ads=$GEOGOGLE;
					$customer->groundtruth_ads=$GEOGRDTR;
					$customer->facebook_ads=$GEOFBADS;
					$customer->save();


					$customerlist = new CustomerListNew;
					$customerlist->customer_id=$CINUM;
					$customerlist->deal_no=$CSOFF.$CSMKT.$CSSUFX;
					$customerlist->kill_status=$CSKILL;
					$customerlist->no_of_impression=$GEOIMPRE;
					$customerlist->entry_date=$entry_date;
					$customerlist->google_ads=$GEOGOGLE;
					$customerlist->groundtruth_ads=$GEOGRDTR;
					$customerlist->facebook_ads=$GEOFBADS;
					$customerlist->start_date=$start_date;
					$customerlist->stop_date=$stop_date;
					$customerlist->geotarget_camp_date=$geodate;
					$customerlist->save();

					
					
				}
			}
			$this->removeRep();
			$data=array('msg'=>'Your AS400 Customer Cron Jon run Successfully');
			// Mail::send('email.cron',$data , function($message) use($data)
			// {
  	// 		// $message->to($data1)->subject('This is test e-mail');
			// 	$message->from('info@geotargetus.net', 'Geotargetus');
			// 	$message->to('faisalk@shopperlocal.com');
			// 	$message->Bcc('chirags@shopperlocal.com');
			// 	$message->subject($data['msg']);

			// });
		}
	}

	// for art received customers
	public function addUserArtRecv(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-artReceived.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
  // echo '<pre>';
  // var_dump($results);
  // die;

		foreach($results as $data){
  // for deal no
			$CSOFF= $data->WDCOFF;
			$CSMKT=$data->WDCMKT;
			$CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
			$CINUM=$CSOFF.$CSMKT.$CSSUFX;
			$WDCDAT=$data->WDCDAT;

			$year=substr($WDCDAT, -2);
			$date=substr($WDCDAT, -4,-2);
			$month=substr($WDCDAT, -6,-4);
			$date1=date_create($year.'-'.$month.'-'.$date);
			$date=date_format($date1,'Y-m-d');

			$find=DB::Select("UPDATE customer_deals SET artwork_recevied_date='".$date."' WHERE deal_no='".$CINUM."'");
// var_dump($find);
// die;
		}
		$data=array('msg'=>'Your AS400 Customer Art Received Cron Jon run Successfully');
		// Mail::send('email.cron',$data , function($message) use($data)
		// {
  // // $message->to($data1)->subject('This is test e-mail');
		// 	$message->from('info@geotargetus.net', 'Geotargetus');
		// 	$message->to('faisalk@shopperlocal.com');
		// 	$message->Bcc('chirags@shopperlocal.com');
		// 	$message->subject($data['msg']);

		// });
	}
	/*Close for art received customers*/
	/*For add user update for art launched*/
	public function addUserArtLaunch(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-artLaunched.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
  // echo '<pre>';
  // var_dump($results);
  // die;

		foreach($results as $data){
  // for deal no
			$CSOFF= $data->WDCOFF;
			$CSMKT=$data->WDCMKT;
			$CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
			$CINUM=$CSOFF.$CSMKT.$CSSUFX;
			$WDCDAT=$data->WDCDAT;
			$year=substr($WDCDAT, -2);
			$date=substr($WDCDAT, -4,-2);
			$month=substr($WDCDAT, -6,-4);
			$date1=date_create($year.'-'.$month.'-'.$date);
			$date=date_format($date1,'Y-m-d');

			$find=DB::Select("UPDATE customer_deals SET artwork_lauch_date='".$date."' WHERE deal_no='".$CINUM."'");
// var_dump($find);
// die;
		}
		$data=array('msg'=>'Your AS400 Customer Art Launch date Cron Jon run Successfully');
		// Mail::send('email.cron',$data , function($message) use($data)
		// {
  // // $message->to($data1)->subject('This is test e-mail');
		// 	$message->from('info@geotargetus.net', 'Geotargetus');
		// 	$message->to('faisalk@shopperlocal.com');
		// 	$message->Bcc('chirags@shopperlocal.com');
		// 	$message->subject($data['msg']);

		// });

	}
	/*Close For add user update for art launched*/
	// for add user update for user campaign report sent
	public function addUserCampRepSent(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-campReportSent.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
  // echo '<pre>';
  // var_dump($results);
  // die;

		foreach($results as $data){
  // for deal no
			$CSOFF= $data->WDCOFF;
			$CSMKT=$data->WDCMKT;
			$CSSUFX=$data->WDCSFX;
  // for BUSSINESS name
			$CINUM=$CSOFF.$CSMKT.$CSSUFX;
			$WDCDAT=$data->WDCDAT;
			$year=substr($WDCDAT, -2);
			$date=substr($WDCDAT, -4,-2);
			$month=substr($WDCDAT, -6,-4);
			$date1=date_create($year.'-'.$month.'-'.$date);
			$date=date_format($date1,'Y-m-d');
			$find=DB::Select("UPDATE customer_deals SET report_sent_date='".$date."' WHERE deal_no='".$CINUM."'");
// var_dump($find);
// die;
		}
		$data=array('msg'=>'Your AS400 Customer Rep Sent Date Cron Jon run Successfully');
		// Mail::send('email.cron',$data , function($message) use($data)
		// {
  // // $message->to($data1)->subject('This is test e-mail');
		// 	$message->from('info@geotargetus.net', 'Geotargetus');
		// 	$message->to('faisalk@shopperlocal.com');
		// 	$message->Bcc('chirags@shopperlocal.com');
		// 	$message->subject($data['msg']);

		// });

	}
	/*Close for add user update for user campaign report sent*/
	// function to update user landing page url
	public function addUserLandingPage(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-landingpageURL.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
  // echo '<pre>';
  // var_dump($results);
  // die;
		$select=DB::Select("SELECT customers_list.customer_id,customers_list.landing_page,customer_deals.deal_no FROM customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id  WHERE customers_list.landing_page is null GROUP BY customers_list.customer_id");
		if(!empty($select)){
			foreach($select as $users){
				foreach($results as $data){
  // for BUSSINESS name
					
					if($data->GTDEAL==$users->deal_no){

						$CINUM=$data->GTDEAL;
						$url1=trim($data->GTHYPLNK);
						$url=substr_replace($url1,"", -1);
						if(!empty($url)){
							$find=DB::Select("UPDATE customers_list INNER JOIN customer_deals ON customers_list.customer_id=customer_deals.customer_id SET customers_list.landing_page='".$url."' WHERE customer_deals.deal_no='".$CINUM."'");

						}
					}
					
					

				}
			}
		}

		
		$data=array('msg'=>'Your AS400 Customer Landing Page Cron Jon run Successfully');
		// Mail::send('email.cron',$data , function($message) use($data)
		// {
  // // $message->to($data1)->subject('This is test e-mail');
		// 	$message->from('info@geotargetus.net', 'Geotargetus');
		// 	$message->to('faisalk@shopperlocal.com');
		// 	$message->Bcc('chirags@shopperlocal.com');
		// 	$message->subject($data['msg']);

		// });

	}

	// function to get camp end date from 30 days before
	public function campEndDate(){
		$date=date('Y-m-d');
		//$date='2020-07-21';
		$date2=date('Y-m-d', strtotime($date. ' +30 days'));
		$data=DB::Select('SELECT * FROM customers_list');
		foreach ($data as $user) {
			$select=DB::Select("SELECT customers_list.salesrep_mail,customers_list.rep_name,customers_list.business_name,google_campaigns.cust_id,google_campaigns.camp_end_date FROM google_campaigns INNER JOIN customers_list ON google_campaigns.cust_id=customers_list.customer_id WHERE google_campaigns.cust_id='".trim($user->customer_id)."' and google_campaigns.camp_start_date is not null and google_campaigns.camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".trim($user->customer_id)."') and customers_list.kill_status='1' GROUP BY google_campaigns.camp_end_date");
			// echo "SELECT customers_list.salesrep_mail,customers_list.rep_name,customers_list.business_name,google_campaigns.cust_id,google_campaigns.camp_end_date FROM google_campaigns INNER JOIN customers_list ON google_campaigns.cust_id=customers_list.customer_id WHERE google_campaigns.cust_id='".trim($user->customer_id)."' and google_campaigns.camp_start_date is not null and google_campaigns.camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".trim($user->customer_id)."') and customers_list.kill_status='1' GROUP BY google_campaigns.camp_end_date";
			// continue;
			if(!empty($select)){
				foreach($select as $user2){
					$info[]=array('cust_id'=>$user2->cust_id,'camp_end_date'=>$user2->camp_end_date,'business_name'=>$user2->business_name,'rep_name'=>$user2->rep_name,'rep_mail'=>$user2->salesrep_mail);
				}
			}
		}
		

		if(!empty($info)){
			
			foreach($info as $inf){
				$date3=date('Y-m-d', strtotime($inf['camp_end_date']. ' +30 days'));
				if($date<$inf['camp_end_date'] && $date2>$inf['camp_end_date']){
					$info1[]=array('cust_id'=>$inf['cust_id'],'camp_end_date'=>$inf['camp_end_date'],'business_name'=>$inf['business_name'],'rep_name'=>$inf['rep_name'],'rep_mail'=>$inf['rep_mail']);
				}
			}
		}
		
		foreach($info1 as $res){
			$mail[]=$res['rep_mail'];
		}
		$unique_email=array_unique($mail);
		$i=0;$j=0;
		foreach($unique_email as $m){
			foreach($info1 as $inf){
				if($m==$inf['rep_mail']){
					$result[$i][$j]=array('cust_id'=>$inf['cust_id'],'camp_end_date'=>$inf['camp_end_date'],'business_name'=>$inf['business_name'],'rep_name'=>$inf['rep_name'],'rep_mail'=>$inf['rep_mail']);
				}
				$j++;
			}
			$i++; 
		}
		// var_dump($unique_email);
		// die;
		for($k=0;$k<count($result);$k++){
			if(!empty($result[$k])){
				// echo '<pre>';
				// var_dump($result[$k]);
				$data=array('msg'=>'Expire Customer Report','users'=>$result[$k],'rep_mail'=>$unique_email[$k]);

				// Mail::send('email.end_date',$data , function($message) use($data)
				// {
				// 	$message->from('info@geotargetus.net', 'Geotargetus');
				// 	$message->To($data['rep_mail']);
				// 	$message->Cc('mharoon@geotargetus.com');
				// 	$message->Cc('tstofsky@geotargetus.com');
				// 	$message->Cc('ipsitab@geotargetus.com');
				// 	$message->Bcc('faisalk@shopperlocal.com');
				// 	$message->Bcc('chirags@shopperlocal.com');
				// 	$message->subject($data['msg']);

				// });
			}
		}
		
	}


	/*Close function to update landing page url*/
	// function to get all campaigns for users
	public  function grndUserCampaigns(){
		//$date3=date('Y-m-d');
		// $date=date("Y-m-d", strtotime("-1 day"));
		// $seven_day_before=date("Y-m-d", strtotime("-1 day"));
		//$date='2020-07-23';
		//$seven_day_before='2020-07-23';
		//echo phpinfo();  
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://reporting.groundtruth.com/demand/v1/org/1008848/totals?start_date=".$seven_day_before."&end_date=".$date."&all_campaigns=1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"X-GT-USER-ID: 49177",
				"X-GT-API-KEY: =ab6hp-0'xVL<f]b"
			),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		
		$data=json_decode($response);
		
		foreach($data as $result){
			$json=array("cumulative_reach"=>$result->cumulative_reach,"video_start"=>$result->video_start,"click_to_call"=>$result->click_to_call,"video"=>$result->video,"visits"=>$result->visits,"spend"=>$result->spend,"ctr"=>$result->ctr,"cpm"=>$result->cpm,"impressions"=>$result->impressions,"clicks"=>$result->clicks);
			$grnd = new GrndTruthCampaign;
			$jsonkeyword=json_encode($json);
			$grnd->cust_id = '1008848';
			$grnd->camp_id = $result->campaign_id;
			$grnd->camp_name = $result->campaign_name;
			$grnd->keyword = $jsonkeyword;
			$grnd->camp_start_date = $seven_day_before;
			$grnd->camp_end_date = $date;
			$grnd->camp_date = $date; 
			$grnd->save();

		}
			//$date2=date('Y-m-d', strtotime($date. ' +1 days'));
			//$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='1'");
		$data=array('msg'=>'Your GT Campaigns Update cron run Successfully');
			// Mail::send('email.cron',$data , function($message) use($data)
			// {

			// 	$message->from('info@geotargetus.net', 'Geotargetus');
			// 	$message->to('faisalk@shopperlocal.com');
			// 	$message->Bcc('chirags@shopperlocal.com');
			// 	$message->subject($data['msg']);

			// });
		//}
		

	}

	// function to add basis campaign on daily

	public function addBasisCamp(){
		$token=$this->basisToken();
		$start_date='2020-07-27';
		$end_date='2020-07-27';
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.basis.net/v1/campaigns",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array($token),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$data=json_decode($response);
		if(!empty($data)){

			$data1=$data->data;
			if(!empty($data1)){
				foreach($data1 as $camp){
					$camp_id=$camp->id;
					$camp_name=$camp->name;
					$status=$camp->status;
					$approved_budget=$camp->approved_budget;
					$client_id=$camp->client_id;
					$brand_id=$camp->brand_id;
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.basis.net/v1/stats/daily_by_line_item?client_id=".$client_id."&campaign_id=".$camp_id."&start_date=".$start_date."&end_date=".$end_date,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => array($token),
					));
					$response1 = curl_exec($curl);
					curl_close($curl);
					$data=json_decode($response1);
					
					if(!empty($data)){
						$new_data=$data->data;
						// echo '<pre>';
						// var_dump($new_data);
						if(!empty($new_data)){

							foreach($new_data as $result){
								$delivered_impressions=$result->delivery_metrics->delivered_impressions;
								$delivered_units=$result->delivery_metrics->delivered_units;
								$delivered_clicks=$result->delivery_metrics->delivered_clicks;
								$delivered_date=$result->delivered_date;
								$delivered_video_starts=$result->delivery_metrics->delivered_video_starts;
								$delivered_video_completions=$result->delivery_metrics->delivered_video_completions;
								$delivered_data_spend=$result->delivery_metrics->delivered_data_spend;
								$json=array("delivered_impressions"=>$delivered_impressions,"delivered_units"=>$delivered_units,"delivered_clicks"=>$delivered_clicks,"delivered_video_starts"=>$delivered_video_starts,"delivered_video_completions"=>$delivered_video_completions,"delivered_data_spend"=>$delivered_data_spend);
								$basis = new BasisCampaign;
								$jsonkeyword=json_encode($json);
								$basis->cust_id = '1008848';
								$basis->camp_id = $camp_id;
								$basis->camp_name = $camp_name;
								$basis->client_id=$client_id;
								$basis->brand_id=$brand_id;
								$basis->keyword = $jsonkeyword;
								$basis->camp_start_date = $start_date;
								$basis->camp_end_date = $end_date;
								$basis->camp_date = $delivered_date;
								$basis->status=$status; 
								$basis->save();
							}
						}

						
					}
				}
			}
			
		}

	}

	// function to update user id in ground camp list for groundtruth
	public function userGrndUpdateId(){
		$date=date('Y-m-d');
		//$query=DB::Select("SELECT * FROM cron_job WHERE id='2' and cron_job_start='".$date."'");
		//if($query){
		$data=DB::Select('SELECT * FROM customers_list');
		foreach ($data as $user) {
			$update=DB::Select("UPDATE groundtruth_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
		}
			// $date2=date('Y-m-d', strtotime($date. ' +6 days'));
			// $update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='2'");
			// $data=array('msg'=>'Your GT Campaigns User Update cron run Successfully');
			// Mail::send('email.cron',$data , function($message) use($data)
			// {

			// 	$message->from('info@geotargetus.net', 'Geotargetus');
			// 	$message->to('faisalk@shopperlocal.com');
			// 	$message->Bcc('chirags@shopperlocal.com');
			// 	$message->subject($data['msg']);

			// });
		//}

	}

	// update basis user update
	public function userBasisUpdate(){
		$date=date('Y-m-d');
		//$query=DB::Select("SELECT * FROM cron_job WHERE id='2' and cron_job_start='".$date."'");
		//if($query){
		$data=DB::Select('SELECT * FROM customers_list');
		foreach ($data as $user) {
			$update=DB::Select("UPDATE basis_campaigns SET cust_id='".trim($user->customer_id)."' WHERE camp_name LIKE '".$user->customer_id."%'");
		}
			// $date2=date('Y-m-d', strtotime($date. ' +6 days'));
			// $update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='2'");
			// $data=array('msg'=>'Your GT Campaigns User Update cron run Successfully');
			// Mail::send('email.cron',$data , function($message) use($data)
			// {

			// 	$message->from('info@geotargetus.net', 'Geotargetus');
			// 	$message->to('faisalk@shopperlocal.com');
			// 	$message->Bcc('chirags@shopperlocal.com');
			// 	$message->subject($data['msg']);

			// });
		//}

	}

	public function useradCampDetail(){
		$date=date('Y-m-d');
		//$query=DB::Select("SELECT * FROM cron_job WHERE id='4' and cron_job_start='".$date."'");
		//if($query){
		$campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
		foreach($campaigns as $camp){
			$this->userfindCampDetails($camp->camp_id);
		}
		$date2=date('Y-m-d', strtotime($date. ' +5 days'));
			//$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='4'");
		$data=array('msg'=>'Your Facebook Campaign for last 30 days  cron run Successfully');
		Mail::send('email.cron',$data , function($message) use($data)
		{
  			// $message->to($data1)->subject('This is test e-mail');
			$message->from('info@geotargetus.net', 'Geotargetus');
			$message->to('faisalk@shopperlocal.com');
			$message->Bcc('chirags@shopperlocal.com');
			$message->subject($data['msg']);

		});
		//}
		
	}

	public function useradCreativeCampaign(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='3' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_admin_campaigns GROUP BY camp_id');

			foreach($campaigns as $camp){
				$this->adCreativeImage($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +2 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='3'");
			$data=array('msg'=>'Your Facebook Creative Image Update cron run Successfully');
			// Mail::send('email.cron',$data , function($message) use($data)
			// {
			// 	$message->to($data1)->subject('This is test e-mail');
			// 	$message->from('info@geotargetus.net', 'Geotargetus');
			// 	$message->to('faisalk@shopperlocal.com');
			// 	$message->Bcc('chirags@shopperlocal.com');
			// 	$message->subject($data['msg']);

			// });
		}
		
	}

	public function userGenderCamp(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='5' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
			foreach($campaigns as $camp){
				$this->userageGenderCampaign($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +3 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='5'");
			$data=array('msg'=>'Your Facebook Campaign for gender 30 days run Successfully');
			Mail::send('email.cron',$data , function($message) use($data)
			{
  // $message->to($data1)->subject('This is test e-mail');
				$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});
		}
		
	}

	public function userPubPlatformCamp(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='6' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
			foreach($campaigns as $camp){
				$this->userpubPlatformCampaign($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +3 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='6'");
			$data=array('msg'=>'Your Facebook Campaign for publisher Platform 30 days run Successfully');
			Mail::send('email.cron',$data , function($message) use($data)
			{
  // $message->to($data1)->subject('This is test e-mail');
				$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});
		}
		
	}

	public function userdisplayvideo(){
		// $date=date('Y-m-d');
		// $query=DB::Select("SELECT * FROM cron_job WHERE id='10' and cron_job_start='".$date."'");
		// if($query){
		$campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
		foreach($campaigns as $camp){
			$this->fbVideoDisplay($camp->camp_id);
		}
			// $date2=date('Y-m-d', strtotime($date. ' +7 days'));
			// $update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='10'");
		// }
		
	}

	public function lifetimedisplayvideo(){
		// $date=date('Y-m-d');
		// $query=DB::Select("SELECT * FROM cron_job WHERE id='10' and cron_job_start='".$date."'");
		// if($query){
		$campaigns=DB::Select('SELECT camp_id FROM fb_campaigns GROUP BY camp_id');
		foreach($campaigns as $camp){
			$this->lifetimefbVideoDisplay($camp->camp_id);
		}
			// $date2=date('Y-m-d', strtotime($date. ' +7 days'));
			// $update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='10'");
		// }
		
	}



	public function fbVideoDisplay($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."ads"."?access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				$url1="https://graph.facebook.com/v7.0/".$result->id."/"."insights?fields=ad_name,impressions,video_play_actions"."&access_token=".$access_token;
				$result1 = file_get_contents($url1);
				$a=json_decode($result1);
				$data1=$a->data;
				if(!empty($data1)){
					foreach($data1 as $result1){
						$data3=NULL;
						if(strstr($result1->ad_name,'Video')){
							if(!empty($result1->video_play_actions[0]->value)){
								$video=$result1->video_play_actions[0]->value;
							}else{
								$video=null;
							}
							$data3[]=array('ad_name'=>$result1->ad_name,'impresison'=>$result1->impressions,'video_played'=>$video);
							$jsonkeyword=json_encode($data3);
							DB::Select("UPDATE fb_campaigns SET video_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
						}else{
							$data4=NULL;
							if(strstr($result1->ad_name,'Display')){
								
								$data4[]=array('ad_name'=>$result1->ad_name,'impresison'=>$result1->impressions,'video_played'=>null);
								$jsonkeyword=json_encode($data4);
								DB::Select("UPDATE fb_campaigns SET display_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
							}
							
						}
					}
					
					
				}else{
					DB::Select("UPDATE fb_campaigns SET video_cammpaign='null',display_campaign='null' WHERE camp_id='".$camp_id."'");
				}

			}
		}

	}

	// function to use update ad creative images
	public function adCreativeImage($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."ads"."?access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);

		$data=$a->data;
		// var_dump($data);
		// die;
		if(!empty($data)){
			foreach($data as $result){
				$url1="https://graph.facebook.com/v7.0/".$result->id."/"."previews?ad_format=DESKTOP_FEED_STANDARD"."&access_token=".$access_token;
				$result1 = file_get_contents($url1);
				$a=json_decode($result1);
				$data1=$a->data;
				if(!empty($data1)){
					foreach($data1 as $result1){
						if(!empty($result1->body)){
							$data3[]=$result1->body;
						}else{
							$data3[]=NULL;
						}
					}
					$jsonkeyword=implode(',', $data3);

					DB::Select("UPDATE fb_admin_campaigns SET ad_creative_image='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
				}
			}
		}
	}

	// function to use fb campaigns for 30 days only
	public function userfindCampDetails($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?fields=spend,clicks,impressions,ctr,cpc,conversions,reach,cost_per_conversion,website_ctr"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			$data=$a->data[0];

			$data1=array("spend"=>$data->spend,"clicks"=>$data->clicks,"impressions"=>$data->impressions,"ctr"=>$data->ctr,"reach"=>$data->reach);
			$jsonkeyword=json_encode($data1);

			DB::Select("UPDATE fb_campaigns SET keyword='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}else{
			DB::Select("UPDATE fb_campaigns SET camp_status='COMPLETED' WHERE camp_id='".$camp_id."' and camp_status='ACTIVE'");
		}
	}

	// function to use fb campaigns for user based on age gender
	public function userageGenderCampaign($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=age,gender"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				if(!empty($result->spend)){
					$data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'age'=>$result->age,'gender'=>$result->gender);
				}else{
					$data1[]=array('impressions'=>$result->impressions,'spend'=>0,'age'=>$result->age,'gender'=>$result->gender);
				}

			}

			$jsonkeyword=json_encode($data1);
			DB::Select("UPDATE fb_campaigns SET age_gender_camp='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}
	}

	public function userpubPlatformCampaign($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=publisher_platform,impression_device,platform_position"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				if(!empty($result->spend)){
					$data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
				}else{
					$data1[]=array('impressions'=>$result->impressions,'spend'=>0,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
				}

			}
			$jsonkeyword=json_encode($data1);
			DB::Select("UPDATE fb_campaigns SET pub_platform_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}
	}

	// facebook lifetime data
	public function lifetimeadCampDetail(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='7' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_lifetime_campaigns GROUP BY camp_id');
			foreach($campaigns as $camp){
				$this->lifetimefindCampDetails($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +3 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='7'");
			$data=array('msg'=>'Your Facebook Campaign for update lifetime run Successfully');
			Mail::send('email.cron',$data , function($message) use($data)
			{
  // $message->to($data1)->subject('This is test e-mail');
				$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});

		}
		
	}

	public function lifetimeGenderCamp(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='8' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_lifetime_campaigns GROUP BY camp_id');
			foreach($campaigns as $camp){
				$this->lifetimeageGenderCampaign($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +3 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='8'");
			$data=array('msg'=>'Your Facebook Campaign for Gender Update Lifetime run Successfully');
			Mail::send('email.cron',$data , function($message) use($data)
			{
  			// $message->to($data1)->subject('This is test e-mail');
				$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});
		}
		
	}

	public function lifetimePubPlatformCamp(){
		$date=date('Y-m-d');
		$query=DB::Select("SELECT * FROM cron_job WHERE id='9' and cron_job_start='".$date."'");
		if($query){
			$campaigns=DB::Select('SELECT camp_id FROM fb_lifetime_campaigns GROUP BY camp_id');
			foreach($campaigns as $camp){
				$this->lifetimepubPlatformCampaign($camp->camp_id);
			}
			$date2=date('Y-m-d', strtotime($date. ' +3 days'));
			$update=DB::Select("UPDATE cron_job SET cron_job_start='".$date2."' WHERE id='9'");
			$data=array('msg'=>'Your Facebook Campaign for publisher Platform Lifetime run Successfully');
			Mail::send('email.cron',$data , function($message) use($data)
			{
  // $message->to($data1)->subject('This is test e-mail');
				$message->from('info@geotargetus.net', 'Geotargetus');
				$message->to('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});
		}
		
	}




	// function to use lifetime fb campaigns only
	public function lifetimefindCampDetails($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?fields=spend,clicks,impressions,ctr,cpc,conversions,reach,cost_per_conversion,website_ctr&date_preset=lifetime"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			$data=$a->data[0];
			$data1=array("spend"=>$data->spend,"clicks"=>$data->clicks,"impressions"=>$data->impressions,"ctr"=>$data->ctr,"reach"=>$data->reach);
			$jsonkeyword=json_encode($data1);
    // var_dump($jsonkeyword);die;
			DB::Select("UPDATE fb_lifetime_campaigns SET keyword='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}else{
			DB::Select("UPDATE fb_lifetime_campaigns SET camp_status='COMPLETED' WHERE camp_id='".$camp_id."' and camp_status='ACTIVE'");
		}
	}

	// function to use lifetime fb campaigns only based on age gender
	public function lifetimeageGenderCampaign($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=age,gender&date_preset=lifetime"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				if(!empty($result->spend)){
					$data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'age'=>$result->age,'gender'=>$result->gender);
				}else{
					$data1[]=array('impressions'=>$result->impressions,'spend'=>0,'age'=>$result->age,'gender'=>$result->gender);
				}

			}

			$jsonkeyword=json_encode($data1);
			DB::Select("UPDATE fb_lifetime_campaigns SET age_gender_camp='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}
	}

	// function to use impression based of device for facebook lifetime
	public function lifetimedeviceCampaign($camp_id){
		$today=date('Y-m-d');
   // $today='2020-01-24';
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=device_platform&date_preset=lifetime"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				if(!empty($result->impressions)){
					$data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>$result->spend);
				}else{
					$data1[]=array('device_platform'=>$result->device_platform,'impressions'=>$result->impressions,'spend'=>0);
				}

			}

			$jsonkeyword=json_encode($data1);
			DB::Select("UPDATE fb_lifetime_campaigns SET device_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}
	}

// Function to use lifetime facebook publisher plateform camp
	public function lifetimepubPlatformCampaign($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."insights?breakdowns=publisher_platform,impression_device,platform_position&date_preset=lifetime"."&access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				if(!empty($result->spend)){
					$data1[]=array('impressions'=>$result->impressions,'spend'=>$result->spend,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
				}else{
					$data1[]=array('impressions'=>$result->impressions,'spend'=>0,'publisher_platform'=>$result->publisher_platform,'impression_device'=>$result->impression_device,'platform_position'=>$result->platform_position);
				}

			}
			$jsonkeyword=json_encode($data1);
			DB::Select("UPDATE fb_lifetime_campaigns SET pub_platform_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
		}
	}

	public function lifetimefbVideoDisplay($camp_id){
		$today=date('Y-m-d');
		$access_token=$this->access_token();
		$url="https://graph.facebook.com/v7.0/".$camp_id."/"."ads"."?access_token=".$access_token;
		$result1 = file_get_contents($url);
		$a=json_decode($result1);
		$data=$a->data;
		if(!empty($data)){
			foreach($data as $result){
				$url1="https://graph.facebook.com/v7.0/".$result->id."/"."insights?fields=ad_name,impressions,video_play_actions&date_preset=lifetime"."&access_token=".$access_token;
				$result1 = file_get_contents($url1);
				$a=json_decode($result1);
				$data1=$a->data;
				if(!empty($data1)){
					foreach($data1 as $result1){
						$data3=NULL;
						if(strstr($result1->ad_name,'Video')){
							if(!empty($result1->video_play_actions[0]->value)){
								$video=$result1->video_play_actions[0]->value;
							}else{
								$video=null;
							}
							$data3[]=array('ad_name'=>$result1->ad_name,'impresison'=>$result1->impressions,'video_played'=>$video);
							$jsonkeyword=json_encode($data3);
							DB::Select("UPDATE fb_campaigns SET video_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
						}else{
							$data4=NULL;
							if(strstr($result1->ad_name,'Display')){
								
								$data4[]=array('ad_name'=>$result1->ad_name,'impresison'=>$result1->impressions,'video_played'=>null);
								$jsonkeyword=json_encode($data4);
								DB::Select("UPDATE fb_campaigns SET display_campaign='".$jsonkeyword."' WHERE camp_id='".$camp_id."'");
							}
							
						}
					}
					
					
				}else{
					DB::Select("UPDATE fb_campaigns SET video_cammpaign='null',display_campaign='null' WHERE camp_id='".$camp_id."'");
				}

			}
		}

	}

	// Cron job function to use groundtruth data
	public function getGrndCampaign(){
		$date=date('Y-m-d');
		$seven_day_before=date('Y-m-d',strtotime("-2 days"));
		
		$campaigns=DB::Select('SELECT camp_id FROM groundtruth_campaigns GROUP BY camp_id');
		// echo '<pre>';
		//  var_dump($campaigns);die;
		foreach($campaigns as $camp){
		 // $this->findGrndCampDetails($camp->camp_id,$date,$seven_day_before);
		 // $this->ageGenderCampaign($camp->camp_id);
		 // $this->locImpressionCampaign($camp->camp_id);
			$this->adCreativeImageGrnd($camp->camp_id,$date,$seven_day_before);
		}
	}


	// for groundtruth campaign
	public function adCreativeImageGrnd($camp_id,$date,$seven_day_before){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL =>  "https://reporting.groundtruth.com/demand/v1/creatives/product/".trim($camp_id)."?start_date=".$seven_day_before."&end_date=".$date."&all_campaigns=1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"X-GT-USER-ID: 49177",
				"X-GT-API-KEY: =ab6hp-0'xVL<f]b"
			),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$data1=json_decode($response);
	}
	// for launch date
	public function launchDate(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-campaign-launched.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
		
		// $this->adCreativeImage($camp_id);
		if(!empty($results)){
			foreach($results as $email){
			//var_dump($email);
				$a='CSCST'."#";
				$cust_id=$email->$a;
				$launchdate=$email->WDCDAT;
				$year=substr($launchdate,-2);
				$day=substr($launchdate,-4,2);
				$month=substr($launchdate,0, -4);
				$start_date=$year.'-'.$month.'-'.$day;
				$date_create=date_create($start_date);
				$date=date_format($date_create,"Y-m-d");
				$check=DB::Select("SELECT * FROM camp_launch WHERE cust_id='".$cust_id."'");
				if(empty($check)){
					$data=array('cust_id'=>$cust_id,"launch_date"=>$date,"flag"=>'1');
					DB::table('camp_launch')->insert($data);
					// if(!empty($fb_creative[0]->camp_id)){
					// 	$this->adCreativeImage($fb_creative[0]->camp_id);
					// }
				}
			}
		}


	}

	// for one day before launch date report
	public function beforelaunchDate(){
		
		$tomorrow = date("Y-m-d", strtotime("-1 day"));
		$check=DB::Select("SELECT * FROM camp_launch WHERE launch_date='".$tomorrow."'");
		// $this->adCreativeImage($camp_id);
		if(!empty($check)){
			foreach($check as $email){

				$cust_id=$email->cust_id;
				$launchdate=$email->launch_date;
				
				$check=DB::Select("SELECT * FROM reminder_mail WHERE cust_id='".$cust_id."'");
				if(empty($check)){
					// if(!empty($fb_creative[0]->camp_id)){
					// 	$this->adCreativeImage($fb_creative[0]->camp_id);
					// }
					$data=DB::Select("SELECT DISTINCT(camp_name),ad_creative_image FROM fb_admin_campaigns WHERE cust_id='".trim($cust_id)."'");
					$user=DB::Select("SELECT * FROM customers_list WHERE customer_id='".trim($cust_id)."'");
					$google=DB::Select("SELECT youtube_link FROM google_campaigns WHERE cust_id='".trim($cust_id)."'");
					$fb_creative=DB::Select("SELECT camp_id FROM fb_admin_campaigns WHERE cust_id='".trim($cust_id)."'");

					if(!empty($data)){
						$fb_url=NULL;
						foreach($data as $image){
							$a=explode(',', $image->ad_creative_image);
						}
						if(!empty($a[0])){
							$string=$a[0];
							preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $string, $match);
							$fb_url[]=$match[0];

						}
						if(!empty($a[1])){
							$string=$a[1];
							preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $string, $match);
							$fb_url[]=$match[0];
						}

					}

					if(!empty($user)){
						foreach($user as $userdata){
							$username=$userdata->username;
							$landing_page=$userdata->landing_page;
							$business_name=$userdata->business_name;
							$useremail=$userdata->email;
							$salesrep_mail=$userdata->salesrep_mail;
						}
					}
					if(!empty($google)){
						foreach($google as $video){
							if(!empty($video->youtube_link)){
								$link= json_decode($video->youtube_link);

								$url=$link[0]->video_url;
							}
						}
					}
					if(empty($fb_url)){
						$fb_url=NULL;
					}
					if(empty($url)){
						$url=NULL;
					}
					$token=str_random(100);
					DB::Select("UPDATE customers_list SET reset_token='".$token."' WHERE customer_id='".$cust_id."'");
					$data=array('msg'=>$business_name.' | '.'Your Geotarget Campaign has Launched!','fb'=>$fb_url,'landing_page'=>$landing_page,"url"=>$url,'username'=>$username,'cust_id'=>$cust_id,'useremail'=>$useremail,'salesrep_mail'=>$salesrep_mail,'token'=>$token);


					Mail::send('email.launch_date',$data , function($message) use($data)
					{
						$message->from('info@geotargetus.net', 'Geotargetus');
						//$message->Bcc('geotargetteam@geotargetus.com');
						//$message->To($data['useremail']);
						//$message->Cc($data['salesrep_mail']);
						$message->Bcc('faisalk@shopperlocal.com');
						$message->Bcc('chirags@shopperlocal.com');
						$message->subject($data['msg']);
					});
					// $year=substr($launchdate,-2);
					// $day=substr($launchdate,-4,2);
					// $month=substr($launchdate,0, -4);
					// $start_date=$year.'-'.$month.'-'.$day;
					// $date_create=date_create($start_date);
					// $date=date_format($date_create,"Y-m-d");

					// $data=array('cust_id'=>$cust_id,"launch_date"=>$launchdate,"flag"=>'1');
					// DB::table('camp_launch')->insert($data);

				}
				

				
				
			}
		}


	}

	// add list of launch date
	public function addLaunchDate(){
		set_time_limit(0);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,'http://dev.shopperlocal.com/as400/GTFILES/GT-campaign-launched-list.php');
		curl_setopt($curl_handle, CURLOPT_HEADER, 0);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$results = curl_exec($curl_handle);
		$results = json_decode($results);
		var_dump($results);
		die;
		// $this->adCreativeImage($camp_id);
		if(!empty($results)){
			foreach($results as $email){
			//var_dump($email);
				$a='CSCST'."#";
				$cust_id=$email->$a;
				$launchdate=$email->WDCDAT;
				$check=DB::Select("SELECT * FROM camp_launch WHERE cust_id='".$cust_id."'");
				if(empty($check)){
					$year=substr($launchdate,-2);
					$day=substr($launchdate,-4,2);
					$month=substr($launchdate,0, -4);
					$start_date=$year.'-'.$month.'-'.$day;
					$date_create=date_create($start_date);
					$date=date_format($date_create,"Y-m-d");
					//die;
					$data=array('cust_id'=>$cust_id,"launch_date"=>$date,"flag"=>'0');
					DB::table('camp_launch')->insert($data);
				}

				
				
			}
		}


	}

	// FOR REMINDER EMAIL
	public function reminderEmail(){
		$data=DB::Select("SELECT * FROM camp_launch");
		if(!empty($data)){
			foreach($data as $email){
				$cust_id=trim($email->cust_id);

				$launch_date=$email->launch_date;
				$next_launch_date=$email->next_launch_date;
				$flag=$email->flag;
				$today=date('Y-m-d');
				$today_date=date('d',strtotime($today));
				$today_month=date('m',strtotime($today));
				$today_year=date('Y',strtotime($today));
				$launch_date_date=date('d',strtotime($launch_date));
				$launch_date_month=date('m',strtotime($launch_date));
				$launch_date_year=date('Y',strtotime($launch_date));
				
				$user=DB::Select("SELECT * FROM customers_list WHERE customer_id='".trim($cust_id)."'");
				$end_date=DB::Select("SELECT DISTINCT(camp_end_date) FROM `google_campaigns` Where cust_id='".$cust_id."' and camp_end_date=(SELECT MAX(camp_end_date) FROM google_campaigns WHERE cust_id='".$cust_id."')");
				if(!empty($end_date[0]->camp_end_date)){
					$camp_end_date=$end_date[0]->camp_end_date;
				}else{
					continue;
				}
				
				
				if(!empty($user)){
					foreach($user as $userdata){
						$username=$userdata->username;
						$landing_page=$userdata->landing_page;
						$business_name=$userdata->business_name;
						$useremail=$userdata->email;
						$salesrep_mail=$userdata->salesrep_mail;
					}
				}

				if($flag==0){
					if($launch_date_date==$today_date && ($launch_date_month<$today_month || $launch_date_month>$today_month) && $launch_date_year<=$today_year && $camp_end_date>=$today){
						$now = time(); // or your date as well
						$your_date = strtotime($launch_date);
						$datediff = $now - $your_date;
						$days=round($datediff / (60 * 60 * 24)) + 30;
						$next_date=date('Y-m-d', strtotime('+'.$days. 'days',strtotime($launch_date)));
						$user=DB::Select("SELECT * FROM customers_list WHERE customer_id='".$cust_id."'");
						$array=array('cust_id'=>trim($cust_id),'created_at'=>date('Y-m-d H:i:s'));
						$insert=DB::table('reminder_mail')->insert($array);
						$update=DB::Select("UPDATE camp_launch SET flag=2,next_launch_date='".$next_date."' WHERE cust_id='".$cust_id."'");
						
						$data=array('msg'=>'REMINDER: CHECK YOUR GEOTARGET DASHBOARD!','username'=>$username,'cust_id'=>$cust_id,'useremail'=>$useremail,'salesrep_mail'=>$salesrep_mail);
						Mail::send('email.reminder',$data , function($message) use($data)
						{
							$message->from('info@geotargetus.net', 'Geotargetus');
							$message->Bcc('geotargetteam@geotargetus.com');
							$message->To($data['useremail']);
							$message->Cc($data['salesrep_mail']);
							$message->Bcc('faisalk@shopperlocal.com');
							$message->Bcc('chirags@shopperlocal.com');
							$message->subject($data['msg']);

						});
						
						
					}
				}else if($flag==1){
					$next_date=date('Y-m-d', strtotime('+30 days',strtotime($launch_date)));
					
					if($next_date==$today_date && $camp_end_date>=$today){
						$user=DB::Select("SELECT * FROM customers_list WHERE customer_id='".$cust_id."'");
						$array=array('cust_id'=>$cust_id,'created_at'=>date('Y-m-d H:i:s'));
						$insert=DB::table('reminder_mail')->insert($array);
						$update=DB::Select("UPDATE camp_launch SET flag=2,next_launch_date='".$next_date."' WHERE cust_id='".$cust_id."'");
						
						$data=array('msg'=>'REMINDER: CHECK YOUR GEOTARGET DASHBOARD!','username'=>$username,'cust_id'=>$cust_id,'useremail'=>$useremail,'salesrep_mail'=>$salesrep_mail);
						Mail::send('email.reminder',$data , function($message) use($data)
						{
							$message->from('info@geotargetus.net', 'Geotargetus');
							$message->Bcc('geotargetteam@geotargetus.com');
							$message->To($data['useremail']);
							$message->Cc($data['salesrep_mail']);
							$message->Bcc('faisalk@shopperlocal.com');
							$message->Bcc('chirags@shopperlocal.com');
							$message->subject($data['msg']);

						});

						
					}
					
				}else{
					if($next_launch_date==$today_date && $camp_end_date>=$today){
						$next_date=date('Y-m-d', strtotime('+30 days',strtotime($next_launch_date)));
						$user=DB::Select("SELECT * FROM customers_list WHERE customer_id='".$cust_id."'");
						$array=array('cust_id'=>$cust_id,'created_at'=>date('Y-m-d H:i:s'));
						$insert=DB::table('reminder_mail')->insert($array);
						$update=DB::Select("UPDATE camp_launch SET next_launch_date='".$next_date."' WHERE cust_id='".$cust_id."'");
						
						$data=array('msg'=>'REMINDER: CHECK YOUR GEOTARGET DASHBOARD!','username'=>$username,'cust_id'=>$cust_id,'useremail'=>$useremail,'salesrep_mail'=>$salesrep_mail);
						Mail::send('email.reminder',$data , function($message) use($data)
						{
							$message->from('info@geotargetus.net', 'Geotargetus');
							$message->Bcc('geotargetteam@geotargetus.com');
							$message->To($data['useremail']);
							$message->Cc($data['salesrep_mail']);
							$message->Bcc('faisalk@shopperlocal.com');
							$message->Bcc('chirags@shopperlocal.com');
							$message->subject($data['msg']);

						});
						
					}
				}

			}
		}
	}


	// for password email
	public function passwordEmail(){
		
		
		$check=DB::Select("SELECT business_name,customer_id,username,email FROM `customers_list` WHERE kill_status='1'");
		// $this->adCreativeImage($camp_id);
		if(!empty($check)){
			foreach($check as $email){
				$business_name=$email->business_name;
				$cust_id=$email->customer_id;
				$username=$email->username;
				$useremail=$email->email;
				$explode=explode(',', $username);
				//var_dump($explode);
				if(!empty($explode[0])){
					$fname=strtolower($explode[0]);
					$user_id=substr($cust_id, -4) ;
					$new_pass=$fname.$user_id;
				}
				
				
				$report[]=array('cust_id'=>$cust_id,'fname'=>$fname,'bussiness_name'=>$business_name,'useremail'=>$useremail,'password'=>$new_pass);
				
			}
			// file name for download
			$fileName = "customerpassword" . date('Ymd') . ".csv";

// headers for download
			header("Content-Disposition: attachment; filename=\"$fileName\"");
			header("Content-Type: text/csv");

			$output = fopen("php://output", "w");
			$headerDisplayed1 = false;
			foreach ($report as $row) {
				if ( !$headerDisplayed1 ) {
					fputcsv($output, array_keys($row));
					$headerDisplayed1 = true;
				}

				fputcsv($output, $row);
			}
			fclose($output);
			exit;
		}


	}


	public function resetpass(Request $request){
		Auth::logout(); // logging out user
		Auth::guard('user')->logout();
		Auth::guard('web')->logout();
		$token=$request->query('token');
		$token1=DB::Select("SELECT * FROM customers_list WHERE reset_token='".$token."' and pass_status='1'");
		// var_dump($token);
		// die;
		if(!empty($token1)){
			return view('auth/expire');
		}else{
			$user=DB::Select("SELECT * FROM customers_list WHERE reset_token='".$token."'");
			// echo "SELECT * FROM customers_list WHERE reset_token='".$token."'";
			// die;
			foreach($user as $data){
				$cust_id=$data->customer_id;
				$reset_token=$data->reset_token;
			}

			if($reset_token==$token){
				return view('auth/passwords/user_reset',compact('cust_id','reset_token'));
			}else{

			}
		}
		

		
	}

	public function userresetpass(Request $request){


		$cust_id= Input::get('cust_id');
		$token=Input::get('token');
		$password=Input::get('password');
		$data=DB::Select("SELECT * FROM customers_list WHERE reset_token='".$token."' and customer_id='".$cust_id."'");
		if(!empty($data[0])){
			$userpassword=Hash::make($password);
			$adminpass=encrypt($password);
			$update=DB::Select("UPDATE customers_list SET password='".$userpassword."',token_pass='".$adminpass."',pass_status='1' WHERE reset_token='".$token."' and customer_id='".$cust_id."'");
			$data=array('msg'=>'Your Password has been Changed Successfully','username'=>$data[0]->username,'cust_id'=>$cust_id,'useremail'=>$data[0]->email,'password'=>$password);
			Mail::send('email.after_reset',$data , function($message) use($data)
			{
				$message->from('info@geotargetus.net', 'Geotargetus');
				//$message->Bcc('geotargetteam@geotargetus.com');
				//$message->To($data['useremail']);
				//$message->Cc($data['salesrep_mail']);
				$message->Cc('mharoon@geotargetus.com');
				$message->Bcc('faisalk@shopperlocal.com');
				$message->Bcc('chirags@shopperlocal.com');
				$message->subject($data['msg']);

			});
			return Redirect::to('userlogin');
		}else{
			return back()->withStatus(('Token is expired'));
		}
	}

}

