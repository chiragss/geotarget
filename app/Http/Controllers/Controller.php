<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
	
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	public function access_token(){
		return $access_token="aaEAAG4FwSs9wEBAIJsS5cmvq6FH2TJDxGzhwbbRJmbuZC3UFFpImCpsmiOIZCq5MEv4zquprIsgTqYdKhcGcOOuUffjSOYlVPA1Q1zMrWlHthfsfLtHaFDShIrlvekFLrbAAFz0jo8WPI1UzTnpLHy4IW7o1DuL1rrKBMAJfpgZDZD";
	}
}
