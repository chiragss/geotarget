<?php

namespace App\Http\Middleware;
use Redirect;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){

        if($guard=='user'  && Auth::guard($guard)->check()){
            //echo 'dd';die;
           /* if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }*/
            // die;
            return Redirect::to('userlogin');
        }
        if($guard=='web' && Auth::guard($guard)->check()){
           return Redirect::to('login'); 
       }

       return $next($request);

   }
}
