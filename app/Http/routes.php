<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::auth();
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('admindashboard', 'HomeController@adminindex');
Route::get('repadmindashboard', 'HomeController@repadminindex');

Route::get('usersession/{id}', 'Auth\LoginController@addSessionUser');

Route::any('userhome/{id}', 'UserController@home');
Route::get('userhomedashboard/{id}', 'UserController@userHomeDashboard');

Route::any('users/{id}', 'HomeController@userList');
Route::any('repusers/{id}', 'HomeController@repuserList');
Route::get('deals/{id}', 'HomeController@dealList');
Route::get('campaigns_list/{id}', 'HomeController@campaignsView');
Route::get('allcampaigns', 'HomeController@allcampaigns');
Route::get('user_info', 'ProfileController@edit')->name('profile.edit');
Route::any('update', 'ProfileController@update')->name('profile.update');
Route::any('userslist/{id}', 'HomeController@userListajax');
Route::any('interaction','HomeController@interaction');
Route::any('interactionajax','HomeController@interactionajax');
//for contact us route 
Route::get('contactus','UserController@contact');
Route::post('contactus','UserController@addcontact');
//close for contact us route
// function to use for ad creatives 
Route::any('ad_creatives', 'UserController@adCreatives');
Route::any('reports','HomeController@reports');
/*function to use for video*/
Route::any('video', 'UserController@adsVideo');

Route::any('create', 'UserController@create')->name('user.create');
Route::any('store', 'UserController@store')->name('user.store');

Route::post('password', 'ProfileController@update')->name('profile.password');
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::get('replogin', 'Auth\LoginController@showRepLoginForm');


Route::get('userloginhome', 'Auth\LoginController@showUserLoginForm');
Route::get('userlogin', 'Auth\LoginController@showUserLoginForm');
Route::post('userloginhome', 'Auth\LoginController@checkuser');
Route::get('passreset', 'CronJob@resetpass');
Route::post('passreset', 'CronJob@userresetpass');

Route::post('login', 'Auth\LoginController@doLogin');
Route::post('replogin', 'Auth\LoginController@doRepLogin');
Route::post('userlogin', 'Auth\LoginController@doUserLogin');
Route::post('logout', 'Auth\LoginController@doLogout');
Route::get('adminlogout', 'Auth\LoginController@doadminLogout');
Route::any('userlogout', 'Auth\LoginController@douserLogout');
Route::any('repuserlogout', 'Auth\LoginController@dorepuserLogout');
   // Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');

Route::any('csv','UserController@csv');
Route::any('csvupload','UserController@csvUpload12')->name('csv.upload');

Route::any('downloadcsv','UserController@downloadCsvVideo');
Route::any('downloadcsvdisplay','UserController@downloadCsvDisplay');

// for monthly
Route::any('monthlydownloadcsv','UserController@monthlydownloadCsvVideo');
Route::any('monthlydownloadcsvdisplay','UserController@monthlydownloadCsvDisplay');


// Add Notes
Route::any('writenote','HomeController@writeNotes');
Route::any('addnote','HomeController@addNotes');
Route::any('notes','HomeController@noteList');
// for basis palcement report download
Route::any('basismonthlydownloadcsv','UserController@monthlydownloadCsvbasis');
Route::any('basislifetimedownloadcsv','UserController@lifetimedownloadCsvbasis');

// for camp launch report
Route::any('camplaunchdownload','UserController@campLaunchReportDownload');
Route::any('reminderreportdownload','UserController@reminderReportDownload');
// Cron Job for add user
Route::any('adduser','UserController@addUser');
// Cron job for add user artwork recv date
Route::any('addartrecv','UserController@addUserArtRecv');
// Cron job for add user update artwork launch date
Route::any('addartlaunch','UserController@addUserArtLaunch');
Route::any('addcampreportsent','UserController@addUserCampRepSent');
Route::any('addlandingpage','UserController@addUserLandingPage');
   // Password Reset Routes...
// $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// $this->post('password/reset', 'Auth\ResetPasswordController@reset');

// CronJobs for campaigns for admin for facebook daily
Route::get('campaigns/{id}', 'HomeController@campaignList');
Route::get('campaignslist', 'HomeController@getCampaign');
// Cronjob for campaigns for user for facebook last 30 days
Route::get('usercampaign/{id}', 'HomeController@userCampaignList');
Route::get('usercampaignslist', 'HomeController@userGetCampaign');
// CronJob for campaigns for lifetime user for facebook for every 15 days
Route::get('lifetimecampaign/{id}', 'LifetimeController@userCampaignList');
Route::get('lifetimecampaignslist', 'LifetimeController@getCampaign');
// Route::get('usercampaignslist', 'HomeController@userGetCampaign');
// CronJob for update adcreative image


// CronJob for get all campaigns of groundtruth 7 days
Route::get('groundtruthcampaign', 'HomeController@grndUserCampaigns');
// Cronjob for getall grndcampaigns for daily basis
// Route::get('dailygroundtruthcampaign', 'HomeController@getAllGrndCamp');
// Cronjon for update user id for user for groundtruth
Route::get('updategrnduserid', 'HomeController@userGrndUpdateId');

// user update groundtruth campaigns
Route::get('updategroundtruthcampaign', 'HomeController@getGrndCampaign');

// Cronjon for update user id for user for facebook
Route::get('updateuserid', 'HomeController@userUpdateId');

/*CronJob For update user id for google*/
Route::get('updategoogleuserid', 'HomeController@userGoogleUpdateId');

// Cronjon for update user id for lifetimeuser for facebook
Route::get('lifetimeupdateuserid', 'LifetimeController@userUpdateId');

Route::get('usercampaigns/{id}', 'UserController@userCampaignList');
Route::any('usercampaignsbyid', 'UserController@userCampaignListbyid');
Route::any('usercampaignsbydate', 'UserController@userCampaignListbyDate');
// For age gender of particular campid

// Route::get('campaigns_age_gender', 'HomeController@ageGenderCampaign');
Route::get('campaign_report/{id}', 'UserController@campaignReport');
Route::get('campaign_monthly_report/{id}', 'UserController@campaignReportMonthly');
Route::post('campaign_bussiness_report/{id}', 'UserController@campaignReportByBussiness');

// Route for cron job
// Route::any('crontruncate','CronJob@truncateUser');
Route::any('cronadduser','CronJob@addUser');
// Cron job for add user artwork recv date
Route::any('cronaddartrecv','CronJob@addUserArtRecv');
// Cron job for add user update artwork launch date
Route::any('cronaddartlaunch','CronJob@addUserArtLaunch');
Route::any('cronaddcampreportsent','CronJob@addUserCampRepSent');
Route::any('cronaddlandingpage','CronJob@addUserLandingPage');
// Route::any('removerep','CronJob@removeRep');
Route::any('cronenddate','CronJob@campEndDate');
// CronJob for get all campaigns of groundtruth 7 days
Route::any('crongroundtruthcampaign', 'CronJob@grndUserCampaigns');
// Cron job for update user for groundtruth users
Route::any('cronupdategrnduserid', 'CronJob@userGrndUpdateId');
// Cron job for facebook for 30 days
Route::get('useradcreativecampaign', 'CronJob@useradCreativeCampaign');
Route::get('useradcampdeatil', 'CronJob@useradCampDetail');
Route::get('usergendercamp','CronJob@userGenderCamp');
Route::get('userpubplatformcampaign','CronJob@userPubPlatformCamp');
Route::get('userdisplayvideo','CronJob@userdisplayvideo');

Route::get('crongroundtruthcreative','CronJob@getGrndCampaign');

// Cron job for facebook for lifetime
Route::get('lifetimeadcampdeatil', 'CronJob@lifetimeadCampDetail');
Route::get('lifetimegendercampaign','CronJob@lifetimeGenderCamp');
Route::get('lifetimepubplatformcampaign','CronJob@lifetimePubPlatformCamp');
Route::get('lifetimedisplayvideo','CronJob@lifetimedisplayvideo');

// Cron Job For basis Api
Route::get('addbasis','CronJob@addBasisCamp');
// Basis User Update
Route::get('basisuserupdate','CronJob@userBasisUpdate');
Route::get('launchdate','CronJob@launchDate');
Route::get('addlaunchdate','CronJob@addLaunchDate');
Route::get('beforelaunchdate','CronJob@beforelaunchDate');
// for reminder email
Route::get('reminderemail','CronJob@reminderEmail');

Route::get('passwordemail','CronJob@passwordEmail');